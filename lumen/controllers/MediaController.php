<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class MediaController extends BaseController
{

    const END_POINT = 'catalog/media';

    /**
     * @SWG\Post(path="/media/upload-file",
     *   tags={"MEDIA"},
     *   summary="Upload file on server",
     *   description="Upload file on server",
     *   operationId="uploadFile",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="file",
     *     in="formData",
     *     description="File",
     *     required=false,
     *     type="file"
     *   ),
     *   @SWG\Parameter(
     *     name="folder",
     *     in="formData",
     *     description="Folder path where you want to upload file, if you want to upload on root then leave it blank ",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success operation")
     * )
     */
    public function uploadFile(Request $request)
    {
        if($file = $request->file('file')) {

            $data['multipart'] = [
                [
                    'name'     => 'file',
                    'contents' => fopen($file->getRealPath(), 'r'),
                    'filename' => $file->getClientOriginalName()
                ],
                [
                    'name'     => 'folder',
                    'contents' => $request->input('folder')
                ]
            ];

        }

        return $this->apiHelper->postWithImages(self::END_POINT . '/create', [], $data);
    }

}
