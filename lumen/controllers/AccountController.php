<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class AccountController extends BaseController
{

    const END_POINT = 'user/account';

    const END_POINT_SESSION = 'user/session';

    /**
     * @SWG\Post(path="/account/signup",
     *   tags={"ACCOUNT"},
     *   summary="Signup Customer into system",
     *   description="",
     *   operationId="signUp",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="first_name",
     *     in="formData",
     *     description="Customer First name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="last_name",
     *     in="formData",
     *     description="Customer Last name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone_number",
     *     in="formData",
     *     description="Customer Phone number",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="Customer Valid Email Address",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="Customer Password",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="type",
     *     in="formData",
     *     description="Customer type LIKE [customer,guest] default will be customer",
     *     required=false,
     *     type="string",
     *     default="customer"
     *   ),
     *   @SWG\Parameter(
     *     name="socialType",
     *     in="formData",
     *     description="Social Type like( Swich, Facebook, Twitter, Google, Linkedin)",
     *     required=true,
     *     type="string",
     *     default="Swich"
     *   ),
     *   @SWG\Parameter(
     *     name="socialID",
     *     in="formData",
     *     description="Social ID required only when Social Type is (Facebook, Twitter, Google, Linkedin)",
     *     required=false,
     *     type="string",
     *     default=""
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(type="string"),
     *     @SWG\Header(
     *       header="X-Rate-Limit",
     *       type="integer",
     *       format="int32",
     *       description="calls per hour allowed by the user"
     *     ),
     *     @SWG\Header(
     *       header="X-Expires-After",
     *       type="string",
     *       format="date-time",
     *       description="date in UTC when toekn expires"
     *     )
     *   ),
     *   @SWG\Response(response=422, description="Invalid form data Or missing fields ")
     * )
     */
    public function signUp(Request $request)
    {

        return $this->apiHelper->postUrl(self::END_POINT_SESSION . '/signup', [], $request->all());
    }

    /**
     * @SWG\Get(path="/account/validate-auth/{auth_token}",
     *   tags={"ACCOUNT"},
     *   summary="Validate customer token and get basic information",
     *   description="",
     *   operationId="validateAuth",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="auth_token",
     *     in="path",
     *     description="Auth Token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=422, description="Invalid form data Or missing fields")
     * )
     */
    public function validateAuth($auth_token)
    {
        return $this->apiHelper->getUrl(self::END_POINT_SESSION . '/validate-auth', ['auth_token' => $auth_token], false);
    }

    /**
     * @SWG\Post(
     *     path="/account/update",
     *     tags={"CUSTOMER"},
     *     operationId="update",
     *     summary="Update an existing customer",
     *     description="",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Parameter(
     *     name="first_name",
     *     in="formData",
     *     description="customer first name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="last_name",
     *     in="formData",
     *     description="customer last name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="dob",
     *     in="formData",
     *     description="the customer date of birth LIKE(yyyy-mm-dd)",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="gender",
     *     in="formData",
     *     description="The customer gender LIKE (1 = Male, 2 = Female)",
     *     required=false,
     *     type="string"
     *   ),
     *  @SWG\Response(
     *         response=422,
     *         description="Data validation failed",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="customer not found",
     *     ),
     *     @SWG\Response(
     *         response=405,
     *         description="Validation exception",
     *     ),
     * )
     */
    public function update(Request $request)
    {
        return $this->apiHelper->patchUrl(self::END_POINT . '/update-customer', [], $request->all());

    }

    /**
     * @SWG\Post(path="/account/social-login",
     *   tags={"ACCOUNT"},
     *   summary="Login Into System with Social User Data LIKE (Facebook, Twitter, Google, LinkedIn)",
     *   description="",
     *   operationId="socialLogin",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="socialType",
     *     in="formData",
     *     description="Social Types LIKE (Facebook, Google, Linkedin, Twitter)",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="socialID",
     *     in="formData",
     *     description="This is the Social Unique ID",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="TOKEN VALUE")
     * )
     */
    public function socialLogin(Request $request)
    {

        return $this->apiHelper->postUrl(self::END_POINT_SESSION . '/social-login', [], $request->all());
    }

    /**
     * @SWG\Post(path="/account/login",
     *   tags={"ACCOUNT"},
     *   summary="Logs customer into the system",
     *   description="",
     *   operationId="login",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="username",
     *     in= "formData",
     *     description="The user name for login",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="The password for login in clear text",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(type="string"),
     *     @SWG\Header(
     *       header="X-Rate-Limit",
     *       type="integer",
     *       format="int32",
     *       description="calls per hour allowed by the user"
     *     ),
     *     @SWG\Header(
     *       header="X-Expires-After",
     *       type="string",
     *       format="date-time",
     *       description="date in UTC when toekn expires"
     *     )
     *   ),
     *   @SWG\Response(response=400, description="Invalid username/password supplied")
     * )
     */
    public function login(Request $request)
    {

        return $this->apiHelper->postUrl(self::END_POINT_SESSION . '/login', [], $request->all());
    }

    /**
     * @SWG\Get(path="/account/profile",
     *   tags={"CUSTOMER"},
     *   summary="Get customer profile by access token",
     *   description="",
     *   operationId="profile",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Invalid username supplied"),
     *   @SWG\Response(response=404, description="User not found")
     * )
     **/
    public function profile()
    {
        return $this->apiHelper->getUrl(self::END_POINT . '/profile', []);
    }


    /**
     * @SWG\Post(path="/account/follow-up",
     *   tags={"CUSTOMER"},
     *   summary="Get customer profile by access token",
     *   description="",
     *   operationId="followUp",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Parameter(
     *     name="order_id",
     *     in="formData",
     *     description="Order ID",
     *     required=true,
     *     type="string",
     *     default="1"
     *   ),
     *   @SWG\Parameter(
     *     name="action_id",
     *     in="formData",
     *     description="Action ID",
     *     required=false,
     *     type="string",
     *     default="54"
     *   ),
     *   @SWG\Parameter(
     *     name="log_type",
     *     in="formData",
     *     description="Log Type",
     *     required=false,
     *     type="string",
     *     default="Follow Up"
     *   ),
     *   @SWG\Parameter(
     *     name="description",
     *     in="formData",
     *     description="Log Description",
     *     required=false,
     *     type="string",
     *     default="Follow up on order ID"
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function followUp(Request $request)
    {
        return $this->apiHelper->postUrl(self::END_POINT . '/follow-up', [], $request->all());
    }

    /**
     * @SWG\Post(path="/account/customer-log",
     *   tags={"CUSTOMER"},
     *   summary="Add log for customer",
     *   description="",
     *   operationId="customerLog",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="order_id",
     *     in="formData",
     *     description="Order ID",
     *     required=true,
     *     type="string",
     *     default="1"
     *   ),
     *   @SWG\Parameter(
     *     name="customer_id",
     *     in="formData",
     *     description="Customer Id",
     *     required=true,
     *     type="string",
     *     default="1"
     *   ),
     *   @SWG\Parameter(
     *     name="action_id",
     *     in="formData",
     *     description="Action ID",
     *     required=false,
     *     type="string",
     *     default="54"
     *   ),
     *   @SWG\Parameter(
     *     name="log_type",
     *     in="formData",
     *     description="Log Type",
     *     required=false,
     *     type="string",
     *     default="Follow Up"
     *   ),
     *   @SWG\Parameter(
     *     name="description",
     *     in="formData",
     *     description="Log Description",
     *     required=false,
     *     type="string",
     *     default="Follow up on order ID"
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function customerLog(Request $request)
    {
        return $this->apiHelper->postUrl(self::END_POINT . '/customer-log', [], $request->all());
    }

    /**
     * @SWG\Post(path="/account/forgot-password",
     *   tags={"ACCOUNT"},
     *   summary="Customer forgot password request",
     *   description="",
     *   operationId="forgotPassword",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="email address for customer",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Account does not exist with provided email")
     * )
     **/
    public function forgotPassword(Request $request)
    {
        return $this->apiHelper->postUrl(self::END_POINT_SESSION . '/forgot-password', [], $request->all());
    }

    /**
     * @SWG\Post(path="/account/reset-password/{password_token}",
     *   tags={"ACCOUNT"},
     *   summary="Reset password request",
     *   description="",
     *   operationId="resetPassword",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="password_token",
     *     in="path",
     *     description="Password reset token",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="The password for login in clear text",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(type="string"),
     *     @SWG\Header(
     *       header="X-Rate-Limit",
     *       type="integer",
     *       format="int32",
     *       description="calls per hour allowed by the user"
     *     ),
     *     @SWG\Header(
     *       header="X-Expires-After",
     *       type="string",
     *       format="date-time",
     *       description="date in UTC when toekn expires"
     *     )
     *   ),
     *   @SWG\Response(response=422, description="Invalid password reset request ")
     * )
     */
    public function resetPassword($password_token, Request $request)
    {
        return $this->apiHelper->postUrl(self::END_POINT_SESSION . '/reset-password', ['password_token' => $password_token], $request->all());
    }

    /**
     * @SWG\Post(path="/account/change-password",
     *   tags={"ACCOUNT"},
     *   summary="Change customer password",
     *   description="Change customer password, this will works only when customer is logged in",
     *   operationId="changePassword",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Parameter(
     *     name="old_password",
     *     in="formData",
     *     description="Old password",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="The password for login in clear text",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Your password has been updated successfully"),
     *   @SWG\Response(response=422, description="Old password is required"),
     *   @SWG\Response(response=400, description="Invalid old password provided")
     * )
     */
    public function changePassword(Request $request)
    {
        return $this->apiHelper->postUrl(self::END_POINT . '/change-password', [], $request->all());
    }

    /**
     * @SWG\Post(path="/account/my-swich",
     *   tags={"CUSTOMER"},
     *   summary="Get customer swich by access token",
     *   description="",
     *   operationId="mySwich",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Parameter(
     *     name="sort",
     *     in="formData",
     *     description="Enter sort by attribute name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Items name search",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="Page number",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function mySwich(Request $request)
    {
        $extraParams = ['category_type' => 10, 'expand' => 'specials,ingredients,itemOptions', 'is_deleted' => 0];

        foreach($request->all() as $key => $value) {
            $extraParams[$key] = $value;
        }

        $response = $this->apiHelper->getUrl(self::END_POINT . '/my-swich', $extraParams, true);


        return [
            'items' => $response['items'],
            'pagination' => $this->getPaginationDetail($response),
        ];

    }

    /**
     * @SWG\Post(path="/account/delete-swich/{item_id}",
     *   tags={"CUSTOMER"},
     *   summary="Delete customer item from his profile.",
     *   description="Delete customer item from his profile.",
     *   operationId="deleteMySwich",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Parameter(
     *     name="item_id",
     *     in="path",
     *     description="Item ID that need to be deleted from customer account",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation."),
     *   @SWG\Response(response=422, description="You are not allowed to perform this action.")
     * )
     **/
    public function deleteMySwich($item_id)
    {
        return $this->apiHelper->getUrl(self::END_POINT . '/delete-swich', ['item_id' => $item_id], false);

    }

    /**
     * @SWG\Post(path="/account/reward-points",
     *   tags={"CUSTOMER"},
     *   summary="Get Customer Reward points History",
     *   description="",
     *   operationId="MyRewardPoints",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Parameter(
     *     name="sort",
     *     in="formData",
     *     description="Enter sort by attribute name (like points, -points)",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="Page number",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function MyRewardPoints(Request $request)
    {
        $extraParams = [];

        foreach($request->all() as $key => $value) {
            $extraParams[$key] = $value;
        }

        $response = $this->apiHelper->getUrl(self::END_POINT . '/reward-points', $extraParams, true);

        return [
            'items' => $response['items'],
            'pagination' => $this->getPaginationDetail($response),
        ];
    }

    /**
     * @SWG\Get(path="/account/account-summary",
     *   tags={"CUSTOMER"},
     *   summary="Get Reward points Calculation based on Customer Token",
     *   description="",
     *   operationId="GetAccountSummary",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function GetAccountSummary()
    {
        return $this->apiHelper->getUrl(self::END_POINT . '/account-summary', []);
    }

    /**
     * @SWG\Get(path="/account/my-favorite",
     *   tags={"CUSTOMER"},
     *   summary="Get Favorite items by customer token",
     *   description="",
     *   operationId="myFavorite",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function myFavorite()
    {
        return $this->apiHelper->getUrl(self::END_POINT . '/my-favorite', []);
    }

    /**
     * @SWG\Post(path="/account/meal-history",
     *   tags={"CUSTOMER"},
     *   summary="Get customer meal history by access token",
     *   description="",
     *   operationId="mealHistory",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Parameter(
     *     name="sort",
     *     in="formData",
     *     description="Enter sort by attribute name (like points, -points)",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="Page number",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Invalid username supplied"),
     *   @SWG\Response(response=404, description="User not found")
     * )
     **/
    public function mealHistory(Request $request)
    {

        $extraParams = ['expand' => 'loyalties,address,branch,agent,origin,orderMenuItems,orderDiscountCategories,promoToOrder,orderHistory,currentStatus,assignedDriver'];

        foreach($request->all() as $key => $value) {
            $extraParams[$key] = $value;
        }

        $response = $this->apiHelper->getUrl(self::END_POINT . '/meal-history', $extraParams, true);


        return [
            'items' => $response['items'],
            'pagination' => $this->getPaginationDetail($response),
        ];

    }

    /**
     * @SWG\Get(path="/account/customer-search/{phone_number}",
     *   tags={"CALL CENTER AGENT"},
     *   summary="Search Customer Profile",
     *   description="Search Customer Profile",
     *   operationId="CustomerSearch",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="cc-agent-token",
     *			description="Agent Token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="H2UQuJ6YJ1H3eaC5jzNAG94vB2dzR7te"
     *   ),
     *	 @SWG\Parameter(
     *			name="phone_number",
     *			description="Customer phone number",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="0559987521"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Invalid username supplied"),
     *   @SWG\Response(response=404, description="User not found")
     * )
     **/
    public function CustomerSearch($phone_number)
    {
        return $this->apiHelper->getUrl(self::END_POINT . '/customer-search', ['phone_number' => $phone_number]);
    }

    /**
     * @SWG\Get(path="/account/get-driver/{driver_id}",
     *   tags={"CALL CENTER AGENT"},
     *   summary="Get Driver information",
     *   description="Get Driver Information",
     *   operationId="GetDriverInformation",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="cc-agent-token",
     *			description="Agent Token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="H2UQuJ6YJ1H3eaC5jzNAG94vB2dzR7te"
     *   ),
     *	 @SWG\Parameter(
     *			name="driver_id",
     *			description="Driver ID",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="2"
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function GetDriverInformation($driver_id)
    {
        return $this->apiHelper->getUrl(self::END_POINT . '/get-driver', ['driver_id' => $driver_id]);
    }

    /**
     * @SWG\Post(path="/account/add-customer-notes",
     *     tags={"CUSTOMER"},
     *   summary="Add Customer notes like on-time, permanent",
     *   description="",
     *   operationId="addCustomerNotes",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="cc-agent-token",
     *			description="Agent Token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="H2UQuJ6YJ1H3eaC5jzNAG94vB2dzR7te"
     *   ),
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Parameter(
     *     name="type",
     *     in="formData",
     *     description="Customer note type LIKE (permanent, one-time)",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="note",
     *     in="formData",
     *     description="Customer message",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success full operations")
     * )
     */
    public function addCustomerNotes(Request $request)
    {
        return $this->apiHelper->postUrl(self::END_POINT . '/add-customer-notes', [], $request->all());
    }

    /**
     * @SWG\Get(path="/account/customer-notes",
     *     tags={"CUSTOMER"},
     *   summary="Get All customer notes, either one time or permanent",
     *   description="Get All customer notes, either one time or permanent",
     *   operationId="getCustomerNotes",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="cc-agent-token",
     *			description="Agent Token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="H2UQuJ6YJ1H3eaC5jzNAG94vB2dzR7te"
     *   ),
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Response(response=200, description="Success full operations")
     * )
     */
    public function getCustomerNotes()
    {
        return $this->apiHelper->getUrl(self::END_POINT . '/customer-notes', [], false);
    }
}
