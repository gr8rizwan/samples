<?php

namespace App\Http\Controllers;

class MenuController extends BaseController
{

    const END_POINT = 'catalog/menu';

    const CACHE_KEY = 'menu-categories';

    /**
     * @SWG\Get(path="/get-menu",
     *   tags={"MENU"},
     *   summary="Get All menu with items",
     *   description="",
     *   operationId="getMenu",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Menu does not exist")
     * )
     **/
    public function getMenu() {

        $key = $this->appId. self::CACHE_KEY;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);

        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT, ['expand' => 'orderBanner,menuBanner', 'sort' => 'sort_order'], false);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;
    }

    /**
     * @SWG\Get(path="/get-menu/{id}",
     *   tags={"MENU"},
     *   summary="GET Menu by ID",
     *   description="",
     *   operationId="actionView",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="id",
     *			description="MENU ID",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="int64"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="MENU does not exist"),
     * )
     **/
    public function actionView($id) {

        $key = $this->appId. self::CACHE_KEY.'-view-'.$id;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);

        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/view', ['id' => $id]);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;
    }

    /**
     * @SWG\Get(path="/menu-by-slug/{slug}",
     *   tags={"MENU"},
     *   summary="GET Menu by ID",
     *   description="",
     *   operationId="actionViewBySlug",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="slug",
     *			description="SEO URL",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     * )
     **/
    public function actionViewBySlug($slug) {

        $key = $this->appId. self::CACHE_KEY.'-view-'.$slug;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);

        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/view-by-slug', ['slug' => $slug]);
            $this->setRedisCache($response, $key, 5200);
        }


        return $response;
    }
}
