<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class AgentController extends BaseController
{

    const END_POINT = 'user/agent';
    /**
     * @SWG\Post(path="/agent/login",
     *   tags={"CALL CENTER AGENT"},
     *   summary="Agent Login",
     *   description="",
     *   operationId="login",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="username",
     *     in= "formData",
     *     description="Agent Email ID",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="Agent Password",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=422, description="Invalid username/password supplied"),
     *   @SWG\Response(response=200, description="access_token will returned on successful login")
     * )
     */
    public function login(Request $request)
    {

        return $this->apiHelper->postUrl(self::END_POINT . '/login', [], $request->all());
    }

    /**
     * @SWG\Get(path="/agent/profile",
     *   tags={"CALL CENTER AGENT"},
     *   summary="Get Agent Information by Access token",
     *   description="",
     *   operationId="profile",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="cc-agent-token",
     *			description="Agent Token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="H2UQuJ6YJ1H3eaC5jzNAG94vB2dzR7te"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Invalid username supplied"),
     *   @SWG\Response(response=404, description="User not found")
     * )
     **/
    public function profile()
    {
        return $this->apiHelper->getUrl(self::END_POINT . '/profile', []);
    }

    /**
     * @SWG\Post(path="/agent/orders-feed",
     *   tags={"CALL CENTER AGENT"},
     *   summary="Get Agent live feed data",
     *   description="",
     *   operationId="OrderFeed",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="cc-agent-token",
     *			description="Agent Token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="H2UQuJ6YJ1H3eaC5jzNAG94vB2dzR7te"
     *   ),
     *   @SWG\Parameter(
     *     name="sort",
     *     in="formData",
     *     description="Enter sort by attribute name (like total_sum, time)",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="customer",
     *     in="formData",
     *     description="Enter Customer name or phone number to search",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="branch_id",
     *     in="formData",
     *     description="Enter Branch ID to see results",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="Page number",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function OrderFeed(Request $request)
    {

        $extraParams = ['expand' => 'loyalties,address, customer, branch, origin,orderMenuItems,orderDiscountCategories,promoToOrder,orderHistory,currentStatus'];

        foreach($request->all() as $key => $value) {
            $extraParams[$key] = $value;
        }

        $response = $this->apiHelper->getUrl(self::END_POINT . '/orders-feed', $extraParams, true);

        return [
            'items' => $response['items'],
            'pagination' => $this->getPaginationDetail($response),
        ];

    }

}
