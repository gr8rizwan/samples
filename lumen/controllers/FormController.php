<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends BaseController
{

    const END_POINT = 'system/form';

    /**
     * @SWG\Get(path="/system/front-forms",
     *   tags={"FRONT FORMS"},
     *   summary="List of Frontend Page forms with Form heading",
     *   description="List of Frontend forms with Form heading",
     *   operationId="index",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation"),
     * )
     **/
    public function index() {

        $key = $this->appId.'-system-forms';;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT, ['expand' => 'forms']);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;

    }

    /**
     * @SWG\Get(path="/system/front-forms/{id}",
     *   tags={"FRONT FORMS"},
     *   summary="Get Page Form by ID with Forms list",
     *   description="Get Page Form by ID with Forms list",
     *   operationId="view",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="id",
     *			description="Page forms ID",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="int64"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Form not founds")
     * )
     **/
    public function view($id) {

        return $this->apiHelper->getUrl(self::END_POINT.'/view', ['expand' => 'forms', 'id' => $id]);
    }

    /**
     * @SWG\Post(path="/system/form-submit",
     *   tags={"FRONT FORMS"},
     *   summary="Create Form data request",
     *   description="Create Form data request",
     *   operationId="formSubmit",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Order placed to get food",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/Forms")
     *   ),
     *   @SWG\Response(response=422, description="Data validation failed "),
     *   @SWG\Response(response=200, description="Successful operation "),
     * )
     */
    public function formSubmit(Request $request) {

        return $this->apiHelper->postUrl(self::END_POINT.'/form-submit', [], $request->all() );
    }
}
