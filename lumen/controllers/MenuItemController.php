<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class MenuItemController extends BaseController
{

    const END_POINT = 'catalog/menu-item';

    const CACHE_KEY = 'catalog-menu-item';

    /**
     * @SWG\Post(path="/menu-item/byCategory/{category_id}",
     *   tags={"MENU ITEM"},
     *   summary="Items by category ID",
     *   description="",
     *   operationId="byCategory",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="category_id",
     *     in="path",
     *     description="Menu ID Or Category ID",
     *     required=true,
     *     type="string",
     *     format="int64",
     *     default=10
     *   ),
     *   @SWG\Parameter(
     *     name="sort",
     *     in="formData",
     *     description="Enter sort by attribute name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Items name search",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="containIngredients",
     *     in="formData",
     *     description="Search ALL those Item that contain ingredients like (165,168)",
     *     required=false,
     *     type="string",
     *     default="165"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="Page number",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Results are not founds")
     * )
     **/
    public function byCategory(Request $request, $category_id) {

        $extraParams = ['category_type' => $category_id, 'expand' => 'specials,ingredients,itemOptions', 'is_deleted' => 0];

        foreach($request->all() as $key => $value) {
            $extraParams[$key] = $value;
        }

        if($category_id == 10) {
            $pagination = true;
            $redis = false;
        } else {
            $pagination = false;
            $redis = true;
        }

        $key = $this->appId. self::CACHE_KEY.'-'.$category_id;

        // Check if data need to check from cache first
        if($redis) {
            $response = $this->redisServer->get($key);

            if($response === null) {

                $response = $this->apiHelper->getUrl(self::END_POINT, $extraParams, $pagination);
                $this->setRedisCache($response, $key, 5200);
            }

        } else {
            $response = $this->apiHelper->getUrl(self::END_POINT, $extraParams, $pagination);
        }


        if($pagination) {

            return [
                'items' => $response['items'],
                'pagination' => $this->getPaginationDetail($response),
            ];

        } else {

            return $response;

        }


    }

    /**
     * @SWG\Post(path="/menu-item/search-item",
     *   tags={"MENU ITEM"},
     *   summary="Search Item By Name, This functional will use when customer give name to Swich",
     *   description="Search Menu Item",
     *   operationId="searchItem",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Items name search",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function searchItem(Request $request) {

        $extraParams = [];

        foreach($request->all() as $key => $value) {
            $extraParams[$key] = $value;
        }

        return $this->apiHelper->postUrl(self::END_POINT.'/search-item', [], $extraParams);

    }

    /**
     * @SWG\Post(path="/menu-item/validate-item",
     *   tags={"MENU ITEM"},
     *   summary="Validate item name before sending to server",
     *   description="Validate item name before sending to server",
     *   operationId="validateItem",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Item name that need to validate before save",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Item is available for name."),
     *   @SWG\Response(response=422, description="Data validation failed.")
     * )
     **/
    public function validateItem(Request $request) {

        $extraParams = [];
        foreach($request->all() as $key => $value) {
            $extraParams[$key] = $value;
        }

        return $this->apiHelper->postUrl(self::END_POINT.'/validate-item', [], $extraParams);

    }


    /**
     * @SWG\Get(path="/menu-item/{id}",
     *   tags={"MENU ITEM"},
     *   summary="Get Menu Item by Id",
     *   description="",
     *   operationId="actionView",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="id",
     *			description="Item ID",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Item does not exist")
     * )
     **/
    public function actionView($id) {

        return $this->apiHelper->getUrl(self::END_POINT.'/view', ['id' => $id] );
    }

    /**
     * @SWG\Get(path="/menu-item/add-favorite/{item_id}",
     *   tags={"MENU ITEM"},
     *   summary="Add Item into Favorite",
     *   description="",
     *   operationId="addToFavorite",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="item_id",
     *			description="Item ID",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Item already exist in favorite list")
     * )
     **/
    public function addToFavorite($item_id) {

        return $this->apiHelper->getUrl(self::END_POINT.'/add-favorite', ['item_id' => $item_id] );
    }

    /**
     * @SWG\Get(path="/menu-item/remove-favorite/{item_id}",
     *   tags={"MENU ITEM"},
     *   summary="Remove Favorite item from account",
     *   description="",
     *   operationId="removeFromFavorite",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="item_id",
     *			description="Item ID",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Item does not exist in favorite list")
     * )
     **/
    public function removeFromFavorite($item_id) {

        return $this->apiHelper->getUrl(self::END_POINT.'/remove-favorite', ['item_id' => $item_id] );
    }

    /**
     * @SWG\Get(path="/menu-item/item-detail/{slug}",
     *   tags={"MENU ITEM"},
     *   summary="Get Menu Item by Seo Url / Slug",
     *   description="",
     *   operationId="itemDetailBySlug",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="slug",
     *			description="Item Slug",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Item does not exist")
     * )
     **/
    public function itemDetailBySlug($slug) {

        return $this->apiHelper->getUrl(self::END_POINT.'/item-detail', ['slug' => trim($slug) ] );
    }


}
