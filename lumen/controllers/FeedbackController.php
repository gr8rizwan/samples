<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class FeedbackController extends BaseController
{

    const END_POINT = 'customer/feedback';


    /**
     * @SWG\Get(path="/customer/feedback",
     *   tags={"FEEDBACK RESOLUTION"},
     *   summary="Get List of Customer feedback & resolution ",
     *   description="Get List of Customer feedback & resolution",
     *   operationId="actionIndex",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function actionIndex()
    {
        return $this->apiHelper->getUrl(self::END_POINT, []);
    }

    /**
     * @SWG\Get(path="/customer/feedback/{feedback_id}",
     *   tags={"FEEDBACK RESOLUTION"},
     *   summary="Get List of Customer feedback & resolution ",
     *   description="Get List of Customer feedback & resolution",
     *   operationId="actionIndex",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *	 @SWG\Parameter(
     *			name="feedback_id",
     *			description="Feedback Id",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default=""
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="data not found")
     * )
     **/
    public function actionView($feedback_id)
    {
        return $this->apiHelper->getUrl(self::END_POINT . '/view', ['id' => $feedback_id]);
    }

    /**
     * @SWG\Post(path="/customer/feedback/feedback-resolution",
     *   tags={"FEEDBACK RESOLUTION"},
     *   summary="Create customer Feedback & feedback resolution",
     *   description="Create customer Feedback & feedback resolution",
     *   operationId="createFeedback",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *	 @SWG\Parameter(
     *			name="cc-agent-token",
     *			description="Agent Token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="H2UQuJ6YJ1H3eaC5jzNAG94vB2dzR7te"
     *   ),
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Save Feedback Resolution",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/Feedback")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(ref="#/definitions/Feedback")
     *   ),
     *   @SWG\Response(response=422, description="Data validation failed ")
     * )
     */
    public function createFeedback(Request $request)
    {
        return $this->apiHelper->postUrl(self::END_POINT . '/create', [], $request->all());
    }

    /**
     * @SWG\Post(path="/customer/feedback/update/{feedback_id}",
     *   tags={"FEEDBACK RESOLUTION"},
     *   summary="Update Feedback Resolution",
     *   description="Update Feedback Resolution",
     *   operationId="updateFeedback",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *	 @SWG\Parameter(
     *			name="cc-agent-token",
     *			description="Agent Token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="H2UQuJ6YJ1H3eaC5jzNAG94vB2dzR7te"
     *   ),
     *	 @SWG\Parameter(
     *			name="feedback_id",
     *			description="Agent Token",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="1"
     *   ),
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Save Feedback Resolution",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/Feedback")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(ref="#/definitions/Feedback")
     *   ),
     *   @SWG\Response(response=422, description="Data validation failed ")
     * )
     */
    public function updateFeedback($feedback_id, Request $request)
    {
        return $this->apiHelper->patchUrl(self::END_POINT . '/update', ['id' => $feedback_id], $request->all());
    }


}
