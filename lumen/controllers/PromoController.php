<?php

namespace App\Http\Controllers;

class PromoController extends BaseController
{

    const END_POINT = 'catalog/promo';

    /**
     * @SWG\Post(path="/promo/get-promo/{promo_code}",
     *   tags={"PROMO CODE"},
     *   summary="Get Promo code information",
     *   description="",
     *   operationId="actionView",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="promo_code",
     *			description="Promo code",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="phone_number",
     *			description="Customer phone number",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="0559987521"
     *   ),
     *	 @SWG\Parameter(
     *			name="area",
     *			description="Customer address area",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="0"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Promo code not exists"),
     *   @SWG\Response(response=422, description="Promo validation failed")
     * )
     **/
    public function actionView($promo_code, $request) {

        $response = $this->apiHelper->postUrl(self::END_POINT.'/promo-info', ['promo_code' => $promo_code], $request->all());
        return $response;
    }

}
