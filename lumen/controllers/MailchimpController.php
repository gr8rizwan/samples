<?php

/**
 * Created by Shoeb Surya.
 * Email: shoeb@myswich.com
 * Date: 7/20/16
 * Time: 1:09 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class MailchimpController extends BaseController
{
    const END_POINT = 'mailchimp/mailchimp';

    /**
     * @SWG\Post(path="/subscribe-user",
     *   tags={"Mailchimp Integration"},
     *   summary="Subscribe a user or update an existing user to the swich list on mailchimp",
     *   description="",
     *   operationId="actionSubscribe",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="email_address",
     *			description="Email Address",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="first_name",
     *			description="Customers First Name",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="last_name",
     *			description="Customers Last Name",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="TODO"),
     *   @SWG\Response(response=422, description="TODO")
     * )
     **/
    public function actionSubscribe($request){
        return $this->apiHelper->postUrl(self::END_POINT . '/subscribe-user', [], $request->all());
    }



}