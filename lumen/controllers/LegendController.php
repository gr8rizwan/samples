<?php

namespace App\Http\Controllers;

class LegendController extends BaseController
{

    const END_POINT = 'catalog/legend';
    const CACHE_KEY = 'legend';

    /**
     * @SWG\Get(path="/legend",
     *   tags={"LEGEND"},
     *   summary="Get all legends",
     *   description="Get all legends",
     *   operationId="getLegend",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function getLegend() {

        $key = $this->appId. self::CACHE_KEY;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);

        if($response === null) {

            $response = $this->apiHelper->getUrl(self::END_POINT, [], false);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;
    }
}
