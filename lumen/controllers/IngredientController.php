<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IngredientController extends BaseController
{

    const END_POINT = 'catalog/ingredient';
    const CACHE_KEY = 'menu-ingredients';


    /**
     * @SWG\Get(path="/ingredients",
     *   tags={"INGREDIENTS"},
     *   summary="Get all ingredients with groups",
     *   description="",
     *   operationId="actionIndex",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="ingredients does not exist")
     * )
     **/
    public function actionIndex() {

        $key = $this->appId. self::CACHE_KEY;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);

        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT, []);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;

    }

    /**
     * @SWG\Get(path="/ingredients/{id}",
     *   tags={"INGREDIENTS"},
     *   summary="Get all ingredients with groups",
     *   description="",
     *   operationId="actionView",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="id",
     *			description="Customer token",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="int64"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="ingredient does not exist")
     * )
     **/
    public function actionView($id) {

        $key = $this->appId. self::CACHE_KEY.'-view-'.$id;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);

        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/view', ['id' => $id]);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;
    }

}
