<?php

namespace App\Http\Controllers;

class SettingController extends BaseController
{

    const END_POINT = 'system/setting';

    const SYSTEM_ENDPOINT = 'system';

    /**
     * @SWG\Get(path="/setting/countries",
     *   tags={"SETTINGS"},
     *   summary="Get All available countries",
     *   description="",
     *   operationId="getCountries",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Countries list not available")
     * )
     **/
    public function getCountries() {

        $key = $this->appId.'-countries';

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/countries', [] );
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;

    }

    /**
     * @SWG\Get(path="/setting/banners",
     *   tags={"SETTINGS"},
     *   summary="GET all available banners",
     *   description="",
     *   operationId="getBanners",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Banner list does not exist")
     * )
     **/
    public function getBanners() {

        $key = $this->appId.'-system-banners';

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl('system/banner', []);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;
    }

    /**
     * @SWG\Get(path="/setting/payment-methods",
     *   tags={"SETTINGS"},
     *   summary="Get ALL payment Types",
     *   description="",
     *   operationId="PaymentMethods",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function PaymentMethods() {

        $key = $this->appId.'-system-payment-options';

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/payment-options', []);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;
    }

    /**
     * @SWG\Get(path="/setting/delivery-charges",
     *   tags={"SETTINGS"},
     *   summary="Get ALL delivery Charges",
     *   description="Get ALL delivery Charges",
     *   operationId="deliveryCharges",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function deliveryCharges() {

        $response = $this->apiHelper->getUrl(self::END_POINT.'/delivery-charges', []);
        return $response;
    }

    /**
     * @SWG\Get(path="/setting/pickup-times",
     *   tags={"SETTINGS"},
     *   summary="Get all Pickup Times",
     *   description="This API will retrn all available option for pickup times",
     *   operationId="pickupTimes",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function pickupTimes() {

        $key = $this->appId.'-system-delivery-times';

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/delivery-times', []);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;
    }

    /**
     * @SWG\Get(path="/setting/discounts",
     *   tags={"CALL CENTER AGENT"},
     *   summary="Get List of available discounts LIKE (Zomato, Talabat, fedoora)",
     *   description="Get List of available discounts",
     *   operationId="getDiscounts",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function getDiscounts() {

        $response = $this->apiHelper->getUrl(self::END_POINT.'/discounts', []);
        return $response;
    }

    /**
     * @SWG\Get(path="/setting/supervisor-messages",
     *   tags={"CALL CENTER AGENT"},
     *   summary="Get List of supervisor messages",
     *   description="Get List of supervisor messages",
     *   operationId="getMessages",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function getMessages() {

        $response = $this->apiHelper->getUrl(self::END_POINT.'/discounts', []);
        return $response;
    }

    /**
     * @SWG\Get(path="/setting/franchise",
     *   tags={"ADMIN DASHBOARD"},
     *   summary="Get List of franchises",
     *   description="Get list of franchises",
     *   operationId="getFranchises",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function getFranchises() {

        $response = $this->apiHelper->getUrl(self::SYSTEM_ENDPOINT.'/franchise', []);
        return $response;
    }

    /**
     * @SWG\Get(path="/setting/pos-vendor",
     *   tags={"ADMIN DASHBOARD"},
     *   summary="Get List of franchise pos",
     *   description="Get List of franchise pos",
     *   operationId="getFranchisePosVendor",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function getFranchisePosVendor() {

        $response = $this->apiHelper->getUrl(self::SYSTEM_ENDPOINT.'/franchise/pos-vendor', ['expand' => 'franchise,vendor']);
        return $response;
    }

    /**
     * @SWG\Get(path="/setting/feedback-types",
     *   tags={"CALL CENTER AGENT"},
     *   summary="Get Feedback types",
     *   description="Get Feedback types",
     *   operationId="getFeedbackTypes",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function getFeedbackTypes() {

        $key = $this->appId.'-system-settings-feedback-types';

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/feedback-types', []);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;
    }
    /**
     * @SWG\Get(path="/setting/notification-groups",
     *   tags={"CALL CENTER AGENT"},
     *   summary="Get Feedback resolution notification groups ",
     *   description="Get Feedback resolution notification groups",
     *   operationId="getFeedbackTypes",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function getNotificationGroups() {

        $key = $this->appId.'-system-settings-notification-groups';

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/notification-groups', []);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;
    }

    /**
     * @SWG\Get(path="/setting/order-origin",
     *   tags={"CALL CENTER AGENT"},
     *   summary="Get Order Origin List",
     *   description="Get Order Origin List",
     *   operationId="getOrderOrigin",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function getOrderOrigin() {

        $key = $this->appId.'-system-settings-order-origin';

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/order-origin', []);
            $this->setRedisCache($response, $key, 5200);
        }

        return $response;

    }

    /**
     * @SWG\Get(path="/setting/opening-hours",
     *   tags={"SETTINGS"},
     *   summary="Get Store opening and closing hours",
     *   description="Get Store opening and closing hours",
     *   operationId="getOpeningHours",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function getOpeningHours() {

        $response = $this->apiHelper->getUrl(self::END_POINT.'/opening-hours', []);
        return $response;
    }

    /**
     * @SWG\Get(path="/setting/announcement",
     *   tags={"ANNOUNCEMENT"},
     *   summary="Get System Announcements or maintenance template",
     *   description="Get System Announcements",
     *   operationId="getAnnouncement",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function getAnnouncement() {

        $key = $this->appId.'-system-settings-announcement';

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/announcement', []);
            $this->setRedisCache($response, $key, 3600);
        }

        return $response;
    }

    /**
     * @SWG\Get(path="/setting/banner/{page}",
     *   tags={"SETTINGS"},
     *   summary="GET all available banners",
     *   description="",
     *   operationId="getBanners",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="page",
     *			description="Banner by page ",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Banner does not exist")
     * )
     **/
    public function getBanner($page) {

        $key = $this->appId.'-system-view-banner-'.$page;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl('system/banner/view-banner', ['page' => $page]);
            $this->setRedisCache($response, $key, 3600);
        }

        return $response;
    }


    /**
     * @SWG\Get(path="/setting/get-promotion/{template_key}",
     *   tags={"SETTINGS"},
     *   summary="Get Promotions by keys like order-confirmation is for order online page.",
     *   description="Get Promotions by keys like order-confirmation is for order online page",
     *   operationId="getPromotion",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="template_key",
     *			description="promotion key",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Invalid request format")
     * )
     **/
    public function getPromotion($template_key) {

        $key = $this->appId.'-system-get-promotion-'.$template_key;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT . '/get-promotion', ['key' => $template_key]);
            $this->setRedisCache($response, $key, 3600);
        }

        return $response;
    }

}
