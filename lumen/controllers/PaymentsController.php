<?php

/**
 * Created by Shoeb Surya.
 * Email: shoeb@myswich.com
 * Date: 7/20/16
 * Time: 1:09 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class PaymentsController extends BaseController
{
    const END_POINT = 'payments';
    const END_POINT_PAYFORT = 'payfort';

    /**
     * @SWG\Post(path="/payments/get-payfort-signature",
     *   tags={"Payments"},
     *   summary="Returns a signature for the auto form submission to payfort",
     *   description="Use this method for getting generate the signature and return the URL for submission to payfort",
     *   operationId="actionGetPayfortSignature",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="amount",
     *			description="Amount",
     *          in="formData",
     *      	required=false,
     *      	type="integer",
     *          format="int32"
     *   ),
     *	 @SWG\Parameter(
     *			name="currency",
     *			description="Currency",
     *          in="formData",
     *      	required=false,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="merchant_identifier",
     *			description="Merchant Identifier code from Payfort account https://testfort.payfort.com/account/MerchantManagement/EntitySecurity",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="xuncDsuM"
     *   ),
     *	 @SWG\Parameter(
     *			name="access_code",
     *			description="Merchant Access code from Payfort account https://testfort.payfort.com/account/MerchantManagement/EntitySecurity",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="NZA83I26FjOjcXqAEViZ"
     *   ),
     *	 @SWG\Parameter(
     *			name="order_description",
     *			description="Description of the products being purchased",
     *          in="formData",
     *      	required=false,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="merchant_reference",
     *			description="Merchant’s Reference; Merchant’s order number. Must be unique for each order",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="customer_ip",
     *			description="Customers Public IP address",
     *          in="formData",
     *      	required=false,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="customer_email",
     *			description="Customers email address",
     *          in="formData",
     *      	required=false,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="language",
     *			description="The checkout page and messages language. Possible/ expected values: en/ ar",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="en"
     *   ),
     *	 @SWG\Parameter(
     *			name="service_command",
     *			description="Possible/ expected values: TOKENIZATION",
     *          in="formData",
     *      	required=false,
     *      	type="string",
     *          format="string",
     *          default="TOKENIZATION"
     *   ),
     *	 @SWG\Parameter(
     *			name="token_name",
     *			description="Token Name",
     *          in="formData",
     *      	required=false,
     *      	type="string",
     *          format="string",
     *          default=""
     *   ),
     *	 @SWG\Parameter(
     *			name="return_url",
     *			description="The URL of the Merchant’s page to be displayed to the customer when the order is processed",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="http://myswich.local/abc"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="TODO"),
     *   @SWG\Response(response=422, description="TODO")
     * )
     **/


    public function actionGetPayfortSignature($request){

        return $this->apiHelper->postUrl(self::END_POINT . '/' . self::END_POINT_PAYFORT . '/get-signature', [], $request->all());
    }

    /**
     * @SWG\Post(path="/payments/save-token",
     *   tags={"Payments"},
     *   summary="Saving token to DB against the customer",
     *   operationId="actionSaveToken",
     *   produces={"application/json"},
     *   consumes={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Input from saving token",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/PayfortSaveToken")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(ref="#/definitions/PayfortSaveToken")
     *   ),
     *   @SWG\Response(response=422, description="Data validation failed ")
     * )
     */


    public function actionSaveToken($request){
        return $this->apiHelper->postUrl(self::END_POINT . '/' . self::END_POINT_PAYFORT . '/save-token', [], $request->all());
    }

    /**
     * @SWG\Post(path="/payments/payment-request",
     *   tags={"Payments"},
     *   summary="Make payment request to Payfort",
     *   operationId="actionPaymentRequest",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *	 @SWG\Parameter(
     *			name="command",
     *			description="Purchase Command",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="PURCHASE"
     *   ),
     *	 @SWG\Parameter(
     *			name="merchant_identifier",
     *			description="Merchant Identifier code from Payfort account https://testfort.payfort.com/account/MerchantManagement/EntitySecurity",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="xuncDsuM"
     *   ),
     *	 @SWG\Parameter(
     *			name="access_code",
     *			description="Merchant Access code from Payfort account https://testfort.payfort.com/account/MerchantManagement/EntitySecurity",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="NZA83I26FjOjcXqAEViZ"
     *   ),
     *	 @SWG\Parameter(
     *			name="merchant_reference",
     *			description="Merchant’s Reference; Merchant’s order number. Must be unique for each order",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="customer_name",
     *			description="Customers name",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default=""
     *   ),
     *	 @SWG\Parameter(
     *			name="customer_email",
     *			description="Customers email address",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *	 @SWG\Parameter(
     *			name="customer_ip",
     *			description="Customers IP address",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default=""
     *   ),
     *	 @SWG\Parameter(
     *			name="language",
     *			description="The checkout page and messages language. Possible/ expected values: en/ ar",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="en"
     *   ),
     *	 @SWG\Parameter(
     *			name="currency",
     *			description="Possible/ expected values: AUTHORIZATION, PURCHASE",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="AED"
     *   ),
     *	 @SWG\Parameter(
     *			name="amount",
     *			description="Amount to be charged. If its 100 send 1000",
     *          in="formData",
     *      	required=false,
     *      	type="string",
     *          format="string",
     *          default="100"
     *   ),
     *	 @SWG\Parameter(
     *			name="token_name",
     *			description="Token Name",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="payfortToken"
     *   ),
     *	 @SWG\Parameter(
     *			name="eci",
     *			description="Ecommerce indicator:  Possible/ expected values: RECURRING / ECOMMERCE",
     *          in="formData",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="RECURRING"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="TODO"),
     *   @SWG\Response(response=422, description="TODO")
     * )
     */


    public function actionPaymentRequest($request){
        return $this->apiHelper->postUrl(self::END_POINT . '/' . self::END_POINT_PAYFORT . '/payment-request', [], $request->all());
    }

    /**
     * @SWG\Get(path="/payments/delete-token/{card_id}",
     *   tags={"Payments"},
     *   summary="Update Saved credit card information ",
     *   description="Update Saved credit card information",
     *   operationId="deleteSavedToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *	 @SWG\Parameter(
     *			name="card_id",
     *			description="Saved Card ID",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string",
     *          default="1"
     *   ),
     *   @SWG\Response(response=200, description="Success operation"),
     *   @SWG\Response(response=422, description="Data not found with given ID")
     * )
     */
    public function deleteSavedToken($card_id, Request $request)
    {
        return $this->apiHelper->patchUrl(self::END_POINT . '/' . self::END_POINT_PAYFORT . '/update', ['id' => $card_id], $request->all());
    }

    /**
     * @SWG\Get(path="/payments/get-saved-token",
     *   tags={"Payments"},
     *   summary="Get Customers saved payment tokens ",
     *   description="Get Customers saved payment tokens",
     *   operationId="getSavedToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Response(response=200, description="Success operation")
     * )
     */
    public function getSavedToken()
    {
        return $this->apiHelper->getUrl(self::END_POINT . '/' . self::END_POINT_PAYFORT, [], false);
    }

}