<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class LiveFeedController extends BaseController
{

    const END_POINT = 'checkout/live-feed';

    const CACHE_KEY = 'live-feed';

    /**
     * @SWG\Get(path="/order/view/{order_id}",
     *   tags={"LIVE FEED"},
     *   summary="Get Order detail with order ID from live feed",
     *   description="This API is used to get order detail with discount, promo code, order history, current status, order menu items",
     *   operationId="actionView",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="order_id",
     *			description="Order ID",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Item does not exist")
     * )
     **/
    public function actionView($order_id) {

        $extraParams = [
            'order_id' => $order_id,
            'expand' => 'customer,loyalties,address,branch,agent,origin,orderMenuItems,orderDiscountCategories,promoToOrder,orderHistory,currentStatus,assignedDriver,callLog'
        ];

        return $this->apiHelper->getUrl(self::END_POINT . '/view-order', $extraParams, false);
    }

    /**
 * @SWG\Get(path="/order/live-feed",
 *   tags={"LIVE FEED"},
 *   summary="Live feed data that will used on dashboard to show real time orders from all shops",
 *   description="Live feed data that will used on dashboard to show real time orders from all shops",
 *   operationId="liveFeed",
 *   produces={"application/json"},
 *   @SWG\Response(response=200, description="successful operation"),
 *   @SWG\Response(response=404, description="TODO")
 * )
 **/
    public function liveFeed() {

        $extraParams = [
            'expand' => 'customer,address,branch,agent,origin,currentStatus,assignedDriver,callLog'
        ];

        $key = $this->appId. self::CACHE_KEY;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);

        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT, $extraParams, false);
            $this->setRedisCache($response, $key, 30);
        }

        return $response;

    }

    /**
     * @SWG\Get(path="/order/refresh-feed",
     *   tags={"LIVE FEED"},
     *   summary="Refresh Live Feed to get order status from live feed",
     *   description="Refresh Live Feed to get order status",
     *   operationId="refreshLiveFeed",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="TODO")
     * )
     **/
    public function refreshLiveFeed() {

        return $this->apiHelper->getUrl(self::END_POINT . '/refresh-order-status', [], false);
    }

    /**
     * @SWG\Post(path="/order/update-status/{order_id}",
     *   tags={"LIVE FEED"},
     *   summary="Create Form data request from live feed",
     *   description="Create Form data request",
     *   operationId="formSubmit",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="order_id",
     *			description="Order ID",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Update Order status Request Data",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/OrderStatus")
     *   ),
     *   @SWG\Response(response=422, description="Data validation failed "),
     *   @SWG\Response(response=200, description="Successful operation "),
     * )
     */
    public function updateStatus($order_id, Request $request) {

        return $this->apiHelper->postUrl(self::END_POINT.'/update-status', ['order_id' => $order_id], $request->all() );
    }


}
