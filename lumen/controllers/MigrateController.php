<?php

namespace App\Http\Controllers;

class MigrateController extends BaseController
{

    const END_POINT = 'system/migration';

    /**
     * @SWG\Get(path="/pos/migrate/{category_id}",
     *   tags={"POS MIGRATION"},
     *   summary="Migration menu items to ho server",
     *   description="Migration menu items to ho server",
     *   operationId="actionMigrate",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="category_id",
     *			description="Category ID",
     *          in="path",
     *      	required=true,
     *      	type="integer",
     *          default="10",
     *          format="int64"
     *   ),
     *   @SWG\Response(response=200, description="successful operation")
     * )
     **/
    public function actionMigrate($category_id) {

        $response = $this->apiHelper->getUrl(self::END_POINT.'/migrate', ['category_id' => $category_id]);
        return $response;
    }

}
