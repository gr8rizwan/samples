<?php

namespace App\Http\Controllers;

class CmsController extends BaseController
{

    const END_POINT = 'system/page';

    const CACHE_KEY = 'system-page';

    /**
     * @SWG\Get(path="/pages",
     *   tags={"CMS PAGES"},
     *   summary="Get all cms pages",
     *   description="Get CMS page by SEO URL",
     *   operationId="getView",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Page not found")
     * )
     **/
    public function getPages() {

        $key = $this->appId. self::CACHE_KEY;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT, []);
            $this->setRedisCache($response, $key, 1800);
        }

        return $response;
    }



    /**
     * @SWG\Get(path="/page/{seo_url}",
     *   tags={"CMS PAGES"},
     *   summary="Get CMS page by SEO URL",
     *   description="Get CMS page by SEO URL",
     *   operationId="getView",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="seo_url",
     *			description="page SEO URL",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="Page not found")
     * )
     **/
    public function getView($seo_url) {

        $key = $this->appId. self::CACHE_KEY.'-'.$seo_url;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);
        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/view-page', ['seo_url' => $seo_url]);
            $this->setRedisCache($response, $key, 1800);
        }

        return $response;
    }
}
