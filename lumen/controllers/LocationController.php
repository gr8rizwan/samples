<?php

namespace App\Http\Controllers;


class LocationController extends BaseController
{

    const END_POINT = 'system/location';

    /**
     * @SWG\Get(path="/location/branches",
     *   tags={"LOCATIONS"},
     *   summary="Get All branches under one franchises",
     *   description="",
     *   operationId="Branches",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="branches does not exist")
     * )
     **/
    public function Branches() {

        $response = $this->redisServer->get('swich-'.$this->appId.'-branches');

        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT, []);
            $response = \GuzzleHttp\json_encode($response);
            $this->redisServer->set('swich-'.$this->appId.'-branches', $response);
        }

        return \GuzzleHttp\json_decode($response);

    }

    /**
     * @SWG\Get(path="/location/areas",
     *   tags={"LOCATIONS"},
     *   summary="Get List of areas under one franchises",
     *   description="",
     *   operationId="Areas",
     *   produces={"application/json"},
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="areas does not exist")
     * )
     **/
    public function Areas() {

        $response = $this->redisServer->get($this->appId.'-Areas');

        if($response === null) {
            $response = $this->apiHelper->getUrl(self::END_POINT.'/areas', []);
            $response = \GuzzleHttp\json_encode($response);
            $this->redisServer->set($this->appId.'-Areas', $response);
        }

        return \GuzzleHttp\json_decode($response);
    }

}
