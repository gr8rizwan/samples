<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class OrderController extends BaseController
{

    const END_POINT = 'checkout/order';

    /**
     * @SWG\Post(path="/order/checkout",
     *   tags={"ORDER"},
     *   summary="Create a new order",
     *   description="Create a new order, Apply discount category, Apply promo code, Add menu item if not exist",
     *   operationId="checkout",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Order placed to get food",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/Order")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(ref="#/definitions/Order")
     *   ),
     *   @SWG\Response(response=422, description="Data validation failed ")
     * )
     */
    public function checkout(Request $request)
    {
        return $this->apiHelper->postUrl(self::END_POINT . '/customer-checkout', [], $request->all());
    }

    /**
     * @SWG\Post(path="/order/guest-checkout",
     *   tags={"ORDER"},
     *   summary="Create a guest Order",
     *   description="Create a new order, Apply discount category, Apply promo code, Add menu item if not exist",
     *   operationId="guestCheckout",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Order placed to get food",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/GuestOrder")
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(ref="#/definitions/GuestOrder")
     *   ),
     *   @SWG\Response(response=422, description="Data validation failed ")
     * )
     */
    public function guestCheckout(Request $request)
    {
        return $this->apiHelper->postUrl(self::END_POINT . '/guest-checkout', [], $request->all());
    }

}
