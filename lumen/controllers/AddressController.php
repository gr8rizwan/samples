<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class AddressController extends BaseController
{

    const END_POINT = 'user/address';

    /**
     * Index function will return all address that belong to customer
     * @return array $address from transformer with areas & branch detail
     * @param integer $customer_id
     *
     * @SWG\Get(
     *   path="/account/address",
     *   summary="List of customer address",
     *   tags={"ADDRESS"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="List of customer address"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     */
    public function index() {
        return $this->apiHelper->getUrl(self::END_POINT.'/index', [] );
    }

    /**
     * Create User address
     * @param integer $customer_id based customer id record will created
     * @return array of created address
     *
     * @SWG\Post(
     *   path="/account/address/create",
     *   summary="Create a new address based on customer ID",
     *  operationId="create",
     *   tags={"ADDRESS"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *       @SWG\Parameter(
     *          name="area",
     *          description="Address Area",
     *          required=true,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="street",
     *          description="Customer street",
     *          required=false,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="building",
     *          description="Address building",
     *          required=true,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="apartment",
     *          description="Apartment number",
     *          required=true,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="phone_number",
     *          description="Address phone number",
     *          required=false,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="title",
     *          description="Address title",
     *          required=true,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="special_instructions",
     *          description="Special Instruction",
     *          required=false,
     *          in= "formData",
     *          type="string"
     *       ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     * */
    public function create(Request $request) {

        return $this->apiHelper->postUrl(self::END_POINT.'/create', [], $request->all() );
    }

    /**
     * Update Customer Address
     * @SWG\Post(
     *   path="/account/address/update/{id}",
     *   summary="Create a new address based on customer ID",
     *  operationId="create",
     *   tags={"ADDRESS"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *       @SWG\Parameter(
     *          name="id",
     *          description="Address Id",
     *          in="path",
     *          required=true,
     *          type="integer"
     *       ),
     *       @SWG\Parameter(
     *          name="area",
     *          description="Address Area",
     *          required=true,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="street",
     *          description="Customer street",
     *          required=false,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="building",
     *          description="Address building",
     *          required=true,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="apartment",
     *          description="Apartment number",
     *          required=true,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="phone_number",
     *          description="Address phone number",
     *          required=false,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="title",
     *          description="Address title",
     *          required=true,
     *          in= "formData",
     *          type="string"
     *       ),
     *       @SWG\Parameter(
     *          name="special_instructions",
     *          description="Special Instruction",
     *          required=false,
     *          in= "formData",
     *          type="string"
     *       ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     * */
    public function update($id, Request $request) {

        return $this->apiHelper->patchUrl(self::END_POINT.'/update', ['id' => $id], $request->all() );
    }

    /**
     *
     * @SWG\Get(
     *   path="/account/address/{id}",
     *   summary="Get Customer address by customer & id ",
     *  operationId="view",
     *   tags={"ADDRESS"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *       @SWG\Parameter(
     *          name="id",
     *          description="Address Id",
     *          in="path",
     *          required=true,
     *          type="integer"
     *       ),
     *   @SWG\Response(
     *     response=200,
     *     description="address detail"
     *   )
     * )
     *
     */
    public function view($id) {

        return $this->apiHelper->getUrl(self::END_POINT.'/view', ['id' => $id] );
    }

    /**
     * Delete Customer address
     * @SWG\Get(
     *   path="/account/address/delete/{id}",
     *   summary="Delete customer by address id",
     *  operationId="delete",
     *   tags={"ADDRESS"},
     *	 @SWG\Parameter(
     *			name="customer-token",
     *			description="Customer token",
     *          in="header",
     *      	required=true,
     *      	type="string",
     *          format="int64",
     *          default="XtiyZqlK9D5B_djRILM5a0t_OW_0FRGf"
     *   ),
     *       @SWG\Parameter(
     *          name="id",
     *          description="Address Id",
     *          in="path",
     *          required=true,
     *          type="integer"
     *       ),
     *   @SWG\Response(
     *     response=200,
     *     description="Deleted successfully"
     *   )
     * )
     *
     */
    public function delete($id) {

        return $this->apiHelper->patchUrl(self::END_POINT.'/update', ['id' => $id], ['is_deleted' => 1] );
    }

}
