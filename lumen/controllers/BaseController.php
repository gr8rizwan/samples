<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\ApiHelper;

class BaseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $requestClient; // Request Client
    public $apiHelper; // API Helper
    public $redisServer; // Redis server
    public $request; // Request
    public $host; // Request host
    public $token; // APP token
    public $appId; // APP ID

    const API_VERSION = 'app-api-version'; // API Version
    const API_TZ = 'app-tz'; // Applicaiton Time zone
    const API_LOCALE = 'app-locale'; // App locale

    /**
     * Required for application grouping ( iphone, andriod, web etc.. )
     */
    const APP_ID = 'app-id'; // APP Id
    const APP_HOST = 'host'; // APP Host

    /* Customer Token for profile*/
    const CUSTOMER_TOKEN = 'customer-token'; // Customer token

    /* Customer Token for profile*/
    const CC_AGENT_TOKEN = 'cc-agent-token'; // Customer token

    /**
     * Create a new event instance.
     * @properity Request object of request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->requestClient   = new \GuzzleHttp\Client([
                'base_uri' => getenv('API_BASE'),
                'headers'  => $this->getHeaders(),
                'timeout'  => 0,
            ]);

        // init redis server
        $this->redisServer = new \Predis\Client(getenv('REDIS_SERVER'));

        // set host & app id
        $this->host = $this->getHeaderInformation(self::APP_HOST);

        $this->appId = $this->getHeaderInformation(self::APP_ID);

        // get & set token
        $this->getToken();

        // Init ApiHelper
        $this->apiHelper = new ApiHelper($this->requestClient, $this->token);

    }

    /*
     * get header information
     *
     *
     * @return void
     * */
    public function getHeaderInformation($attribute) {

        $header = $this->request->header($attribute);

        if($header) {
            return $header;
        } else {
            abort(401, 'Invalid access method');
        }
    }

    /*
     * Get headers
     *
     *
     * */

    public function getHeaders() {

        $customer_token = ($this->request->header(self::CUSTOMER_TOKEN) ? $this->request->header(self::CUSTOMER_TOKEN) : '');
        $agent_token = ($this->request->header(self::CC_AGENT_TOKEN) ? $this->request->header(self::CC_AGENT_TOKEN) : '');

        return [
            'Content-Type'     => 'application/json',
            'x-app-locale' => $this->getHeaderInformation(self::API_LOCALE),
            'x-app-tz' => $this->getHeaderInformation(self::API_TZ),
            'x-app-version' => 'v1', //$this->getHeaderInformation(self::API_VERSION),
            'x-app-id' => $this->getHeaderInformation(self::APP_ID),
            'customer-token' => $customer_token,
            'cc-agent-token' => $agent_token,
        ];
    }


    /*
     * Get Token from Redis
     * This function will get token from server and store in redis OR get token from redis and assign to call
     *
     * */

    public function getToken() {

        if($token = $this->redisServer->get($this->appId.'access_token')) {
            $this->token = $token;
        } else {
            $tokenInfo = $this->generateToken();
            $this->token = $tokenInfo['access_token'];
            $this->redisServer->set($this->appId.'access_token', $this->token);
            $this->redisServer->expire($this->appId.'access_token', $tokenInfo['expires_in']);
        }
    }


    /**
     * generate Token token remote server
     * @return array of token information
     * */
    public function generateToken() {

        if(!$this->apiHelper) {
            // Init ApiHelper
            $this->apiHelper = new ApiHelper($this->requestClient, $this->token);
        }

        /**
         * TODO
         * This part will be handled from Server end.
         * */
        $params = [ 'expand' => 'oauthUser', 'app_id' => $this->appId ];

        $credentials = $this->apiHelper->getUrl('user/client/find-client', $params, false);

        if($credentials == null || !isset($credentials['client_secret'])) {
            abort(401, 'Error validating access token:  un authorized application APP_ID');
        }

        $client = [
            'client_id' => $this->appId,
            'client_secret' => $credentials['client_secret'],
            'grant_type' => 'password',
            'username' => $credentials['oauthUser']['username'],
            'password' => $credentials['oauthUser']['password'],
            'scope' => $credentials['scope'],
        ];

        return $this->apiHelper->postUrl('user/auth/token', [], $client);

    }

    /**
     * Get Pagination Detail
     * @return array of pagination
     * */
    public function getPaginationDetail($response) {
        return [
            'current_page' => (int)$response['headers']['X-Pagination-Current-Page'],
            'page_count' => (int)$response['headers']['X-Pagination-Page-Count'],
            'per_page' => (int)$response['headers']['X-Pagination-Per-Page'],
            'total_count' => (int)$response['headers']['X-Pagination-Total-Count']
        ];
    }

    /**
     * Set redis cache is use to set cache into storage for limited time
     * @param array $request
     * @param integer $time
     * @param string $key
     * */
    public function setRedisCache($data, $key, $time) {

        $cacheData = \GuzzleHttp\json_encode($data);
        $this->redisServer->set($key, $cacheData);
        $this->redisServer->expireat($key, time() + $time);

    }

}