<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class BlogController extends BaseController
{

    const END_POINT = 'information/blog';

    const CACHE_KEY = 'blogs';

    /**
     * @SWG\Post(path="/information/blogs",
     *   tags={"BLOGS"},
     *   summary="Get ALL BLOGS",
     *   description="Get ALL BLOGS",
     *   operationId="getBlogs",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="sort",
     *     in="formData",
     *     description="Enter sort by attribute name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="title",
     *     in="formData",
     *     description="Search By Blog Title",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="formData",
     *     description="Page number",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="TODO")
     * )s
     **/
    public function getBlogs(Request $request) {

        $extraParams = [];

        foreach($request->all() as $key => $value) {
            $extraParams[$key] = $value;
        }



        $key = $this->appId. self::CACHE_KEY;

        // Check if data need to check from cache first
        $response = $this->redisServer->get($key);

        if($response === null) {

            $response = $this->apiHelper->getUrl(self::END_POINT, $extraParams, true);
            $this->setRedisCache($response, $key, 1800);
        }

        return [
            'items' => $response['items'],
            'pagination' => $this->getPaginationDetail($response),
        ];
    }

    /**
     * @SWG\Get(path="/information/blogs/{id}",
     *   tags={"BLOGS"},
     *   summary="GET Blog by ID for detail page",
     *   description="GET Blog by ID for detail page",
     *   operationId="getView",
     *   produces={"application/json"},
     *	 @SWG\Parameter(
     *			name="id",
     *			description="Blog ID",
     *          in="path",
     *      	required=true,
     *      	type="string",
     *          format="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="TODO")
     * )
     **/
    public function getView($id) {

        return $this->apiHelper->getUrl(self::END_POINT.'/view', ['id' => $id]);
    }
}
