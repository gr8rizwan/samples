<?php

namespace backend\controllers;

use backend\models\Branch;
use Yii;
use backend\models\OrderSearch;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use backend\models\CustomerSearch;
use yii\web\UnauthorizedHttpException;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class ReportController extends OrderController
{

    /**
     * Live Feed function that will list out all today orders with status
     * This function will keep refreshing after 5 mintues
     * @exception return an empty array if data not available
     * @response return an Orders array
     * @author Nadeem Akhtar <nadeem@myswich.com>
     * @return mixed
     * */
    public function actionLiveFeed() {

        $searchModel = new OrderSearch();
        $searchModel->isToday = true;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Hide revenue related information from users group e.g; non super admin
        $canViewRevenue = Yii::$app->user->can('view_revenue_data_on_live_feed');

        if (Yii::$app->session->get('CLIENT_ID')) {
            return $this->render('live_feed', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'canViewRevenue' => $canViewRevenue
            ]);
        } else {
            throw new UnauthorizedHttpException('You don\'t have live feed module installed, kindly contact with admin.');
        }
    }

    /**
     * Display Area Analysis Report, Bar Chart with Grid,
     * Per Area: Total Orders, Average Ticket, Revenue
     * @return mixed
     * @author Rizwan Arshad
     *
     * */
    public function actionAreaAnalysis() {

        $searchModel = new OrderSearch();

        if (isset(Yii::$app->request->queryParams['OrderSearch']['from_date']) || isset(Yii::$app->request->queryParams['OrderSearch']['to_date'])) {
            $isToday = false;
        } else {
            $isToday = true;
        }
        $searchModel->isToday = $isToday;

        // get all branches
        $branchs = Branch::find()->all();
        $dataProvider = $searchModel->reportSearch(Yii::$app->request->queryParams, 'area');

        // clone data provider for chart calculations
        $myDataProvider = clone $dataProvider;

        $myDataProvider->setPagination(false);

        // set aggregate and data for area analysis chart
        $totalOrders = 0;
        $totalRevenue = 0;
        $area_names = [];
        $area_orders_count = [];
        foreach ($myDataProvider->models as $area) {
            $area_names[] = $area->area_name;
            $area_orders_count[] = $area->total_orders;

            $totalOrders += $area->total_orders;
            $totalRevenue += $area->revenue;
        }

        return $this->render('area_analysis', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'area_names' => $area_names,
            'area_orders_count' => $area_orders_count,
            'totalOrders' => $totalOrders,
            'totalRevenue' => $totalRevenue,
            'branchs' => $branchs,
        ]);
    }


    /**
     * Lists all Order models with group data.
     * @return mixed
     */
    public function actionBranchAnalysis()
    {
        $searchModel = new OrderSearch();

        if (!(isset(Yii::$app->request->queryParams['OrderSearch']['time_filter']) &&
            !empty((Yii::$app->request->queryParams['OrderSearch']['time_filter'])) )
        ) {
            $searchModel->time_filter = 'monthly';
        }
        $dataProvider = $searchModel->branchSearch(Yii::$app->request->queryParams);

        $branchData = ArrayHelper::map(Branch::find()->where(['is_deleted' => 0])->orderBy(['id' => SORT_DESC])->all(), 'id', 'name');

        $myDataProvider = clone $dataProvider; // need no pagination for graph data, but pagination for grid view

        $dataProvider->setPagination(false);

        // make data for group chart/gridview
        $chartData = [];
        $timeData = [];
        $branchOrders = [];
        $branchRevenue = [];
        $gridLabels = [];
        $avgTicket = [];
        $totalOrders = 0;
        $totalRevenue = 0;

        $branchOrders['label'] = 'Total Orders';
        $branchRevenue['label'] = 'Total Revenue';
        $avgTicket['label'] = 'Average Ticket';

        foreach ($branchData as $branch_id => $branch) {
            $index = $branchData[$branch_id];
            $branchOrders[$index] = 0;
            $branchRevenue[$index] = 0;
            $gridLabels[$index] = $branch;
        }

        $gridLabels[0] = '';
        $gridLabels['total'] = 'Total Orders';
        $gridLabels['average'] = 'Average';

        foreach ($dataProvider->models as $data) {
            if (!in_array($data->timing, $timeData)) {
                $timeData[] = $data->timing;
            }
        }

        foreach (array_keys($branchData) as $branch) {
            foreach ($timeData as $time) {
                if (!isset($chartData[$branch][$time])) {
                    $chartData[$branch][$time] = 0;
                }
            }
        }

        foreach ($dataProvider->models as $data) {
            $index = isset($branchData[$data->branch_id]) ? $branchData[$data->branch_id] : false;

            if ($index) {
                $chartData[$data->branch_id][$data->timing] = $data->total_orders;
                $branchOrders[$index] += $data->total_orders;
                $branchRevenue[$index] += $data->revenue;
                $totalOrders += $data->total_orders;
                $totalRevenue += $data->revenue;
            }
        }

        krsort($chartData);

        $branchOrders['total'] = $totalOrders;
        $branchRevenue['total'] = $totalRevenue;
        $totalAvgTicket = 0;

        foreach ($branchData as $key => $value) {
            $avgTicket[$branchData[$key]] = @round($branchRevenue[$branchData[$key]] / $branchOrders[$branchData[$key]], 2);
            $totalAvgTicket += $avgTicket[$branchData[$key]];
        }

        $avgTicket['total'] = @round($branchRevenue['total'] / $branchOrders['total'], 2);

        foreach ($branchData as $key => $value) {
            $branchRevenue[$value] = Yii::$app->setting->formatePrice($branchRevenue[$value]);
            $avgTicket[$value] = Yii::$app->setting->formatePrice($avgTicket[$value]);
        }
        $branchRevenue['total'] = Yii::$app->setting->formatePrice($branchRevenue['total']);
        $avgTicket['total'] = Yii::$app->setting->formatePrice($avgTicket['total']);
        $gridData = [$branchOrders, $branchRevenue, $avgTicket];

        $gridDataProvider = new ArrayDataProvider(['allModels' => $gridData]);

        $xAxisData = $timeData;
        switch ($searchModel->time_filter) {
            case 'daily':
                foreach ($xAxisData as $key => $value) {
                    $xAxisData[$key] = yii::$app->dateTime->getReadableDateAndTimeFormat($value, 'D d M');
                }
                break;
            case 'weekly':
                $xAxisData = [];
                foreach ($dataProvider->models as $data) {
                    if (strlen($data->timing) == 1) {
                        $data->timing = '0' . $data->timing;
                    }
                    $xAxisData["{$data->time}W{$data->timing}"] = date('D d M, Y', strtotime("{$data->time}W{$data->timing} -1 day"))
                        . ' - ' . date('D d M, Y', strtotime("{$data->time}W{$data->timing} +5 day"));
                }
                $xAxisData = array_unique($xAxisData);
                ksort($xAxisData);
                break;
            case 'monthly':
                $xAxisData = [];
                foreach ($dataProvider->models as $data) {
                    $xAxisData[] = yii::$app->dateTime->getReadableDateAndTimeFormat($data->time, 'M Y');
                }
                $xAxisData = array_unique($xAxisData);
                break;
        }


        return $this->render('branch_analysis', [
            'searchModel' => $searchModel,
            'dataProvider' => $myDataProvider,
            'chartData' => $chartData,
            'timeData' => $xAxisData,
            'gridProvider' => $gridDataProvider,
            'branchData' => $branchData
        ]);
    }

    /**
     * Lists all Order models with group data.
     * @return mixed
     */
    public function actionBranchReport()
    {
        $searchModel = new OrderSearch();

        if (isset(Yii::$app->request->queryParams['OrderSearch']['from_date']) ||
            isset(Yii::$app->request->queryParams['OrderSearch']['to_date'])) {
            $isToday = false;
        } else {
            $isToday = true;
        }
        $searchModel->isToday = $isToday;

        $dataProvider = $searchModel->branchReport(Yii::$app->request->queryParams, 'platform');

        $branchData = ArrayHelper::map(Branch::find()->where(['is_deleted' => 0])->orderBy(['id' => SORT_DESC])->all(), 'id', 'name');

        return $this->render('branch_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'branchData' => $branchData
        ]);
    }



    /**
     * Lists all Order models with group data.
     * @return mixed
     */
    public function actionThirdParty()
    {
        $searchModel = new OrderSearch();

        if ((isset(Yii::$app->request->queryParams['OrderSearch']['from_date']) && !empty(Yii::$app->request->queryParams['OrderSearch']['from_date'])) ||
            (isset(Yii::$app->request->queryParams['OrderSearch']['to_date']) && !empty(Yii::$app->request->queryParams['OrderSearch']['to_date']))) {
            $isToday = false;
        } else {
            $isToday = true;
        }

        $searchModel->isToday = $isToday;

        $dataProvider = $searchModel->thirdPartyReport(Yii::$app->request->queryParams, 'platform');

        $branchData = ArrayHelper::map(Branch::find()->where(['is_deleted' => 0])->orderBy(['id' => SORT_DESC])->all(), 'id', 'name');

        return $this->render('third_party', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'branchData' => $branchData
        ]);
    }


    /**
     * Lists all Order models with group data.
     * @return mixed
     */
    public function actionPromoCode()
    {
        $searchModel = new OrderSearch();

        if ((isset(Yii::$app->request->queryParams['OrderSearch']['from_date']) && !empty(Yii::$app->request->queryParams['OrderSearch']['from_date'])) ||
            (isset(Yii::$app->request->queryParams['OrderSearch']['to_date']) && !empty(Yii::$app->request->queryParams['OrderSearch']['to_date']))) {
            $isToday = false;
        } else {
            $isToday = true;
        }

        $searchModel->isToday = $isToday;

        $dataProvider = $searchModel->PromoCode(Yii::$app->request->queryParams, 'platform');

        $branchData = ArrayHelper::map(Branch::find()->where(['is_deleted' => 0])->orderBy(['id' => SORT_DESC])->all(), 'id', 'name');

        return $this->render('promo_code', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'branchData' => $branchData
        ]);
    }



    /**
     * Display Hourly Report, Bar Chart with Grid,
     * Per Hour: Total Orders, Average Ticket, Revenue
     * @return mixed
     * @author Rizwan Arshad
     * */
    public function actionHourlySales() {

        $searchModel = new OrderSearch();

        if (isset(Yii::$app->request->queryParams['OrderSearch']['from_date']) || isset(Yii::$app->request->queryParams['OrderSearch']['to_date'])) {
            $isToday = false;
        } else {
            $isToday = true;
        }
        $searchModel->isToday = $isToday;

        $branchs = Branch::find()->all();
        $dataProvider = $searchModel->reportSearch(Yii::$app->request->queryParams, 'hourly');

        $myDataProvider = clone $dataProvider;                      // need no pagination for graph data, but pagination for grid view

        $myDataProvider->setPagination(false);

        // set data for area analysis chart
        $totalOrders = 0;
        $totalRevenue = 0;
        $hourlyLabels = [];
        $hourlyOrdersCount = [];
        foreach ($myDataProvider->models as $hour) {
            $hourlyLabels[] = $hour->hourly_labels;
            $hourlyOrdersCount[] = $hour->total_orders;
            $totalOrders += $hour->total_orders;
            $totalRevenue += $hour->revenue;
        }



        return $this->render('hourly_sales', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'branchs' => $branchs,
            'hourlyLabels' => $hourlyLabels,
            'hourlyOrdersCount' => $hourlyOrdersCount,
            'totalOrders' => $totalOrders,
            'totalRevenue' => $totalRevenue
        ]);
    }

    /**
     * Display Hourly Report, Bar Chart with Grid,
     * Per Hour: Total Orders, Average Ticket, Revenue
     * @return mixed
     * @author Rizwan Arshad
     * */
    public function actionPlatformAnalysis() {

        $searchModel = new OrderSearch();

        if (isset(Yii::$app->request->queryParams['OrderSearch']['from_date']) || isset(Yii::$app->request->queryParams['OrderSearch']['to_date'])) {
            $isToday = false;
        } else {
            $isToday = true;
        }
        $searchModel->isToday = $isToday;

        $branchs = Branch::find()->all();
        $dataProvider = $searchModel->reportSearch(Yii::$app->request->queryParams, 'platform');

        return $this->render('platform_analysis', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'branchs' => $branchs,
        ]);
    }

    /**
     * Agent analysis report ,
     * Per Hour: Total Orders, Average Ticket, Revenue
     * @return array of object
     * */
    public function actionAgentAnalysis() {

        $searchModel = new OrderSearch();

        if (isset(Yii::$app->request->queryParams['OrderSearch']['from_date']) || isset(Yii::$app->request->queryParams['OrderSearch']['to_date'])) {
            $isToday = false;
        } else {
            $isToday = true;
        }
        $searchModel->isToday = $isToday;

        $searchModel->source = 'call';

        $branchs = Branch::find()->all();
        $dataProvider = $searchModel->reportSearch(Yii::$app->request->queryParams, 'agent');

        return $this->render('agent_analysis', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'branchs' => $branchs,
        ]);
    }


    /**
     * Dispatch Report on daily basic
     * @return mixed
     * @author Nadeem Akhtar <nadeem@myswich.com>
     * */
    public function actionDispatchReport() {

        $searchModel = new OrderSearch();

        if (isset(Yii::$app->request->queryParams['OrderSearch']['from_date']) && !empty(Yii::$app->request->queryParams['OrderSearch']['from_date'])
            || isset(Yii::$app->request->queryParams['OrderSearch']['to_date']) && !empty(Yii::$app->request->queryParams['OrderSearch']['to_date']) ) {
            $isToday = false;
        } else {
            $isToday = true;
        }

        $searchModel->isToday = $isToday;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $branchs = [];
        foreach (Yii::$app->setting->getAllBranches() as $branch) {
            $generalData = $searchModel->getDispatchCalculation(Yii::$app->request->queryParams, $branch->id);

            // Orders that took more then 20 min
            $searchModel->time_took = 1200;
            $moreThen20min = $searchModel->getDispatchCalculation(Yii::$app->request->queryParams, $branch->id);

            $branchs[$branch->id] = ArrayHelper::merge($branch->toArray(), ['lateDispatch' => $moreThen20min[0], 'normalDispatch' => $generalData[0]]);

            $searchModel->time_took = null;
        }

        return $this->render('reports/dispatching', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'branchs' => $branchs,
        ]);
    }

    /**
     * Report to view new customers
     * @return mixed
     */
    public function actionNewCustomers(){
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->searchCustomersReport(Yii::$app->request->queryParams);
        $branchs = Branch::find()->all();
        return $this->render('new_customers', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'branches'  => $branchs
        ]);
    }
}
