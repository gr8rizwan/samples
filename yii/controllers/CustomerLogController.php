<?php

namespace backend\controllers;

use Yii;
use backend\models\CustomerLog;
use backend\models\CustomerLogSearch;
use backend\base\controllers\BaseController as Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomerLogController implements the CRUD actions for CustomerLog model.
 */
class CustomerLogController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerLog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomerLog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomerLog();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CustomerLog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CustomerLog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerLog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /* Follow-up Report on daily basic
     * @params array for search result
     * @exception return an empty array if data not available
     * @response return an Customer Log array
     * @author Rizwan Arshad <rizwan.a@myswich.com>
     * */

    public function actionFollowUp() {

        $searchModel = new CustomerLogSearch();

        if( isset(Yii::$app->request->queryParams['CustomerLogSearch']['from_date']) && !empty(Yii::$app->request->queryParams['CustomerLogSearch']['from_date'])
            || isset(Yii::$app->request->queryParams['CustomerLogSearch']['to_date']) && !empty(Yii::$app->request->queryParams['CustomerLogSearch']['to_date']) ) {
            $isToday = false;
        } else {
            $isToday = true;
        }

        $searchModel->isToday = $isToday;
        $dataProvider = $searchModel->searchFollowUp(Yii::$app->request->queryParams);

        $branchs = Yii::$app->setting->getAllBranches();

        // get total orders
        $totalOrders = 0;
        $params = [];
        if(isset(Yii::$app->request->queryParams['CustomerLogSearch'])) {
            $params['from_date'] = Yii::$app->request->queryParams['CustomerLogSearch']['from_date'];
            $params['to_date'] = Yii::$app->request->queryParams['CustomerLogSearch']['to_date'];
        }
        $myDataProvider = clone $dataProvider;
        $myDataProvider->setPagination(false);
        foreach ($myDataProvider->models as $order) {
            $from_date = false;
            $to_date = false;
            if(isset($params['from_date']))
                $from_date = $params['from_date'];
            if(isset($params['to_date']))
                $to_date = $params['to_date'];
            $totalOrders += $order->customer->getOrderCount(['from_date' => $from_date, 'to_date' => $to_date]);
        }

        return $this->render('follow-up', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'branchs' => $branchs,
            'totalOrders' => $totalOrders
        ]);

    }


}
