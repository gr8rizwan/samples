<?php

namespace backend\controllers;

use app\models\AgentIncentive;
use app\models\CategoryType;
use backend\models\Branch;
use common\helpers\StoreProcedures;
use Yii;
use backend\models\Order;
use backend\models\OrderSearch;
use backend\base\controllers\BaseController as Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use backend\widgets\order\models\ComplaintsSummary;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        // increase the memory limit to generate excel.
        ini_set('memory_limit', '-1');

        $searchModel = new OrderSearch();
        $searchModel->isToday = true;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // clone data provider for chart calculations
        $myDataProvider = clone $dataProvider;

        $myDataProvider->setPagination(false);

        // set aggregate data
        $totalOrders = 0;
        foreach ($myDataProvider->models as $model) {
            $totalOrders += $model->total_orders;
        }

        return $this->render('reports/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalOrders' => $totalOrders
        ]);
    }

    /**
     * @return string
     */
    public function actionLateOrderSummary(){

        $searchModel = new OrderSearch();
        $searchModel->isToday = true;
        $dataProvider = $searchModel->lateOrderReport(Yii::$app->request->queryParams);

        return $this->renderAjax('reports/late-order', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return string
     */
    public function actionComplaintsSummary(){

        $searchModel = new ComplaintsSummary();
        $searchModel->isToday = true;
        $dataProvider = $searchModel->complaintsSummaryReport(Yii::$app->request->queryParams);

        return $this->renderAjax('reports/complaints-summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return string
     */
    public function actionPromoCode(){

        $searchModel = new OrderSearch();
        $searchModel->isToday = true;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('reports/promo_code', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }


    /**
     * Displays a single Order model.
     * @param integer $id if for to view object
     * @return mixed
     */
    public function actionView($id)
    {
        // check if request is Ajax
        if (Yii::$app->request->isAjax) {
            // Request Helper call the Curl Request and get data Throw API's
            //This call will goes to Our Frontend API's

            $data = Yii::$app->RequestHelper->getCurl('orders/' . $id);
            return $this->renderAjax('ajax/_view', [
                'model' => Json::decode($data),
            ]);
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionAgentIncentiveReport() {

        $model = new AgentIncentive();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $fromDate = $model->fromDate;
            $toDate = $model->toDate;
            $attachments = [];

            foreach ($model->categories as $category) {
                $header = ['totalAmount', 'menuItem', 'agentName'];

                $report = StoreProcedures::agentsIncentive($category, $fromDate, $toDate);
                $category = CategoryType::findOne($category);
                $fileName = 'agentIncentive/' . time() . '_' . $category->name . '_' . $fromDate . '_TO_' . $toDate . '_data.csv';
                $file = fopen($fileName, 'w');

                // Set Headers
                fputcsv($file, $header);

                foreach ($report as $line) {
                    fputcsv($file, $line);
                }

                fclose($file);

                $attachments[] = \Yii::getAlias('@webroot') . '/' . $fileName;
            }


            Yii::$app->emailHelper->sendEmail('irene@myswich.com', ['nadeem@myswich.com', 'damian@myswich.com', 'fadel@myswich.com'], 'Agent Incentive report from ' . $fromDate . ' TO ' . $toDate, 'incentive-report', $model, $attachments);
            \Yii::$app->getSession()->setFlash('success', 'Agent Incentive reports is generated and sent to your email address, please check your email, thanks');

            foreach ($attachments as $attachment) {
                unlink($attachment);
            }

            return $this->render('email-me/incentive', [
                'model' => $model,
            ]);
        } else {
            return $this->render('email-me/incentive', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id if for update
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id if for to delete
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id is for find model
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
