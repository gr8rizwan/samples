<?php

namespace backend\controllers;

use backend\models\Config;
use backend\models\UserToBranch;
use Yii;
use backend\models\Area;
use backend\models\Branch;
use backend\models\BranchSearch;
use yii\helpers\Json;
use backend\base\controllers\BaseController as Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * BranchController implements the CRUD actions for Branch model.
 */
class BranchController extends Controller
{
    /**
     * Behavior actions
     * @return array of behaviors
     * */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Branch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BranchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Branch model.
     * @param integer $id is for to view object
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Branch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Branch();
        $config = new Config(['convert_rewards_point' => 0, 'twitter_points' => 0, 'facebook_points' => 0]);

        if (($model->load(Yii::$app->request->post()) && $model->validate()) &&
            ($config->load(Yii::$app->request->post()) && $config->validate())
        ) {
            /*Branch Configuration*/
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                $model->save();

                $config->open_time = $model->open_time;
                $config->close_time = $model->close_time;
                $config->branch_id = $model->id;


                if ($config->save()) {
                    $transaction->commit();
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'config' => $config,
            ]);
        }
    }

    /**
     * Updates an existing Branch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id is for to update object
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $config = Config::findOne(['branch_id' => $id]);

        $location_image = $config->location_image;
        $faq_image = $config->faq_image;

        if (($model->load(Yii::$app->request->post()) && $model->validate()) &&
            ($config->load(Yii::$app->request->post()) && $config->validate())
        ) {
            /*Branch Configuration*/
            $model->save();

            $config->open_time = $model->open_time;
            $config->close_time = $model->close_time;

            $config->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'config' => $config,
            ]);
        }
    }

    /**
     * Deletes an existing Branch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id is for to delete object
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Branch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id is for to find model
     * @return Branch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Branch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Display list of areas for selected branch
     * @return mixed
     */
    public function actionAreas()
    {
        $area_input = Yii::$app->request->post('depdrop_parents');
        $branch_id = array_pop($area_input);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $areas = (new Area)->getBranchAreas($branch_id);
        return ['output' => $areas, 'selected' => 'Select Area ...'];
    }

    /**
     *  get all user/driver from relation
     * @return mixed
     * */
    public function actionGetUsers()
    {

        if (yii::$app->request->post('depdrop_parents')) {
            $branch = yii::$app->request->post('depdrop_parents');
            $selected = yii::$app->request->post('depdrop_params');

            if ($branch != null) {
                $id = $branch[0];
                $drivers = UserToBranch::find()->with('user')->where(['branch_id' => $id])->all();
                $dropdown = [];
                foreach ($drivers as $driver) {
                    if ($driver->user->type == 'Driver') {
                        $dropdown[] = [
                            'id' => $driver->user->id,
                            'name' => $driver->user->first_name . ' ' . $driver->user->last_name
                        ];
                    }
                }

                echo Json::encode(['output' => $dropdown, 'selected' => (isset($selected[0]) ? $selected[0] : '')]);
                exit;
            }
        }

        echo Json::encode(['output' => '', 'selected' => '']);
    }
}
