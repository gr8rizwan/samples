<?php

namespace backend\controllers;

use yii;
use yii\filters\AccessControl;
use backend\base\controllers\BaseController as Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     * @return array of behaviors
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return array of actions
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * This is dashbaoard page
     * @return array of home page
     * */
    public function actionIndex()
    {

        return $this->render('index');
    }

    /**
     * Login function allow admin to login into application
     * @return boolean tru or false
     * */
    public function actionLogin()
    {
        // Get Franchises from live
        $franchiseList = Yii::$app->cache->get('franchise-lists');

        foreach ($franchiseList as $franchise) {
            $franchiseDropdown[$franchise['id']] = $franchise['name'];
        }


        $this->layout = 'before_auth';

        if (!yii::$app->user->isGuest) {
            return $this->goHome();
        }


        $model = new LoginForm();

        if ($model->load(yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
                'franchises' => $franchiseDropdown
            ]);
        }
    }

    /**
     * Logout function let user logout from application
     * @return boolean true or false
     * */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
