<?php

namespace backend\controllers;

use Yii;
use backend\models\BranchPosSetting;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PosSettingController implements the CRUD actions for BranchPosSetting model.
 */
class PosSettingController extends Controller
{
    /**
     * @inheritdoc
     * @return array of behavior
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BranchPosSetting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BranchPosSetting::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BranchPosSetting model.
     * @param integer $id is for to view object
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BranchPosSetting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $branch_id is for to create pos setting for specific branch
     * @return mixed
     */
    public function actionCreate($branch_id)
    {
        $model = new BranchPosSetting(['branch_id' => $branch_id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['status' => 'OK', 'message' => 'Pos vendor has been added.'];
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BranchPosSetting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id is for to update object
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['status' => 'OK', 'message' => 'Pos vendor has been updated.'];
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BranchPosSetting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id is for to delete object
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BranchPosSetting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id id for to find object
     * @return BranchPosSetting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BranchPosSetting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
