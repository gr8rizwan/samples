<?php

namespace backend\controllers;

use app\models\ReportGroup;
use Yii;
use backend\models\Customer;
use backend\models\CustomerSearch;
use backend\models\Order;
use backend\models\Address;
use backend\base\controllers\BaseController as Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Response;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Customer model.
     * @param integer $id id of the customer
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Customer();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

     /* Creates a new Customer Address
     * If creation is successful, the browser will be redirected to the 'view' page in case of not ajax request.
     * If creation is successful, the browser will be return success message.
     * @return if request is ajax then success message
     * @return if request is post sumbit then render 'view'
     * @params $customer_id
     */

    /**
     * @param null $customer_id customer id
     * @return string|Response
     */
    public function actionCreateAddress($customer_id = null)
    {
        $model = new Address;
        $model->customer_id = $customer_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // check if request is ajax then use render ajax view
            if (Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;

                $data['status'] = 'OK';
                $data['message'] = 'New address added successfully!';
                return $data;
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            // check if request is ajax then use render ajax view
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('address/_new', [
                    'model' => $model,
                ]);
            }

            // if request is not ajax the render view as plain html
            return $this->render('address/_new', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Update Customer Address
     * If update is successful, the browser will be redirected to the 'view' page in case of not ajax request.
     * If update is successful, the browser will be return success message.
     * @return if request is ajax then success message
     * @return if request is post sumbit then render 'view'
     * @params $customer_id
     */

    /**
     * @param null $address_id address id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateAddress($address_id = null)
    {

        if (($model = Address::findOne($address_id)) !== null) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                // check if request is ajax then use render ajax view
                if (Yii::$app->request->isAjax) {
                    \Yii::$app->response->format = Response::FORMAT_JSON;

                    $data['status'] = 'OK';
                    $data['message'] = 'New address updated successfully!';
                    return $data;
                } else {
                    return $this->redirect(['view', 'id' => $model->customer_id]);
                }
            } else {
                // check if request is ajax then use render ajax view
                if (Yii::$app->request->isAjax) {
                    return $this->renderAjax('address/_update', [
                        'model' => $model,
                    ]);
                }

                // if request is not ajax the render view as plain html
                return $this->render('address/_update', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id id of the customer
     * @return mixed
     */

    /**
     * @param integer $id   id of the customer
     * @param string  $type role type
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id, $type = '')
    {
        $model = $this->findModel($id);
        $request = Yii::$app->request;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // check if request is ajax then use render ajax view
            if (Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;

                $data['status'] = 'OK';
                $data['message'] = 'Customer profile updated successfully!';
                return $data;
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            // check if request is ajax then use render ajax view
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('ajax/_update', [
                    'model' => $model,
                    'type' => $type,
                ]);
            }

            // if request is not ajax the render view as plain html
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /* ViewOrders
     * Param: Phone_number
     * Get All Customer Orders by Phone #
     * */

    /**
     * view orders
     * @return object
     */
    public function actionViewOrders()
    {
        if (yii::$app->request->post('depdrop_parents')) {
            $customers = yii::$app->request->post('depdrop_parents');
            $selected = yii::$app->request->post('depdrop_params');
            if ($customers != null) {
                $id = $customers[0];
                $orders = Order::find()->where(['customer_id' => $id])->all();
                $dropdown = [];
                foreach ($orders as $order) {
                    $dropdown[] = [
                        'id' => $order->id,
                        'name' => yii::$app->dateTime->getReadableFormat($order->time) . ' ' . yii::$app->setting->formatePrice($order->total_sum) . ' (' . $order->branch->name . ')',
                    ];
                }

                echo Json::encode(['output' => $dropdown, 'selected' => (isset($selected[0]) ? $selected[0] : '')]);
                exit;
            }
        }

        echo Json::encode(['output' => '', 'selected' => '']);
    }


    /**
     * @param null $q  query string
     * @param null $id id list of customers
     * @return array
     */
    public function actionCustomerList($q = null, $id = null)
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $response = ['results' => ['id' => '', 'name' => '']];

        if (!is_null($q)) {
            $customers = Customer::find()
                ->select(['id', 'first_name', 'last_name', 'phone_number'])
                ->where(['LIKE', 'first_name', $q])
                ->orWhere(['LIKE', 'last_name', $q])
                ->orWhere(['LIKE', 'phone_number', $q])
                ->limit(20)
                ->all();

            foreach ($customers as $customer) {
                $response[] = ['id' => $customer->id, 'name' => $customer->first_name . ' ' . $customer->last_name . ' (' . $customer->phone_number . ')'];
            }

            $response = ['results' => array_values($response)];
        } elseif ($id > 0) {
            $customer = Customer::findOne($id);
            $response['results'] = ['id' => $id, 'name' => $customer->first_name . ' ' . $customer->last_name . ' (' . $customer->phone_number . ')'];
        }

        return $response;
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    /**
     * @param integer $id id of the customer
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Customer address.
     * If deletion is successful, the browser will be redirected to the 'index' page if request is not ajax else will return sucess or error message
     * @param integer $address_id id of the customer address
     * @return mixed
     */
    public function actionDeleteAddress($address_id)
    {
        $address = Address::findOne($address_id);
        $address->is_deleted = 1;
        $address->save();

        // check if request is ajax then use render ajax view
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

            $data['status'] = 'OK';
            $data['message'] = 'Customer Address status deleted successfully!';
            return $data;
        }
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id id of the customer
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Customer::find()
            ->with([
                'firstOrder',
                'highSumOrder',
                'lowestSumOrder',
                'order',
                'loyalty'
            ])
            ->where(['id' => $id])
            ->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
