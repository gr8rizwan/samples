<?php

namespace backend\controllers;

use backend\models\Branch;
use backend\models\OrderOrigin;
use Yii;
use app\models\ReportGroup;
use app\models\ReportTypes;
use backend\models\CustomerSearch;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class CustomerReportController
 * @package backend\controllers
 */
class CustomerReportController extends OrderController
{
    public $enableCsrfValidation = false;   // make angularjs ajax call work

    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex() {

        $params = Yii::$app->request->queryParams;
        $groupId = $params['CustomerSearch']['groupId'];

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);

        if (!$groupId) {
            $groupModel = ReportGroup::findOne(['type_id' => 1]);
        } else {
            $groupModel = ReportGroup::findOne($groupId);
            if (!$groupModel) {
                throw new \yii\web\NotFoundHttpException();
            }
        }

        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search($params);


        $reportModel = ReportTypes::find()->where(['id' => 1])->with('reportGroups')->one();
        $originModel = OrderOrigin::find()->where(['status' => 1])->all();
        $branchModel = Branch::find()->where(['is_deleted' => 0])->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'reportModel' => $reportModel,
            'groupModel' => $groupModel,
            'dataProvider' => $dataProvider,
            'originModel' => $originModel,
            'branchModel' => $branchModel,
        ]);
    }

    /**
     * @return array reportTypeProperties
     */
    public function actionReportProperties(){

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->get('id') != '-1') {
            $reportModel = ReportTypes::find()->where(['id' => Yii::$app->request->get('id')])->with('reportTypeProperties')->one();
        } else {
            $reportModel = ReportTypes::findOne(['id' => 1])->with('reportTypeProperties');
        }


        return ArrayHelper::toArray($reportModel->reportTypeProperties);
    }

    /**
     * @return array reportGroupProperties
     */
    public function actionGroupProperties()
    {

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->get('id') != '-1') {
            $propertiesModel = ReportGroup::find()->where(['id' => Yii::$app->request->get('id')])->with('reportGroupProperties')->one();
        } else {
            $propertiesModel = ReportGroup::find()->where(['type_id' => 1])->with('reportGroupProperties')->one();
        }

        if ($propertiesModel !== null) {
            return ArrayHelper::toArray($propertiesModel->reportGroupProperties);
        } else {
            return [];
        }
    }

    /**
     * @return array
     */
    public function actionSubmitGroup(){

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $postData = \GuzzleHttp\json_decode(Yii::$app->request->rawBody, true);
        $groupName = $postData['name'];

        if ($groupName == '') {
            return [
                'status' => false,
                'message' => 'group will be handled.'
            ]; //todo: create temp group
        }

        $properties = [];
        foreach ($postData['properties'] as $key => $property) {
            $groupProperty = [
                'property_id' => $property,
                'sort_order' => $key
            ];
            array_push($properties, $groupProperty);
        }

        $postData['properties'] = $properties;

        if (!empty($postData['id'])) {
            $model = ReportGroup::findOne($postData['id']);
        } else {
            $model = new ReportGroup();
        }

        if ($model->load($postData, '') && $model->validate()) {
            $model->save();

            return [
                'id' => $model->id,
                'status' => true,
                'message' => 'form has been created'
            ];
        } else {
            return [
                'status' => false,
                'message' => 'Error occurred, try again.'
            ];
        }
    }

    /**
     * @return array
     */
    public function actionDeleteGroup(){

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->get('id');

        ReportGroup::deleteAll(['id' => $id]);

        return [
            'status' => true,
            'message' => 'form has been deleted.'
        ];
    }
}
