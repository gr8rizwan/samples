<?php

namespace backend\controllers;

use Yii;
use backend\models\CustomerFeedback;
use backend\models\CustomerFeedbackSearch;
use backend\models\FeedbackResolutionAssign;
use backend\base\controllers\BaseController as Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * CustomerFeedbackController implements the CRUD actions for CustomerFeedback model.
 */
class CustomerFeedbackController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerFeedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerFeedbackSearch();
        $searchModel->status = '0';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerFeedback model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        //Yii::$app->RequestHelper->getCurl('orders/'.$model->order_id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }


    /**
     * Displays a single CustomerFeedback model.
     * @param integer $feedback_id
     * @return mixed
     */

    public function actionViewItems($feedback_id) {

        if(Yii::$app->request->isAjax) {

            $data = Yii::$app->RequestHelper->getCurl('feedbacks/view-feedback/feedback_id/'.$feedback_id);

            return $this->renderAjax('ajax/_view-items', [
                'model' => Json::decode($data),
            ]);

        }

    }

    /**
     * Creates a new CustomerFeedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomerFeedback();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CustomerFeedback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CustomerFeedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /*Feedback Assignment */

    public function actionFeedbackAssignment($assignment_id, $type) {

        $assignment = FeedbackResolutionAssign::findOne($assignment_id);

        $model = new FeedbackResolutionAssign;

        $model->status = $assignment->status;
        $model->assigned_to = $assignment->assigned_to;
        $model->assigned_by = Yii::$app->user->id;

        if(Yii::$app->request->isPost) {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if($model->load(Yii::$app->request->post()) && $model->validate() ) {

                $assignment->status = 'close';
                $assignment->updated_at = Yii::$app->dateTime->getCurrentDateTime();

                if($model->save() && $assignment->save()) {

                    if($model->status == 'close') {
                        $feedback = $assignment->feedback;
                        $feedback->status = '1';
                        $feedback->save();
                    }

                    $data['status'] = 'OK';
                    $data['message'] = 'Feedback status updated successfully!';
                    return $data;
                }

            }

            $data['status'] = 'ERROR';
            $data['message'] = 'There is problem with your request!';
            return $data;

        }

        $model->feedback_id = $assignment->feedback_id;
        $model->assigned_by = Yii::$app->user->id;

        return $this->renderAjax('ajax/_form', [
            'model' => $model,
            'type' => $type
        ]);

    }

    /**
     * Finds the CustomerFeedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerFeedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerFeedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
