<?php

namespace backend\controllers;

use backend\models\BannerImages;
use Yii;
use backend\models\Banners;
use backend\models\BannersSearch;
use backend\base\controllers\BaseController as Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BannerController implements the CRUD actions for Banners model.
 */
class BannerController extends Controller
{
    /**
     * @inheritdoc
     * @return array of behavior
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banners models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banners model.
     * @param integer $id viewable ID
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Banners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banners();

        $inputs = [];

        if (Yii::$app->request->isPost) {
            $inputs = Yii::$app->request->post();

            $inputs['Banners']['images'] = $inputs['images'];
        }

        if ($model->load($inputs) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Banners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id updateable ID
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $inputs = [];

        if (Yii::$app->request->isPost) {
            $inputs = Yii::$app->request->post();
            $inputs['Banners']['images'] = $inputs['images'];
        }

        if ($model->load($inputs) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Banners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id Delete id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Remove banner image
     * @param integer $id of removable
     * @throws HttpException
     * @return object $model of removable
     * */
    public function actionRemoveImage($id) {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = BannerImages::findOne($id);

        if ($model !== null) {
            $model->delete();
            return ['status' => 'OK', 'message' => 'Banner item deleted successfully.'];
        } else {
            throw new HttpException(422, 'Invalid request id', 422);
        }
    }

    /**
     * Finds the Banners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id find ID
     * @return Banners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banners::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
