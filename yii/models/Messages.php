<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property string $message
 * @property string $type
 * @property integer $status
 * @property string $start_date
 * @property string $end_date
 */
class Messages extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message', 'admin_id'], 'required'],
            [['type'], 'string'],
            [['status', 'admin_id'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Message',
            'type' => 'Type',
            'status' => 'Status',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'admin_id'  => 'Created By',
        ];
    }
}
