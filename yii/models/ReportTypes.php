<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_types".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 *
 * @property ReportGroup[] $reportGroups
 * @property ReportTypeProperties[] $reportTypeProperties
 */
class ReportTypes extends \yii\db\ActiveRecord
{

    /**
     * @var array
     */
    public $properties = [];

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'report_types';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['properties'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportGroups()
    {
        return $this->hasMany(ReportGroup::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportTypeProperties()
    {
        return $this->hasMany(ReportTypeProperties::className(), ['type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @param boolean $insert            true or false
     * @param array   $changedAttributes list of attributes
     * @return null
     */
    public function afterSave($insert, $changedAttributes) {


        foreach ($this->properties as $property) {
            if (isset($property['id']) && (int)$property['id'] > 0) {
                $bannerImage = ReportTypeProperties::findOne($property['id']);
            } else {
                $bannerImage = new ReportTypeProperties();
            }

            $bannerImage->attributes = $property;
            $bannerImage->type_id = $this->id;
            $bannerImage->save();
        }
    }
}
