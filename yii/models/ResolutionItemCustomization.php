<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "resolution_item_customization".
 *
 * @property integer $id
 * @property integer $quantity
 * @property integer $resolution_item_id
 * @property integer $ingredient_id
 *
 * @property FeedbackResolutionItems $resolutionItem
 * @property Ingredient $ingredient
 */
class ResolutionItemCustomization extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resolution_item_customization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity', 'resolution_item_id', 'ingredient_id'], 'integer'],
            [['resolution_item_id', 'ingredient_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quantity' => 'Quantity',
            'resolution_item_id' => 'Resolution Item ID',
            'ingredient_id' => 'Ingredient ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResolutionItem()
    {
        return $this->hasOne(FeedbackResolutionItems::className(), ['id' => 'resolution_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredient_id']);
    }
}
