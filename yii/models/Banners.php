<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "banners".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property integer $status
 *
 * @property BannerImages[] $bannerImages
 */
class Banners extends BaseModel
{
    public $images;
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'banners';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type', 'page'], 'string'],
            [['status'], 'integer'],
            [['images'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return array of labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'page' => 'Page',
            'status' => 'Status',
        ];
    }

    /**
     * @inheritdoc
     * @param boolean $insert            true or false
     * @param array   $changedAttributes list of attributes
     * @return null
     */
    public function afterSave($insert, $changedAttributes) {

        foreach ($this->images as $image) {
            if ((int)$image['id'] > 0) {
                $bannerImage = BannerImages::findOne($image['id']);
            } else {
                $bannerImage = new BannerImages();
            }

            $bannerImage->attributes = $image;
            $bannerImage->banner_id = $this->id;
            $bannerImage->save();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerImages()
    {
        return $this->hasMany(BannerImages::className(), ['banner_id' => 'id']);
    }
}
