<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CategoryType;

/**
 * CategoryTypeSearch represents the model behind the search form about `app\models\CategoryType`.
 */
class CategoryTypeSearch extends CategoryType
{
    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['id', 'status', 'sort_order', 'onMenu'], 'integer'],
            [['title', 'description', 'name', 'banner_image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return object of Scenarios
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params array of searchable
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CategoryType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'sort_order' => $this->sort_order,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
