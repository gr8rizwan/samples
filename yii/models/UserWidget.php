<?php

namespace app\models;

use backend\models\User;
use Yii;

/**
 * This is the model class for table "user_widget".
 *
 * @property integer $id
 * @property integer $widget_id
 * @property integer $user_id
 * @property integer $sort_order
 * @property string $screen_size
 *
 * @property Widgets $widget
 * @property User $user
 */
class UserWidget extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $widgets = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_widget';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['widget_id', 'user_id'], 'required'],
            [['widget_id', 'user_id', 'sort_order'], 'integer'],
            [['screen_size'], 'string'],
            [['widgets', 'widgets'], 'safe'],
            [['widget_id'], 'exist', 'skipOnError' => true, 'targetClass' => Widgets::className(), 'targetAttribute' => ['widget_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'widget_id' => Yii::t('app', 'Widget ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'screen_size' => Yii::t('app', 'Screen Size'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidget()
    {
        return $this->hasOne(Widgets::className(), ['id' => 'widget_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function afterFind() {

    }

}
