<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "banner_images".
 *
 * @property integer $id
 * @property integer $banner_id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $mobile_image
 * @property string $href
 * @property integer $sort_order
 * @property integer $status
 *
 * @property Banners $banner
 */
class BannerImages extends BaseModel
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'banner_images';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['banner_id', 'image'], 'required'],
            [['banner_id', 'sort_order', 'status'], 'integer'],
            [['title', 'image', 'href', 'mobile_image'], 'string', 'max' => 255],
            [['description'], 'safe'],
            [['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banners::className(), 'targetAttribute' => ['banner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'banner_id' => 'Banner ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'mobile_image' => 'Mobile banner',
            'href' => 'Href',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banners::className(), ['id' => 'banner_id']);
    }

    /**
     * @inheritdoc
     * @param boolean $insert true or false
     * @return boolean true or false
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (empty($this->mobile_image)) {
                $this->mobile_image = $this->image;
            }

            return true;
        } else {
            return false;
        }
    }
}
