<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_data_fields".
 *
 * @property integer $id
 * @property integer $form_data_id
 * @property string $attribute
 * @property string $value
 *
 * @property FormsData $formData
 */
class FormDataFields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'form_data_fields';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['form_data_id'], 'required'],
            [['form_data_id'], 'integer'],
            [['attribute', 'value'], 'string', 'max' => 255],
            [['form_data_id'], 'exist', 'skipOnError' => true, 'targetClass' => FormsData::className(), 'targetAttribute' => ['form_data_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attributes
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'form_data_id' => Yii::t('app', 'Form Data ID'),
            'attribute' => Yii::t('app', 'Attribute'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormData()
    {
        return $this->hasOne(FormsData::className(), ['id' => 'form_data_id']);
    }
}
