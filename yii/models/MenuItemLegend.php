<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_item_legend".
 *
 * @property integer $id
 * @property integer $legend_id
 * @property integer $menu_item_id
 * @property integer $status
 *
 * @property Legend $legend
 * @property MenuItem $menuItem
 */
class MenuItemLegend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return 'menu_item_legend';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['legend_id', 'menu_item_id'], 'required'],
            [['legend_id', 'menu_item_id', 'status'], 'integer'],
            [['legend_id'], 'exist', 'skipOnError' => true, 'targetClass' => Legend::className(), 'targetAttribute' => ['legend_id' => 'id']],
            [['menu_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => MenuItem::className(), 'targetAttribute' => ['menu_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'legend_id' => Yii::t('app', 'Legend ID'),
            'menu_item_id' => Yii::t('app', 'Menu Item ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegend()
    {
        return $this->hasOne(Legend::className(), ['id' => 'legend_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItem::className(), ['id' => 'menu_item_id']);
    }
}
