<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "branch_pos_setting".
 *
 * @property integer $id
 * @property integer $franchise_vendor_id
 * @property integer $branch_id
 * @property string $server_endpoint
 * @property string $server_url
 * @property string $endpoint_vendor
 * @property string $endpoint_user
 * @property string $endpoint_password
 * @property string $location_id
 * @property integer $status
 *
 * @property Branch $branch
 */
class BranchPosSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'branch_pos_setting';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['franchise_vendor_id', 'branch_id', 'server_endpoint', 'server_url', 'endpoint_vendor', 'endpoint_user', 'endpoint_password', 'location_id'], 'required'],
            [['franchise_vendor_id', 'branch_id', 'status'], 'integer'],
            [['server_endpoint', 'server_url', 'endpoint_vendor', 'endpoint_user', 'endpoint_password', 'location_id'], 'string', 'max' => 255],
            [['branch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Branch::className(), 'targetAttribute' => ['branch_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'franchise_vendor_id' => Yii::t('app', 'Franchise Vendor ID'),
            'branch_id' => Yii::t('app', 'Branch ID'),
            'server_endpoint' => Yii::t('app', 'Server Endpoint'),
            'server_url' => Yii::t('app', 'Server Url'),
            'endpoint_vendor' => Yii::t('app', 'Endpoint Vendor'),
            'endpoint_user' => Yii::t('app', 'Endpoint User'),
            'endpoint_password' => Yii::t('app', 'Endpoint Password'),
            'location_id' => Yii::t('app', 'Location ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branch_id']);
    }
}
