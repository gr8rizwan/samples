<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "feedback_resolution_items".
 *
 * @property integer $id
 * @property integer $feedback_id
 * @property integer $quantity
 * @property integer $is_customized
 * @property integer $menu_item_id
 *
 * @property MenuItem $menuItem
 * @property CustomerFeedback $feedback
 * @property ResolutionItemCustomization[] $resolutionItemCustomizations
 */
class FeedbackResolutionItems extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback_resolution_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feedback_id'], 'required'],
            [['single_price'], 'safe'],
            [['feedback_id', 'quantity', 'is_customized', 'menu_item_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'feedback_id' => 'Feedback ID',
            'quantity' => 'Quantity',
            'is_customized' => 'Is Customized',
            'menu_item_id' => 'Menu Item ID',
            'single_price' => 'Item Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItem::className(), ['id' => 'menu_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedback()
    {
        return $this->hasOne(CustomerFeedback::className(), ['id' => 'feedback_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResolutionItemCustomizations()
    {
        return $this->hasMany(ResolutionItemCustomization::className(), ['resolution_item_id' => 'id']);
    }
}
