<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "branch".
 *
 * @property integer $id
 * @property string $name
 * @property string $detailed_address
 * @property string $opening_hours
 * @property string $open_time
 * @property string $close_time
 * @property string $lng
 * @property string $lat
 * @property integer $sequence
 * @property integer $is_deleted
 * @property string $server_url
 * @property string $endpoint_url
 * @property string $endpoint_vendor
 * @property string $endpoint_user
 * @property string $endpoint_password
 * @property string $locationID
 * @property string $locationToken
 *
 * @property Order[] $orders
 */
class Branch extends BaseModel
{

    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'branch';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['city', 'country_id', 'name', 'detailed_address', 'open_time', 'close_time'], 'required'],
            [['detailed_address'], 'string'],
            [['open_time', 'close_time', 'opening_hours'], 'safe'],
            [['detailed_address', 'opening_hours'], 'string', 'max' => 255],
            [['lng', 'lat'], 'number'],
            [['sequence', 'country_id', 'is_deleted'], 'integer'],
            [['name', 'server_url', 'endpoint_url', 'endpoint_vendor', 'endpoint_user', 'endpoint_password', 'locationID', 'locationToken'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'detailed_address' => 'Detailed Address',
            'opening_hours' => 'Opening Hours',
            'open_time' => 'Open Time',
            'close_time' => 'Close Time',
            'lng' => 'Lng',
            'lat' => 'Lat',
            'sequence' => 'Sequence',
            'is_deleted' => 'Is Deleted',
            'server_url' => 'Server Url',
            'endpoint_url' => 'End point URL',
            'endpoint_vendor' => 'End point Vendor',
            'endpoint_user' => 'End Point User',
            'endpoint_password' => 'End point password',
            'locationToken' => 'Location Token',
            'locationID' => 'Location ID',
            'country_id'    => 'Country ID',
            'city'  => 'City',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['branch_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTodayOrders()
    {
        return  $this->hasMany(Order::className(), ['branch_id' => 'id'])
                    ->where(['DATE(time)' => Yii::$app->dateTime->getDate()])
                    ->andWhere(['OR',['!=', 'twox_id', 0],['!=', 'om_id', 0]])
                    ->andWhere(['!=', Order::tableName() . '.status', 'cancelled']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranchPosSetting()
    {
        return $this->hasMany(BranchPosSetting::className(), ['branch_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTodayResolutions()
    {
        return $this->hasMany(Order::className(), ['branch_id' => 'id'])
                    ->where(['DATE(time)' => Yii::$app->dateTime->getDate()])
                    ->andWhere(['=', 'price', 0])
                    ->andWhere(['OR',['!=', 'twox_id', 0],['!=', 'om_id', 0]]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfig()
    {
        return $this->hasOne(Config::className(), ['branch_id' => 'id']);
    }
}
