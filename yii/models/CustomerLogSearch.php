<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CustomerLog;

/**
 * CustomerLogSearch represents the model behind the search form about `backend\models\CustomerLog`.
 */
class CustomerLogSearch extends CustomerLog
{

    public $isToday;
    public $area;
    public $log_type;
    public $status;
    public $branch_id;
    public $time_category;
    public $from_date;
    public $to_date;
    public $type;
    public $customer;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'order_id', 'action_id'], 'integer'],
            [['description', 'time', 'log_type', 'isToday', 'area', 'log_type', 'status', 'branch_id', 'time_category', 'from_date','to_date', 'type', 'customer'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'order_id' => $this->order_id,
            'action_id' => $this->action_id,
            'time' => $this->time,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'log_type', $this->log_type]);

        return $dataProvider;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchFollowUp($params)
    {
        // by default show data for today
        if(!isset($this->from_date))
            $this->from_date = Yii::$app->dateTime->getDate();

        if(!isset($this->to_date))
            $this->to_date = Yii::$app->dateTime->getDate();

        $query = CustomerLog::find()->with(['customer', 'order', 'order.address', 'order.branch', 'order.area', 'orderTimeCategory']);

        $this->load($params);

        if($this->customer) {
            $query->joinWith(['customer'], true, ' INNER JOIN');
        }

        if($this->branch_id) {
            $query->joinWith(['order.branch'], true, ' INNER JOIN');
        }

        if ($this->area) {
            $query->joinWith(['order.address'], true, ' INNER JOIN');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // if in params there is customer name then search with nama wild card...
        if(isset($this->customer) && !empty($this->customer)) {
            // split customer name
            $customer_name = explode(' ', trim($this->customer));
            $customer_name = implode('|', $customer_name);

            $query->andFilterWhere(['REGEXP', 'customer.first_name', $customer_name])
                ->orFilterWhere(['REGEXP', 'customer.last_name', $customer_name])
                ->orFilterWhere(['REGEXP', 'customer.phone_number', $customer_name]);

        }

        //this is used when we need today results
        if($this->isToday) {
            $query->andFilterWhere(['Date(customer_log.time)' => Yii::$app->dateTime->getDate()]);
        }

        //Chreck if params has from_date parameter, if yes then allow search on from_date
        if(!empty($this->from_date)) {
            $query->andFilterWhere(['>=', 'Date(customer_log.time)', $this->from_date]);
        }

        //Chreck if params has to_date parameter, if yes then allow search on to_date
        if(!empty($this->to_date)) {
            $query->andFilterWhere(['<=', 'Date(customer_log.time)', $this->to_date]);
        }

        // If branch is selected, put condition in query
        if($this->branch_id) {
            $query->andFilterWhere(['branch_id' => $this->branch_id]);
        }

        // fetch all records based on area
        if($this->area) {
            $query->andFilterWhere(['area' => $this->area]);
        }

        // log type should be 'follow-up'
        $query->andFilterWhere(['log_type' => 'follow-up']);

        if(!isset($params['sort'])) {
            $query->orderBy(['time' => SORT_DESC]);
        }
//        echo '<pre>';
//        var_dump($params);
//        var_dump($this->isToday);
//        echo $query->createCommand()->getSql();
//        die;
        return $dataProvider;

    }


}
