<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "static_pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $seo_url
 * @property string $description
 * @property string $top_description
 * @property string $bottom_description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $seo_keywords
 * @property integer $bottom
 * @property integer $top
 * @property integer $sort_order
 * @property string $banner
 * @property string $mobile_banner
 * @property string $banner_title
 * @property string $banner_description
 * @property integer $status
 * @property mixed $page_type
 */
class StaticPages extends BaseModel
{
    /**
     * @inheritdoc
     * @return string static_pages
     */
    public static function tableName()
    {
        return 'static_pages';
    }

    /**
     * @inheritdoc
     * @return array rules
     */
    public function rules()
    {
        return [
            [['title', 'seo_url', 'description'], 'required'],
            [['description', 'page_type', 'top_description', 'bottom_description'], 'string'],
            [['bottom', 'top', 'sort_order', 'status', 'tab_id'], 'integer'],
            [['title', 'seo_url', 'banner', 'mobile_banner'], 'string', 'max' => 255],
            [['meta_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 150],
            [['seo_keywords', 'banner_title', 'banner_description'],   'string', 'max' => 100],
            [['title'], 'unique'],
            [['seo_url'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     * @return array attributeLabels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'seo_url' => 'Seo Url',
            'description' => 'Description',
            'top_description' => 'Top description',
            'bottom_description' => 'Bottom description',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'seo_keywords' => 'Seo Keywords',
            'bottom' => 'Bottom',
            'top' => 'Top',
            'sort_order' => 'Sort Order',
            'banner' => 'Banner',
            'banner_title' => 'Banner Title',
            'banner_description' => 'Banner Description',
            'status' => 'Status',
            'page_type' => 'Page Type',
        ];
    }

    /**
     *
     * Before save function that will create seo_url
     * @return string $seo_url
     * */
    public function beforeValidate() {

        $this->seo_url = strtolower(str_replace(' ', '-', $this->title));

        if (empty($this->mobile_banner)) {
            $this->mobile_banner = $this->banner;
        }

        return true;
    }
}
