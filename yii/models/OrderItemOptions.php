<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_item_options".
 *
 * @property integer $id
 * @property integer $order_menu_item_id
 * @property integer $item_options_value_id
 * @property integer $quantity
 */
class OrderItemOptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'order_item_options';
    }

    /**
     * @inheritdoc
     * @return array rules
     */
    public function rules()
    {
        return [
            [['order_menu_item_id', 'item_options_value_id'], 'required'],
            [['order_menu_item_id', 'item_options_value_id', 'quantity'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     * @return array attributeLabels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_menu_item_id' => Yii::t('app', 'Order Menu Item ID'),
            'item_options_value_id' => Yii::t('app', 'Item Options Value ID'),
            'quantity' => Yii::t('app', 'Quantity'),
        ];
    }
}
