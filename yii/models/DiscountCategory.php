<?php

namespace app\models;

use backend\base\models\BaseModel;
use backend\models\OrderOrigin;
use Yii;

/**
 * This is the model class for table "discount_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $points
 * @property string $percentage
 * @property string $amount
 * @property string $origin_id
 * @property integer $is_deleted
 * @property integer $pos_id
 *
 * @property OrderDiscountCategory[] $orderDiscountCategories
 * @property Origin[] $origin
 */

class DiscountCategory extends BaseModel
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'discount_category';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['name', 'percentage', 'start_date', 'end_date', 'origin_id'], 'required'],
            [['points', 'is_deleted', 'pos_id', 'origin_id'], 'integer'],
            [['percentage', 'amount'], 'number'],
            [['name', 'start_date', 'end_date'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     * @return array attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'points' => 'Points',
            'percentage' => 'Discount Type',
            'amount' => 'Value',
            'origin_id' => 'Origin',
            'is_deleted' => 'Status',
            'start_date'    => 'Start Date',
            'end_date'  => 'End Date',
            'pos_id'  => 'POS ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDiscountCategories()
    {
        return $this->hasMany(OrderDiscountCategory::className(), ['discount_cat_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrigin()
    {
        return $this->hasOne(OrderOrigin::className(), ['id' => 'origin_id']);
    }

    /**
     * @param object $model     model object
     * @param string $attribute attribute
     * @return mixed
     */
    public function getOrderSum($model, $attribute){
        return $model->getOrderDiscountCategories()->select('o.id')->join('INNER JOIN', 'order o', 'order_id = o.id')->sum($attribute);
    }
}
