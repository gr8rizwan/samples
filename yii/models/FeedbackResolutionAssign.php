<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "feedback_resolution_assign".
 *
 * @property integer $id
 * @property integer $feedback_id
 * @property integer $assigned_by
 * @property integer $assigned_to
 * @property string $description
 * @property string $status
 * @property string $created_at
 *
 * @property User $assignedBy
 * @property CustomerFeedback $feedback
 * @property User $assignedTo
 */
class FeedbackResolutionAssign extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback_resolution_assign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feedback_id', 'assigned_by', 'assigned_to', 'description'], 'required'],
            [['feedback_id', 'assigned_by', 'assigned_to'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['description'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'feedback_id' => 'Feedback ID',
            'assigned_by' => 'Assigned By',
            'assigned_to' => 'Assigned To',
            'description' => 'Description',
            'status' => 'Status',
            'created_at' => 'Created',
            'updated_at' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssignedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'assigned_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedback()
    {
        return $this->hasOne(CustomerFeedback::className(), ['id' => 'feedback_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssignedTo()
    {
        return $this->hasOne(User::className(), ['id' => 'assigned_to']);
    }
}
