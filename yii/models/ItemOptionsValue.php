<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_options_value".
 *
 * @property integer $id
 * @property integer $item_option_id
 * @property integer $ingredient_id
 * @property string $price
 * @property integer $om_id
 * @property integer $twox_id
 * @property string $image
 * @property integer $sort_order
 */
class ItemOptionsValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'item_options_value';
    }

    /**
     * @inheritdoc
     * @return array rules
     */
    public function rules()
    {
        return [
            [['item_option_id', 'ingredient_id'], 'required'],
            [['item_option_id', 'ingredient_id', 'om_id', 'twox_id', 'sort_order'], 'integer'],
            [['price'], 'number'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return array attributeLabels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_option_id' => Yii::t('app', 'Item Option ID'),
            'ingredient_id' => Yii::t('app', 'Ingredient ID'),
            'price' => Yii::t('app', 'Price'),
            'om_id' => Yii::t('app', 'Om ID'),
            'twox_id' => Yii::t('app', 'Twox12 ID'),
            'image' => Yii::t('app', 'Image'),
            'sort_order' => Yii::t('app', 'Sort Order'),
        ];
    }
}
