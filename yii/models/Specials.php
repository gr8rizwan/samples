<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;
use app\models\MenuItem;

/**
 * This is the model class for table "specials".
 *
 * @property integer $id
 * @property integer $menu_item_id
 * @property double $unit_sale_price
 * @property string $special_title
 * @property string $onAmount
 * @property string $start_date
 * @property string $end_date
 *
 * @property MenuItem $menuItem
 */
class Specials extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'specials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_item_id', 'unit_sale_price', 'special_title', 'start_date', 'end_date'], 'required'],
            [['menu_item_id'], 'integer'],
            [['unit_sale_price', 'onAmount'], 'number'],
            [['start_date', 'end_date'], 'safe'],
            [['special_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_item_id' => 'Menu Item ID',
            'unit_sale_price' => 'Unit Sale Price',
            'special_title' => 'Special Title',
            'onAmount' => 'On Amount',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItem::className(), ['id' => 'menu_item_id']);
    }
}
