<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "feedback_resolution".
 *
 * @property integer $id
 * @property integer $feedback_id
 * @property string $description
 * @property string $status
 * @property string $created_at
 *
 * @property CustomerFeedback $feedback
 */
class FeedbackResolution extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback_resolution';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feedback_id'], 'required'],
            [['feedback_id', 'order_id', 'order_id'], 'integer'],
            [['status'], 'string'],
            [['created_at'], 'safe'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'feedback_id' => 'Feedback ID',
            'description' => 'Description',
            'status' => 'Status',
            'created_at' => 'Created At',
            'order_id' => 'Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedback()
    {
        return $this->hasOne(CustomerFeedback::className(), ['id' => 'feedback_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
