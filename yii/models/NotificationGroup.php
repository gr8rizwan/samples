<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "notification_group".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property string $created_at
 */
class NotificationGroup extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }
}
