<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "ingredient_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $sequence
 * @property integer $is_deleted
 * @property string $scope
 * @property string $om_type
 * @property string $om_domainid
 * @property string $om_minqty
 * @property string $om_maxqty
 * @property string $om_reducedzone
 * @property integer $LevelId
 * @property integer $RuleOnLevel
 *
 * @property Ingredient[] $ingredients
 */
class IngredientCategory extends BaseModel
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'ingredient_category';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['name', 'title', 'om_domainid', 'single_max_qty', 'scope'], 'required'],
            [['sequence', 'quantity', 'is_deleted', 'LevelId', 'RuleOnLevel'], 'integer'],
            [['name', 'title', 'om_type', 'om_domainid', 'om_minqty', 'om_maxqty', 'extra_unit_price', 'om_reducedzone', 'scope'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     * @return array of attributes label
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Category Title',
            'sequence' => 'Sort Order',
            'is_deleted' => 'Is Deleted',
            'scope' => 'Scope',
            'om_type' => 'Om Type',
            'om_domainid' => 'Order Mate ID',
            'om_minqty' => 'Order Mate Min qty',
            'om_maxqty' => 'Order Mate Max qty',
            'om_reducedzone' => 'Order Mate Reducedzone',
            'extra_unit_price' => 'Extra Unit Price',
            'quantity'  => 'Quantity',
            'LevelId'  => 'Level ID',
            'single_max_qty'  => 'Single Max Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredients()
    {
        return $this->hasMany(Ingredient::className(), ['ingredient_category_id' => 'id']);
    }
}
