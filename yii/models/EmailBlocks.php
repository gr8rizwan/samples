<?php

namespace app\models;

use Yii;
use backend\models\Banners;

/**
 * This is the model class for table "email_blocks".
 *
 * @property integer $id
 * @property integer $column
 * @property integer $banner_id
 * @property integer $template_id
 * @property string $position
 * @property integer $sort_order
 *
 * @property EmailTemplates $template
 * @property Banners $banner
 */
class EmailBlocks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'email_blocks';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['column', 'banner_id', 'template_id', 'sort_order'], 'integer'],
            [['banner_id', 'template_id'], 'required'],
            [['position'], 'string'],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmailTemplates::className(), 'targetAttribute' => ['template_id' => 'id']],
            [['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banners::className(), 'targetAttribute' => ['banner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribte labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'column' => Yii::t('app', 'Column'),
            'banner_id' => Yii::t('app', 'Banner ID'),
            'template_id' => Yii::t('app', 'Template ID'),
            'position' => Yii::t('app', 'Position'),
            'sort_order' => Yii::t('app', 'Sort Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(EmailTemplates::className(), ['id' => 'template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banners::className(), ['id' => 'banner_id']);
    }
}
