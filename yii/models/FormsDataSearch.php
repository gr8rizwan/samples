<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormsData;

/**
 * FormsDataSearch represents the model behind the search form about `app\models\FormsData`.
 */
class FormsDataSearch extends FormsData
{
    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['id', 'form_id'], 'integer'],
            [['source', 'browser', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return array of model scenario
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params of array
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormsData::find()->with('formDataFields', 'form');

//        echo $query->createCommand()->getRawSql(); die;

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'form_id' => $this->form_id

        ]);

        if (empty($this->created_at) || !isset($this->created_at)) {
            $query->andFilterWhere(['like', 'created_at', Yii::$app->dateTime->getDate()]);
        } else {
            $query->andFilterWhere(['like', 'created_at', $this->created_at]);
        }

        $query->orderBy(['created_at' => SORT_DESC]);

        return $dataProvider;
    }
}
