<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "forms_data".
 *
 * @property integer $id
 * @property integer $form_id
 * @property string $source
 * @property string $browser
 * @property string $created_at
 *
 * @property FormDataFields[] $formDataFields
 * @property Forms $form
 */
class FormsData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return 'forms_data';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['form_id'], 'required'],
            [['form_id'], 'integer'],
            [['created_at'], 'safe'],
            [['source', 'browser'], 'string', 'max' => 255],
            [['form_id'], 'exist', 'skipOnError' => true, 'targetClass' => Forms::className(), 'targetAttribute' => ['form_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'form_id' => Yii::t('app', 'Form ID'),
            'source' => Yii::t('app', 'Source'),
            'browser' => Yii::t('app', 'Browser'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormDataFields()
    {
        return $this->hasMany(FormDataFields::className(), ['form_data_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Forms::className(), ['id' => 'form_id']);
    }
}
