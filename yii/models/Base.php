<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "branch".
 *
 * @property integer $id
 * @property string $name
 * @property string $detailed_address
 * @property string $open_time
 * @property string $close_time
 * @property string $lng
 * @property string $lat
 * @property integer $sequence
 * @property integer $is_deleted
 * @property string $server_url
 *
 * @property Order[] $orders
 */
class Base extends BaseModel
{
    /**
     * @inheritdoc
     */

    public $file;

    public function UploadFile($attribute) {

        $this->file = UploadedFile::getInstance($this, 'file');
        if($this->file) {
            $file_path =  microtime(). $this->file->baseName . '.' . $this->file->extension;

            if ($this->file->saveAs(Yii::getAlias('@webroot').'/upload/' . $file_path)) {
                $this->{$attribute} = $file_path;
            }
        }

    }

}
