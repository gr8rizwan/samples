<?php

namespace app\models;

use backend\models\StaticPages;
use Yii;
use backend\base\models\BaseModel;

/**
 * This is the model class for table "announcements".
 *
 * @property integer $id
 * @property string $short_description
 * @property integer $page_id
 * @property string $start_date
 * @property string $end_date
 * @property string $start_time
 * @property string $end_time
 * @property string $background
 * @property string $orderOnline
 * @property integer $status
 */
class Announcements extends BaseModel
{
    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return 'announcements';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['short_description', 'start_date', 'end_date', 'start_time', 'end_time'], 'required'],
            [['short_description', 'orderOnline'], 'string'],
            [['page_id', 'status'], 'integer'],
            [['start_date', 'end_date', 'start_time', 'end_time'], 'safe'],
            [['background'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'short_description' => Yii::t('app', 'Short Description'),
            'page_id' => Yii::t('app', 'Page ID'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'start_time' => Yii::t('app', 'Start Time'),
            'end_time' => Yii::t('app', 'End Time'),
            'background' => Yii::t('app', 'Background'),
            'orderOnline' => Yii::t('app', 'Order Online'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCmsPage()
    {
        return $this->hasOne(StaticPages::className(), ['id' => 'page_id']);
    }
}
