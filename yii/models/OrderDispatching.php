<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

class OrderDispatching extends BaseModel
{
    /**
     * @inheritdoc
     */

    // Model table name
    public static $table = 'orderdispatching'; // this will be override based on conrtroller logic, SEE dispatching function in report controller

    public static function tableName()
    {
        return self::$table;
    }

    public static function primaryKey()
    {
        return ['id'];
    }

}
