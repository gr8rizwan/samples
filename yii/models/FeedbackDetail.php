<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "feedback_detail".
 *
 * @property integer $id
 * @property integer $feedback_id
 * @property integer $feedback_type_id
 *
 * @property CustomerFeedback $feedback
 * @property FeedbackType $feedbackType
 */
class FeedbackDetail extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feedback_id', 'feedback_type_id'], 'required'],
            [['feedback_id', 'feedback_type_id'], 'integer'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'feedback_id' => 'Feedback',
            'feedback_type_id' => 'Type',
            'description' => 'description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedback()
    {
        return $this->hasOne(CustomerFeedback::className(), ['id' => 'feedback_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackType()
    {
        return $this->hasOne(FeedbackType::className(), ['id' => 'feedback_type_id']);
    }
}
