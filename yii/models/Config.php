<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "config".
 *
 * @property integer $id
 * @property integer $toppings_free_count
 * @property integer $branch_id
 * @property string $extra_topping_cost
 * @property integer $max_toppings
 * @property integer $max_bread_type
 * @property integer $max_meat_type
 * @property integer $max_sauces_type
 * @property string $app_store_link
 * @property string $order_phone_number
 * @property integer $facebook_points
 * @property integer $twitter_points
 * @property string $facebook_link
 * @property string $instagram_link
 * @property string $twitter_link
 * @property string $convert_rewards_point
 * @property string $drivers_carry_change
 * @property string $open_time
 * @property string $close_time
 * @property string $menu_url
 * @property string $additem_url
 * @property string $om_swich_id
 * @property string $om_side_id
 * @property string $om_drink_id
 * @property string $om_size_id
 * @property string $om_portion_id
 * @property string $om_price_id
 * @property string $om_swich_plus_group
 * @property string $om_printer_group
 * @property string $faq_image
 * @property string $location_image
 * @property string $om_custom_swich_id
 * @property string $youtube_link
 * @property string $om_category_id
 * @property integer $sauce_free_count
 * @property string $extra_sauce_cost
 *
 * @property Branch $branch
 */
class Config extends BaseModel
{
    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['toppings_free_count', 'extra_topping_cost', 'max_toppings', 'max_bread_type', 'max_meat_type', 'max_sauces_type', 'orderLimit', 'agentBonus'], 'required'],
            [['toppings_free_count', 'branch_id', 'max_toppings', 'max_bread_type', 'max_meat_type', 'max_sauces_type', 'facebook_points', 'twitter_points', 'sauce_free_count'], 'integer'],
            [['extra_topping_cost', 'convert_rewards_point', 'extra_sauce_cost'], 'number'],
            [['open_time', 'close_time'], 'safe'],
            [['app_store_link', 'order_phone_number', 'facebook_link', 'instagram_link', 'twitter_link', 'drivers_carry_change', 'menu_url', 'additem_url', 'om_swich_id', 'om_side_id', 'om_drink_id', 'om_size_id', 'om_portion_id', 'om_price_id', 'om_swich_plus_group', 'om_printer_group', 'faq_image', 'location_image', 'om_custom_swich_id', 'youtube_link', 'om_category_id', 'mangerMessage'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     * @return array of attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id'                        => 'ID',
            'toppings_free_count'       => 'Toppings Free Count',
            'branch_id'                 => 'Branch ID',
            'extra_topping_cost'        => 'Extra Topping Cost',
            'max_toppings'              => 'Max Toppings',
            'max_bread_type'            => 'Max Bread Type',
            'max_meat_type'             => 'Max Meat Type',
            'max_sauces_type'           => 'Max Sauces Type',
            'app_store_link'            => 'App Store Link',
            'order_phone_number'        => 'Order Phone Number',
            'facebook_points'           => 'Facebook Points',
            'twitter_points'            => 'Twitter Points',
            'facebook_link'             => 'Facebook Link',
            'instagram_link'            => 'Instagram Link',
            'twitter_link'              => 'Twitter Link',
            'convert_rewards_point'     => 'Convert Rewards Point',
            'drivers_carry_change'      => 'Drivers Carry Change',
            'open_time'                 => 'Open Time',
            'close_time'                => 'Close Time',
            'menu_url'                  => 'Menu Url',
            'additem_url'               => 'Additem Url',
            'om_swich_id'               => 'Om Swich ID',
            'om_side_id'                => 'Om Side ID',
            'om_drink_id'               => 'Om Drink ID',
            'om_size_id'                => 'Om Size ID',
            'om_portion_id'             => 'Om Portion ID',
            'om_price_id'               => 'Om Price ID',
            'om_swich_plus_group'       => 'Om Swich Plus Group',
            'om_printer_group'          => 'Om Printer Group',
            'faq_image'                 => 'Faq Image',
            'location_image'            => 'Location Image',
            'om_custom_swich_id'        => 'Om Custom Swich ID',
            'youtube_link'              => 'Youtube Link',
            'om_category_id'            => 'Om Category ID',
            'sauce_free_count'          => 'Sauce Free Count',
            'extra_sauce_cost'          => 'Extra Sauce Cost',
            'mangerMessage'             => 'Manger Message',
            'agentBonus'                => 'Agent Bonus',
            'orderLimit'                => 'Minimum Order Limit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branch_id']);
    }
}
