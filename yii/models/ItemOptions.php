<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_options".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $ingredient_category
 * @property integer $status
 * @property integer $is_required
 * @property integer $sort_order
 * @property string $updated_at
 */
class ItemOptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'item_options';
    }

    /**
     * @inheritdoc
     * @return array rules
     */
    public function rules()
    {
        return [
            [['item_id'], 'required'],
            [['item_id', 'ingredient_category', 'status', 'is_required', 'sort_order'], 'integer'],
            [['updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return array attributeLabels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'ingredient_category' => Yii::t('app', 'Ingredient Category'),
            'status' => Yii::t('app', 'Status'),
            'is_required' => Yii::t('app', 'Is Required'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
