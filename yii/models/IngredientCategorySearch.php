<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IngredientCategory;

/**
 * IngredientCategorySearch represents the model behind the search form about `app\models\IngredientCategory`.
 */
class IngredientCategorySearch extends IngredientCategory
{
    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['id', 'sequence', 'is_deleted', 'LevelId'], 'integer'],
            [['name', 'om_type', 'om_domainid', 'om_minqty', 'om_maxqty', 'om_reducedzone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return object model scenarios
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params search parameters
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IngredientCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sequence' => $this->sequence,
            'LevelId' => $this->LevelId,
            'RuleOnLevel' => $this->RuleOnLevel,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'om_type', $this->om_type])
            ->andFilterWhere(['like', 'om_domainid', $this->om_domainid])
            ->andFilterWhere(['like', 'om_minqty', $this->om_minqty])
            ->andFilterWhere(['like', 'om_maxqty', $this->om_maxqty])
            ->andFilterWhere(['like', 'om_reducedzone', $this->om_reducedzone]);

        return $dataProvider;
    }
}
