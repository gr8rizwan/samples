<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "delivery_times".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property integer $sort_order
 */
class DeliveryTimes extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'delivery_times';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status', 'sort_order'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'sort_order' => 'Sort Order',
        ];
    }
}
