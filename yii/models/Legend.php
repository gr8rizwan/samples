<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "legend".
 *
 * @property integer $id
 * @property string $name
 * @property string $icon
 */
class Legend extends BaseModel
{
    public $ingredient_legend;

    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'legend';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['ingredient_legend'], 'safe'],
            [['name', 'icon'], 'string', 'max' => 255]
        ];
    }

    /**
     * Set ingredient legend values
     * @return void
     * */
    public function setIngredientLegend() {

        if (!$this->isNewRecord) {
            IngredientLegends::deleteAll('legend_id = :legend_id', [':legend_id' => $this->id]);
        }

        if (isset($this->ingredient_legend) && !empty($this->ingredient_legend)) {
            foreach ($this->ingredient_legend as $key => $value) {
                $ing_legend = new IngredientLegends();
                $ing_legend->ingredient_id = $value;
                $ing_legend->legend_id = $this->id;
                $ing_legend->save();
            }
        }
    }

    /**
     * @inheritdoc
     * @return array of attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'icon' => 'Icon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientLegends()
    {
        return $this->hasMany(IngredientLegends::className(), ['legend_id' => 'id']);
    }
}
