<?php

namespace backend\models;

use common\helpers\QueryHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class CustomerSearch
 * @package backend\models
 */
class CustomerSearch extends Customer
{

    public $area;
    public $from_date;
    public $to_date;
    public $time;
    public $dry_customers; // customers with no order

    /**
     * Customer Filters
     * @var
     */
    public $retentionFilter;

    public $groupId;

    public $stateFilter;

    public $retention;
    public $customer_status;
    public $customer_type;

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['id', 'invited_by', 'image', 'facebook_id', 'is_active', 'is_deleted', 'gender'], 'integer'],
            [['first_name', 'last_name', 'email', 'phone_number', 'password', 'role', 'activation_code', 'dob', 'om_id', 'customerkey', 'branch_id',
                'area', 'from_date', 'to_date', 'time', 'dry_customers', 'groupId'], 'safe'],
            [['created_at','type', 'password_reset_token', 'status', 'socialType', 'socialID'], 'safe'],
            [['retentionFilter', 'stateFilter', 'retention', 'customer_status', 'customer_type'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     * @return object of model scenraio
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params if dor search customer objects
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customer::find()->select('customer.*')->with('order', 'order.branch', 'lowestSumOrder', 'highSumOrder', 'firstOrder', 'lastOrder', 'loyalty');

        $query->join('LEFT JOIN', 'franchise-1.order o', 'o.customer_id = customer.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'invited_by' => $this->invited_by,
            'image' => $this->image,
            'facebook_id' => $this->facebook_id,
            'is_active' => $this->is_active,
            'is_deleted' => $this->is_deleted,
            'gender' => $this->gender,
            'dob' => $this->dob,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'activation_code', $this->activation_code])
            ->andFilterWhere(['like', 'om_id', $this->om_id])
            ->andFilterWhere(['like', 'customerkey', $this->customerkey]);

        /**
         * Status Filter
         */
        if ($this->retention) {
            if ($this->retention == 'new') {
                $query->addSelect(['count(o.id) totalOrders']);
                $query->andHaving('DATE(lastOrderDate) >= (DATE_SUB(CURDATE(), INTERVAL 45 day))');
                $query->andHaving('totalOrders = 1');
            } elseif ($this->retention == 'returning') {
                $query->addSelect(['count(o.id) totalOrders']);
                $query->andHaving('DATE(lastOrderDate) >= (DATE_SUB(CURDATE(), INTERVAL 45 day))');
                $query->andHaving('totalOrders > 1');
            } elseif ($this->retention == 'lead') {
                $subQuery = QueryHelper::getCustomerIds();

                $query->where(['not in', 'customer.id', $subQuery]);
            }
        }

        if ($this->customer_status) {
            if ($this->customer_status == 'active') {
                $query->andHaving('DATE(lastOrderDate) >= (DATE_SUB(CURDATE(), INTERVAL 45 day))');
            } elseif ($this->customer_status == 'in-active') {
                $query->andHaving('DATE(lastOrderDate) <= (DATE_SUB(CURDATE(), INTERVAL 45 day))');
            }
        }

        if ($this->customer_type) {
            if ($this->customer_type == 'registered') {
                $query->andFilterWhere(['=', 'checkout_type', 'customer']);
            }

            if ($this->customer_type == 'unregistered') {
                $query->andFilterWhere(['=', 'checkout_type', 'guest']);
            }
        }

        $query->addSelect(['max(o.time) lastOrderDate']);
        $query->groupBy('customer.id');

        if ($this->from_date) {
            $query->andHaving(['>=', 'DATE(lastOrderDate)', $this->from_date]);
        }

        if ($this->to_date) {
            $query->andHaving(['<=', 'DATE(lastOrderDate)', $this->to_date]);
        }



//        echo $query->createCommand()->rawSql;
//        die();

        $query->orderBy(['customer.id' => SORT_DESC]);


        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params is for get customer report based on search
     *
     * @return ActiveDataProvider
     */
    public function searchCustomersReport($params)
    {
        // by default show data for today
        if (!isset($this->from_date)) {
            $this->from_date = Yii::$app->dateTime->getDate();
        }

        if (!isset($this->to_date)) {
            $this->to_date = Yii::$app->dateTime->getDate();
        }

        $query = Customer::find();

        $query->addSelect([
            'customer.*',
            'total_orders' => Order::find()->select('count(*)')->where(
                'customer.id = order.customer_id'
            )
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($this->dry_customers === '1' || $this->branch_id) {
            $query->joinWith('order', true, 'LEFT JOIN');
        }

        if ($this->dry_customers === '0' && $this->area) {
            $query->joinWith('orderAddress', true, 'LEFT JOIN');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (($this->area || $this->branch_id) && $this->dry_customers === '0') {
            if (!empty($this->from_date)) {
                $query->andFilterWhere(['>=', 'DATE(order.time)', $this->from_date]);
            }

            if (!empty($this->to_date)) {
                $query->andFilterWhere(['<=', 'DATE(order.time)', $this->to_date]);
            }
        }

        if ($this->dry_customers === '0') {
            if ($this->branch_id) {
                $query->andFilterWhere([
                    'branch_id' => $this->branch_id,
                ]);
            }

            if ($this->area) {
                $query->andFilterWhere([
                    'area' => $this->area,
                ]);
            }
        }

        //Chreck if params has from_date parameter, if yes then allow search on from_date
        if (!empty($this->from_date)) {
            $query->andFilterWhere(['>=', 'DATE(created_at)', $this->from_date]);
        }

        //Chreck if params has to_date parameter, if yes then allow search on to_date
        if (!empty($this->to_date)) {
            $query->andFilterWhere(['<=', 'DATE(created_at)', $this->to_date]);
        }

        $query->groupBy('customer.id');

        if ($this->dry_customers === '1') {
            $query->having(['total_orders' => '0']);
        }

        $query->orderBy(['created_at' => SORT_DESC]);

        return $dataProvider;
    }
}
