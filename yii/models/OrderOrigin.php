<?php

namespace backend\models;

use app\models\DiscountCategory;
use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "order_origin".
 *
 * @property integer $id
 * @property string $name
 * @property integer $sort_order
 * @property integer $status
 * @property integer $delivery_id
 * @property integer $pickup_id
 */
class OrderOrigin extends BaseModel
{

    public $origin_count;   // count of the origin against customer

    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return 'order_origin';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort_order', 'status', 'pickup_id', 'delivery_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     * @return array of attributes label
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'delivery_id' => 'Delivery ID',
            'pickup_id' => 'Pickup ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        return $this->hasMany(DiscountCategory::className(), ['origin_id' => 'id']);
    }
}
