<?php

namespace backend\models;


use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "feedback_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property FeedbackDetail[] $feedbackDetails
 */
class FeedbackType extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackDetails()
    {
        return $this->hasMany(FeedbackDetail::className(), ['feedback_type_id' => 'id']);
    }
}
