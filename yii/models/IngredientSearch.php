<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Ingredient;

/**
 * IngredientSearch represents the model behind the search form about `app\models\Ingredient`.
 */
class IngredientSearch extends Ingredient
{
    /**
     * @inheritdoc
     * @return array rules array
     */
    public function rules()
    {
        return [
            [['id', 'ingredient_category_id', 'calories_count', 'max_quantity', 'sequence', 'is_deleted', 'protein', 'carbs', 'fat', 'pending', 'premium', 'menu_id'], 'integer'],
            [['name', 'image', 'description', 'om_id', 'legend'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     * @return object of model scenraio
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params searchable params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ingredient::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ingredient_category_id' => $this->ingredient_category_id,
            'price' => $this->price,
            'calories_count' => $this->calories_count,
            'max_quantity' => $this->max_quantity,
            'sequence' => $this->sequence,
            'is_deleted' => $this->is_deleted,
            'protein' => $this->protein,
            'carbs' => $this->carbs,
            'fat' => $this->fat,
            'pending' => $this->pending,
            'premium' => $this->premium,
            'menu_id' => $this->menu_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'om_id', $this->om_id])
            ->andFilterWhere(['like', 'legend', $this->legend]);

        return $dataProvider;
    }
}
