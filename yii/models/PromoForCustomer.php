<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "promo_for_customer".
 *
 * @property integer $id
 * @property integer $promo_code_id
 * @property integer $customer_id
 *
 * @property Customer $customer
 * @property PromoCodes $promoCode
 */
class PromoForCustomer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return 'promo_for_customer';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['promo_code_id', 'customer_id'], 'required'],
            [['promo_code_id', 'customer_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     * @return array of attributes
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promo_code_id' => 'Promo Code ID',
            'customer_id' => 'Customer ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCode()
    {
        return $this->hasOne(PromoCodes::className(), ['id' => 'promo_code_id']);
    }
}
