<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property string $city
 * @property integer $area
 * @property string $building
 * @property string $apartment
 * @property string $lng
 * @property string $lat
 * @property string $image
 * @property integer $customer_id
 * @property integer $country_id
 * @property string $phone_number
 * @property string $title
 * @property string $street
 * @property integer $special_instructions
 *
 * @property Customer $customer
 * @property Area $area0
 * @property Country $country
 * @property Order[] $orders
 */
class Address extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area', 'building', 'customer_id'], 'required'],
            [['area', 'customer_id', 'is_deleted'], 'integer'],
            [['building', 'apartment', 'phone_number', 'title', 'street', 'special_instructions'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area' => 'Area',
            'building' => 'Building',
            'apartment' => 'Apartment',
            'lng' => 'Lng',
            'lat' => 'Lat',
            'customer_id' => 'Customer ID',
            'phone_number' => 'Phone Number',
            'title' => 'Title',
            'street' => 'Street',
            'special_instructions' => 'Special Instructions',
            'is_deleted'    => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['address_id' => 'id']);
    }
}
