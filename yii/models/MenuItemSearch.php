<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MenuItem;

/**
 * MenuItemSearch represents the model behind the search form about `app\models\MenuItem`.
 */
class MenuItemSearch extends MenuItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'menu_id', 'category_type', 'calories_count', 'sequence', 'is_deleted', 'customer_id', 'pending', 'protein', 'carbs', 'fat', 'om_id'], 'integer'],
            [['name', 'image', 'description', 'descriptionApp', 'ingredients_map', 'new_image'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MenuItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'menu_id' => $this->menu_id,
            'category_type' => $this->category_type,
            'price' => $this->price,
            'calories_count' => $this->calories_count,
            'sequence' => $this->sequence,
            'is_deleted' => $this->is_deleted,
            'customer_id' => $this->customer_id,
            'pending' => $this->pending,
            'protein' => $this->protein,
            'carbs' => $this->carbs,
            'fat' => $this->fat,
            'om_id' => $this->om_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'descriptionApp', $this->descriptionApp])
            ->andFilterWhere(['like', 'ingredients_map', $this->ingredients_map])
            ->andFilterWhere(['like', 'new_image', $this->new_image]);

        return $dataProvider;
    }
}
