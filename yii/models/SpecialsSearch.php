<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Specials;

/**
 * SpecialsSearch represents the model behind the search form about `app\models\Specials`.
 */
class SpecialsSearch extends Specials
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'menu_item_id'], 'integer'],
            [['unit_sale_price', 'onAmount'], 'number'],
            [['special_title', 'start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Specials::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'menu_item_id' => $this->menu_item_id,
            'unit_sale_price' => $this->unit_sale_price,
            'onAmount' => $this->onAmount,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
        ]);

        $query->andFilterWhere(['like', 'special_title', $this->special_title]);

        return $dataProvider;
    }
}
