<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;
use backend\models\Order;

/**
 * This is the model class for table "order_discount_category".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $discount_cat_id
 *
 * @property Order $order
 * @property DiscountCategory $discountCat
 */
class OrderDiscountCategory extends BaseModel
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'order_discount_category';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'discount_cat_id'], 'required'],
            [['order_id', 'discount_cat_id'], 'integer']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'discount_cat_id' => 'Discount Cat ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountCat()
    {
        return $this->hasOne(DiscountCategory::className(), ['id' => 'discount_cat_id']);
    }

    /**
     * Get Total Sum Value
     * @param object $object    of findings
     * @param array  $attribute that need to calculate value
     * @return integer of value
     * */
    public function getOrderTotalSum($object, $attribute) {

        $value = 0;

        foreach ($object as $discount) {
            $value += isset($discount->order->{$attribute}) ? $discount->order->{$attribute} : 0;
        }

        return $value;
    }
}
