<?php

namespace backend\models;

use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property string $title
 * @property string $seo_url
 * @property string $short_description
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $seo_keywords
 * @property integer $sort_order
 * @property string $banner_image
 * @property integer $status
 * @property mixed $tags
 * @property mixed $created_at
 * @property mixed $created_by
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     * @return object of TimeStampBehabior class
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['title', 'seo_url', 'short_description', 'description', 'meta_title'], 'required'],
            [['short_description', 'description'], 'string'],
            [['sort_order', 'status', 'banner_id'], 'integer'],
            [['created_at'], 'safe'],
            [['banner_id'], 'default', 'value' => 0],
            [['title', 'seo_url', 'banner_image'], 'string', 'max' => 255],
            [['meta_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 150],
            [['seo_keywords', 'created_by', 'tags'], 'string', 'max' => 100],
            [['title'], 'unique'],
            [['seo_url'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'seo_url' => Yii::t('app', 'Seo Url'),
            'short_description' => Yii::t('app', 'Short Description'),
            'description' => Yii::t('app', 'Description'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'seo_keywords' => Yii::t('app', 'Seo Keywords'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'banner_image' => Yii::t('app', 'Banner Image'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'tags' => Yii::t('app', 'Category Tag'),
            'created_at' => Yii::t('app', 'Crated Date'),
            'banner_id' => Yii::t('app', 'Banners'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banners::className(), ['id' => 'banner_id']);
    }
}
