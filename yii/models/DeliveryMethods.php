<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "delivery_methods".
 *
 * @property integer $id
 * @property string $method_title
 * @property string $free_on_amount
 * @property string $amount_type
 * @property string $amount_value
 * @property integer $is_free
 * @property string $free_start_date
 * @property string $free_end_date
 * @property string $free_start_time
 * @property string $free_end_time
 * @property integer $status
 * @property string $updated_at
 */
class DeliveryMethods extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'delivery_methods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['method_title', 'amount_value', 'status', 'amount_type', 'is_free'], 'required'],
            [['free_on_amount', 'amount_value'], 'number'],
            [['amount_type'], 'string'],
            [['is_free', 'status'], 'integer'],
            [['free_start_date', 'free_end_date', 'free_start_time', 'free_end_time', 'updated_at'], 'safe'],
            [['method_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'method_title' => 'Method Title',
            'free_on_amount' => 'Free On Amount',
            'amount_type' => 'Amount Type',
            'amount_value' => 'Amount Value',
            'is_free' => 'Is Free',
            'free_start_date' => 'Free Start Date',
            'free_end_date' => 'Free End Date',
            'free_start_time' => 'Free Start Time',
            'free_end_time' => 'Free End Time',
            'status' => 'Status',
            'updated_at' => 'Updated At',
        ];
    }
}
