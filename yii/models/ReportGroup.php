<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_group".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $name
 *
 * @property ReportTypes $type
 * @property ReportGroupProperties[] $reportGroupProperties
 */
class ReportGroup extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $properties = [];

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'report_group';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['type_id', 'name'], 'required'],
            [['type_id'], 'integer'],
            [['properties'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReportTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ReportTypes::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportGroupProperties()
    {
        return $this->hasMany(ReportGroupProperties::className(), ['group_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @param boolean $insert            true or false
     * @param array   $changedAttributes list of attributes
     * @return null
     */
    public function afterSave($insert, $changedAttributes) {

        ReportGroupProperties::deleteAll(['group_id' => $this->id]);

        foreach ($this->properties as $property) {
            $bannerImage = new ReportGroupProperties();

            $bannerImage->attributes = $property;
            $bannerImage->group_id = $this->id;
            $bannerImage->save();
        }
    }
}
