<?php

namespace backend\models;

use backend\models\Branch;
use backend\models\Order;
use backend\base\models\BaseModel;
use Yii;

use backend\models\Customer;

/**
 * This is the model class for table "customer_feedback".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $branch_id
 * @property integer $order_type
 * @property integer $rating
 * @property string $feedback
 * @property integer $status
 * @property string $created_at
 */
class CustomerFeedback extends BaseModel
{
    public $feedback_count; // total number of customer feedbacks
    public $timing; // timing for filters, i.e; daily, weekly or yearly.
    public $time; // customer feedback created at
    public $address_id;
    public $branch;
    public $total_sum;
    public $feedback_created_at;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'customer_feedback';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['feedback'], 'required'],
            [['customer_id', 'order_id', 'created_by', 'branch_id','rating', 'status', 'closed_by', 'parent_id', 'order_branch'], 'integer'],
            [['feedback', 'resolution', 'source'], 'string'],
            [['created_at', 'closed_at'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order',
            'branch_id' => 'Branch',
            'order_branch' => 'Branch',
            'customer_id' => 'Customer detail',
            'rating' => 'Rating',
            'feedback' => 'Feedback',
            'status' => 'Status',
            'source' => 'Source',
            'resolution' => 'Resolution',
            'closed_at' => 'Closed at',
            'closed_by' => 'Closed by',
            'parent_id' => 'Parent',
            'created_at' => 'Created at',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'order_branch']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackAssignment()
    {
        return $this->hasMany(FeedbackResolutionAssign::className(), ['feedback_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackDetail()
    {
        return $this->hasMany(FeedbackDetail::className(), ['feedback_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackParent()
    {
        return $this->hasOne(CustomerFeedback::className(), ['parent_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackResolution()
    {
        return $this->hasOne(FeedbackResolution::className(), ['feedback_id' => 'id']);
    }
}
