<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * StickyNotesSearch represents the model behind the search form about `app\models\StickyNotes`.
 */
class StickyNotesSearch extends StickyNotes
{
    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'status', 'admin_id'], 'integer'],
            [['note', 'type', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return object model
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params array of search attaributes
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StickyNotes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'admin_id' => $this->admin_id,
            'status' => $this->status,
            'type' => $this->type,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
