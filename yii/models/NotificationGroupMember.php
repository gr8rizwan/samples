<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "notification_group_member".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property integer $isTo
 *
 * @property User $user
 */
class NotificationGroupMember extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_group_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status', 'isTo'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'isTo' => 'Is To',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
