<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "promo_to_customer".
 *
 * @property integer $id
 * @property integer $promo_code_id
 * @property integer $customer_id
 * @property integer $order_id
 *
 * @property Order $order
 * @property PromoCodes $promoCode
 * @property Customer $customer
 */
class PromoToCustomer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'promo_to_customer';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['promo_code_id', 'customer_id', 'order_id'], 'required'],
            [['promo_code_id', 'customer_id', 'order_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promo_code_id' => 'Promo Code ID',
            'customer_id' => 'Customer ID',
            'order_id' => 'Order ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCode()
    {
        return $this->hasOne(PromoCodes::className(), ['id' => 'promo_code_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}
