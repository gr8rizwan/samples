<?php

namespace app\models;

use backend\base\models\BaseModel;
use backend\models\Customer;
use backend\models\Order;
use Yii;

/**
 * This is the model class for table "sticky_notes".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $status
 * @property string $note
 * @property string $type
 * @property integer $admin_id
 * @property integer $order_id
 * @property string $created_at
 *
 * @property Customer $customer
 * @property Order $order
 */
class StickyNotes extends BaseModel
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'sticky_notes';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['customer_id', 'status', 'note', 'admin_id'], 'required'],
            [['customer_id', 'status', 'admin_id', 'order_id'], 'integer'],
            [['note', 'type'], 'string'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     * @return array of attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer',
            'status' => 'Status',
            'note' => 'Note',
            'type' => 'Type',
            'created_at' => 'Created At',
            'admin_id' => 'created by',
            'order_id' => 'Order ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
