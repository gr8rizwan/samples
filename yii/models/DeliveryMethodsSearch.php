<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DeliveryMethods;

/**
 * DeliveryMethodsSearch represents the model behind the search form about `backend\models\DeliveryMethods`.
 */
class DeliveryMethodsSearch extends DeliveryMethods
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_free', 'status'], 'integer'],
            [['method_title', 'amount_type', 'free_start_date', 'free_end_date', 'free_start_time', 'free_end_time', 'updated_at'], 'safe'],
            [['free_on_amount', 'amount_value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DeliveryMethods::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'free_on_amount' => $this->free_on_amount,
            'amount_value' => $this->amount_value,
            'is_free' => $this->is_free,
            'free_start_date' => $this->free_start_date,
            'free_end_date' => $this->free_end_date,
            'free_start_time' => $this->free_start_time,
            'free_end_time' => $this->free_end_time,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'method_title', $this->method_title])
            ->andFilterWhere(['like', 'amount_type', $this->amount_type]);

        return $dataProvider;
    }
}
