<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "order_item_customization".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $quantity
 * @property integer $order_item_id
 * @property integer $ingredient_id
 *
 * @property Ingredient $ingredient
 * @property OrderMenuItem $orderItem
 */
class OrderItemCustomization extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_item_customization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'order_item_id', 'ingredient_id'], 'required'],
            [['type', 'quantity', 'order_item_id', 'ingredient_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'quantity' => 'Quantity',
            'order_item_id' => 'Order Item ID',
            'ingredient_id' => 'Ingredient ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItem()
    {
        return $this->hasOne(OrderMenuItem::className(), ['id' => 'order_item_id']);
    }
}
