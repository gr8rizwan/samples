<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "ingredient_legends".
 *
 * @property integer $id object ID
 * @property integer $ingredient_id
 * @property integer $legend_id
 * @property string $description
 */
class IngredientLegends extends BaseModel
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'ingredient_legends';
    }

    /**
     * @inheritdoc
     * @return array rules array
     */
    public function rules()
    {
        return [
            [['ingredient_id', 'legend_id'], 'integer'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ingredient_id' => 'Ingredient ID',
            'legend_id' => 'Legend ID',
            'description' => 'Description',
        ];
    }
}
