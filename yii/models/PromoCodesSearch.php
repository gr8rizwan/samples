<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PromoCodes;
use yii\db\Expression;

/**
 * PromoCodesSearch represents the model behind the search form about `backend\models\PromoCodes`.
 */
class PromoCodesSearch extends PromoCodes
{
    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['id', 'status', 'branch_id'], 'integer'],
            [['name', 'code', 'type', 'redeem_type', 'created_at', 'valid_on_day'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return object of scenrio
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params for searching results
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PromoCodes::find()->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!isset($this->status)) {
            $this->status = '1';
        }

        if ($this->status == '1') {
            $query->andWhere(['<=', 'from_date', new Expression('DATE(NOW())')]);
            $query->andWhere(['>=', 'to_date', new Expression('DATE(NOW())')]);
        } else if ($this->status == '0') {
            $query->andWhere(['<=', 'to_date', new Expression('DATE(NOW())')]);
        } else if ($this->status == '-1') {
            $query->andWhere(['>=', 'from_date', new Expression('DATE(NOW())')]);
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'branch_id' => $this->branch_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'min_order', $this->min_order])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'valid_on_day', $this->valid_on_day])
            ->andFilterWhere(['like', 'redeem_type', $this->redeem_type]);

//        echo $query->createCommand()->getRawSql();die;

        return $dataProvider;
    }
}
