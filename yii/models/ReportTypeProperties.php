<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_type_properties".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $attribute
 * @property string $sort_order
 *
 * @property ReportGroupProperties[] $reportGroupProperties
 * @property ReportTypes $type
 */
class ReportTypeProperties extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_type_properties';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'attribute'], 'required'],
            [['type_id'], 'integer'],
            [['sort_order'], 'integer'],
            [['attribute'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReportTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'attribute' => Yii::t('app', 'Attribute'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportGroupProperties()
    {
        return $this->hasMany(ReportGroupProperties::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ReportTypes::className(), ['id' => 'type_id']);
    }
}
