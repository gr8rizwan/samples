<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "order_menu_item".
 *
 * @property integer $id
 * @property integer $quantity
 * @property string $single_price
 * @property integer $is_customized
 * @property integer $order_id
 * @property integer $menu_item_id
 * @property resource $ingredients_map
 *
 * @property OrderItemCustomization[] $orderItemCustomizations
 * @property Order $order
 * @property MenuItem $menuItem
 */
class OrderMenuItem extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_menu_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity', 'is_customized', 'order_id', 'menu_item_id', 'isFree', 'has_options'], 'integer'],
            [['single_price', 'order_id'], 'required'],
            [['single_price'], 'number'],
            [['ingredients_map', 'freeReason', 'comments'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quantity' => 'Quantity',
            'single_price' => 'Single Price',
            'is_customized' => 'Is Customized',
            'order_id' => 'Order ID',
            'menu_item_id' => 'Menu Item ID',
            'ingredients_map' => 'Ingredients Map',
            'freeReason'    => 'Free Reason',
            'isFree'    => 'Is Free',
            'comments'    => 'Comments',
            'has_options'    => 'Has Option',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItemCustomizations()
    {
        return $this->hasMany(OrderItemCustomization::className(), ['order_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        return $this->hasOne(MenuItem::className(), ['id' => 'menu_item_id']);
    }
}
