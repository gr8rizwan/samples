<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property string $name
 * @property integer $branch_id
 *
 * @property Address[] $addresses
 * @property Branch $branch
 */
class Area extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'branch_id'], 'required'],
            [['branch_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'branch_id' => 'Branch ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['area' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branch_id']);
    }

    /*
     * get areas under one branch
     * @param integer $branch_id
     * @return list of areas
     */
    public function getBranchAreas($branch_id)
    {
        $query = self::find()->select(['id', 'name']);

        if(!empty($branch_id)){
            $query->where(['branch_id' => $branch_id]);
        }

        return $query->orderBy(['name' => SORT_ASC])->all();
    }

}
