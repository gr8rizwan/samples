<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "special_comments".
 *
 * @property integer $id
 * @property string $comment
 * @property integer $status
 * @property integer $is_deleted
 */
class SpecialComments extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'special_comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment', 'status'], 'required'],
            [['status', 'is_deleted'], 'integer'],
            [['type'], 'safe'],
            [['comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comment' => 'Comment',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'type' => 'Type',
        ];
    }
}
