<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "loyalty".
 *
 * @property integer $id
 * @property integer $points
 * @property string $action
 * @property string $time
 * @property integer $is_deleted
 * @property integer $customer_id
 * @property integer $order_id
 * @property string $sharing_type
 * @property integer $sandwich_id
 *
 * @property Customer $customer
 * @property MenuItem $sandwich
 * @property Order $order
 */
class Loyalty extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loyalty';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['points', 'action', 'customer_id'], 'required'],
            [['points', 'is_deleted', 'customer_id', 'order_id', 'sandwich_id'], 'integer'],
            [['time'], 'safe'],
            [['action', 'sharing_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'points' => 'Points',
            'action' => 'Action',
            'time' => 'Time',
            'is_deleted' => 'Is Deleted',
            'customer_id' => 'Customer ID',
            'order_id' => 'Order ID',
            'sharing_type' => 'Sharing Type',
            'sandwich_id' => 'Sandwich ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSandwich()
    {
        return $this->hasOne(MenuItem::className(), ['id' => 'sandwich_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
