<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;
use backend\models\Address;
use backend\models\Branch;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $status
 * @property string $payment_type
 * @property integer $is_favorite
 * @property string $total_sum
 * @property string $price
 * @property string $discount
 * @property string $time
 * @property string $delivery_time
 * @property integer $customer_id
 * @property integer $address_id
 * @property integer $branch_id
 * @property string $comments
 * @property string $om_id
 * @property integer $twox_id
 * @property string $pickup_time
 * @property string $checkout_type
 * @property string $payment_gateway
 * @property string $payment_reference_id
 *
 * @property Address $address
 * @property Branch $branch
 * @property Customer $customer
 */
class Order extends BaseModel
{
    /**
     * @inheritdoc
     *
     */
    public $count;

    // area analysis report
    public $area_name;          // name of the area
    public $revenue;            // revenue for an area
    public $total_orders;       // total number of orders in an area
    public $average_ticket;     // average ticket for an area
    public $hourly_labels;      // Display hourly timing on X-Axis
    public $order_source;             // Display source of order like in hourly sales report
    public $timing;             // Timing Axis in Charts
    public $totalOrders;        // Agregate sum of orders
    public $totalRevenue;       // Agregate sum of revenue
    public $order_time_category;       // order_time_category
    public $completed_time;      // order completed time
    public $time_category;      // order time category
    public $time_filter;      // order time category
    public $difference;      // order dispatched_at - created_at
    public $dispatch_time;      // order dispatched_at - created_at
    public $prev_order_count;      // count of prev order, given date and customer_id
    public $origin_name;    // third party source
    public $origin_count;   // total number of origins per customer
    public $branch_count;   // total number of orders per branch

    //Payment Type
    const COD ='Cash on delivery';
    const CCD ='Credit card payment';
    const RP ='Reward points';
    const PUS ='Pickup From Store';
    const FOC ='Free of charge';

    //Order Status
    const OPEN = 'open';
    const CLOSED = 'closed';
    const CANCELED = 'canceled';
    const SAVED = 'saved';
    const INVALID = 'invalid';

    // Model table name
    public static $table = 'order'; // this will be override based on conrtroller logic, SEE dispatching function in report controller

    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return self::$table;
    }

    /**
     * @inheritdoc
     * @return integer primary key
     */
    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['status', 'checkout_type', 'pickup_time', 'agent_id'], 'required'],

            [['created_at', 'updated_at', 'assigned_at', 'dispatched_at', 'completed_at'], 'integer'],

            [['is_favorite', 'customer_id', 'address_id', 'branch_id', 'order_origin', 'twox_id'], 'integer'],
            [['total_sum', 'price', 'discount', 'delivery_charges'], 'number'],
            [['time', 'delivery_time', 'current_status', 'timing'], 'safe'],

            [['comments', 'agentMessage', 'current_status'], 'string'],
            [['status', 'payment_type', 'om_id'], 'string', 'max' => 255],
            [['customer_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     * @return array of attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'current_status' => 'Status',
            'payment_type' => 'Payment Type',
            'is_favorite' => 'Is Favorite',
            'total_sum' => 'Total',
            'price' => 'Price',
            'discount' => 'Discount',
            'time' => 'Time',
            'created_at' => 'Created',
            'updated_at' => 'Updated',
            'dispatched_at' => 'Dispatched',
            'assigned_at' => 'Assigned',
            'completed_at' => 'Completed',
            'delivery_time' => 'Delivery Time',
            'customer_id' => 'Customer',
            'address_id' => 'Address',
            'branch_id' => 'Branch',
            'comments' => 'Comments',
            'om_id' => 'Om ID',
            'twox_id' => 'Twox12 ID',
            'agentMessage'  => 'Agent Message',
            'order_origin'  => 'Origin',
            'delivery_charges'  => 'Delivery Charges',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*public static function find()
    {
        return parent::find()->where(['!=', self::tableName() . '.status', 'cancelled']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoyalties()
    {
        return $this->hasMany(Loyalty::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNonecustomerOrders()
    {
        return $this->hasMany(NonecustomerOrder::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area'])->via('address');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(User::className(), ['id' => 'agent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDiscountCategories()
    {
        return $this->hasMany(OrderDiscountCategory::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderMenuItems()
    {
        return $this->hasMany(OrderMenuItem::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOrigin()
    {
        return $this->hasOne(OrderOrigin::className(), ['id' => 'order_origin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerFeedback()
    {
        return $this->hasOne(CustomerFeedback::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderAssigned()
    {
        return $this->hasOne(OrderHistory::className(), ['order_id' => 'id'])->where(['description' => 'Assigned to driver']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderTimeCategory()
    {
        return $this->hasOne(OrderDispatching::className(), ['id' => 'id']);
    }
}
