<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_cardtoken".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $card_number
 * @property string $token_name
 * @property integer $card_bin
 * @property integer $expiry_date
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $last_used
 * @property integer $last_used_order_id
 *
 * @property Order $customer
 * @property Customer $customer0
 */
class CustomerCardtoken extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'customer_cardtoken';
    }

    /**
     * @inheritdoc
     * @return array Rules
     */
    public function rules()
    {
        return [
            [['customer_id', 'card_number', 'token_name', 'card_bin', 'expiry_date'], 'required'],
            [['customer_id', 'card_bin', 'expiry_date', 'status', 'created_at', 'updated_at', 'last_used', 'last_used_order_id'], 'integer'],
            [['card_number', 'token_name'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     * @return array attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'card_number' => 'Card Number',
            'token_name' => 'Token Name',
            'card_bin' => 'Card Bin',
            'expiry_date' => 'Expiry Date',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'last_used' => 'Last Used',
            'last_used_order_id' => 'Last Used Order ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Order::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer0()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}
