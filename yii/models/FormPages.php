<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $type
 * @property integer $status
 *
 * @property Forms[] $forms
 */
class FormPages extends \yii\db\ActiveRecord
{
    public $formsArray = [];
    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return 'form_pages';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['title', 'type', 'status'], 'required'],
            [['status'], 'integer'],
            [['formsArray'], 'safe'],
            [['title', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForms()
    {
        return $this->hasMany(Forms::className(), ['form_page_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return boolean true if successfully stored heading
     * @param boolean $insert            whether this method called while inserting a record. If false, it means the method is called while updating a record.
     * If false, it means the method is called while updating a record.
     * @param array   $changedAttributes The old values of attributes that had changed and were saved.
     */
    public function afterSave($insert, $changedAttributes) {

        foreach ($this->formsArray as $form) {
            if ((int)$form['id'] > 0) {
                $formObject = Forms::findOne($form['id']);
            } else {
                $formObject = new Forms();
            }

            $formObject->attributes = $form;
            $formObject->form_page_id = $this->id;

            if (!$formObject->save()) {
                return false;
            }
        }
    }
}
