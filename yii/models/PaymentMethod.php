<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "payment_method".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property integer $status
 * @property string $scope
 */
class PaymentMethod extends BaseModel
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'payment_method';
    }

    /**
     * @inheritdoc
     * @return mixed rules
     */
    public function rules()
    {
        return [
            [['name', 'code', 'status'], 'required'],
            [['name', 'code', 'scope'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     * @return mixed attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'scope' => 'Scope',
            'status' => 'Status',
        ];
    }
}
