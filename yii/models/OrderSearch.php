<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\helpers\WidgetHelper;
use yii\helpers\ArrayHelper;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order
{
    public $isToday = false;
    public $customer; // this is for custom filters
    public $area; // this is for custom filters
    public $from_date; // this is for custom filters
    public $to_date; // this is for custom filters
    public $time_took; // this is for custom filters
    public $type; // this is for custom filters
    public $time_filter; // time filter like daily, weekly, monthly or yearly
    public $time_category;      // display time categories in area analysis as filter
    public $order_origin; // third party platforms
    public $filter_promos; // promo codes
    public $code_status; // status of promo code
    public $code; // promo code
    public $promo_code; // promo code

    /**
     * Group by attributes
     * @var
     */
    public $groupByArea;
    public $groupByOrigin;
    public $groupBySource;
    public $groupByTimeCategory;

    /**
     * @inheritdoc
     * @return array of fields
     */
    public function rules()
    {
        return [
            [['id', 'is_favorite', 'discount', 'customer_id', 'address_id', 'agent_id', 'branch_id', 'order_origin', 'code_status'], 'integer'],
            [['status', 'payment_type', 'time', 'delivery_time', 'comments', 'om_id', 'delivery_type', 'source', 'order_origin', 'agentMessage', 'current_status', 'customer', 'area', 'from_date', 'to_date', 'time_category', 'time_filter', 'time_took' , 'code_status', 'promo_code'], 'safe'],
            [['groupByArea', 'groupByOrigin', 'groupByTimeCategory', 'groupBySource'], 'safe'],
            [['total_sum', 'price'], 'number']
        ];
    }

    /**
     * @inheritdoc
     * @return Model::scenarios
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params search fields
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Order::find();
        $query->addSelect([
            self::$table . '.*',
            'total_orders' => Order::find()->select('count(*)')->from(self::$table . ' customer_order')->where(
                'customer_order.customer_id = ' . self::$table . '.customer_id'
            )
        ]);
        $this->load($params);


        if ($this->area) {
            $query->joinWith(['address'], true, ' INNER JOIN');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_favorite' => $this->is_favorite,
            'total_sum' => $this->total_sum,
            'price' => $this->price,
            'discount' => $this->discount,
            'time' => $this->time,
            'delivery_time' => $this->delivery_time,
            'customer_id' => $this->customer_id,
            'address_id' => $this->address_id,
            'agent_id' => $this->agent_id,
            'order.branch_id' => $this->branch_id,
        ]);

        // if in params there is customer name then search with name wild card...
        if (isset($this->customer) && !empty($this->customer)) {
            // split customer name
            $customer_name = explode(' ', trim($this->customer));
            $customer_name = implode('|', $customer_name);
            $customers = Customer::find()->select(['id'])->where(['REGEXP', 'first_name', $customer_name])
                ->orFilterWhere(['REGEXP', 'last_name', $customer_name])
                ->orFilterWhere(['REGEXP', 'phone_number', $customer_name])
                ->all();

            $customer_ids = [];
            foreach ($customers as $customer) {
                $customer_ids[] = $customer->id;
            }

            $query->andFilterWhere(['IN', 'order.customer_id', $customer_ids]);
        }

//        echo $query->createCommand()->getRawSql(); die;


        //Chreck if params has from_date parameter, if yes then allow search on from_date
        if (!empty($this->from_date)) {
            $query->andFilterWhere(['>=', 'DATE(time)', $this->from_date]);
            $this->isToday = false;
        }

        //Chreck if params has to_date parameter, if yes then allow search on to_date
        if (!empty($this->to_date)) {
            $query->andFilterWhere(['<=', 'DATE(time)', $this->to_date]);
            $this->isToday = false;
        }

        //this is used when we need today results
        if ($this->isToday) {
            $query->andFilterWhere(['DATE(time)' => Yii::$app->dateTime->getDate()]);
        }

        // fetch all records based on area
        if ($this->area) {
            $query->andFilterWhere(['=', 'address.area', $this->area]);
        }

        // Check if filter has time_category then find based on time categpry
        if ($this->time_category) {
            $query = Yii::$app->setting->getTimeCategory($this->time_category, $query);
        }


        if ($this->promo_code) {
            $query->join('INNER JOIN', 'promo_to_customer ptc', 'order.id = ptc.order_id');
            $query->join('LEFT JOIN', 'promo_codes pc', 'ptc.promo_code_id = pc.id');
            $query->andFilterWhere(['=', 'code', $this->promo_code]);
        }

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'payment_type', $this->payment_type])
            ->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['OR', ['=', 'twox_id', $this->om_id], ['=', 'om_id', $this->om_id]])
            ->andFilterWhere(['like', 'current_status', $this->current_status])
            ->andFilterWhere(['like', 'delivery_type', $this->delivery_type])
            ->andFilterWhere(['like', 'order.source', $this->source])
            ->andFilterWhere(['like', 'agentMessage', $this->agentMessage]);


        if (!isset($params['sort'])) {
            $query->orderBy(['time' => SORT_DESC]);
        }

//        echo $query->createCommand()->getRawSql(); die;

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params search fields
     * @return ActiveDataProvider
     */
    public function lateOrderReport($params)
    {

        $query = Order::find();
        $query->addSelect([
            self::$table . '.*', '(dispatched_at - created_at) as dispatch_time',
            'total_orders' => Order::find()->select('count(*)')->from(self::$table . ' customer_order')->where(
                'customer_order.customer_id = ' . self::$table . '.customer_id'
            )
        ]);
        $this->load($params);

        $this->timing = $params['timing'];
        $this->time_filter = $params['time_filter'];

        if ($this->area) {
            $query->joinWith(['address'], true, ' INNER JOIN');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_favorite' => $this->is_favorite,
            'total_sum' => $this->total_sum,
            'price' => $this->price,
            'discount' => $this->discount,
            'time' => $this->time,
            'delivery_time' => $this->delivery_time,
            'customer_id' => $this->customer_id,
            'address_id' => $this->address_id,
            'agent_id' => $this->agent_id,
            'branch_id' => $this->branch_id,
        ]);

        // check the time filter
        if ($this->time_filter == 'daily') {
            $query->andWhere(['DATE(time)' => $this->timing]);
        } else if ($this->time_filter == 'monthly') {
            $query->andWhere('MONTHNAME(time)=MONTHNAME("' . $this->timing . '")');
        } else if ($this->time_filter == 'yearly') {
            $query->andWhere('YEAR(time)=YEAR(' . $this->timing . ')');
        }

        // fetch all records based on area
        if ($this->area) {
            $query->andFilterWhere(['=', 'address.area', $this->area]);
        }

        // Check if filter has time_category then find based on time categpry
        if ($this->time_category) {
            $query = Yii::$app->setting->getTimeCategory($this->time_category, $query);
        }

        $query->andFilterWhere(['<>', 'dispatched_at', 'NULL']);
        $query->andFilterWhere(['<>', 'created_at', 'NULL']);
        $query->andFilterWhere(['>', '(dispatched_at - created_at)', '1500']);


        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'payment_type', $this->payment_type])
            ->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['OR', ['=', 'twox_id', $this->om_id], ['=', 'om_id', $this->om_id]])
            ->andFilterWhere(['like', 'current_status', $this->current_status])
            ->andFilterWhere(['like', 'delivery_type', $this->delivery_type])
            ->andFilterWhere(['like', 'order.source', $this->source])
            ->andFilterWhere(['like', 'agentMessage', $this->agentMessage]);

        // Ordermate ID should be > 0
//        $query->andFilterWhere(['OR',['=', 'twox_id', self::$table.'.om_id'],['=', 'om_id', self::$table.'.om_id']]);

        if (!isset($params['sort'])) {
            $query->orderBy(['time' => SORT_DESC]);
        }

//        echo $query->createCommand()->getRawSql(); die;

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params branch search form fields
     * @return ActiveDataProvider
     */
    public function branchSearch($params)
    {
        $query = Order::find();

        $this->load($params);

        if ($this->customer) {
            $query->joinWith(['customer'], true, ' INNER JOIN');
        }

        if ($this->area) {
            $query->joinWith(['address'], true, ' INNER JOIN');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_favorite' => $this->is_favorite,
            'total_sum' => $this->total_sum,
            'price' => $this->price,
            'discount' => $this->discount,
            'time' => $this->time,
            'delivery_time' => $this->delivery_time,
            'customer_id' => $this->customer_id,
            'address_id' => $this->address_id,
            'agent_id' => $this->agent_id,
            'branch_id' => $this->branch_id,
        ]);


        // if in params there is customer name then search with nama wild card...
        if (isset($this->customer) && !empty($this->customer)) {
            // split customer name
            $customer_name = explode(' ', trim($this->customer));
            $customer_name = implode('|', $customer_name);

            $query->andFilterWhere(['REGEXP', 'customer.first_name', $customer_name])
                ->orFilterWhere(['REGEXP', 'customer.last_name', $customer_name])
                ->orFilterWhere(['REGEXP', 'customer.phone_number', $customer_name]);
        }

        //this is used when we need today results
        if ($this->isToday) {
            $query->andFilterWhere(['DATE(time)' => Yii::$app->dateTime->getDate()]);
        }

        //Chreck if params has from_date parameter, if yes then allow search on from_date
        if (!empty($this->from_date)) {
            $query->andFilterWhere(['>=', 'DATE(time)', $this->from_date]);
        }

        //Chreck if params has to_date parameter, if yes then allow search on to_date
        if (!empty($this->to_date)) {
            $query->andFilterWhere(['<=', 'DATE(time)', $this->to_date]);
        }

        // fetch all records based on area
        if ($this->area) {
            $query->andFilterWhere(['=', 'address.area', $this->area]);
        }

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'payment_type', $this->payment_type])
            ->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'om_id', $this->om_id])
            ->andFilterWhere(['like', 'current_status', $this->current_status])
            ->andFilterWhere(['like', 'delivery_type', $this->delivery_type])
            ->andFilterWhere(['like', 'order.source', $this->source])
            ->andFilterWhere(['like', 'agentMessage', $this->agentMessage]);


        if (!isset($params['sort'])) {
            $query->orderBy(['time' => SORT_ASC]);
        }

        $query->select([
            'branch_id',
            'ROUND(AVG(price), 2) as average_ticket',
            'SUM(price) as revenue',
            'count(*) as total_orders'
        ]);

        switch ($this->time_filter) {
            case 'daily':
                $query->addSelect(['DATE(`time`) as timing']);
                break;
            case 'weekly':
                $query->addSelect(['WEEK(`time`) as timing']);
                $query->addSelect(['YEAR(`time`) as time']);
                break;
            case '':
            case 'monthly':
                $query->addSelect(['MONTH(`time`) as timing']);
                $query->addSelect(['time']);
                break;
            case 'yearly':
                $query->addSelect(['YEAR(`time`) as timing']);
                break;
        }

        $query->groupBy(['timing', 'branch_id']);

//        echo $query->createCommand()->getSql();  die;

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * @param array  $params report form fields
     * @param string $report area, hourly, platform
     * @return ActiveDataProvider
     */
    public function reportSearch($params, $report)
    {

        switch ($report) {
            case 'area':
                $columns = [
                    'area.name as area_name',
                    'count(*) as total_orders',
                    'sum(price) as revenue',
                    'AVG(price) as average_ticket'];
                break;
            case 'hourly':
                $columns = [
                    'sum(price) as revenue',
                    'avg(price) as average_ticket',
                    'CONCAT(hour(time), ":00 to ", hour(time), ":59") as hourly_labels',
                    'count(*) as total_orders'];
                break;
            case 'platform':
                $columns = [
                    'source as order_source',
                    'sum(price) as revenue',
                    'avg(price) as average_ticket',
                    'count(*) as total_orders'];
                break;
            case 'agent':
                $columns = [
                    'order.*',
                    'source as order_source',
                    'sum(price) as revenue',
                    'avg(price) as average_ticket',
                    'count(*) as total_orders'];
                break;
        }

        $query = Order::find();

        $query->select($columns);

        $this->load($params);

        if ($this->customer) {
            $query->joinWith(['customer'], true, ' LEFT JOIN');
        }


        $query->joinWith(['address'], true, ' LEFT JOIN');
        $query->joinWith(['area'], true, ' LEFT JOIN');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            Order::$table . '.address_id' => $this->address_id,
            Order::$table . '.branch_id' => $this->branch_id,
            Order::$table . '.source' => $this->source,
        ]);

        //this is used when we need today results
        if ($this->isToday) {
            $query->andFilterWhere(['DATE(time)' => Yii::$app->dateTime->getDate()]);
        }

        //Chreck if params has from_date parameter, if yes then allow search on from_date
        if (!empty($this->from_date)) {
            $query->andFilterWhere(['>=', 'DATE(time)', $this->from_date]);
        }

        //Chreck if params has to_date parameter, if yes then allow search on to_date
        if (!empty($this->to_date)) {
            $query->andFilterWhere(['<=', 'DATE(time)', $this->to_date]);
        }

        // fetch all records based on area
        if ($this->area) {
            $query->andFilterWhere(['=', 'address.area', $this->area]);
        }

        // filter on time category
        if ($this->time_category) {
            $query = Yii::$app->setting->getTimeCategory($this->time_category, $query);
        }

        switch ($report) {
            case 'area':
                $query->groupBy('address.area')->orderBy(['total_orders' => SORT_DESC]);
                break;
            case 'hourly':
                $query->groupBy('hourly_labels');
                break;
            case 'platform':
                $query->groupBy('source');
                break;
            case 'agent':
                $query->groupBy('agent_id');
                break;
        }

//        echo $query->createCommand()->getSql(); die;

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params report form fields
     * @return ActiveDataProvider
     */
    public function branchReport($params)
    {

        $columns = [
            '(CASE
                WHEN TIME(time) BETWEEN \'11:00:00\' AND \'12:00:00\' OR TIME(time) BETWEEN \'14:00:00\' AND \'16:00:00\'
                  THEN \'LUNCH NON PEAK\'
                WHEN TIME(time) BETWEEN \'12:00:00\' AND \'14:00:00\'
                  THEN \'LUNCH PEAK\'
                WHEN TIME(time) BETWEEN \'16:00:00\' AND \'19:00:00\' OR TIME(time) BETWEEN \'22:00:00\' AND \'23:59:59\'
                  THEN \'DINNER PEAK\'
                WHEN TIME(time) BETWEEN \'19:00:00\' AND \'22:00:00\'
                  THEN \'DINNER NON PEAK\'
                END) AS order_time_category',
            'order.*',
            'source as order_source',
            'sum(total_sum) as revenue',
            'avg(total_sum) as average_ticket',
            'count(*) as total_orders'
        ];

        $query = Order::find();

        $query->select($columns);

        $this->load($params);

        if ($this->customer) {
            $query->joinWith(['customer'], true, ' LEFT JOIN');
        }


        $query->joinWith(['address'], true, ' LEFT JOIN');
        $query->joinWith(['area'], true, ' LEFT JOIN');
        $query->joinWith(['branch'], true, ' LEFT JOIN');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            Order::$table . '.address_id' => $this->address_id,
            Order::$table . '.branch_id' => $this->branch_id,
            Order::$table . '.source' => $this->source,
        ]);

        //this is used when we need today results
        if ($this->isToday) {
            $query->andFilterWhere(['DATE(time)' => Yii::$app->dateTime->getDate()]);
        }

        //Chreck if params has from_date parameter, if yes then allow search on from_date
        if (!empty($this->from_date)) {
            $query->andFilterWhere(['>=', 'DATE(time)', $this->from_date]);
        }

        //Chreck if params has to_date parameter, if yes then allow search on to_date
        if (!empty($this->to_date)) {
            $query->andFilterWhere(['<=', 'DATE(time)', $this->to_date]);
        }

        // fetch all records based on area
        if ($this->area) {
            $query->andFilterWhere(['=', 'address.area', $this->area]);
        }

        // Check if filter has time_category then find based on time categpry
        if ($this->time_category) {
            $query = Yii::$app->setting->getTimeCategory($this->time_category, $query);
        }

        $query->groupBy(['branch.name', 'area.name', 'order.source', 'order_time_category']);
        $query->orderBy([
            'branch.name' => SORT_DESC,
            'area.name' => SORT_DESC,
            'order.source' => SORT_DESC,
            'order_time_category' => SORT_DESC
        ]);

//        echo $query->createCommand()->getRawSql();die;


        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params report form fields
     * @return ActiveDataProvider
     */
    public function thirdPartyReport($params)
    {

        $columns = [
            '(CASE
                WHEN TIME(time) BETWEEN \'11:00:00\' AND \'12:00:00\' OR TIME(time) BETWEEN \'14:00:00\' AND \'16:00:00\'
                  THEN \'LUNCH NON PEAK\'
                WHEN TIME(time) BETWEEN \'12:00:00\' AND \'14:00:00\'
                  THEN \'LUNCH PEAK\'
                WHEN TIME(time) BETWEEN \'16:00:00\' AND \'19:00:00\' OR TIME(time) BETWEEN \'22:00:00\' AND \'23:59:59\'
                  THEN \'DINNER PEAK\'
                WHEN TIME(time) BETWEEN \'19:00:00\' AND \'22:00:00\'
                  THEN \'DINNER NON PEAK\'
                END) AS order_time_category',
            'order_origin.name origin_name',
            'order.*',
            'source as order_source',
            'sum(total_sum) as revenue',
            'avg(total_sum) as average_ticket',
            'count(*) as total_orders'
        ];

        $query = Order::find();

        $query->select($columns);

        $this->load($params);

        if ($this->customer) {
            $query->joinWith(['customer'], true, ' LEFT JOIN');
        }


        $query->joinWith(['address'], true, ' LEFT JOIN');
        $query->joinWith(['area'], true, ' LEFT JOIN');
        $query->joinWith(['branch'], true, ' LEFT JOIN');
        $query->joinWith(['orderOrigin'], true, ' LEFT JOIN');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            Order::$table . '.address_id' => $this->address_id,
            Order::$table . '.branch_id' => $this->branch_id,
            Order::$table . '.source' => $this->source,
        ]);

        $query->andWhere(['<>', 'order_origin', 'NULL']);

        //this is used when we need today results
        if ($this->isToday) {
            $query->andFilterWhere(['DATE(time)' => Yii::$app->dateTime->getDate()]);
        }

        //Chreck if params has from_date parameter, if yes then allow search on from_date
        if (!empty($this->from_date)) {
            $query->andFilterWhere(['>=', 'DATE(time)', $this->from_date]);
        }

        //Chreck if params has to_date parameter, if yes then allow search on to_date
        if (!empty($this->to_date)) {
            $query->andFilterWhere(['<=', 'DATE(time)', $this->to_date]);
        }

        // fetch all records based on area
        if ($this->area) {
            $query->andFilterWhere(['=', 'address.area', $this->area]);
        }

        // Check if filter has time_category then find based on time categpry
        if ($this->time_category) {
            $query = Yii::$app->setting->getTimeCategory($this->time_category, $query);
        }

        if ($this->order_origin) {
            $query->andFilterWhere(['=', 'order_origin', $this->order_origin]);
        }

        $groups[] = 'branch.name';

        if ($this->groupByArea) {
            $groups[] = 'area.name';
        }

        if ($this->groupBySource) {
            $groups[] = 'order.source';
        }

        if ($this->groupByTimeCategory) {
            $groups[] = 'order_time_category';
        }

        if ($this->groupByOrigin) {
            $groups[] = 'order_origin.name';
        }



        $query->groupBy($groups);

        $query->orderBy([
            'branch.name' => SORT_DESC,
            'area.name' => SORT_DESC,
            'order.source' => SORT_DESC,
            'order_time_category' => SORT_DESC,
            'order_origin.name' => SORT_DESC
        ]);

//        echo $query->createCommand()->getRawSql();die;

        return $dataProvider;
    }

    /**
     * getDispatchCalculation
     * get Dispatch calculation besed on current search params & by branch
     * @param array $params    dispatch form fields
     * @param array $branch_id ID of the branch
     * @return ActiveDataProvider
     */
    public function getDispatchCalculation($params, $branch_id)
    {
        $query = Order::find();

        $query->select(['count(*) as total_orders', 'AVG( (dispatched_at - created_at) ) average_ticket']);

        $this->load($params);

        if ($this->customer) {
            $query->joinWith(['customer'], true, ' LEFT JOIN');
        }

        $query->joinWith(['branch'], true, ' LEFT JOIN');
        $query->joinWith(['address'], true, ' LEFT JOIN');
        $query->joinWith(['area'], true, ' LEFT JOIN');

        //this is used when we need today results
        if ($this->isToday) {
            $query->andFilterWhere(['DATE(time)' => Yii::$app->dateTime->getDate()]);
        }

        //Chreck if params has from_date parameter, if yes then allow search on from_date
        if (!empty($this->from_date)) {
            $query->andFilterWhere(['>=', 'DATE(time)', $this->from_date]);
        }

        //Chreck if params has to_date parameter, if yes then allow search on to_date
        if (!empty($this->to_date)) {
            $query->andFilterWhere(['<=', 'DATE(time)', $this->to_date]);
        }

        // Check if filter has time_category then find based on time categpry
        if ($this->time_category) {
            $query = Yii::$app->setting->getTimeCategory($this->time_category, $query);
        }


        //All order that took 20 or more then 20 minutes TODO may be dynamics
        if ($this->time_took) {
            $query->andFilterWhere(['>=', '(dispatched_at - created_at)', $this->time_took]);
        } else {
            $query->andFilterWhere(['<', '(dispatched_at - created_at)', 1200]);
        }

        // fetch all records based on area
        if ($this->area) {
            $query->andFilterWhere(['=', 'address.area', $this->area]);
        }

        $query->andFilterWhere([
            self::$table . '.branch_id' => $branch_id,
        ]);

        return $query->all();
    }

    /**
     * get report search data provider
     * @param array $params values to get report aka filters
     * @return object ActiveDataProvider
     */
    public function PromoCode($params)
    {
        $columns = [
            'order.*',
            'pc.id id',
            'order.time order_time',
            'pc.name Title',
            'pc.code code',
            'sum(order.price) as revenue',
            'pc.amount Value',
            'pc.type type',
            'sum(order.total_sum) totalRevenue',
            'count(order.id) total_orders',
            'sum(order.discount) discount',
            'b.name branch_name'
        ];

        $query = self::find();

        $this->load($params);


        // get time filters
        $timeFilter = (new WidgetHelper())->timeFilter($this->time_filter);

        foreach ($timeFilter['columns'] as $column) {
            $columns[] = $column;
        }

        $query->select($columns);

        $query->join('INNER JOIN', 'promo_to_customer ptc', 'order.id = ptc.order_id');
        $query->join('LEFT JOIN', 'promo_codes pc', 'ptc.promo_code_id = pc.id');
        $query->join('LEFT JOIN', 'branch b', 'order.branch_id = b.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);


        $query->andFilterWhere([
            Order::$table . '.address_id' => $this->address_id,
            Order::$table . '.branch_id' => $this->branch_id,
            Order::$table . '.source' => $this->source,
        ]);


        //this is used when we need today results
        if ($this->isToday) {
            $this->to_date = Yii::$app->dateTime->getDate();
            $this->from_date = date('Y-m-d', strtotime('-7 day'));
        }


        //Chreck if params has from_date parameter, if yes then allow search on from_date
        if (!empty($this->from_date)) {
            $query->andFilterWhere(['>=', 'DATE(time)', $this->from_date]);
        }

        //Chreck if params has to_date parameter, if yes then allow search on to_date
        if (!empty($this->to_date)) {
            $query->andFilterWhere(['<=', 'DATE(time)', $this->to_date]);
        }

        // filter on time category
        if ($this->time_category) {
            $query = Yii::$app->setting->getTimeCategory($this->time_category, $query);
        }

        //Chreck if params has from_date parameter, if yes then allow search on from_date
        if (isset($this->code_status)) {
            $query->andFilterWhere(['pc.status' => $this->code_status]);
        }

        //Chreck if params has from_date parameter, if yes then allow search on from_date
        if ($this->filter_promos) {
            $query->andFilterWhere(['IN', 'pc.id',$this->filter_promos]);
        }

        $query->groupBy(['order.branch_id', 'source', 'pc.name']);

        $query->orderBy(['order.branch_id' => SORT_ASC, 'source' => SORT_ASC, 'code' => SORT_ASC]);

//        echo $query->createCommand()->getRawSql(); die;

        return $dataProvider;
    }
}
