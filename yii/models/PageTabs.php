<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page_tabs".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property string $type
 * @property integer $status
 *
 * @property PageTabs $parent
 * @property PageTabs[] $pageTabs
 * @property TabHeadings[] $tabHeadings
 */
class PageTabs extends \yii\db\ActiveRecord
{

    public $headings = [];
    /**
     * @inheritdoc
     * @return string tableName
     */
    public static function tableName()
    {
        return 'page_tabs';
    }

    /**
     * @inheritdoc
     * @return array rules
     */
    public function rules()
    {
        return [
            [['name', 'type', 'status'], 'required'],
            [['parent_id', 'status'], 'integer'],
            ['parent_id', 'default', 'value' => 0],
            [['type'], 'string'],
            [['headings'], 'safe'],
            [['name'], 'string', 'max' => 255],
            //[['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => PageTabs::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     * @return array attributeLabels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(PageTabs::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageTabs()
    {
        return $this->hasMany(PageTabs::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabHeadings()
    {
        return $this->hasMany(TabHeadings::className(), ['tab_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return boolean true if successfully stored heading
     * @param boolean $insert            whether this method called while inserting a record. If false, it means the method is called while updating a record.
     * If false, it means the method is called while updating a record.
     * @param array   $changedAttributes The old values of attributes that had changed and were saved. You can use this parameter to take action based on the changes made for example send an email when the password had changed or implement audit trail that tracks all the changes. `$changedAttributes` gives you the old attribute values while the active record (`$this`) has already the new, updated values.
     * You can use this parameter to take action based on the changes made for example send an email
     * when the password had changed or implement audit trail that tracks all the changes.
     * `$changedAttributes` gives you the old attribute values while the active record (`$this`) has
     * already the new, updated values.
     */
    public function afterSave($insert, $changedAttributes) {

        TabHeadings::deleteAll(['tab_id' => $this->id]);
        // Save ALL banners Images

        foreach ($this->headings as $heading) {
            $bannerImage = new TabHeadings();
            $bannerImage->attributes = $heading;
            $bannerImage->tab_id = $this->id;
            $bannerImage->save(false);
        }
    }
}
