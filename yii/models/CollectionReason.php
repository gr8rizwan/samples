<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "collection_reason".
 *
 * @property integer $id
 * @property string $title
 * @property integer $status
 */
class CollectionReason extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collection_reason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
        ];
    }
}
