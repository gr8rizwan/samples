<?php

namespace app\models;

use backend\base\models\BaseModel;
use common\models\CategoryType;
use backend\models\Specials;
use Yii;

/**
 * This is the model class for table "menu_item".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property integer $category_type
 * @property string $name
 * @property string $price
 * @property string $image
 * @property string $detail_image
 * @property string $description
 * @property string $descriptionApp
 * @property integer $calories_count
 * @property integer $sequence
 * @property integer $is_deleted
 * @property integer $deleted_by_customer
 * @property resource $ingredients_map
 * @property integer $customer_id
 * @property integer $pending
 * @property integer $protein
 * @property integer $carbs
 * @property integer $fat
 * @property string $new_image
 * @property integer $om_id
 * @property integer $twox_id
 *
 * @property string $slug
 *
 * @property Loyalty[] $loyalties
 * @property Customer $customer
 * @property Menu $menu
 * @property MenuItemIngredients[] $menuItemIngredients
 * @property OrderMenuItem[] $orderMenuItems
 * @property Specials[] $specials
 */
class MenuItem extends BaseModel
{
    public $menu_item_ingredients;
    public $legends;

    /**
     * @inheritdoc
     * @return string Table name
     */
    public static function tableName()
    {
        return 'menu_item';
    }

    /**
     * @inheritdoc
     * @return array rules
     */
    public function rules()
    {
        return [
            [['menu_id', 'category_type', 'calories_count', 'sequence', 'is_deleted', 'customer_id', 'pending', 'protein', 'carbs', 'fat', 'om_id'], 'integer'],
            [['isFeatured', 'orderCount', 'favoriteCount', 'twox_id'], 'integer'],
            [['menu_id', 'category_type', 'name'], 'required'],
            [['name'], 'unique'],
            [['price'], 'number'],
            [['menu_item_ingredients', 'quantity', 'isCustomizable', 'slug', 'legends'], 'safe'],
            [['description', 'descriptionApp', 'ingredients_map'], 'string'],
            [['name', 'image', 'new_image', 'detail_image'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     * @return array Attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'category_type' => 'Category',
            'name' => 'Name',
            'price' => 'Price',
            'image' => 'Image',
            'detail_image' => 'Detail Image (1800x1200 PX)',
            'description' => 'Description',
            'descriptionApp' => 'Description App',
            'calories_count' => 'Calories Count',
            'sequence' => 'Sequence',
            'is_deleted' => 'Is Deleted',
            'deleted_by_customer' => 'Deleted By Customer',
            'ingredients_map' => 'Ingredients Map',
            'customer_id' => 'Customer ID',
            'pending' => 'Pending',
            'protein' => 'Protein',
            'carbs' => 'Carbs',
            'fat' => 'Fat',
            'new_image' => 'New Image',
            'slug' => 'Slug',
            'om_id' => 'OrderMate ID',
            'twox_id' => 'TwoX12 ID',
            'menu_item_ingredients' => 'menu item ingredients',
            'Quantity' => 'Min quantity in Order',
            'isCustomizable' => 'Allow Customization',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoyalties()
    {
        return $this->hasMany(Loyalty::className(), ['sandwich_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryType()
    {
        return $this->hasOne(CategoryType::className(), ['id' => 'category_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemIngredients()
    {
        return $this->hasMany(MenuItemIngredients::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderMenuItems()
    {
        return $this->hasMany(OrderMenuItem::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemLegends()
    {
        return $this->hasMany(MenuItemLegend::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecials()
    {
        return $this->hasMany(Specials::className(), ['menu_item_id' => 'id']);
    }

    /**
     * Save menu Item ingredients
     * @return boolean true or false
     * */
    public function setMenuItemIngredient() {

        if (!$this->isNewRecord) {
            MenuItemIngredients::deleteAll('menu_item_id = :menu_item_id', [':menu_item_id' => $this->id]);
        }

        if (isset($this->menu_item_ingredients) && !empty($this->menu_item_ingredients)) {
            foreach ($this->menu_item_ingredients as $key => $value) {
                $menu_item_ingredient = new MenuItemIngredients();
                $menu_item_ingredient->ingredient_id = $value;
                $menu_item_ingredient->menu_item_id = $this->id;
                $menu_item_ingredient->quantity = 1;
                $menu_item_ingredient->save();
            }
        }

        if (!$this->isNewRecord) {
            MenuItemLegend::deleteAll('menu_item_id = :menu_item_id', [':menu_item_id' => $this->id]);
        }

        if (isset($this->legends) && !empty($this->legends)) {
            foreach ($this->legends as $key => $value) {
                $legend = new MenuItemLegend();
                $legend->legend_id = $value;
                $legend->menu_item_id = $this->id;
                $legend->status = 1;
                $legend->save();
            }
        }
    }
}
