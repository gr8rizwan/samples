<?php

namespace backend\models;

use backend\base\models\BaseModel;
use common\models\User;
use Yii;

/**
 * This is the model class for table "collection".
 *
 * @property integer $id
 * @property integer $branch_id
 * @property integer $customer_id
 * @property integer $order_id
 * @property integer $driver_id
 * @property integer $created_by
 * @property integer $reason_id
 * @property integer $status
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Customer $customer
 * @property Branch $branch
 */
class Collection extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch_id', 'customer_id', 'driver_id', 'created_by', 'reason_id', 'amount'], 'required'],
            [['branch_id', 'customer_id', 'order_id', 'driver_id', 'created_by', 'reason_id', 'status', 'is_deleted'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'branch_id' => 'Branch',
            'customer_id' => 'Customer',
            'order_id' => 'Order',
            'driver_id' => 'Driver',
            'created_by' => 'Created By',
            'reason_id' => 'Reason',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created Date',
            'updated_at' => 'Updated Date',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReason()
    {
        return $this->hasOne(CollectionReason::className(), ['id' => 'reason_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branch_id']);
    }
}
