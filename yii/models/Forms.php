<?php


namespace app\models;

use Yii;

/**
 * This is the model class for table "forms".
 *
 * @property integer $id
 * @property string $title
 * @property string $html_id
 * @property integer $form_page_id
 * @property integer $status
 * @property integer $sort_order
 * @property string $notification_emails
 *
 * @property FormPages $formPage
 * @property FormsData[] $formsDatas
 */
class Forms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return 'forms';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['title', 'html_id', 'form_page_id', 'status'], 'required'],
            [['form_page_id', 'status', 'sort_order'], 'integer'],
            [['title', 'html_id', 'notification_emails'], 'string', 'max' => 255],
            [['form_page_id'], 'exist', 'skipOnError' => true, 'targetClass' => FormPages::className(), 'targetAttribute' => ['form_page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'html_id' => Yii::t('app', 'Html ID'),
            'form_page_id' => Yii::t('app', 'Form Page ID'),
            'status' => Yii::t('app', 'Status'),
            'sort_order' => Yii::t('app', 'Sorting'),
            'notification_emails' => Yii::t('app', 'Notification Emails'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormPage()
    {
        return $this->hasOne(FormPages::className(), ['id' => 'form_page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormsDatas()
    {
        return $this->hasMany(FormsData::className(), ['form_id' => 'id']);
    }
}
