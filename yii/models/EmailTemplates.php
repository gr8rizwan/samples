<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email_templates".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $template_key
 * @property integer $status
 *
 * @property array $blocks
 *
 * @property EmailBlocks[] $emailBlocks
 */
class EmailTemplates extends \yii\db\ActiveRecord
{
    //Blocks is a array of banners with some extra info
    public $blocks = [];

    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return 'email_templates';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['title', 'template_key'], 'required'],
            [['status'], 'integer'],
            [['blocks', 'description'], 'safe'],
            [['title', 'template_key'], 'string', 'max' => 255],
            [['template_key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'template_key' => Yii::t('app', 'Template Key'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     * @param boolean $insert            true or false
     * @param array   $changedAttributes list of attributes
     * @return null
     */
    public function afterSave($insert, $changedAttributes) {

        EmailBlocks::deleteAll(['template_id' => $this->id]);

        foreach ($this->blocks as $block) {
            $blockObj = new EmailBlocks();

            $blockObj->attributes = $block;
            $blockObj->template_id = $this->id;
            if (!$blockObj->save()) {
                return false;
            }
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailBlocks()
    {
        return $this->hasMany(EmailBlocks::className(), ['template_id' => 'id']);
    }
}
