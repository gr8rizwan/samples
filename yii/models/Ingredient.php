<?php

namespace backend\models;

use app\models\IngredientCategory;
use app\models\MenuItemIngredients;
use app\models\OrderItemCustomization;
use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "ingredient".
 *
 * @property integer $id
 * @property integer $ingredient_category_id
 * @property string $name
 * @property string $price
 * @property string $image
 * @property string $community_image
 * @property string $description
 * @property integer $calories_count
 * @property integer $max_quantity
 * @property integer $sequence
 * @property integer $is_deleted
 * @property integer $protein
 * @property integer $carbs
 * @property integer $fat
 * @property string $om_id
 * @property integer $twox_id
 * @property integer $pending
 * @property string $legend
 * @property integer $premium
 * @property integer $menu_id
 * @property integer $extra_unit_price
 * @property integer $extra_unit_id
 *
 * @property IngredientCategory $ingredientCategory
 * @property MenuItemIngredients[] $menuItemIngredients
 * @property OrderItemCustomization[] $orderItemCustomizations
 */
class Ingredient extends BaseModel
{

    public $file;

    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'ingredient';
    }

    /**
     * @inheritdoc
     * @return array rules array
     */
    public function rules()
    {
        return [
            [['ingredient_category_id', 'name', 'price', 'max_quantity', 'om_id'], 'required'],
            [['ingredient_category_id', 'quantity', 'calories_count', 'max_quantity', 'sequence', 'is_deleted', 'protein', 'carbs', 'fat', 'pending', 'premium', 'menu_id', 'extra_unit_id'], 'integer'],
            [['twox_id'], 'integer'],
            [['price'], 'number'],
            [['image', 'extra_unit_price', 'extra_unit_id', 'community_image'], 'safe'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif'],
            [['description'], 'string'],
            [['name', 'om_id', 'legend', 'community_image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     * @return array attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ingredient_category_id' => 'Ingredient Category ID',
            'name' => 'Name',
            'price' => 'Price',
            'image' => 'Image',
            'community_image' => 'Community Image',
            'description' => 'Description',
            'calories_count' => 'Calories Count',
            'max_quantity' => 'Max Quantity',
            'sequence' => 'Sort Order',
            'is_deleted' => 'Is Deleted',
            'protein' => 'Protein',
            'carbs' => 'Carbs',
            'fat' => 'Fat',
            'om_id' => 'OM ID',
            'twox_id' => 'Twox12 ID',
            'pending' => 'Pending',
            'legend' => 'Legend',
            'premium' => 'Premium',
            'menu_id' => 'Menu ID',
            'quantity'  => 'Quantity',
            'extra_unit_price'  => 'Extra Unit Price',
            'extra_unit_id'  => 'Extra Unit ID',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientCategory()
    {
        return $this->hasOne(IngredientCategory::className(), ['id' => 'ingredient_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItemIngredients()
    {
        return $this->hasMany(MenuItemIngredients::className(), ['ingredient_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItemCustomizations()
    {
        return $this->hasMany(OrderItemCustomization::className(), ['ingredient_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtraUnit()
    {
        return $this->hasOne(self::className(), ['extra_unit_id' => 'id']);
    }
}
