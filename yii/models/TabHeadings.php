<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tab_headings".
 *
 * @property integer $id
 * @property integer $tab_id
 * @property string $question
 * @property string $description
 * @property integer $sort_order
 * @property integer $status
 *
 * @property PageTabs $tab
 */
class TabHeadings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string tab_headings
     */
    public static function tableName()
    {
        return 'tab_headings';
    }

    /**
     * @inheritdoc
     * @return array rules
     */
    public function rules()
    {
        return [
            [['tab_id', 'question', 'description'], 'required'],
            [['tab_id', 'sort_order', 'status'], 'integer'],
            [['description'], 'string'],
            [['question'], 'string', 'max' => 255],
            [['tab_id'], 'exist', 'skipOnError' => true, 'targetClass' => PageTabs::className(), 'targetAttribute' => ['tab_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     * @return array labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tab_id' => Yii::t('app', 'Tab ID'),
            'question' => Yii::t('app', 'Question'),
            'description' => Yii::t('app', 'Description'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return  PageTabs $tab
     */
    public function getTab()
    {
        return $this->hasOne(PageTabs::className(), ['id' => 'tab_id']);
    }
}
