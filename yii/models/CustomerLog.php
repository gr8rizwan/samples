<?php

namespace backend\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "customer_log".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $order_id
 * @property integer $action_id
 * @property string $description
 * @property string $time
 *
 * @property Customer $customer
 */
class CustomerLog extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'log_type', 'order_id'], 'required'],
            [['customer_id', 'order_id', 'action_id'], 'integer'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer',
            'order_id' => 'Order',
            'action_id' => 'Action',
            'description' => 'Description',
            'time' => 'Time',
            'log_type' => 'Log type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderTimeCategory()
    {
        return $this->hasOne(OrderDispatching::className(), ['id' => 'order_id']);
    }
}
