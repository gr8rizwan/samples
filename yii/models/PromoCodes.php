<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "promo_codes".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $type
 * @property string $redeem_type
 * @property integer $status
 * @property integer $branch_id
 * @property string $created_at
 */
class PromoCodes extends \yii\db\ActiveRecord
{

    // promo for customer
    public $promoForCustomer = [];

    public $revenue;
    public $discounted;
    public $redeemed;
    public $redeemed_amount;

    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return 'promo_codes';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['name', 'code', 'amount', 'type', 'redeem_type', 'from_date', 'to_date', 'customer_specific', 'status'], 'required'],
            [['type', 'redeem_type'], 'string'],
            [['status', 'branch_id', 'customer_specific'], 'integer'],
            [['created_at', 'min_order', 'promoForCustomer', 'redeemed', 'redeemed_amount'], 'safe'],
            [['name', 'code'], 'string', 'max' => 255],
            [['time_from', 'time_to', 'valid_on_day'], 'string', 'max' => 50],
            [['name'], 'unique'],
            [['code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     * @return array attribute label
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'type' => 'Type',
            'amount' => 'Value',
            'min_order' => 'Min Order',
            'redeem_type' => 'Redeem Type',
            'status' => 'Status',
            'branch_id' => 'Branch ID',
            'from_date' => 'Valid from',
            'to_date' => 'Valid Till',
            'created_at' => 'Created At',
            'customer_specific' => 'Customer specific',
            'time_from' => 'Time from',
            'time_to' => 'Time till',
            'valid_on_day' => 'Valid on day',
            'discounted' => 'Total Discount',
            'revenue' => 'Valid on day',
            'redeemed' => 'Redeem count',
            'redeemed_amount' => 'Revenue Generated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoForCustomerList()
    {
        return $this->hasMany(PromoForCustomer::className(), ['promo_code_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoToCustomer()
    {
        return $this->hasMany(PromoToCustomer::className(), ['promo_code_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @param boolean $insert true or false
     * @return boolean true or false
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->time_from === $this->time_to) {
                $this->time_from = 0;
                $this->time_to = 0;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     * @param boolean $insert            true or false
     * @param boolean $changedAttributes array for before saved data
     * @return boolean true or false
     */
    public function afterSave($insert, $changedAttributes) {

        PromoForCustomer::deleteAll(['promo_code_id' => $this->id]);
        // Save all customer with promo
        foreach ($this->promoForCustomer as $promo_customer) {
            $promoFor = new PromoForCustomer();
            $promoFor->promo_code_id = $this->id;
            $promoFor->customer_id = $promo_customer;
            $promoFor->save();
        }
    }

    /**
     * Get Total Sum Value
     * @param object $object    of findings
     * @param array  $attribute that need to calculate value
     * @return integer of value
     * */
    public function getOrderTotalSum($object, $attribute) {

        $value = 0;

        foreach ($object as $promo) {
            $value += isset($promo->order->{$attribute}) ? $promo->order->{$attribute} : 0;
        }

        return $value;
    }
}
