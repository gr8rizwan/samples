<?php

namespace backend\models;

use app\models\Favorite;
use backend\base\models\MasterBaseModel;
use Yii;
use app\models\Loyalty;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "customer".
 *
 * This Model Will use Master DB for customer Records
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone_number
 * @property integer $country_code
 * @property integer $invited_by
 * @property string $password
 * @property integer $image
 * @property integer $facebook_id
 * @property integer $is_active
 * @property integer $is_deleted
 * @property string $role
 * @property string $activation_code
 * @property integer $gender
 * @property string $dob
 * @property string $om_id
 * @property string $customerkey
 * @property string $source
 * @property string $created_at
 * @property string $type
 * @property string $password_reset_token
 * @property string $status
 * @property string $socialType
 * @property string $socialID
 *
 * @property Address[] $addresses
 * @property Customer $invitedBy
 * @property Customer[] $customers
 * @property Loyalty[] $loyalties
 * @property MenuItem[] $menuItems
 * @property Order $order
 */
class Customer extends MasterBaseModel
{
    /**
     * @inheritdoc
     */

    public $re_password;
    public $rewardPoints;
    public $branch_id;
    public $area;
    public $from_date;
    public $to_date;
    public $name;

    /**
     * @return string table name
     * */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['invited_by', 'image', 'facebook_id', 'is_active', 'is_deleted', 'gender'], 'integer'],
            [['password', 'first_name', 'email'], 'required'],
            [['dob', 'area', 'from_date', 'to_date', 'name', 'source'], 'safe'],
            [['created_at', 'type', 'password_reset_token', 'status', 'socialType', 'socialID'], 'safe'],
            [['first_name', 'last_name', 'email', 'phone_number', 'password', 'role', 'activation_code', 'om_id', 'customerkey'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     * @return array attributes labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'phone_number' => 'Phone Number',
            'invited_by' => 'Invited By',
            'password' => 'Password',
            'image' => 'Image',
            'facebook_id' => 'Facebook ID',
            'is_active' => 'Is Active',
            'is_deleted' => 'Is Deleted',
            'role' => 'Role',
            'activation_code' => 'Activation Code',
            'gender' => 'Gender',
            'dob' => 'Dob',
            'om_id' => 'Om ID',
            'customerkey' => 'Customerkey',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvitedBy()
    {
        return $this->hasOne(Customer::className(), ['id' => 'invited_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['invited_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoyalties()
    {
        return $this->hasMany(Loyalty::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItems()
    {
        return $this->hasMany(MenuItem::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavorite()
    {
        return $this->hasMany(Favorite::className(), ['customer_id' => 'id']);
    }

    /**
     * Get Loyalty points, When customer place a order he will get reward points
     * @relation Custom has many Loyality points
     * @return \yii\db\ActiveQuery
     */
    public function getLoyalty()
    {
        return $this->hasMany(Loyalty::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTotalRewardEarn()
    {
        return $this->hasOne(Loyalty::className(), ['customer_id' => 'id'])->select(['SUM(points) as points'])->where(['action' => '+']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTotalRewardUsed()
    {
        return $this->hasOne(Loyalty::className(), ['customer_id' => 'id'])->select(['SUM(points) as points'])->where(['action' => '-']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasMany(Order::className(), ['customer_id' => 'id'])->orderBy(['time' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderAddress()
    {
        return $this->hasMany(Address::className(), ['id' => 'address_id'])->via('order');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstOrder()
    {
        return $this->hasOne(Order::className(), ['customer_id' => 'id'])->orderBy(['time' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastOrder()
    {
        return $this->hasOne(Order::className(), ['customer_id' => 'id'])->orderBy(['time' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHighSumOrder()
    {
        return $this->hasOne(Order::className(), ['customer_id' => 'id'])->orderBy(['total_sum' => SORT_DESC]);
    }

    /**
     * Lowest sum of order with customer ID
     * Relational Query
     * @return \yii\db\ActiveQuery
     */
    public function getLowestSumOrder()
    {
        return $this->hasOne(Order::className(), ['customer_id' => 'id'])->orderBy(['total_sum' => SORT_ASC]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoToCustomer()
    {
        return $this->hasMany(PromoToCustomer::className(), ['customer_id' => 'id']);
    }


    /**
     * @param integer $origin_id id of the order origin
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getOriginOrderCount($origin_id)
    {
        $orderQuery = $this->getOrder();
        $orderQuery->select('count(order.id) as origin_count');
        $orderQuery->where(['order_origin' => $origin_id]);
        return $orderQuery->one();
    }

    /**
     * @param integer $branch_id id of the branch
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getBranchCount($branch_id)
    {
        $orderQuery = $this->getOrder();
        $orderQuery->select('count(order.id) as branch_count');
        $orderQuery->where(['branch_id' => $branch_id]);
        return $orderQuery->one();
    }

    /**
     * get order count based on dynamic parameters
     * @param array $options from_date & to_date
     * @return integer count of order
     * */
    public function getOrderCount($options = [])
    {

        $query = Order::find();

        $query->filterWhere(['customer_id' => $this->id]);

        if ($options['from_date']) {
            $query->andFilterWhere(['>=', 'DATE(time)', $options['from_date']]);
        }

        if ($options['to_date']) {
            $query->andFilterWhere(['<=', 'DATE(time)', $options['to_date']]);
        }

        return $query->count();
    }

    /**
     * @return array
     */
    public function preferredOrderTime()
    {

        $columns = [
            '(CASE
                WHEN TIME(time) BETWEEN \'11:00:00\' AND \'12:00:00\' OR TIME(time) BETWEEN \'14:00:00\' AND \'16:00:00\'
                  THEN \'LUNCH NON PEAK\'
                WHEN TIME(time) BETWEEN \'12:00:00\' AND \'14:00:00\'
                  THEN \'LUNCH PEAK\'
                WHEN TIME(time) BETWEEN \'16:00:00\' AND \'19:00:00\' OR TIME(time) BETWEEN \'22:00:00\' AND \'23:59:59\'
                  THEN \'DINNER PEAK\'
                WHEN TIME(time) BETWEEN \'19:00:00\' AND \'22:00:00\'
                  THEN \'DINNER NON PEAK\'
                END) AS order_time_category',
            'count(*) as total_orders'
        ];

        $orders = $this->getOrder()->select($columns)->groupBy(['order_time_category'])->orderBy(['order_time_category' => SORT_DESC])->all();

        return ArrayHelper::map($orders, 'order_time_category', 'total_orders');
    }

    /**
     * Get all customers
     * @return array of customers
     * */
    public static function getAllCustomers()
    {

        $customers = Yii::$app->cache->get('customers');

        if (!$customers) {
            $customers = self::find()->all();
            Yii::$app->cache->set('customers', $customers, 3600);
        }

        return $customers;
    }
}
