<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\StaticPages;

/**
 * StaticPagesSearch represents the model behind the search form about `backend\models\StaticPages`.
 */
class StaticPagesSearch extends StaticPages
{
    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['id', 'bottom', 'top', 'sort_order', 'status'], 'integer'],
            [['title', 'seo_url', 'description', 'meta_title', 'meta_description', 'meta_keywords', 'seo_keywords', 'banner', 'page_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return object of scenraio
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params of searchable
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StaticPages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'bottom' => $this->bottom,
            'top' => $this->top,
            'sort_order' => $this->sort_order,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'seo_url', $this->seo_url])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords])
            ->andFilterWhere(['like', 'banner', $this->banner]);

        return $dataProvider;
    }
}
