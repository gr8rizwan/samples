<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DiscountCategory;
use yii\db\Expression;

/**
 * DiscountCategorySearch represents the model behind the search form about `app\models\DiscountCategory`.
 */
class DiscountCategorySearch extends DiscountCategory
{

    public $type;   // percentage or fixed amount
    public $branch_id;  // id of branch where discount is redeemed
    public $status;     // active or expired

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'points', 'is_deleted'], 'integer'],
            [['name', 'type', 'start_date', 'end_date', 'status'], 'safe'],
            [['percentage', 'amount'], 'number'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params searchable parameters
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DiscountCategory::find();
        $query->join('LEFT JOIN', 'order_discount_category odc', 'discount_cat_id = discount_category.id');
        $query->join('INNER JOIN', 'order o', 'order_id = o.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!isset($this->status)) {
            $this->status = '1';
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->type == 'percentage') {
            $query->andWhere(['amount' => null]);
        } else if ($this->type == 'fixed') {
            $query->andWhere(['percentage' => '0']);
        }

        if ($this->status == '1') {
            $query->andWhere(['<', 'end_date', new Expression('DATE(NOW())')]);
            $query->andWhere(['>', 'start_date', new Expression('DATE(NOW())')]);
        } else if ($this->status == '0') {
            $query->andWhere(['<', 'start_date', new Expression('DATE(NOW())')]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'points' => $this->points,
            'percentage' => $this->percentage,
            'amount' => $this->amount,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        $query->groupBy(['discount_category.id']);

//        echo $query->createCommand()->getRawSql();die;

        return $dataProvider;
    }
}
