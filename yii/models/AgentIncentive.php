<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Agent incentive report model that is used to send email report
 *
 * @property array $categories
 * @property string $fromDate
 * @property string $toDate
 */
class AgentIncentive extends Model
{
    /**
     * @inheritdoc
     */
    public $categories;
    public $fromDate;
    public $toDate;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['categories', 'fromDate', 'toDate'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'categories' => 'Categories',
            'fromDate' => 'From Date',
            'toDate' => 'To Date'
        ];
    }
}
