<?php

namespace app\models;

use backend\base\models\BaseModel;
use backend\models\BannerImages;
use backend\models\Banners;
use Yii;

/**
 * This is the model class for table "category_type".
 *
 * @property integer $id
 * @property string $title
 * @property integer $status
 * @property string $description
 * @property string $name
 * @property integer $sort_order
 * @property integer $menu_banner_id
 * @property integer $order_banner_id
 * @property integer $slug
 * @property integer $category_image
 * @property integer $onMenu
 */
class CategoryType extends BaseModel
{
    /**
     * @inheritdoc
     * @return string table name
     */
    public static function tableName()
    {
        return 'category_type';
    }

    /**
     * @inheritdoc
     * @return array rules list
     */
    public function rules()
    {
        return [
            [['title', 'isStatic'], 'required'],
            [['status', 'sort_order', 'menu_banner_id', 'order_banner_id', 'onMenu'], 'integer'],
            [['title', 'description', 'name', 'slug', 'category_image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     * @return array attribute list
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
            'description' => 'Description',
            'name' => 'Name',
            'sort_order' => 'Sort Order',
            'isStatic'  => 'Is Static',
            'menu_banner_id'  => 'Menu Page Banner',
            'order_banner_id'  => 'Order Online Banner',
            'slug'  => 'SEO URL',
            'onMenu'  => 'On Menu',
            'category_image'  => 'Category Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuBannerImage()
    {
        return $this->hasOne(BannerImages::className(), ['id' => 'menu_banner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderBannerImage()
    {
        return $this->hasOne(BannerImages::className(), ['id' => 'order_banner_id']);
    }

    /**
     *
     * Before save function that will create seo_url
     * @return string $seo_url
     * */
    public function beforeValidate() {

        $this->slug = strtolower(str_replace(' ', '-', $this->name));
        return true;
    }
}
