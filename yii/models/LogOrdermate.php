<?php

namespace app\models;

use backend\base\models\BaseModel;
use Yii;

/**
 * This is the model class for table "log_ordermate".
 *
 * @property integer $id
 * @property string $name
 * @property string $request
 * @property string $response
 * @property string $url
 * @property string $status
 * @property string $time
 */
class LogOrdermate extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_ordermate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['request', 'response'], 'string'],
            [['time'], 'safe'],
            [['name', 'url', 'status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'request' => 'Request',
            'response' => 'Response',
            'url' => 'Url',
            'status' => 'Status',
            'time' => 'Time',
        ];
    }
}
