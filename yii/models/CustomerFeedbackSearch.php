<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CustomerFeedback;

/**
 * CustomerFeedbackSearch represents the model behind the search form about `backend\models\CustomerFeedback`.
 */
class CustomerFeedbackSearch extends CustomerFeedback
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'order_id', 'branch_id','rating', 'status', 'closed_by', 'parent_id'], 'integer'],
            [['feedback', 'resolution'], 'string'],
            [['created_at', 'closed_at'], 'safe']

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerFeedback::find()->orderBy('created_at DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'            => $this->id,
            'order_id'      => $this->order_id,
            'branch_id'     => $this->branch_id,
            'order_branch'  => $this->order_branch,
            'customer_id'   => $this->customer_id,
            'closed_by'     => $this->closed_by,
            'closed_at'     => $this->closed_at,
            'parent_id'     => $this->parent_id,
            'rating'        => $this->rating,
            'status'        => $this->status,
        ]);

        $query->andFilterWhere(['like', 'feedback', $this->feedback]);
        $query->andFilterWhere(['like', 'resolution', $this->resolution]);
        $query->andFilterWhere(['like', 'created_at', $this->created_at]);

        return $dataProvider;
    }
}
