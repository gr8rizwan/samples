<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IngredientCategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ingredient-category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'sequence') ?>

    <?= $form->field($model, 'is_deleted') ?>

    <?= $form->field($model, 'om_type') ?>

    <?php // echo $form->field($model, 'om_domainid') ?>

    <?php // echo $form->field($model, 'om_minqty') ?>

    <?php // echo $form->field($model, 'om_maxqty') ?>

    <?php // echo $form->field($model, 'om_reducedzone') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
