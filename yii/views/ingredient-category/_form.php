<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IngredientCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ingredient-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'sequence')->textInput() ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'scope')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'om_type')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'om_domainid')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'om_minqty')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'om_maxqty')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'om_reducedzone')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'extra_unit_price')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'is_deleted')->dropDownList(['Active', 'In-Active']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
