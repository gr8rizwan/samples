<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IngredientCategory */

$this->title = 'Update Ingredient Category: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ingredient Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ingredient-category-update card-box">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
