<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IngredientCategory */

$this->title = 'Create Ingredient Category';
$this->params['breadcrumbs'][] = ['label' => 'Ingredient Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredient-category-create card-box">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
