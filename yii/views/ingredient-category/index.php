<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IngredientCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ingredient Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredient-category-index card-box">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?= Html::a('Create Ingredient Category', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'title',
            'sequence',
            'om_type',
            'scope',
            'om_domainid',
            // 'om_minqty',
            // 'om_maxqty',
            // 'om_reducedzone',

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
