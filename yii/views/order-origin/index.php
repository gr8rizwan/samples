<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrderOriginSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Origins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-origin-index card-box">

    <p class="pull-right">
        <?= Html::a('Create Origin', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'delivery_id',
            'pickup_id',
            'sort_order',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return ($model->status == 1 ? 'Active' : 'In-Active');
                },
            ],

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
