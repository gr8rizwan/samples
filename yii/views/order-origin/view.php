<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\OrderOrigin */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Order Origins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-origin-view card-box">


    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'sort_order',
            'delivery_id',
            'pickup_id',
            'status',
        ],
    ]) ?>

</div>
