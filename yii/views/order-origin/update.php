<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OrderOrigin */

$this->title = 'Update Order Origin: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Order Origins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-origin-update card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
