<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\OrderOrigin */

$this->title = 'Create Order Origin';
$this->params['breadcrumbs'][] = ['label' => 'Order Origins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-origin-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
