<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Messages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="messages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'message')->textarea(['maxlength' => true, 'rows' => 5]) ?>

    <?= $form->field($model, 'type')->hiddenInput(['value' => 'Supervisor'])->label(false); ?>
    <?= $form->field($model, 'admin_id')->hiddenInput(['value' => yii::$app->user->identity->id])->label(false); ?>

    <?= $form->field($model, 'status')->dropDownList([ '0' => 'In-active', '1' => 'Active', '2' => 'Expire' ], ['prompt' => 'Select Status']) ?>

    <?=  $form->field($model, 'start_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter start date'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]); ?>

    <?=  $form->field($model, 'end_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter end date'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

