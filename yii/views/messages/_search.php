<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\MessagesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="messages-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-lg-8 col-md-8 col-sm-12">
            <?= $form->field($model, 'message') ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12">
            <?= $form->field($model, 'admin_id')->dropDownList(ArrayHelper::map(User::find()->all(),
                'id',
                function($model) {
                    return $model->email;
                }
                ), ['prompt'=>'Created By']) ?>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList([ '0' => 'In-active', '1' => 'Active', '2' => 'Expire' ], ['prompt' => 'Select Status']) ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?=  $form->field($model, 'start_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Enter start date'],
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]); ?>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?=  $form->field($model, 'end_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Enter end date'],
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]); ?>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="form-group pull-right">
                <label class="control-label" style="width: 100%">&nbsp;</label>
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

        </div>



    </div>

    <?php ActiveForm::end(); ?>

</div>
