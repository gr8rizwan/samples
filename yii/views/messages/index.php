<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Operational Messages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>

<div class="card-box">

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="box-body">

                        <p class="pull-right">
                            <?= Html::a('Create Messages', ['create'], ['class' => 'btn btn-primary']) ?>
                        </p>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                [
                                    'attribute' => 'message',
                                    'filter' => false,
                                ],

                                [
                                    'attribute' => 'status',
                                    'filter' => false,
                                    'value' => function($data) {
                                        return ($data->status == 1 ? 'Active' : 'In-Active');
                                    },
                                ],
                                [
                                    'attribute' => 'start_date',
                                    'filter' => false,
                                ],
                                [
                                    'attribute' => 'end_date',
                                    'filter' => false,
                                ],

                                ['class' => 'common\helpers\CustomActionColumn'],
                            ],
                        ]); ?>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col (left) -->
        </div><!-- /.row -->

    </section><!-- /.content -->
</div>

