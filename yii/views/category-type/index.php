<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategoryTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Category Types';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>

<div class="category-type-index card-box">

    <p class="pull-right">
        <?= Html::a('Create Category Type', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'menu_banner_id',
                'value' => function($model) {
                    return ($model->menu_banner_id > 0 ? $model->menuBannerImage->title : 'N/A');
                },
            ],
            [
                'attribute' => 'order_banner_id',
                'value' => function($model) {
                    return ($model->order_banner_id > 0 ? $model->orderBannerImage->title : 'N/A');
                },
            ],
            'name',
            'isStatic',
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return ($data->status == 0 ? 'In-Active' : 'Active');
                },
            ],
            'sort_order',

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
