<?php

use backend\models\CollectionReason;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?= $form->field($model, 'title') ?>
        </div>

        <div class="col-lg-3 col-sm-12">
            <?= $form->field($model, 'name') ?>
        </div>

        <div class="col-lg-3 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList([ '0' => 'Pending', '1' => 'Received'], ['prompt' => 'Select Status']) ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="form-group pull-right">
                <label class="control-label" style="width: 100%">&nbsp;</label>
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

        </div>

    </div>


    <?php ActiveForm::end(); ?>

</div>
