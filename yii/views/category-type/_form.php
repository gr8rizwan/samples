<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$banner = \backend\models\Banners::find()->where(['page' => 'category'])->one();
/* @var $this yii\web\View */
/* @var $model app\models\CategoryType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-type-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="col-lg-12">
        <ul class="nav nav-tabs tabs">
            <li class="active tab">
                <a href="#home-2" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                    <span class="hidden-xs">General</span>
                </a>
            </li>
            <li class="tab">
                <a href="#profile-2" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                    <span class="hidden-xs">Data</span>
                </a>
            </li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="home-2">
                <div class="row">
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-sm-6">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

                    <?= $form->field($model, 'sort_order')->textInput() ?>

                    <div class="row">

                        <div class="col-sm-6">
                            <?= $form->field($model, 'isStatic')->dropDownList(['' => 'Select type', 'Yes' => 'Static', 'No' => 'Dynamic']); ?>
                        </div>

                        <div class="col-sm-6">
                            <?= $form->field($model, 'status')->dropDownList(['In-Active', 'Active'], ['prompt' => 'Select Status']); ?>
                        </div>

                    </div>

                </div>
            </div>

            <div class="tab-pane" id="profile-2">

                <div class="row">

                    <div class="col-sm-6">
                        <div id="images" class="form-group">
                            <label class="control-label" for="input-image">Category Image</label>
                            <a href="" id="banner-image" data-toggle="image" class="img-thumbnail">
                                <img src="<?php echo $model->category_image; ?>" alt="" height="125" title="" data-placeholder="no_image.png" />
                            </a>

                            <?= $form->field($model, 'category_image')->hiddenInput(['id' => 'input-image'])->label(false) ?>

                        </div>

                    </div>

                </div>


                <div class="row">
                    <?= $form->field($model, 'onMenu')->dropDownList(['No', 'Yes'], ['prompt' => 'On Menu']); ?>

                    <?= $form->field($model, 'menu_banner_id')->dropDownList(ArrayHelper::map($banner->bannerImages, 'id', 'title'), ['prompt'=>'Select Menu Banner']) ?>

                    <?= $form->field($model, 'order_banner_id')->dropDownList(ArrayHelper::map($banner->bannerImages, 'id', 'title'), ['prompt'=>'Select Order Banner']) ?>

                </div>




            </div>

        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
