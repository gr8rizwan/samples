<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryType */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Category Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-type-view card-box">

    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'menu_banner_id',
                'value' => ($model->menu_banner_id > 0 ? $model->menuBannerImage->title : 'N/A'),
            ],
            [
                'attribute' => 'order_banner_id',
                'value' => ($model->order_banner_id > 0 ? $model->orderBannerImage->title : 'N/A'),
            ],
            'description',
            'name',
            'sort_order',
            'slug',
            'status',
            'isStatic',
            'category_image'
        ],
    ]) ?>

</div>
