<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use app\models\IngredientCategory;
use \app\models\Menu;

/* @var $this yii\web\View */
/* @var $model app\models\Ingredient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ingredient-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create Ingredient' : 'Update Ingredient', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <div class="tabbable-panel">
        <div class="tabbable-line">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#tab_general" data-toggle="tab">
                        General Information
                    </a>
                </li>
                <li>
                    <a href="#tab_data" data-toggle="tab">
                        Data
                    </a>
                </li>
                <li>
                    <a href="#tab_image" data-toggle="tab">
                        Image
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_general">

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'legend')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'max_quantity')->textInput() ?>

                    <?= $form->field($model, 'sequence')->textInput() ?>

                    <?= $form->field($model, 'om_id')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'twox_id')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="tab-pane" id="tab_data">

                    <?= $form->field($model, 'ingredient_category_id')->dropDownList(ArrayHelper::map(IngredientCategory::find()->all(), 'id', 'name'), ['prompt'=>'Choose category']) ?>

                    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'calories_count')->textInput() ?>

                    <?= $form->field($model, 'protein')->textInput() ?>

                    <?= $form->field($model, 'carbs')->textInput() ?>

                    <?= $form->field($model, 'fat')->textInput() ?>

                    <?= $form->field($model, 'pending')->textInput() ?>

                    <?= $form->field($model, 'premium')->textInput() ?>

                    <?=
                        $form->field($model, 'menu_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Menu::find()->all(), 'id', 'name'),
                            'options' => ['placeholder' => 'Select Menu'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                    ?>

                </div>
                <div class="tab-pane" id="tab_image">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">

                                <label class="control-label" for="input-image">Ingredients Image</label> <br />
                                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                                    <img src="<?php echo $model->image; ?>" alt="" height="125" title="" data-placeholder="no_image.png" />
                                </a>

                                <?= $form->field($model, 'image')->hiddenInput(['id' => 'input-image'])->label(false) ?>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-image">
                                    Community s'wich image <br >
                                    <small class="font-normal"> This images will be used for community s'wich that is create with this as filling.</small>
                                </label>
                                <br />
                                <a href="" id="community-image" data-toggle="image" class="img-thumbnail">
                                    <img src="<?php echo $model->community_image; ?>" alt="" height="125" title="" data-placeholder="no_image.png" />
                                </a>

                                <?= $form->field($model, 'community_image')->hiddenInput(['id' => 'input-community-image'])->label(false) ?>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
