<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\IngredientCategory;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\IngredientSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ingredient-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'ingredient_category_id')->dropDownList( ArrayHelper::map( IngredientCategory::find()->all(), 'id', 'name'), ['prompt' => 'Select Category']) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'name') ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'price') ?>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-4">
            <?php echo $form->field($model, 'om_id') ?>
        </div>
        <div class="col-sm-4">
            <?php echo $form->field($model, 'sequence') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group pull-right">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
