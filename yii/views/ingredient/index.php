<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IngredientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ingredients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>

<div class="ingredient-index card-box">

    <p class="pull-right">
        <?= Html::a('Create Ingredient', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Image',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::img($data->image, ['width' => '60']);
                }
            ],
            [
                'label' => 'Category',
                'value' => function($data) {
                    return $data->ingredientCategory->name;
                }
            ],
            'name',
            'price',
            'description:ntext',
            // 'calories_count',
            // 'max_quantity',
            // 'sequence',
            // 'is_deleted',
            // 'protein',
            // 'carbs',
            // 'fat',
            // 'om_id',
            // 'pending',
            // 'legend',
            // 'premium',
            // 'menu_id',

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
