<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ingredient */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ingredients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredient-view card-box">

    <p class="card-box">

        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ingredient_category_id',
            'name',
            'price',
            'image',
            'description:ntext',
            'calories_count',
            'max_quantity',
            'sequence',
            'is_deleted',
            'protein',
            'carbs',
            'fat',
            'om_id',
            'twox_id',
            'pending',
            'legend',
            'premium',
            'menu_id',
        ],
    ]) ?>

</div>
