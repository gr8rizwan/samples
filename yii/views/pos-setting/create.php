<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\BranchPosSetting */

$this->title = Yii::t('app', 'Create Branch Pos Setting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Branch Pos Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="branch-pos-setting-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
