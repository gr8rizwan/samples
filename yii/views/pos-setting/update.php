<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BranchPosSetting */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Branch Pos Setting',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Branch Pos Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="branch-pos-setting-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
