<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Branch Pos Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="branch-pos-setting-index">

    <p class="pull-right">
        <?=
        Html::button('New pos vendor', ['value' => Url::to(['/pos-setting/create', 'branch_id' => $model->id]), 'title' => 'Add POS settings', 'class' => 'showModalButton btn btn-info btn-custom waves-effect waves-light']);
        ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => $model->getBranchPosSetting(),
            'pagination' => [
                'pageSize' => 10,
            ]]),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'franchise_vendor_id',
            'server_endpoint',
            'server_url:url',
            'endpoint_vendor',
            //'endpoint_user',
            //'endpoint_password',
            'location_id',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return ($model->status == 1 ? 'Active' : 'Inactive');
                }
            ],

            [
                'class' => 'common\helpers\CustomActionColumn',
                'template' => '{update}',
                'buttons' => [

                    'update' => function ($url, $model) {
                        return Html::button('<i class="ion-edit"></i>', ['value' => Url::to(['/pos-setting/update', 'id' => $model->id]), 'title' => 'Update POS vendor settings', 'class' => 'showModalButton btn btn-info btn-custom waves-effect waves-light']);
                    },
                ]

            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
