<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$vendorInfo = Yii::$app->httpclient->get(Yii::$app->params['MASTER_API'].'setting/pos-vendor');

$vendorList = [];

foreach($vendorInfo as $vendor) {
    $vendorList[] = [
        'id' => $vendor['id'],
        'name' => $vendor['vendor']['name'],
    ];
}

/* @var $this yii\web\View */
/* @var $model backend\models\BranchPosSetting */
/* @var $form yii\widgets\ActiveForm */
?>

<!--Ajax form submit -->
<script src="<?= Yii::getAlias('@web') . '/themes/ubold/assets/js/ajax.submit.js' ?>"></script>

<div class="branch-pos-setting-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'role' => 'form',
            'id' => 'ajaxSubmit',
            'refId' => 'pos-vendor-setting',
        ]
    ]); ?>

    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'franchise_vendor_id')->dropDownList(\yii\helpers\ArrayHelper::map($vendorList, 'id', 'name'), ['prompt' => 'Select Vendor']) ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'location_id')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'server_endpoint')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'server_url')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'endpoint_vendor')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'endpoint_user')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'endpoint_password')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'status')->textInput()->dropDownList(['In-Active', 'Active'], ['prompt' => 'Select Status']) ?>
        </div>
    </div>


    <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-custom waves-effect waves-light" data-dismiss="modal">Close</button>
        <?= Html::submitButton('Update', ['id' => 'ajaxSubmitBtn', 'class' => 'btn btn-info btn-custom waves-effect waves-light']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
