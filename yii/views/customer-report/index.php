<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$baseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/feed/';
$themeBaseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/ubold/';

$this->title = 'Customers database' ;
$this->params['breadcrumbs'][] = $this->title;

$propertyIds = [];
if(isset($groupModel->reportGroupProperties)){
    foreach ($groupModel->reportGroupProperties as $index => $reportGroupProperty) {
        array_push($propertyIds, $reportGroupProperty->property_id);
    }
}

?>


<?php

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'first_name',
        'visible' => in_array(2, $propertyIds) || is_null($groupModel)
    ],
    [
        'attribute' => 'last_name',
        'visible' => in_array(3, $propertyIds) || is_null($groupModel)
    ],
    [
        'attribute' => 'email',
        'visible' => in_array(4, $propertyIds) || is_null($groupModel)
    ],
    [
        'attribute' => 'phone_number',
        'visible' => in_array(5, $propertyIds) || is_null($groupModel)
    ],

    [
        'label' => 'Promo codes used',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            return count($model->promoToCustomer);
        },
        'visible' => in_array(6, $propertyIds) || is_null($groupModel)
    ],

    [
        'label' => 'Promo discount',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {

            if($model->promoToCustomer) {
                return $model->getPromoToCustomer()->select('sum(o.discount) discount')->join('INNER JOIN', 'order o', 'order_id = o.id')->sum('discount');
            } else {
                return 0;
            }

        },
        'visible' => in_array(7, $propertyIds) || is_null($groupModel)
    ],
    [
        'label' => 'Lowest order',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            $html = '';
            if($model->lowestSumOrder) {

                $totlTip = '<b>Date: </b> ' . $model->lowestSumOrder->time .'<br />';
                $totlTip .= '<b>Branch: </b> ' . $model->lowestSumOrder->branch->name;

                $html .= '<div data-toggle="tooltip" data-placement="top" title="" data-original-title="'.$totlTip.'">';
                $html .= Yii::$app->setting->formatePrice($model->lowestSumOrder->total_sum);
                $html .= '</div>';

                return $html;

            } else {
                return '-';
            }
        },
        'visible' => in_array(8, $propertyIds) || is_null($groupModel)
    ],
    [
        'label' => 'Highest order',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            $html = '';
            if($model->highSumOrder) {

                $totlTip = '<b>Date: </b> ' . $model->highSumOrder->time .'<br />';
                $totlTip .= '<b>Branch: </b> ' . $model->highSumOrder->branch->name;

                $html .= '<div data-toggle="tooltip" data-placement="top" title="" data-original-title="'.$totlTip.'">';
                $html .= Yii::$app->setting->formatePrice($model->highSumOrder->total_sum);
                $html .= '</div>';

                return $html;

            } else {
                return '-';
            }

        },
        'visible' => in_array(9, $propertyIds) || is_null($groupModel)
    ],

    [
        'label' => 'Reward points',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            $html = '';
            if($model->loyalty) {

                $totalEarned = $model->getLoyalty()->where(['action' => '+'])->sum('points');
                $totalRedeemed = $model->getLoyalty()->where(['action' => '-'])->sum('points');

                $totlTip = '<b>Earned: </b> ' . $totalEarned .'<br />';
                $totlTip .= '<b>Redeemed: </b> ' . $totalRedeemed;

                $html .= '<div data-toggle="tooltip" data-placement="top" title="" data-original-title="'.$totlTip.'">';
                $html .= (int)$totalEarned - (int)$totalRedeemed;
                $html .= '</div>';
            } else {
                return 0;
            }

            return $html;
        },
        'visible' => in_array(10, $propertyIds) || is_null($groupModel)
    ],
    [
        'label' => 'First Order',
        'format' => 'raw',
        'value' => function($model) {

            $html = '';
            if($model->firstOrder && $model->firstOrder !== null) {

                $totlTip = '<b>Total: </b> ' . (isset($model->firstOrder->total_sum) ? $model->firstOrder->total_sum : '') .'<br />';
                $totlTip .= '<b>Date: </b> ' . (isset($model->firstOrder->time) ? $model->firstOrder->time : '') .'<br />';
                $totlTip .= '<b>Branch: </b> ' . (isset($model->firstOrder->branch->name) ? $model->firstOrder->branch->name : '');

                $html .= '<div data-toggle="tooltip" data-placement="top" title="" data-original-title="'.$totlTip.'">';
                $html .= $model->firstOrder->time;
                $html .= '</div>';

                return $html;

            } else {
                return '----';
            }
        },
        'visible' => in_array(11, $propertyIds) || is_null($groupModel)
    ],

    [
        'label' => 'Last Order',
        'format' => 'raw',
        'value' => function($model) {

            $html = '';
            if($model->lastOrder) {

                $totlTip = '<b>Total: </b> ' . $model->lastOrder->total_sum .'<br />';
                $totlTip .= '<b>Date: </b> ' . $model->lastOrder->time .'<br />';
                $totlTip .= '<b>Branch: </b> ' . $model->lastOrder->branch->name;

                $html .= '<div data-toggle="tooltip" data-placement="top" title="" data-original-title="'.$totlTip.'">';
                $html .= $model->lastOrder->time;
                $html .= '</div>';

                return $html;

            } else {
                return '----';
            }
        },
        'visible' => in_array(12, $propertyIds) || is_null($groupModel)
    ],


    [
        'label' => 'Avg Order',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            return ($model->order ?  Yii::$app->setting->formatePrice($model->getOrder()->average('total_sum')) : '----') ;
        },
        'visible' => in_array(13, $propertyIds) || is_null($groupModel)
    ],

    [
        'label' => 'Number of orders',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            return ($model->order ?  count($model->order) : 0) ;
        },
        'visible' => in_array(14, $propertyIds) || is_null($groupModel)
    ],

    [
        'label' => 'Total Order value',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            return ($model->order ? $model->getOrder()->sum('total_sum') : '----') ;
        },
        'visible' => in_array(15, $propertyIds) || is_null($groupModel)
    ],

    [
        'attribute' => 'type',
        'label' => 'Registered',
        'value' => function($model) {
            return ($model->type === 'guest' ?  'No' : 'Yes') ;
        },
        'visible' => in_array(16, $propertyIds) || is_null($groupModel)
    ],

    [
        'label' => 'Order Lunch',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            return $model->getOrder()->where(['BETWEEN', 'TIME(time)', '11:00:00', '16:00:00'])->count('id');
        },
        'visible' => in_array(17, $propertyIds) || is_null($groupModel)
    ],

    [
        'label' => 'Order Dinner',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            return $model->getOrder()->where(['BETWEEN', 'TIME(time)', '16:00:00', '23:59:59'])->count('id');
        },
        'visible' => in_array(18, $propertyIds) || is_null($groupModel)
    ],

    [
        'label' => 'Preferred order time',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            $preferredTime =  $model->preferredOrderTime();

            $html = '';
            if(count($preferredTime)) {

                $maxs = array_keys($preferredTime, max($preferredTime));

                $totlTip = '';
                foreach($preferredTime as $key => $time) {
                    $totlTip .= '<b>'.$key.': </b> ' . $time .'<br />';
                }

                $html .= '<div data-toggle="tooltip" data-placement="top" title="" data-original-title="'.$totlTip.'">';
                $html .= $maxs[0];
                $html .= '</div>';

                return $html;

            } else {
                return '----';
            }
        },
        'visible' => in_array(19, $propertyIds) || is_null($groupModel)
    ],

    [
        'label' => 'Avg delivery time',
        'format' => 'raw',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            return Yii::$app->dateTime->getSecondToHoursSecond($model->getOrder()->average('`completed_at` - `created_at`'));
        },
        'visible' => in_array(20, $propertyIds) || is_null($groupModel)
    ],

    [
        'attribute' => 'gender',
        'value' => function($model) {
            return is_numeric($model->is_deleted) ? (
            $model->is_deleted == 1 ? 'Male' : 'Female'
            ) : '-';
        },
        'visible' => in_array(21, $propertyIds) || is_null($groupModel)
    ],

    [
        'attribute' => 'created_at',
        'filter' => false,
        'format' => 'raw',
        'value' => function($model) {
            return yii::$app->dateTime->getReadableDateAndTimeFormat($model->created_at);
        },
        'visible' => in_array(22, $propertyIds) || is_null($groupModel)
    ],
    [
        'attribute' => 'is_deleted',
        'value' => function($model) {
            return $model->is_deleted == 1 ? 'Deleted' : 'Active';
        },
        'visible' => in_array(23, $propertyIds) || is_null($groupModel)
    ],
    [
        'attribute' => 'dob',
        'visible' => in_array(24, $propertyIds) || is_null($groupModel)
    ]
];

if(in_array(25, $propertyIds) || is_null($groupModel)){
    foreach ($originModel as $index => $item) {
        $origin = [
            'label' => $item->name,
            'format' => 'raw',
            'contentOptions' =>['class' => 'text-center'],
            'value' => function($model) use ($item) {
                $originData = $model->getOriginOrderCount($item->id);
                if($originData !== null) {
                    return $originData->origin_count;
                }
                return '-';
            }
        ];
        array_push($gridColumns, $origin);
    }
}

if(in_array(26, $propertyIds) || is_null($groupModel)){

    foreach ($branchModel as $index => $item) {
        $branch = [
            'label' => $item->name,
            'format' => 'raw',
            'contentOptions' =>['class' => 'text-center'],
            'value' => function($model) use ($item) {
                $branchData = $model->getBranchCount($item->id);
                if($branchData !== null) {
                    return $branchData->branch_count;
                }
                return '-';
            }
        ];
        array_push($gridColumns, $branch);
    }
}

if(in_array(27, $propertyIds) || is_null($groupModel)){

    $branch = [
        'label' => 'Order Per Area',
        'format' => 'raw',
        'contentOptions' => ['class' => 'text-center'],
        'value' => function ($model) {
            $orderData = $model->getOrder()->select(['count(order.id)  AS prev_order_count', 'area.name AS area_name'])
                ->join('INNER JOIN', 'address', 'order.address_id = address.id')
                ->join('INNER JOIN', 'area', 'address.area = area.id')
                ->groupBy(['area.id'])
                ->orderBy(['prev_order_count' => SORT_DESC])
                ->all();

            $html = '';
            foreach ($orderData as $index => $order) {
                $html .= $order->area_name . ': ' . $order->prev_order_count . '<br />';
            }
            return $html;
        }
    ];
    array_push($gridColumns, $branch);
}

$actions = [
    'class' => 'common\helpers\CustomActionColumn',
    'template'=>'&nbsp;&nbsp;&nbsp;{view} {update}',

];

array_push($gridColumns, $actions);


?>

<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

<div ng-app="swichFeed" ng-controller="reportColumnsController as reportColumns">

    <div class="row">

        <div class="col-lg-4"></div>
        <div class="col-md-6">
            <form role="form">
                <div class="form-group contact-search m-b-30">
                    <input type="text" id="search" class="form-control" placeholder="Search...">
                    <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                </div> <!-- form-group -->
            </form>
        </div>

        <div class="col-md-2">



            &nbsp;
            &nbsp;

            <div class="btn-group dropdown pull-right">

                <button type="button" class="btn btn-default waves-effect waves-light">Actions</button>
                <button type="button" class="btn btn-default waves-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false"><i class="caret"></i></button>
                <ul class="dropdown-menu" role="menu">
                    <?php if(!is_null($groupModel)){ ?>
                    <li><a href="#custom-modal" data-animation="scale" data-plugin="custommodal"
                       data-overlaySpeed="100" data-overlayColor="#36404a">Edit Columns</a>
                    <li class="divider"></li>
                    <?php } ?>
                    <li><a href="#custom-modal" ng-click="resetAll()" data-animation="scale" data-plugin="custommodal"
                       data-overlaySpeed="100" data-overlayColor="#36404a">New Group</a>
                    <li class="divider"></li>
                    <li><a href="#">Automation</a></li>
                </ul>

            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-3">

            <div class="panel panel-color panel-info">
                <div class="panel-body">
                    <?php echo $this->render('_search', ['model' => $searchModel, 'groupModel' => $groupModel, 'reportModel' => $reportModel]); ?>
                </div>
            </div>

            <?php if(!is_null($groupModel)){ ?>
                <div class="panel panel-color panel-info">
                    <div class="panel-body">
                        <h4>Groups:</h4>
                        <ul class="group-list">
                            <?php  foreach($reportModel->reportGroups as $reportGroup){ ?>
                                <li class="report-group-<?=$reportGroup->id?> <?= ($reportGroup->id == $groupModel->id)?'selected':''; ?>">
                                    <a class="<?= ($reportGroup->id == $groupModel->id)?'bold':''; ?>"><?=$reportGroup->name ?></a>
                                    <?php if ($reportGroup->id != $groupModel->id) { ?>
                                        <i ng-click="deleteGroup(<?=$reportGroup->id ?>)" class="pull-right fa fa-times">&nbsp;</i>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

                <div class="clearfix"></div>
            <?php } ?>

        </div>


        <div class="col-lg-9">

            <div class="panel panel-color panel-info">

                <div class="panel-body">

                    <div class="table-responsive customer-report">

                        <?php Pjax::begin(); ?>

                        <?= GridView::widget([
                            'tableOptions' => ['class' => 'table table-hover'],
                            'dataProvider' => $dataProvider,
                            'columns' => $gridColumns,
                        ]); ?>

                        <?php Pjax::end(); ?>
                    </div>
                </div>
            </div>

        </div> <!-- end col -->

    </div>

    <!-- Modal -->
    <div id="custom-modal" class="modal-demo column-selection-modal" >
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only">Close</span>
        </button>
        <h4 class="custom-modal-title">Choose which properties you see</h4>
        <div class="custom-modal-text">
            <div class="row">

                <div class="form-group col-sm-12">
                    <input type="text" ng-model="searchProperty" placeholder="Search Property" class="form-control">
                </div>
                <div class="col-lg-6 col-sm-12 border-right">

                    <h4>All Fields ({{reportProperties.length}})</h4>
                    <div class="properties-list all-properties">

                        <div ng-repeat="field in reportProperties | orderBy:'sort_order' | filter : searchFilter">
                            <div ng-click="toggleSelection(field)" class="report-property propertyItem" ng-class="{'selected': field.selected}">
                                {{field.attribute.replace("_"," ");}}
                            </div>
                        </div>

                    </div>

                </div>

                <div class="col-lg-6 col-sm-12">
                    <h4>Selected Fields ( {{ (reportProperties | filter : {selected:'true'}).length }} )</h4>
                    <div class="properties-list">

                        <div ng-repeat="field in reportProperties | orderBy:'sort_order' | filter: searchFilter" ng-if="field.selected">
                            <div ng-click="toggleSelection(field)" class="report-property propertyItem" ng-class="{'selected': field.selected}">
                                {{field.attribute.replace("_"," ")}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row model-footer">
                <div class="col-sm-12">
                    <div class="form-group ">
                        <div class="col-lg-7 col-sm-12">
                            <input type="hidden" name="groupId" ng-init="groupId='<?=($groupModel)?$groupModel->id:'' ?>'" ng-model="groupId">
                            <input ng-init="groupName='<?=($groupModel)?$groupModel->name:'' ?>'" ng-model="groupName" type="text" placeholder="Group Name, Empty otherwise." class="form-control">
                        </div>
                        <div class="col-lg-5 col-sm-12 pull-right">
                            <button class="btn btn-success" ng-click="submitGroup()">{{ (groupId =='')?'Create':'Save'}}</button>
                            <button class="btn btn-danger" onclick="Custombox.close();">Cancel</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>


<script src="<?= $themeBaseURL; ?>assets/js/jquery.min.js"></script>
<script src="<?= $baseURL; ?>js/angular-timeago.js"></script>
<script src="<?= $baseURL; ?>js/app.js"></script>
<script src="<?= $baseURL; ?>js/reportColumnsFactory.js"></script>
<script src="<?= $baseURL; ?>js/reportColumnsController.js"></script>