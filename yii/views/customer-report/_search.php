<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\CollectionSearch */
/* @var $form yii\widgets\ActiveForm */


?>

<?php $form = ActiveForm::begin([
    'id' => 'search-form',
    'action' => ['index'],
    'method' => 'get',
]); ?>

    <div class="scrol-box height-635">


        <?php if(!is_null($groupModel)){ ?>
            <?= $form->field($model, 'groupId')->dropDownList(ArrayHelper::map($reportModel->reportGroups, 'id', 'name'), [
                'prompt' => 'Select Group',
                'onchange' => '$("#search-form").submit()'
            ]) ?>
        <?php } ?>


        <div class="row">

            <div class="col-sm-12">
                <?= $form->field($model, 'first_name') ?>
            </div>

            <div class="col-sm-12">
                <?= $form->field($model, 'last_name') ?>
            </div>

            <div class="col-sm-12">
                <?= $form->field($model, 'email') ?>
            </div>

            <div class="col-sm-12">
                <?= $form->field($model, 'phone_number') ?>
            </div>

            <div class="col-sm-12">

                <?= $form->field($model, 'from_date')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Start Date'],
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ])->label('Start Date'); ?>

            </div>

            <div class="col-sm-12">

                <?= $form->field($model, 'to_date')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'End Date'],
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ])->label('End Date'); ?>

            </div>

        </div>

        <div class="row">
            <div class="col-sm-12">
                
                <?= $form->field($model, 'retention')->dropDownList(
                    [
                        'all' => 'All',
                        'new' => 'New',
                        'returning' => 'Returning',
                        'lead' => 'Lead'
                    ]) ?>


            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                

                <?= $form->field($model, 'customer_status')->dropDownList(
                    [
                        'all' => 'All',
                        'active' => 'Active',
                        'in-active' => 'In-Active',
                    ]) ?>


            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                

                <?= $form->field($model, 'customer_type')->dropDownList(
                    [
                        'all' => 'All',
                        'registered' => 'Registered',
                        'unregistered' => 'Un Registered',
                    ]) ?>


            </div>
        </div>


    </div>

    <div class="row panel-button">
        <div class="col-sm-12">
            <div class="form-group pull-right">
                <label class="control-label" style="width: 100%">&nbsp;</label>
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

        </div>

    </div>

<?php ActiveForm::end(); ?>