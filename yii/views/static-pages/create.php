<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\StaticPages */

$this->title = 'Create CMS page';
$this->params['breadcrumbs'][] = ['label' => 'CMS page', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="static-pages-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
