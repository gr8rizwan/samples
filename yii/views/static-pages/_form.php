<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\PageTabs;

/* @var $this yii\web\View */
/* @var $model backend\models\StaticPages */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#general" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                    <span class="hidden-xs">Information</span>
                </a>
            </li>
            <li class="">
                <a href="#page-description" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                    <span class="hidden-xs">Page Description</span>
                </a>
            </li>

            <li class="">
                <a href="#data" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                    <span class="hidden-xs">Settings</span>
                </a>
            </li>

        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="general">

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'seo_url')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'meta_description')->textarea(['maxlength' => true]) ?>

                <?= $form->field($model, 'meta_keywords')->textarea(['maxlength' => true]) ?>

                <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>


            </div>

            <div class="tab-pane" id="page-description">

                <?= $form->field($model, 'top_description')->textarea(['rows' => 6, 'class' => "editor-box"]) ?>

                <?= $form->field($model, 'description')->textarea(['rows' => 6, 'class' => "editor-box"]) ?>

                <?= $form->field($model, 'bottom_description')->textarea(['rows' => 6, 'class' => "editor-box"]) ?>


            </div>

            <div class="tab-pane" id="data">

                <?= $form->field($model, 'sort_order')->textInput() ?>

                <?= $form->field($model, 'page_type')->dropDownList(['cms' => 'CMS', 'faq' => 'FAQ', 'both' => 'Both'], ['prompt' => 'Select Type']) ?>

                <?=
                $form->field($model, 'tab_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(PageTabs::find()->where(['=', 'parent_id', 0])->all(),
                        'id',
                        'name'
                    ),
                    'options' => ['placeholder' => 'Select Tabs'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>


                <div class="row">

                    <div class="col-lg-6 col-sm-12">

                        <div id="images" class="form-group">
                            <label class="control-label" for="input-image">Desktop Banner</label>
                            <br />
                            <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                                <img src="<?php echo $model->banner; ?>" alt="" height="125" title="" data-placeholder="no_image.png" />
                            </a>
                            <input type="hidden" name="StaticPages[banner]" value="<?php echo $model->banner; ?>" id="input-image" />

                        </div>

                    </div>

                    <div class="col-lg-6 col-sm-12">

                        <div id="images" class="form-group">
                            <label class="control-label" for="input-image">Mobile Banner</label>
                            <br />
                            <a href="" id="thumb-mobile_banner" data-toggle="image" class="img-thumbnail">
                                <img src="<?php echo $model->mobile_banner; ?>" alt="" height="125" title="" data-placeholder="no_image.png" />
                            </a>
                            <input type="hidden" name="StaticPages[mobile_banner]" value="<?php echo $model->mobile_banner; ?>" id="input-mobile_banner" />

                        </div>

                    </div>

                </div>

                <?= $form->field($model, 'banner_title')->textInput() ?>

                <?= $form->field($model, 'banner_description')->textarea() ?>

                <?= $form->field($model, 'status')->dropDownList(['0' => 'In-Active', '1' => 'Active'], ['prompt' => 'Select Status']) ?>


            </div>
        </div>
    </div>
</div>

<div class="card-box">
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-group pull-right">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-lg' : 'btn btn-primary btn-lg']) ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
