<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menus';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="menu-index card-box">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <p class="pull-right">
        <?= Html::a('Create Menu', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'version',
            'order',
            'data:ntext',

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
