<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\StaticPages;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use kartik\widgets\ColorInput;

/* @var $this yii\web\View */
/* @var $model app\models\Announcements */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="announcements-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'short_description')->textarea(['rows' => 6, 'id' => 'elm1']) ?>

    <?= $form->field($model, 'page_id')->dropDownList(ArrayHelper::map(StaticPages::find()->all(), 'id', 'title'), ['prompt' => 'Select Page']) ?>

    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?=
            $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'End Date'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'todayBtn' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]);
            ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?=
            $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'End Date'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'todayBtn' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]);
            ?>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'start_time')->widget(TimePicker::classname(), [
                'pluginOptions' => [
                    'showSeconds' => true,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                ]
            ]); ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'end_time')->widget(TimePicker::classname(), [
                'pluginOptions' => [
                    'showSeconds' => true,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                ]
            ]); ?>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?=
            $form->field($model, 'background')->widget(ColorInput::classname(), [
                'options' => ['placeholder' => 'Select color ...'],
            ]);
            ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'orderOnline')->dropDownList([ 'Yes' => 'Yes', 'No' => 'No', ], ['prompt' => 'Allow order online']) ?>
        </div>
    </div>

    <?= $form->field($model, 'status')->dropDownList([ 'In-Active', 'Active' ], ['prompt' => 'Select Status']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-default btn-custom waves-effect waves-light' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
