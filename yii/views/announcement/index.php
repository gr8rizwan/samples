<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Announcements');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="announcements-index card-box">

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Create Announcements'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    div.
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'short_description:ntext',
            'page_id',
            'start_date',
            'end_date',
            // 'start_time',
            // 'end_time',
            // 'background',
            // 'orderOnline',
            // 'status',

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>
</div>
