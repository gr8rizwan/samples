<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Banners */
/* @var $form yii\widgets\ActiveForm */

$types = ['home' => 'Home', 'category' => 'Category', 'product' => 'Product', 'account' => 'Account', 'email-templates' => 'Email Template', 'other' => 'Other'];

?>

<div class="banners-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'type')->dropDownList($types, ['prompt' => 'Select Type']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'page')->dropDownList($types, ['prompt' => 'Select Layout']) ?>
        </div>
    </div>

    <?= $form->field($model, 'status')->dropDownList(['0' => 'In-active', '1' => 'Active'], ['prompt' => 'Select status']) ?>

    <div class="row">
        <div class="col-sm-12">

            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Banner Images</b></h4>
                <p class="text-muted m-b-30 font-13">Images should be 1535x865 pixel.</p>


                <table id="images" class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td class="text-left">Title</td>
                        <td class="text-left">Description</td>
                        <td class="text-left">Href(Link)</td>
                        <td class="text-left">Image</td>
                        <td class="text-right">Sort Order</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $image_row = 0; ?>
                    <?php foreach ($model->bannerImages as $BannerImages) { ?>
                        <tr id="image-row<?php echo $image_row; ?>">

                            <input type="hidden" name=images[<?php echo $image_row; ?>][id]" value="<?= $BannerImages->id ?>" placeholder="id" class="form-control" />


                            <td>
                                <div class="form-group">
                                    <input type="text" name=images[<?php echo $image_row; ?>][title]" value="<?= $BannerImages->title ?>" placeholder="Title" class="form-control" />
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <textarea name="images[<?php echo $image_row; ?>][description]" placeholder="description" class="form-control editor-box"> <?= $BannerImages->description ?> </textarea>
                                </div>
                            </td>

                            <td class="text-left" style="width: 30%;">
                                <input type="text" name="images[<?php echo $image_row; ?>][href]" value="<?= $BannerImages->href; ?>" placeholder="Href (Link)" class="form-control" />
                            </td>

                            <td>
                                <div class="form-group">
                                    <label>Desktop Banner</label>
                                    <a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail">
                                        <img src="<?php echo $BannerImages->image; ?>" alt="" title="" data-placeholder="no_image.png" />
                                    </a>
                                    <input type="hidden" name="images[<?php echo $image_row; ?>][image]" value="<?php echo $BannerImages->image; ?>" id="input-image<?php echo $image_row; ?>" />
                                </div>

                                <div class="form-group">
                                    <label>Mobile Banner</label>
                                    <a href="" id="thumb-mobile-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail">
                                        <img src="<?php echo $BannerImages->mobile_image; ?>" alt="" title="" data-placeholder="no_image.png" />
                                    </a>
                                    <input type="hidden" name="images[<?php echo $image_row; ?>][mobile_image]" value="<?php echo $BannerImages->mobile_image; ?>" id="input-mobile-image<?php echo $image_row; ?>" />
                                </div>

                            </td>

                            <td class="text-right" style="width: 10%;">
                                <input type="text" name="images[<?php echo $image_row; ?>][sort_order]" value="<?php echo $BannerImages->sort_order; ?>" placeholder="Sort order" class="form-control" />
                            </td>

                            <td class="text-left">
                                <button type="button" onclick="removeImage('<?= $image_row ?>', '<?= $BannerImages->id; ?>')" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                            </td>
                        </tr>
                        <?php $image_row++; ?>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="5"></td>
                        <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="Add" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                    </tfoot>
                </table>

            </div>


        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    var image_row = <?= $image_row ?>;

    function addImage() {
        html  = '<tr id="image-row'+ image_row +'">';
        html += '<td>';
        html += '    <div class="form-group">';
        html += '      <input type="hidden" name="images[' + image_row + '][id]" value="" placeholder="id" class="form-control" />';
        html += '      <input type="text" name="images[' + image_row + '][title]" value="" placeholder="Title" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td>';
        html += '    <div class="form-group">';
        html += '      <textarea name="images[' + image_row + '][description]" placeholder="description" class="form-control" id="editor-'+image_row+'"> </textarea>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td  style="width: 30%;"><input type="text" name="images[' + image_row + '][href]" value="" placeholder="Href (Link)" class="form-control" /></td>';

        html += '  <td>';

        html += ' <div class="form-group">';
        html += ' <label>Desktop Banner</label><a href="" id="thumb-image' + image_row + '" data-toggle="image" class="img-thumbnail"><img src="<?= Yii::getAlias('@web').'/themes/ubold/assets/images/no_image.png' ?>" alt="" title="" data-placeholder="<?= Yii::getAlias('@web').'/themes/ubold/assets/images/no_image.png' ?>" /></a><input type="hidden" name="images[' + image_row + '][image]" value="" id="input-image' + image_row + '" />';
        html += '</div>';

        html += ' <div class="form-group">';
        html += ' <label>Mobile Banner</label><a href="" id="thumb-mobile_image' + image_row + '" data-toggle="image" class="img-thumbnail"><img src="<?= Yii::getAlias('@web').'/themes/ubold/assets/images/no_image.png' ?>" alt="" title="" data-placeholder="<?= Yii::getAlias('@web').'/themes/ubold/assets/images/no_image.png' ?>" /></a><input type="hidden" name="images[' + image_row + '][mobile_image]" value="" id="input-mobile_image' + image_row + '" />';
        html += '</div>';

        html += '</td>';

        html += '  <td style="width: 10%;"><input type="text" name="images[' + image_row + '][sort_order]" value="" placeholder="Sort order" class="form-control" /></td>';

        html += '  <td><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#images tbody').append(html);

        // initialized editor
        initEditor('editor-' + image_row);

        image_row++;
    }

    var removeImage = function(rowId, imageId) {


        swal({
            title: "Are you sure?",
            text: 'Delete banner image',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plz!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {

                $.ajax({
                    url: '<?= \yii\helpers\Url::to('/banner/remove-image'); ?>?id=' + imageId,
                    type: 'get',
                    success: function (response) {
                        swal("Deleted!", response.message, "success");
                        $('#image-row'+rowId+', .tooltip').remove();
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting banner image.", "error");
            }
        });

    }
</script>
