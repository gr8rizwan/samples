<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model backend\models\Banners */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-box">

    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'type',
            'page',
            [
                'attribute' => 'status',
                'value' =>  ($model->status == 1 ? 'Active' : 'In-active'),
            ],
        ],
    ]) ?>

    <h3>Images</h3>
    <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => $model->getBannerImages(),
            'pagination' => [
                'pageSize' => 10,
            ]]),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'attribute' => 'description',
                'format' => 'raw'
            ],
            'image',
            'sort_order',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return ($model->status == 1 ? 'Active' : 'In-active');
                }
            ],

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
