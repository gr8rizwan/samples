<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CustomerFeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Feedback';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">

                    <div class="table-rep-plugin">
                        <div class="table-responsive" data-pattern="priority-columns">
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    [
                                        'attribute' => 'customer_id',
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($data) {
                                            $html = '';
                                            $html .= strtoupper($data->customer->first_name.' '.$data->customer->last_name).' <br />';
                                            $html .= '<b>'.$data->customer->phone_number.'</b><br />';
                                            $html .= yii::$app->setting->getLatestAddress($data->customer->id).'<br />';
                                            return $html;
                                        }

                                    ],

                                    [
                                        'attribute' => 'Categories',
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($data) {
                                            $category = [];

                                            foreach($data->feedbackDetail as $detail) {
                                                $category[$detail->feedbackType->name] = $detail->feedbackType->name;
                                            }

                                            return implode(",", $category);
                                        }
                                    ],

                                    [
                                        'attribute' => 'order_branch',
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($data) {
                                            return $data->orderBranch->name;
                                        }
                                    ],

                                    [
                                        'attribute' => 'Resolution',
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($data) {
                                            return strtoupper($data->resolution);
                                        }
                                    ],


                                    [
                                        'attribute' => 'created_at',
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($data) {
                                            return yii::$app->dateTime->getReadableDateAndTimeFormat($data->created_at);
                                        }
                                    ],



                                    [
                                        'attribute' => 'status',
                                        'filter' => false,
                                        'format' => 'raw',
                                        'value' => function($data) {
                                            return ($data->status == 0 ? 'Pending' : 'Resolved');
                                        }
                                    ],
                                    [
                                        'class' => 'common\helpers\CustomActionColumn',
                                        'template' => '{view}',
                                    ],

                                ],
                            ]); ?>
                        </div>

                    </div>



                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
    </div>
</div>


