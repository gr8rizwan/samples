<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $model backend\models\CustomerFeedback */

$this->title = 'Feedback';
$this->params['breadcrumbs'][] = ['label' => 'Customer Feedback', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$html = '';
$html .= strtoupper($model->customer->first_name . ' ' . $model->customer->last_name) . ' <br />';
$html .= '<b>' . $model->customer->phone_number . '</b><br />';
$html .= yii::$app->setting->getLatestAddress($model->customer->id) . '<br />';

$category_description = Yii::$app->setting->getFeedbackCategories($model->feedbackDetail);

?>

<?php if ($model->status == '0') { ?>

    <div class="alert alert-warning">
        <p>Feedback is not yet closed</p>
    </div>


<?php } else if ($model->status == '1') { ?>

    <div class="alert alert-success">
        <p>Feedback has been closed with <?= strtoupper($model->resolution) ?> Resolution.</p>
    </div>

<?php } ?>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">

            <div class="row">
                <div class="box box-primary">
                    <!--Feedback Detail -->
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-12">

                            <div class="box-body">

                                <h3>Feedback & Customer detail</h3>

                                <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [

                                        [
                                            'attribute' => 'Feedback Categories',
                                            'format' => 'raw',
                                            'value' => $category_description,

                                        ],

                                        'feedback:ntext',

                                        [
                                            'attribute' => 'resolution',
                                            'value' => strtoupper($model->resolution),

                                        ],

                                        [
                                            'attribute' => 'Created by',
                                            'format' => 'raw',
                                            'value' => $model->createdBy->first_name . ' ' . $model->createdBy->last_name,

                                        ],

                                        [
                                            'attribute' => 'created_at',
                                            'value' => yii::$app->dateTime->getReadableDateAndTimeFormat($model->created_at),

                                        ],

                                        [
                                            'attribute' => 'status',
                                            'value' => ($model->status == 0 ? 'Pending' : 'Resolved'),

                                        ],

                                    ],
                                ]) ?>

                            </div>
                        </div>

                        <div class="col-sm-6">
                            <!--If feedback is on Order then Order detail-->

                            <?php if ($model->order_id) { ?>

                                <div class="box-body" ng-app="swichFeed" ng-controller="feedController as feed">

                                    <h3 class="text-left"> Customer & Order Details </h3>

                                    <?= DetailView::widget([
                                        'model' => $model,
                                        'attributes' => [

                                            [
                                                'attribute' => 'customer_id',
                                                'format' => 'raw',
                                                'value' => $html,

                                            ],

                                            [
                                                'attribute' => 'Order date',
                                                'format' => 'raw',
                                                'value' => yii::$app->dateTime->getReadableDateAndTimeFormat($model->order->time),

                                            ],

                                            [
                                                'attribute' => 'source',
                                                'format' => 'raw',
                                                'value' => $model->order->source,


                                            ],

                                            [
                                                'attribute' => 'Branch',
                                                'format' => 'raw',
                                                'value' => $model->orderBranch->name,

                                            ],

                                            [
                                                'attribute' => 'Discount',
                                                'format' => 'raw',
                                                'value' => Yii::$app->setting->formatePrice($model->order->discount),

                                            ],


                                            [
                                                'attribute' => 'Total Sum',
                                                'format' => 'raw',
                                                'value' => Yii::$app->setting->formatePrice($model->order->total_sum),

                                            ],


                                            [
                                                'attribute' => 'Payment Type',
                                                'format' => 'raw',
                                                'value' => $model->order->payment_type,

                                            ],


                                            [
                                                'attribute' => 'Comments',
                                                'format' => 'raw',
                                                'value' => $model->order->comments,
                                            ],


                                        ],
                                    ]) ?>

                                    <br/>

                                    <p class="text-right">
                                        <?= Html::button('Order Details', [ 'ng-click' => 'showOrderDetails(' . $model->order_id .')', 'title' => 'Order Detail', 'class' => 'btn btn-full-width']); ?>

                                    </p>

                                    <?php echo $this->render('_order_modal', []); ?>

                                </div>

                            <?php } else { ?>

                                <div class="box-body">

                                    <h3 class="text-left"> Customer & Branch Details </h3>

                                    <?= DetailView::widget([
                                        'model' => $model,
                                        'attributes' => [

                                            [
                                                'attribute' => 'customer_id',
                                                'format' => 'raw',
                                                'value' => $html,

                                            ],

                                            [
                                                'attribute' => 'Branch Name',
                                                'format' => 'raw',
                                                'value' => $model->orderBranch->name,

                                            ],
                                            [
                                                'attribute' => 'Detail Address',
                                                'format' => 'raw',
                                                'value' => $model->orderBranch->detailed_address,

                                            ],
                                            [
                                                'attribute' => 'Opening timing',
                                                'format' => 'raw',
                                                'value' => $model->orderBranch->open_time,

                                            ],
                                            [
                                                'attribute' => 'Closing timing',
                                                'format' => 'raw',
                                                'value' => $model->orderBranch->close_time,

                                            ]

                                        ],
                                    ]) ?>

                                    <br/>

                                </div>

                            <?php } ?>

                        </div>

                    </div>
                </div>

                <!--Resolution-->

                <h3 class="box-title pull-left">Resolution</h3>

                <?php

                if ((int)$model->parent_id > 0) {
                    echo Html::a('View Parent Feedback', ['/customer-feedback/view', 'id' => $model->parent_id], ['target' => '_blank', 'title' => 'View Parent feedback', 'class' => 'btn btn-success pull-right', 'onclick' => "this.disabled=true"]);

                }

                if ($model->feedbackParent != null) {
                    echo Html::a('View Resolution feedback', ['/customer-feedback/view', 'id' => $model->feedbackParent->id], ['target' => '_blank', 'title' => 'View Resolution feedback', 'class' => 'btn btn-success pull-right', 'onclick' => "this.disabled=true"]);
                }

                ?>

                <div class="" style="clear: both">


                    <!--Feedback Detail -->
                    <div class="row">

                        <!-- Feedback Resolution detail -->
                        <?php if ($model->feedbackResolution != null) { ?>
                            <div class="col-lg-6 col-md-6 col-sm-12">

                                <div class="box-body">

                                    <h3>Resolution Cart</h3>

                                    <?= DetailView::widget([
                                        'model' => $model,
                                        'attributes' => [

                                            [
                                                'attribute' => 'Description',
                                                'format' => 'raw',
                                                'value' => $model->feedbackResolution->description,

                                            ],

                                            [
                                                'attribute' => 'Status',
                                                'format' => 'raw',
                                                'value' => $model->feedbackResolution->status,

                                            ],

                                            [
                                                'attribute' => 'created_at',
                                                'format' => 'raw',
                                                'value' => yii::$app->dateTime->getReadableDateAndTimeFormat($model->feedbackResolution->created_at),

                                            ],
                                        ],
                                    ]) ?>

                                    <p class="text-right">
                                        <?= Html::button('Resolution Item', ['value' => Url::to(['/customer-feedback/view-items', 'feedback_id' => $model->id]), 'title' => 'Resolution Item', 'class' => 'showModalButton btn btn-full-width']); ?>
                                    </p>

                                </div>


                            </div>
                            <?php if ($model->feedbackResolution->status == 'close' && !empty($model->feedbackResolution->order_id)) { ?>

                                <div class="col-lg-6 col-md-6 col-sm-12">

                                    <div class="box-body">

                                        <h3 class="text-left"> Resolution Order detail </h3>

                                        <?= DetailView::widget([
                                            'model' => $model,
                                            'attributes' => [

                                                [
                                                    'attribute' => 'Payment Type',
                                                    'format' => 'raw',
                                                    'value' => $model->feedbackResolution->order->payment_type,

                                                ],

                                                [
                                                    'attribute' => 'Total Sum',
                                                    'format' => 'raw',
                                                    'value' => Yii::$app->setting->formatePrice($model->feedbackResolution->order->total_sum),

                                                ],

                                                [
                                                    'attribute' => 'Discount',
                                                    'format' => 'raw',
                                                    'value' => Yii::$app->setting->formatePrice($model->feedbackResolution->order->discount),

                                                ],


                                                [
                                                    'attribute' => 'Delivery type',
                                                    'format' => 'raw',
                                                    'value' => $model->feedbackResolution->order->delivery_type,

                                                ],

                                                [
                                                    'attribute' => 'Payment Type',
                                                    'format' => 'raw',
                                                    'value' => $model->feedbackResolution->order->payment_type,

                                                ],

                                                [
                                                    'attribute' => 'source',
                                                    'format' => 'raw',
                                                    'value' => $model->feedbackResolution->order->source,


                                                ],

                                                [
                                                    'attribute' => 'Comments',
                                                    'format' => 'raw',
                                                    'value' => $model->feedbackResolution->order->comments,
                                                ],


                                                [
                                                    'attribute' => 'time',
                                                    'format' => 'raw',
                                                    'value' => yii::$app->dateTime->getReadableDateAndTimeFormat($model->feedbackResolution->order->time),

                                                ],

                                                [
                                                    'attribute' => 'Delivery',
                                                    'format' => 'raw',
                                                    'value' => ($model->feedbackResolution->order->payment_type == 'Pickup From Store' ? $model->feedbackResolution->order->payment_type : Yii::$app->setting->formatAddress($model->feedbackResolution->order->address_id)),

                                                ],


                                            ],
                                        ]) ?>

                                        <br/>

                                        <p class="text-right">
                                            <?= Html::button('Resolution Order', ['value' => Url::to(['/order/view', 'id' => $model->feedbackResolution->order_id]), 'title' => 'Resolution Order', 'class' => 'showModalButton btn btn-full-width']); ?>
                                        </p>

                                    </div>


                                </div>

                            <?php } ?>
                        <?php } ?>

                    </div>
                </div>

            </div>

        </div>

        <!-- If resolution is assigned then should be history detail -->
        <?php if ($model->resolution == 'assigned') { ?>

            <?php Pjax::begin(['id' => 'assignment-detail']) ?>

            <section id="cd-timeline" class="cd-container" style="width: 100%">

            <div class="cd-timeline-block">

                <div class="cd-timeline-img cd-success">
                    <i class="fa fa-tag"></i>
                </div>
                <!-- cd-timeline-img -->

                <div class="cd-timeline-content">
                    <h3>Assigned on</h3>

                    <p>Assigned resolution created</p>
                    <span
                        class="cd-date"><?= yii::$app->dateTime->getReadableDateAndTimeFormat($model->created_at) ?></span>
                </div>
                <!-- cd-timeline-content -->
            </div> <!-- cd-timeline-block -->


            <?php $count = 0;
            foreach ($model->feedbackAssignment as $assignment) { ?>
                <div class="cd-timeline-block">

                    <div class="cd-timeline-img cd-success">
                        <i class=" md-message"></i>
                    </div> <!-- cd-timeline-img -->



                <div class="cd-timeline-content">
                    <h3><?php if ($assignment->assigned_by == $assignment->assigned_to) { ?>
                            <b> <?= strtoupper($assignment->assignedBy->username) ?>  </b> commented on this assignment

                        <?php } else { ?>
                            <b> <?= strtoupper($assignment->assignedBy->username) ?> </b>  assign this ticket to
                            <b><?= strtoupper($assignment->assignedTo->username) ?></b>
                        <?php } ?>
                    </h3>

                    <p><?= $assignment->description; ?></p>

                    <?php
                    if ($assignment->status == 'open' && $assignment->assigned_to == Yii::$app->user->id) {
                        echo Html::button('Update status', ['value' => Url::to(['customer-feedback/feedback-assignment', 'assignment_id' => $assignment->id, 'type' => 'update']), 'title' => 'Update Status', 'class' => 'showModalButton btn btn-primary btn-rounded waves-effect waves-light m-t-15']);
                    }

                    ?>

                    <?php
                    if ($assignment->status == 'open' && $assignment->assigned_to == Yii::$app->user->id) {
                        echo Html::button('Close Feedback', ['value' => Url::to(['customer-feedback/feedback-assignment', 'assignment_id' => $assignment->id, 'type' => 'close']), 'title' => 'Close Feedback', 'class' => 'showModalButton btn btn-primary btn-rounded waves-effect waves-light m-t-15']);
                    }

                    ?>
                    <?php if (count($model->feedbackAssignment) - 1 == $count): ?>
                        <?php

                        if ($model->status == 'open' && $model->resolution == 'assigned') {
                            echo Html::a('Create Resolution Cart', Yii::$app->params['feEndPoint'] . 'feedbackLanding/number/' . $model->customer->phone_number . '/agent/' . Yii::$app->user->id . '/feedbackId/' . $model->id, ['target' => '_blank', 'title' => 'Create Resolution Cart', 'class' => 'btn btn-primary btn-rounded waves-effect waves-light m-t-15', 'onclick' => "this.disabled=true"]);
                        }

                        ?>
                    <?php endif; ?>
                    <span class="cd-date"><?= Yii::$app->dateTime->getAgoTime($assignment->created_at); ?></span>
                    </div> <!-- cd-timeline-content -->

                </div> <!-- cd-timeline-block -->
                <?php $count++;
            } ?>

            </section> <!-- cd-timeline -->

            <?php Pjax::end() ?>

        <?php } ?>

    </div>
</div>



