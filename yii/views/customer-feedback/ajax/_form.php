<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\CustomerFeedback */
/* @var $form yii\widgets\ActiveForm */

?>

<<<<<<< HEAD
=======
<!--Ajax form submit -->
<script src="<?= Yii::getAlias('@web') . '/themes/ubold/assets/js/ajax.submit.js' ?>"></script>

>>>>>>> CCB-65-customer-profile

<div class="customer-feedback-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'ajaxSubmit',
            'refId' => 'assignment-detail',
        ]

    ]); ?>

    <?= $form->field($model, 'feedback_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'assigned_by')->hiddenInput()->label(false) ?>


    <?php if($type == 'update') { ?>
        <?=
        $form->field($model, 'assigned_to')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(User::find()->all(),
                'id',
                function($model) {
                    return $model->email;
                }
            ),
            'options' => ['placeholder' => 'Select User'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    <?php } else { ?>
        <?= $form->field($model, 'assigned_to')->hiddenInput()->label(false) ?>
    <?php } ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php if($type == 'update') { ?>
        <?= $form->field($model, 'status', ['options' => ['value'=> 'open'] ])->hiddenInput()->label(false) ?>
    <?php } else { $model->status = 'close'; ?>
        <?= $form->field($model, 'status', ['options' => ['value'=> 'close'] ])->hiddenInput()->label(false) ?>
    <?php } ?>



    <div class="modal-footer">

        <?php if($type == 'update') { ?>
            <?= Html::submitButton('Update status', ['id' => 'ajaxSubmitBtn', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php } else { ?>
            <?= Html::submitButton('Close Feedback', ['id' => 'ajaxSubmitBtn', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'data-confirm' => Yii::t('yii', 'Are you sure you want to close this feedback?')]) ?>
        <?php } ?>

        <a href="#" class="btn btn-success close_link" data-dismiss="modal">Close</a>
    </div>


    <?php ActiveForm::end(); ?>



</div>
