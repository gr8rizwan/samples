<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */


$provider = new ArrayDataProvider([
    'allModels' => $model['feedbackResolution']['items'],
]);


$this->title = 'Resolution items';
?>

<div class="order-view">

    <?= GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Name',
                'filter' => false,
                'format' => 'raw',
                'value' => function($data) {
                    $html = '<b>'.$data['name'].'</b><br />';
                    $html .= '<p>'.$data['description'].'<p/>';
                    return $html;
                }
            ],

            [
                'attribute' => 'Quantity',
                'filter' => false,
                'format' => 'raw',
                'value' => function($data) {
                    return $data['quantity'];
                }
            ],
        ],
    ]); ?>

    <div class="modal-footer">
        <a href="#" class="btn btn-inverse btn-custom waves-effect waves-light close_link" data-dismiss="modal">Close</a>
    </div>

</div>
