<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Customer;
use backend\models\Branch;

/* @var $this yii\web\View */
/* @var $model backend\models\CustomerFeedback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-feedback-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'customer_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Customer::find()->all(),
            'id',
            function($model) {
                return $model->first_name.' '.$model->last_name. '('. $model->phone_number .')';
            }
        ),
        'options' => ['placeholder' => 'Select Customer'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'order_id')->textInput() ?>

    <?=
    $form->field($model, 'branch_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Branch::find()->all(),
            'id',
            'name'
        ),
        'options' => ['placeholder' => 'Select Branch'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'rating')->textInput() ?>

    <?= $form->field($model, 'feedback')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList([ '0' => 'In-Active', '1' => 'Active', ], ['prompt' => 'Select Status']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
