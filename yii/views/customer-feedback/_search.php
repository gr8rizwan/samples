<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use backend\models\Branch;

/* @var $this yii\web\View */
/* @var $model backend\models\CollectionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-box">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?= $form->field($model, 'order_branch')->dropDownList(ArrayHelper::map(Branch::find()->all(), 'id', 'name'), ['prompt'=>'Select branch']) ?>
        </div>


        <div class="col-lg-3 col-md-3 col-sm-12">

            <?=
            $form->field($model, 'customer_id')->widget(Select2::classname(), [
                'initValueText' => $model->customer_id, // set the initial display text
                'options' => ['placeholder' => 'Select Customer'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 4,
                    'ajax' => [
                        'url' => Url::to(['customer/customer-list']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(customer) { return customer.name; }'),
                    'templateSelection' => new JsExpression('function (customer) { return customer.name; }'),
                ],
            ]);

            ?>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList([ '0' => 'Pending', '1' => 'Resolved'], ['prompt' => 'Select Status']) ?>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?=  $form->field($model, 'created_at')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Reported at'],
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]); ?>

        </div>

        <div class="col-lg-6 col-md-6 col-sm-12">
            <?= $form->field($model, 'feedback') ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?= $form->field($model, 'resolution')->dropDownList([ 'immediately' => 'Immediately', 'later' => 'Later', 'assigned' => 'Assigned'], ['prompt' => 'Select Status']) ?>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="form-group pull-right">
                <label class="control-label" style="width: 100%">&nbsp;</label>
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

        </div>



    </div>

    <?php ActiveForm::end(); ?>

</div>

