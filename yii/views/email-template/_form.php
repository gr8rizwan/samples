<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Banners;

/* @var $this yii\web\View */
/* @var $model app\models\EmailTemplates */
/* @var $form yii\widgets\ActiveForm */

$positions = ['primary' => 'Primary', 'secondary' => 'Secondary'];
$banners = ArrayHelper::map( Banners::find()->where(['type' => 'email-templates', 'status' => 1])->all(), 'id', 'name');
$templateKeys = [
    'order-confirmation' => 'Order Confirmation',
    'sign-up' => 'SignUp',
    'reset-password' => 'Reset Password',
    'forgot-password' => 'Forgot Password',
    'contact-us' => 'Contact us',
    'catering' => 'Catering'
];

?>

<div class="email-templates-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'template_key')->dropDownList($templateKeys, ['prompt' => 'Select template']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'description')->textarea(['maxlength' => true, 'id' => 'elm1']) ?>
        </div>
    </div>

    <?= $form->field($model, 'status')->dropDownList(['In-Active', 'Active'], ['prompt' => 'Select Status']) ?>

    <div class="row">
        <div class="col-sm-12">
            <h2>Add template blocks</h2>
            <table id="blocks" class="table table-striped table-bordered table-hover blocks-table">
                <thead>
                <tr>
                    <td class="text-left">Column</td>
                    <td class="text-left">Block Banner</td>
                    <td class="text-left">Block Position</td>
                    <td class="text-right">Sort Order</td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                <?php $block_row = 0; ?>
                <?php foreach ($model->emailBlocks as $block) { ?>
                    <tr id="image-row<?php echo $block_row; ?>">

                        <input type="hidden" name="EmailTemplates[blocks][<?php echo $block_row; ?>][id]" value="<?= $block->id ?>" placeholder="id" class="form-control" />


                        <td class="text-left">
                            <div class="form-group">
                                <input type="text" name="EmailTemplates[blocks][<?php echo $block_row; ?>][column]" value="<?= $block->column ?>" placeholder="Column" class="form-control" />
                            </div>
                        </td>

                        <td class="text-left">
                            <select name="EmailTemplates[blocks][<?php echo $block_row; ?>][banner_id]" class="form-control">
                                <?php foreach($banners as $key => $value ) { ?>
                                    <?php if($block->banner_id == $key) { ?>
                                        <option value="<?= $key ?>" selected><?= $value ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $key ?>"><?= $value ?></option>
                                    <?php } ?>

                                <?php } ?>
                            </select>
                        </td>

                        <td class="text-left">
                            <select name="EmailTemplates[blocks][<?php echo $block_row; ?>][position]" class="form-control">
                                <?php foreach($positions as $key => $value ) { ?>
                                    <?php if($block->position == $key) { ?>
                                        <option value="<?= $key ?>" selected><?= $value ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $key ?>"><?= $value ?></option>
                                    <?php } ?>

                                <?php } ?>
                            </select>
                        </td>

                        <td class="text-right" style="width: 10%;">
                            <input type="text" name="EmailTemplates[blocks][<?php echo $block_row; ?>][sort_order]" value="<?php echo $block->sort_order; ?>" placeholder="Sort order" class="form-control" />
                        </td>

                        <td class="text-left">
                            <button type="button" onclick="$('#image-row<?php echo $block_row; ?>, .tooltip').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                        </td>
                    </tr>
                    <?php $block_row++; ?>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="4"></td>
                    <td class="text-left"><button type="button" onclick="addBlock();" data-toggle="tooltip" title="Add" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script type="text/javascript">
    var block_row = <?= $block_row ?>;

    function addBlock() {
        html  = '<tr id="image-row'+ block_row +'">';
        html += '<td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <input type="hidden" name="EmailTemplates[blocks][' + block_row + '][id]" value="" placeholder="id" class="form-control" />';
        html += '      <input type="text" name="EmailTemplates[blocks][' + block_row + '][column]" value="" placeholder="column" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '<td class="text-left">';
        html += '<select name="EmailTemplates[blocks][' + block_row + '][banner_id]" class="form-control">';
            <?php foreach($banners as $key => $value ) { ?>
                html += ' <option value="<?= $key ?>"><?= $value ?></option>';
            <?php } ?>
        html += '</select>';
        html += '</td>';

        html += '<td class="text-left">';
        html += '<select name="EmailTemplates[blocks][' + block_row + '][positions]" class="form-control">';
        <?php foreach($positions as $key => $value ) { ?>
            html += ' <option value="<?= $key ?>"><?= $value ?></option>';
        <?php } ?>
        html += '</select>';
        html += '</td>';

        html += '  <td class="text-right" style="width: 10%;"><input type="text" name="EmailTemplates[blocks][' + block_row + '][sort_order]" value="" placeholder="Sort order" class="form-control" /></td>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + block_row  + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#blocks tbody').append(html);

        block_row++;
    }
</script>
