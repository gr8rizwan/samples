<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EmailTemplates */

$this->title = Yii::t('app', 'Create Email Templates');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Email Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-templates-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
