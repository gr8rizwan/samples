<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\EmailTemplates */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Email Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-templates-view card-box">

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'template_key',
            'status',
        ],
    ]) ?>

    <h3>Images</h3>
    <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => $model->getEmailBlocks(),
            'pagination' => [
                'pageSize' => 10,
            ]]),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'column',
            'banner_id',
            'template_id',
            'position',
            'sort_order',
        ],
    ]); ?>

</div>
