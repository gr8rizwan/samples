<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReportGroup */

$this->title = Yii::t('app', 'Create Report Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Report Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="report-group-create card card-box">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>

</div>
