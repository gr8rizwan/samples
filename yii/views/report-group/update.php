<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReportGroup */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Report Group',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Report Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row">

    <div class="report-group-create card card-box">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>

</div>
