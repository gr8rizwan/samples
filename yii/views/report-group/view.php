<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReportGroup */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Report Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

use app\models\ReportTypeProperties;
$properties = ReportTypeProperties::find()->where(['type_id' => $model->type_id])->all();

?>

<div class="row">

    <div class="report-group-create card card-box">

        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'type_id',
                'name',
            ],
        ]) ?>

    </div>


    <div class="row">
        <div class="col-sm-12">

            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Group Properties</b></h4>

                <table id="images" class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td class="text-left">Property</td>
                        <td class="text-left">Sort Order</td>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $image_row = 0; ?>
                    <?php foreach ($model->reportGroupProperties as $property) { ?>
                        <tr id="image-row<?php echo $image_row; ?>">


                            <td>
                                <div class="form-group">
                                    <select name="ReportGroup[properties][<?php echo $image_row; ?>][property_id]" class="form-control">
                                        <?php foreach($properties as $typePro) { ?>
                                            <?php if( $property->property_id === $typePro->id ) { ?>
                                                <option value="<?= $typePro->id ?>" selected><?= $typePro->attribute ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $typePro->id ?>"><?= $typePro->attribute ?></option>
                                            <?php } ?>
                                        <?php } ?>

                                    </select>
                                </div>
                            </td>

                            <td class="text-right" style="width: 10%;">
                                <input type="text" name="ReportGroup[properties][<?php echo $image_row; ?>][sort_order]" value="<?php echo $property->sort_order; ?>" placeholder="Sort order" class="form-control" />
                            </td>


                        </tr>
                        <?php $image_row++; ?>
                    <?php } ?>
                    </tbody>

                </table>

            </div>


        </div>
    </div>

</div>

