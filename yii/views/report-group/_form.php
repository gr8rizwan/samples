<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ReportTypeProperties;

/* @var $this yii\web\View */
/* @var $model app\models\ReportGroup */
/* @var $form yii\widgets\ActiveForm */


$properties = ReportTypeProperties::find()->where(['type_id' => $model->type_id])->all();
?>

<div class="report-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


    <div class="row">
        <div class="col-sm-12">

            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Group Properties</b></h4>

                <table id="images" class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td class="text-left">Property</td>
                        <td class="text-left">Sort Order</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $image_row = 0; ?>
                    <?php foreach ($model->reportGroupProperties as $property) { ?>
                        <tr id="image-row<?php echo $image_row; ?>">

                            <input type="hidden" name="ReportGroup[properties][<?php echo $image_row; ?>][id]" value="<?= $property->id ?>" placeholder="id" class="form-control" />


                            <td>
                                <div class="form-group">
                                    <select name="ReportGroup[properties][<?php echo $image_row; ?>][property_id]" class="form-control">
                                        <?php foreach($properties as $typePro) { ?>
                                            <?php if( $property->property_id === $typePro->id ) { ?>
                                                <option value="<?= $typePro->id ?>" selected><?= $typePro->attribute ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $typePro->id ?>"><?= $typePro->attribute ?></option>
                                            <?php } ?>
                                        <?php } ?>

                                    </select>
                                 </div>
                            </td>

                            <td class="text-right" style="width: 10%;">
                                <input type="text" name="ReportGroup[properties][<?php echo $image_row; ?>][sort_order]" value="<?php echo $property->sort_order; ?>" placeholder="Sort order" class="form-control" />
                            </td>

                            <td class="text-left">
                                <button type="button" onclick="removeImage('<?= $image_row ?>', '<?= $property->id; ?>')" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                            </td>
                        </tr>
                        <?php $image_row++; ?>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2"></td>
                        <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="Add" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                    </tfoot>
                </table>

            </div>


        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script type="text/javascript">
    var image_row = <?= $image_row ?>;

    function addImage() {
        html  = '<tr id="image-row'+ image_row +'">';


        html += '  <td>';
        html += '    <div class="form-group">';
        html += '      <select  name="ReportGroup[properties][' + image_row + '][property_id]" class="form-control">';
        <?php foreach($properties as $typePro) { ?>
         html += '      <option value="<?= $typePro->id ?>"><?= $typePro->attribute ?></option>';
        <?php } ?>
        html += '      </select>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td><input type="text" name="ReportGroup[properties][' + image_row + '][sort_order]" value="'+image_row+'" placeholder="Sort order" class="form-control" /></td>';

        html += '  <td><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#images tbody').append(html);

        // initialized editor
        initEditor('editor-' + image_row);

        image_row++;
    }

    var removeImage = function(rowId, imageId) {


        swal({
            title: "Are you sure?",
            text: 'Delete the property',
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plz!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {

                $.ajax({
                    url: '<?= \yii\helpers\Url::to('/report-group/delete-property'); ?>?id=' + imageId,
                    type: 'get',
                    success: function (response) {
                        swal("Deleted!", response.message, "success");
                        $('#image-row'+rowId+', .tooltip').remove();
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting property.", "error");
            }
        });

    }
</script>
