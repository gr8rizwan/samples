<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Forms;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\FormsDataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="forms-data-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'form_id')->dropDownList(ArrayHelper::map(Forms::find()->all(), 'id', 'title'), ['prompt' => 'Select Form']) ?>
        </div>

        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'created_at')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter birth date ...'],
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'todayBtn' => true,
                    'format' => 'yyyy-mm-dd',
                    'autoclose' => true,
                ]
            ]);
            ?>
        </div>

       <!-- <div class="col-lg-4 col-sm-12">
            <?/*= $form->field($model, 'source') */?>
        </div>-->

    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group pull-right">
                <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
