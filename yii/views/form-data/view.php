<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FormsData */

$this->title = $model->form->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Forms Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-box">
    <div >

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    "attribute" => "title",
                    'value' => $model->form->title,
                    "format" => "raw",
                ],
                [
                    "attribute" => "recipients",
                    'value' => $model->form->notification_emails,
                    "format" => "raw",
                ],
                'source',
                'browser',
                'created_at',
                [
                    "attribute" => "recipients",
                    'label' => 'Form Fields',
                    'format' => 'html',
                    'value' =>call_user_func(function ($data) {

                        $field = '<p>&nbsp;</p>';
                        foreach($data->formDataFields as $formField) {
                            $field .= '<p><strong>' . $formField->attribute . " </strong>: " . $formField->value . "</p><br />";
                        }
                        return $field;
                    }, $model),
                ],
            ],
        ]) ?>

    </div>
</div>
