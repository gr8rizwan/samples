<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\CategoryType;

/* @var $this yii\web\View */
/* @var $model app\models\AgentIncentive */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Generate Agent incentive report & email';
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="card-box">


    <?php if(Yii::$app->session->getFlash('success')) { ?>

        <div class="alert alert-success">
            <strong>Success!</strong> <?= Yii::$app->session->getFlash('success'); ?>
        </div>

    <?php } ?>

    <div class="order-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'categories')->widget(Select2::classname(), [
            'data' => ArrayHelper::map( CategoryType::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Select a state ...', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>

        <?=
        $form->field($model, 'fromDate')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Enter From Date'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);

        ?>

        <?=
        $form->field($model, 'toDate')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Enter From Date'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);

        ?>

        <div class="form-group">
            <?= Html::submitButton('Email Me', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
