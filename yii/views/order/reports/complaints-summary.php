<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'customer_id',
        'format' => 'raw',
        'value' => function($data) {

            $html = 'N/A';

            if($data->customer) {
                $html = '';
                $html .= strtoupper($data->customer->first_name . ' ' . $data->customer->last_name) . '</a> ';
                $html = Html::a($html, ['customer/view', 'id' => $data->customer->id], ['target' => '_blank', 'data-pjax' => 'false']);
                $html .= '<br /><b>' . $data->customer->phone_number . '</b><br />';
            }

            return $html;
        }
    ],
    [
        'attribute' => 'Area',
        'format' => 'raw',
        'value' => function( $model ) {
            $html = (int)$model->address_id > 0 ? yii::$app->setting->formatAddress($model->address_id, true) : 'Pickup';
            return $html;
        }
    ],
    [
        'attribute' => 'total_sum',
        'value' => function($model) {
            return Yii::$app->setting->formatePrice($model->total_sum);
        }
    ],
    [
        'attribute' => 'created_at',
        'format' => 'raw',
        'value' => function($model){
            return $model->feedback_created_at;
            return yii::$app->dateTime->getReadableDateAndTimeFormat($model->created_at, 'M j, Y') .
                ' ' . yii::$app->dateTime->getReadableDateAndTimeFormat($model->created_at, 'G:i:s');
        }
    ]
];

?>

<div class="order-index card-box">

    <?php Pjax::begin(['id' => 'modal-grid-LOS']); ?>


    <div class="row table-responsive">


        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'footerRowOptions'=>['style'=>'font-weight:bold;background-color: lightgray;color: black;', 'class' => 'text-primary'],
        'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
        'columns' => $gridColumns,
    ]);
        ?>


    </div>

    <?php Pjax::end(); ?>


    <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-custom waves-effect waves-light" data-dismiss="modal">Close</button>
    </div>

</div>