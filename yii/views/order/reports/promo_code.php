<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'customer_id',
        'format' => 'raw',
        'value' => function($data) {

            $html = 'N/A';

            if($data->customer) {
                $html = '';
                $html .= strtoupper($data->customer->first_name . ' ' . $data->customer->last_name) . '</a> ';
                $html = Html::a($html, ['customer/view', 'id' => $data->customer->id], ['target' => '_blank', 'data-pjax' => 'false']);
                $html .= '<br /><b>' . $data->customer->phone_number . '</b><br />';
            }

            return $html;
        }
    ],
    [
        'attribute' => 'Area',
        'format' => 'raw',
        'value' => function( $model ) {
            return (int)$model->address_id > 0 ? yii::$app->setting->formatAddress($model->address_id, true) : 'Pickup';
        }
    ],
    [
        'attribute' => 'branch_id',
        'format' => 'raw',
        'value' => function($data) {
            return $data->branch->name;
        }
    ],
    [
        'attribute' => 'total_sum',
        'value' => function($model) {
            return Yii::$app->setting->formatePrice($model->total_sum);
        }
    ],
    'source',
    [
        'attribute' => 'time',
        'value' => function($model){
            return yii::$app->dateTime->getReadableDateAndTimeFormat($model->time);
        }
    ]
];

?>

<div class="order-index card-box">

    <div class="row table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'footerRowOptions'=>['style'=>'font-weight:bold;background-color: lightgray;color: black;', 'class' => 'text-primary'],
        'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
        'columns' => $gridColumns,
    ]); ?>
    </div>

</div>

<div class="modal-footer">
    <button type="button" class="btn btn-primary btn-custom waves-effect waves-light" data-dismiss="modal">Close</button>
</div>