<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'customer_id',
        'format' => 'raw',
        'value' => function($data) {

            $html = 'N/A';

            if($data->customer) {
                $html = '';
                $html .= strtoupper($data->customer->first_name . ' ' . $data->customer->last_name) . '</a> ';
                $html = Html::a($html, ['customer/view', 'id' => $data->customer->id], ['target' => '_blank', 'data-pjax' => 'false']);
                $html .= '<br /><b>' . $data->customer->phone_number . '</b><br />';
            }

            return $html;
        }
    ],
    [
        'attribute' => 'Area',
        'format' => 'raw',
        'value' => function( $model ) {
            return (int)$model->address_id > 0 ? yii::$app->setting->formatAddress($model->address_id, true) : 'Pickup';
        }
    ],
    [
        'attribute' => 'branch_id',
        'format' => 'raw',
        'value' => function($data) {
            return $data->branch->name;
        }
    ],
    [
        'attribute' => 'total_sum',
        'value' => function($model) {
            return Yii::$app->setting->formatePrice($model->total_sum);
        }
    ],
    'source',
    [
        'attribute' => 'time',
        'value' => function($model){
            return yii::$app->dateTime->getReadableDateAndTimeFormat($model->time);
        }
    ],
    /*[
        'attribute' => 'Time Category',
        'format' => 'raw',
        'value' => function($model) {
            return ($model->orderTimeCategory) ? $model->orderTimeCategory->time_category : "N/A";
        },
        'footer' => '<b>Total:</b>'
    ],*/
    'current_status',
    [
        'attribute' => 'Total Order',
        'value' => function($model) {
            return $model->total_orders;
        },
        'footer' => $totalOrders
    ],
    [
        'attribute' => 'Feedback',
        'format' => 'raw',
        'value' => function($model) {
            if($model->customerFeedback) {
                return Html::a('Resolution', ['customer-feedback/view', 'id' => $model->customerFeedback->id], ['target' => '_blank']);
            } else {
                return 'N/A';
            }
        }
    ],
    [
        'class' => 'common\helpers\CustomActionColumn',
        'template'=>'{view}',
        'buttons' => [
            'view' => function ($url, $model) {
                return '<button ng-click="showOrderDetails(' . $model->id .')"  class="btn btn-info pull-right glyphicon glyphicon-zoom-in"></button>';
            }
        ]

    ],
];

?>
<div class="card-box">
        <?php echo $this->render('_search', ['model' => $searchModel, 'filters' => ['customer', 'branch_id', 'current_status', 'from_date', 'to_date', 'area', 'source']]); ?>
</div>

<div class="order-index card-box" ng-app="swichFeed" ng-controller="feedController as feed">
    <div class="row text-right">
    <?php

    // Renders a export dropdown menu
    echo ExportMenu::widget([
        'options' => [ 'class' => 'pull-right'],
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'showColumnSelector' => false,
        'exportConfig' => Yii::$app->params['exportConfig']

    ]);

    ?>
    </div>
    <div class="row table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'showFooter' => true,
        'footerRowOptions'=>['style'=>'font-weight:bold;background-color: lightgray;color: black;', 'class' => 'text-primary'],
        'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
        'columns' => $gridColumns,
    ]); ?>
    </div>
    <?php echo $this->render('_order_modal', []); ?>

</div>