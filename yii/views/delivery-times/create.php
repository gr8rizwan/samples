<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DeliveryTimes */

$this->title = 'Create Delivery Times';
$this->params['breadcrumbs'][] = ['label' => 'Delivery Times', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-times-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
