<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DeliveryTimesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Delivery Times';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-times-index card-box">

    <p class="pull-right">
        <?= Html::a('Create Delivery Times', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'status',
            'sort_order',

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>
</div>
