<!-- MODAL BOX TO DISPLAY ORDER DETAILS -->
<div class="modal fade bs-example-modal-lg " id="modal-order-details" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="pull-left m-t-30">
                            <strong>Customer Name: </strong> {{ orderDetails.customer.first_name
                            }} {{ orderDetails.customer.last_name }} <br>
                            <strong>Phone Number: </strong> {{ orderDetails.customer.phone_number
                            }}<br>
                                <span ng-show="orderDetails.delivery_type == 'delivery'">
                                    <strong>Address: </strong>
                                    {{ orderDetails.address.street }} <br/>
                                    {{ orderDetails.address.building }}<br/>
                                    {{ orderDetails.address.apartment }} <br/>
                                    {{ orderDetails.address.area.name }}<br/>
                                </span>
                            <strong>Branch Name: </strong> {{ orderDetails.branch.name }}
                        </div>
                        <div class="pull-right m-t-30">
                            <p><strong>Order Date: </strong> {{ (orderDetails.time) }}
                                <br><strong>Payment Type: </strong> {{ orderDetails.payment_type
                                }}
                                <br><strong>Delivery Type: </strong> {{ orderDetails.delivery_type
                                }}
                                <br><strong>Source: </strong> {{ orderDetails.source }}
                            </p>
                            <p ng-show="orderDetails.promoToOrder.length != 0">
                                <strong>Promo Code:</strong> {{ orderDetails.promoToOrder.code }}
                            </p>
                            <p ng-show="orderDetails.source == 'call' && orderDetails.agent[0] !== null ">
                                <strong>Agent Name:</strong> {{ orderDetails.agent.first_name }} {{
                                orderDetails.agent.last_name }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table m-t-30">
                                <thead>
                                <tr>
                                    <th width="70%" style="white-space: normal !important;">Item</th>
                                    <th width="10%">Quantity</th>
                                    <th width="10%">Unit Cost</th>
                                    <th width="10%">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="item in orderDetails.orderItems">

                                    <td style="white-space: normal !important;">
                                        <p>
                                            <strong>{{ item.name }}</strong><br>
                                            <span ng-show="item.description", ng-if="item.orderOptions.length == 0">{{ item.description }}<br></span>
                                            <span ng-repeat="option in item.orderOptions">
                                                {{  (!$first)?', ':'' }}
                                                <span ng-repeat="ingred in option.ingredients">
                                                    {{ ingred.name }}
                                                </span>
                                            </span>
                                            <br>
                                                <span ng-show="item.comments">
                                                    <strong>Comments: </strong> {{ item.comments }}
                                                </span>
                                        </p>
                                    </td>
                                    <td>{{ item.quantity }}</td>
                                    <td>{{ item.single_price | currency: "" }}</td>
                                    <td>{{ item.quantity * item.single_price | currency : "" }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row" style="border-radius: 0px;">
                    <div class="col-md-8 order-comments">
                        <strong>Comments: </strong> {{ orderDetails.comments }}
                    </div>
                    <div class="col-md-3 col-md-offset-9">
                        <p class="text-right"><b>Sub-total:</b> {{ orderDetails.price }} AED</p>

                        <p class="text-right"><b>Discount:</b> {{ orderDetails.discount }}</p>

                        <p class="text-right"><b>Delivery Charges:</b> {{ orderDetails.delivery_charges }} AED</p>

                        <hr>
                        <h3 class="text-right"><strong>Total: </strong>{{
                            orderDetails.total_sum }} AED</h3>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>