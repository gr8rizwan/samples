<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PageTabs */

$this->title = Yii::t('app', 'Create Page Tabs');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Page Tabs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-tabs-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
