<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PageTabsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Page Tabs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-tabs-index card-box">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Create Page Tabs'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'parent_id',
                'value' => function($model) {
                    return ( isset($model->parent) ? $model->parent->name : 'N/A');
                },
            ],
            'type',
            'status',

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>
