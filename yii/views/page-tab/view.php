<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PageTabs */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Page Tabs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-tabs-view card-box">


    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'parent_id',
            'type',
            'status',
        ],
    ]) ?>


    <h3>Tabs Headings</h3>
    <table id="headings" class="table table-striped table-bordered table-hover headings-table">
        <thead>
        <tr>
            <td class="text-left">Title</td>
            <td class="text-left">Description</td>
            <td class="text-right">Sort Order</td>

        </tr>
        </thead>
        <tbody>

        <?php foreach ($model->tabHeadings as $heading) { ?>
            <tr id="image-row">

                <td class="text-left">
                    <?= $heading->question ?>
                </td>

                <td class="text-left">
                    <?= $heading->description ?>
                </td>


                <td class="text-right" style="width: 10%;">
                    <?php echo $heading->sort_order; ?>
                </td>

            </tr>
        <?php } ?>
        </tbody>

    </table>



</div>
