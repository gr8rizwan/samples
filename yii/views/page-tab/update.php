<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PageTabs */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Page Tabs',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Page Tabs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="page-tabs-update card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
