<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\PageTabs;

/* @var $this yii\web\View */
/* @var $model app\models\PageTabs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-tabs-form" xmlns="http://www.w3.org/1999/html">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'parent_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(PageTabs::find()->where(['=', 'parent_id', 0])->all(),
            'id',
            'name'
        ),
        'options' => ['placeholder' => 'Select parent'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'type')->dropDownList([ 'faq' => 'Faq', 'cms' => 'Cms', ], ['prompt' => 'Select Type']) ?>

    <?= $form->field($model, 'status')->dropDownList(['0' => 'In-Active', '1' => 'Active'], ['prompt' => 'Select Status']) ?>

    <h3>Tabs Headings</h3>
    <table id="headings" class="table table-striped table-bordered table-hover headings-table">
        <thead>
        <tr>
            <td class="text-left">Title</td>
            <td class="text-left">Description</td>
            <td class="text-right">Sort Order</td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        <?php $heading_row = 0; ?>
        <?php foreach ($model->tabHeadings as $heading) { ?>
            <tr id="image-row<?php echo $heading_row; ?>">

                <td class="text-left">
                    <div class="form-group">
                        <input type="text" name="PageTabs[headings][<?php echo $heading_row; ?>][question]" value="<?= $heading->question ?>" placeholder="Title" class="form-control" />
                    </div>
                </td>

                <td class="text-left">
                    <div class="form-group">
                        <textarea name="PageTabs[headings][<?php echo $heading_row; ?>][description]" placeholder="description" class="form-control editor-box" /> <?= $heading->description ?> </textarea>
                    </div>
                </td>


                <td class="text-right" style="width: 10%;">
                    <div class="form-group">
                        <input type="text" name="PageTabs[headings][<?php echo $heading_row; ?>][sort_order]" value="<?php echo $heading->sort_order; ?>" placeholder="Sort order" class="form-control" />
                    </div>
                </td>

                <td class="text-left">
                    <button type="button" onclick="$('#image-row<?php echo $heading_row; ?>, .tooltip').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                </td>
            </tr>
            <?php $heading_row++; ?>
        <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3"></td>
            <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="Add" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
        </tr>
        </tfoot>
    </table>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script type="text/javascript">
    var heading_row = <?= $heading_row ?>;

    function addImage() {
        html  = '<tr id="image-row'+ heading_row +'">';

        html += '<td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="PageTabs[headings][' + heading_row + '][question]" value="" placeholder="Title" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <textarea  name="PageTabs[headings][' + heading_row + '][description]" rows="6" placeholder="description" class="form-control editor-box" id="editor-'+heading_row+'" /></textarea>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="text-right" style="width: 10%;">';
        html += '    <div class="form-group">';
        html += '  <input type="text" name="PageTabs[headings][' + heading_row + '][sort_order]" value="'+heading_row+'" placeholder="Sort order" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + heading_row  + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#headings tbody').append(html);


        // initialized editor
        initEditor('editor-' + heading_row);

        heading_row++;
    }
</script>
