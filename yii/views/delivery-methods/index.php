<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DeliveryMethodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Delivery Charges';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>

<div class="delivery-methods-index card-box">

    <p class="pull-right">
        <?= Html::a('Create Delivery Methods', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-actions-bar'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'method_title',
            'free_on_amount',
            'amount_type',
            'amount_value',
            [
                'attribute' => 'is_free',
                'value' => function($model) {
                    return ($model->is_free == 0 ? 'No' : 'Yes');
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return ($model->status == 0 ? 'In-active' : 'Active');
                }
            ],
            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
