<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DeliveryMethods */

$this->title = 'Create Delivery Charges';
$this->params['breadcrumbs'][] = ['label' => 'Delivery Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-methods-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
