<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\DeliveryMethods */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-methods-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'method_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'free_on_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount_type')->dropDownList([ 'fixed' => 'Fixed', 'percentage' => 'Percentage', ], ['prompt' => 'Select Amount type']) ?>

    <?= $form->field($model, 'amount_value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_free')->dropDownList([0 => 'No', 1 => 'Yes'], ['prompt' => 'Is Free']); ?>

    <div class="row">

        <div class="col-lg-6 col-sm-12">
            <?=
            $form->field($model, 'free_start_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Select free from'],
                'readonly' => true,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]);

            ?>
        </div>

        <div class="col-lg-6 col-sm-12">
            <?=
            $form->field($model, 'free_end_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Select free end'],
                'readonly' => true,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',

                ]
            ]);

            ?>
        </div>
    </div>


    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($model, 'free_start_time')->widget(TimePicker::classname(), [
                'options' => ['readonly' => true],
                'pluginOptions' => [
                    'showSeconds' => true,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                ]
            ]); ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'free_end_time')->widget(TimePicker::classname(), [
                'options' => ['readonly' => true],
                'pluginOptions' => [
                    'showSeconds' => true,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                ]
            ]); ?>
        </div>

    </div>

    <?= $form->field($model, 'status')->dropDownList(['In-active', 'Active'], ['prompt' => 'Select Status']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
