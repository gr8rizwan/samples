<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DeliveryMethods */

$this->title = 'Update Delivery charges: ' . ' ' . $model->method_title;
$this->params['breadcrumbs'][] = ['label' => 'Delivery Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->method_title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="delivery-methods-update card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
