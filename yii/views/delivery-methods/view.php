<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\DeliveryMethods */

$this->title = $model->method_title;
$this->params['breadcrumbs'][] = ['label' => 'Delivery Charges', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-methods-view card-box">

    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'visible' => false,
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'method_title',
            'free_on_amount',
            'amount_type',
            'amount_value',
            [
                'attribute' => 'is_free',
                'value' => $model->is_free == 0 ? 'No' : 'Yes',
            ],
            'free_start_date',
            'free_end_date',
            'free_start_time',
            'free_end_time',
            [
                'attribute' => 'status',
                'value' => $model->status == 0 ? 'In-active' : 'Active',
            ],
            'updated_at',
        ],
    ]) ?>

</div>
