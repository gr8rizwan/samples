<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DeliveryMethodsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-methods-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'method_title') ?>
        </div>

        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'amount_type')->dropDownList(['fixed' => 'Fixed', 'percentage' => 'Percentage'], ['prompt' => 'Amount Type']); ?>
        </div>

        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'amount_value') ?>
        </div>

    </div>

    <div class="row">

        <div class="col-lg-4 col-sm-12">
            <?php echo $form->field($model, 'is_free')->dropDownList(['0' => 'NO', '1' => 'Yes'], ['prompt' => 'Select IsFree']); ?>
        </div>


        <div class="col-lg-3 col-sm-12">
            <?php echo $form->field($model, 'status')->dropDownList(['0' => 'In-Active', '1' => 'Active'] , ['prompt' => 'Select Status']); ?>
        </div>

    </div>

    <div class="form-group">

        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
