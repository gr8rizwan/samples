<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReportTypes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Report Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="report-types-create card card-box">

        <p class="pull-right">
            <?= Html::a(Yii::t('app', 'Create Property Group'), ['/report-group/create', 'type_id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'status',
            ],
        ]) ?>

        <div class="row">
            <div class="col-sm-12">

                <h4 class="m-t-0 header-title"><b>Properties</b></h4>

                <table id="images" class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td class="text-left">Attribute</td>
                        <td class="text-left">Sort Order</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $image_row = 0; ?>
                    <?php foreach ($model->reportTypeProperties as $property) { ?>
                        <tr id="image-row<?php echo $image_row; ?>">

                            <td>
                                <?= $property->attribute ?>
                            </td>

                            <td class="text-right" style="width: 10%;">
                                <?php echo $property->sort_order; ?>
                            </td>

                        </tr>
                        <?php $image_row++; ?>
                    <?php } ?>
                    </tbody>
                </table>



            </div>
        </div>

    </div>

</div>
