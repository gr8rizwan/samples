<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FormPages */

$this->title = Yii::t('app', 'Create Form Pages');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Form Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-pages-create card-box">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
