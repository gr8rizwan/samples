<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FormPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Form Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-pages-index card-box">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Create Form Pages'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'type',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return ($model->status == 1 ? 'Active' : 'In-Active');
                },
            ],

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

<?php Pjax::end(); ?>

</div>
