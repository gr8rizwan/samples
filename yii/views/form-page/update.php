<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FormPages */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Form Pages',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Form Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="form-pages-update card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
