<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FormPages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(['contact-us' => 'Contact Us', 'other' => 'Other'], ['prompt' => 'Select Type']) ?>

    <?= $form->field($model, 'status')->dropDownList(['0' => 'In-Active', '1' => 'Active'], ['prompt' => 'Select Status']) ?>


    <h3>Forms</h3>
    <table id="forms" class="table table-striped table-bordered table-hover headings-table">
        <thead>
        <tr>
            <td class="text-left" style="width: 30%;">Title</td>
            <td class="text-left">HTML ID</td>
            <td class="text-left">Email</td>
            <td class="text-left" style="width: 8%;">Sorting</td>
            <td class="text-left" style="width: 8%;">Status</td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        <?php $form_row = 0; ?>
        <?php foreach ($model->forms as $form) { ?>
            <tr id="image-row<?php echo $form_row; ?>">

                <input type="hidden" name="FormPages[formsArray][<?php echo $form_row; ?>][id]" value="<?= $form->id ?>" placeholder="Title" class="form-control" />

                <td class="text-left">
                    <div class="form-group">
                        <input type="text" name="FormPages[formsArray][<?php echo $form_row; ?>][title]" value="<?= $form->title ?>" placeholder="Title" class="form-control" />
                    </div>
                </td>

                <td class="text-left">
                    <div class="form-group">
                        <input type="text" name="FormPages[formsArray][<?php echo $form_row; ?>][html_id]" value="<?= $form->html_id ?>" placeholder="Form Html ID" class="form-control" />
                    </div>
                </td>

                <td class="text-left">
                    <div class="form-group">
                        <input type="text" name="FormPages[formsArray][<?php echo $form_row; ?>][notification_emails]" value="<?= $form->notification_emails ?>" placeholder="notification emails (xxxx@gmail.com,abc@gmail.com)" class="form-control" />
                    </div>
                </td>

                <td class="text-left">
                    <div class="form-group">
                        <input type="text" name="FormPages[formsArray][<?php echo $form_row; ?>][sort_order]" value="<?= $form->sort_order ?>" placeholder="1" class="form-control" />
                    </div>
                </td>

                <td class="text-left">
                    <div class="form-group">
                        <input type="text" name="FormPages[formsArray][<?php echo $form_row; ?>][status]" value="<?= $form->status ?>" placeholder="Status ID (1, 0)" class="form-control" />
                    </div>
                </td>

                <td class="text-left">
                    <button type="button" onclick="$('#image-row<?php echo $form_row; ?>, .tooltip').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                </td>
            </tr>
            <?php $form_row++; ?>
        <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="5"></td>
            <td class="text-left"><button type="button" onclick="addForm();" data-toggle="tooltip" title="Add" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
        </tr>
        </tfoot>
    </table>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    var form_row = <?= $form_row ?>;

    function addForm() {
        html  = '<tr id="image-row'+ form_row +'">';

        html += '      <input type="hidden" name="FormPages[formsArray][' + form_row + '][id]" value="" placeholder="Title" class="form-control" />';

        html += '<td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="FormPages[formsArray][' + form_row + '][title]" value="" placeholder="Title" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '<td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="FormPages[formsArray][' + form_row + '][html_id]" value="" placeholder="Form Html ID" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '<td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="FormPages[formsArray][' + form_row + '][notification_emails]" value="" placeholder="notification emails (xxxx@gmail.com,abc@gmail.com)" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '<td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="FormPages[formsArray][' + form_row + '][sort_order]" value="" placeholder="1" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '<td class="text-left">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="FormPages[formsArray][' + form_row + '][status]" value="" placeholder="Status (1, 0)" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + form_row  + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#forms tbody').append(html);

        form_row++;
    }
</script>
