<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FormPages */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Form Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-pages-view card-box">


    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'type',
            [
                'attribute' => 'status',
                'value' => ($model->status == 1 ? 'Active' : 'In-Active'),
            ],
        ],
    ]) ?>


    <br/>
    <br/>
    <h3>Forms</h3>
    <table id="forms" class="table table-striped table-bordered table-hover headings-table">
        <thead>
        <tr>
            <td class="text-left">Title</td>
            <td class="text-left">HTML Form ID</td>
            <td class="text-left">Notification Email</td>
            <td class="text-left">Status</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($model->forms as $form) { ?>
            <tr>

                <td class="text-left">
                    <?= $form->title ?>
                </td>

                <td class="text-left">
                    <?= $form->html_id ?>
                </td>

                <td class="text-left">
                    <?= $form->notification_emails ?>
                </td>

                <td class="text-left">
                    <?= ($form->status == 1 ? 'Active' : 'In-Active')  ?>
                </td>

            </tr>
        <?php } ?>
        </tbody>
    </table>


</div>
