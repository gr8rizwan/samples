<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StickyNotes */

$this->title = 'Customer Message ID:'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sticky Notes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="card-box">
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'customer_id',
                            'status',
                            'note:ntext',
                            'created_at',
                        ],
                    ]) ?>

                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col (left) -->
    </div><!-- /.row -->

</section><!-- /.content -->
