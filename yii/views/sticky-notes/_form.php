<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\StickyNotes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sticky-notes-form">


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'admin_id')->hiddenInput(['value' => yii::$app->user->identity->id])->label(false); ?>

    <?=
    $form->field($model, 'customer_id')->widget(Select2::classname(), [
        'initValueText' => $model->customer_id, // set the initial display text
        'options' => ['placeholder' => 'Select Customer'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 4,
            'ajax' => [
                'url' => Url::to(['customer/customer-list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(customer) { return customer.name; }'),
            'templateSelection' => new JsExpression('function (customer) { return customer.name; }'),
        ],
    ]);

    ?>

</div>

    <?= $form->field($model, 'status')->dropDownList([ '0' => 'In-Active', '1' => 'Active', ], ['prompt' => 'Select Status']) ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
