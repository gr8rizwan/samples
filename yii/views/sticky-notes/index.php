<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StickyNotesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Notes';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>

<div class="card-box">
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="box-body">

                        <p class="pull-right">
                            <?= Html::a('Create Notes', ['create'], ['class' => 'btn btn-primary']) ?>
                        </p>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                [
                                    "attribute" => "customer_id",
                                    'filter' => false,
                                    "format" => "raw",
                                    'value' => function($data) {
                                        $html = '';
                                        $html .= strtoupper($data->customer->first_name.' '.$data->customer->last_name).' <br />';
                                        $html .= '<b>'.$data->customer->phone_number.'</b><br />';
                                        $html .= yii::$app->setting->getLatestAddress($data->customer->id).'<br />';
                                        return $html;
                                    }
                                ],
                                [
                                    "attribute" => "note",
                                    'filter' => false,
                                ],

                                [
                                    "attribute" => "status",
                                    'filter' => false,
                                    'value' => function($data) {
                                        return ($data->status == 0 ? 'In-Active' : 'Active');
                                    },

                                ],

                                [
                                    "attribute" => "created_at",
                                    'filter' => false,
                                ],

                                ['class' => 'common\helpers\CustomActionColumn'],
                            ],
                        ]); ?>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col (left) -->
        </div><!-- /.row -->

    </section><!-- /.content -->
</div>

