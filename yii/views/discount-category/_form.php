<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use backend\models\OrderOrigin;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\DiscountCategory */
/* @var $form yii\widgets\ActiveForm */

$discounts = Yii::$app->httpclient->get(Yii::$app->params['POS_URL'] . 'GetDiscounts/046a5e6b-d905-499b-8e72-dacb2f46a0cd');

?>

<div class="discount-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'points')->textInput() ?>

    <?= $form->field($model, 'percentage')->dropDownList(['Fixed', 'Percentage'], ['prompt' => 'Select Discount Type']) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-6"><?= $form->field($model, 'origin_id')->dropDownList( ArrayHelper::map(OrderOrigin::find()->all(), 'id', 'name'), ['prompt' => 'Select Origin']) ?></div>

        <div class="col-lg-6 col-sm-12">
            <?=
            $form->field($model, 'pos_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($discounts, 'Id', 'Category'),
                'options' => ['placeholder' => 'Select POS Mapping'],
                'size' => Select2::LARGE,
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>

        </div>

    </div>

    <?=  $form->field($model, 'start_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter start date'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]); ?>

    <?=  $form->field($model, 'end_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter end date'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]); ?>

    <?= $form->field($model, 'is_deleted')->dropDownList(['In-Active', 'Active'], ['prompt' => 'Select Status']); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
