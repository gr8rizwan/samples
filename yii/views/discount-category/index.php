<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DiscountCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Discount Categories';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'name',
    ],

    [
        'attribute' => 'type',
        'value' => function($model){
            return ($model->percentage == '1')? 'Percentage' : 'Fixed';
        }
    ],
    [
        'attribute' => 'amount',
        'value' => function($model) {
            return (($model->percentage == '1') ? ( (int)$model->amount ).' %' : ( (int)$model->amount ).' Fixed');
        },
    ],

    [
        'attribute' => 'redeemed_count',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            return count($model->orderDiscountCategories).' Times';
        },
    ],

    [
        'attribute' => 'redeemed_amount',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            return Yii::$app->setting->formatePrice($model->getOrderSum($model, 'discount'));
        },
    ],

    [
        'attribute' => 'revenue_generated',
        'contentOptions' =>['class' => 'text-center'],
        'value' => function($model) {
            return Yii::$app->setting->formatePrice( $model->getOrderSum($model, 'total_sum'));
        },
    ],

    [
        'attribute' => 'Validity',
        'format' => 'raw',
        'value' => function($model){
            return (
                yii::$app->dateTime->getDateFromTimeStamp($model->start_date). '<b>/</b> <br /> '.
                yii::$app->dateTime->getDateFromTimeStamp($model->end_date)
            );
        }
    ],

    [
        'class' => 'common\helpers\CustomActionColumn',
        'template'=>'&nbsp;&nbsp;&nbsp;{view} {update}',

    ],
]

?>


<?php  echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="discount-category-index card-box">

    <p class="pull-right">
        <?= Html::a('Create Discount Category', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
    ]); ?>

</div>
