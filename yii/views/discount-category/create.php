<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DiscountCategory */

$this->title = 'Create Discount Category';
$this->params['breadcrumbs'][] = ['label' => 'Discount Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-category-create card-box">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
