<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Branch;

/* @var $this yii\web\View */
/* @var $model app\models\DiscountCategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="card-box">
    <div class=" row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-lg-4 col-md-6 col-sm-12">
            <?= $form->field($model, 'name') ?>
        </div>

        <div class="col-lg-4 col-md-3 col-sm-12">
            <?= $form->field($model, 'type')->dropDownList([ 'percentage' => 'Percentage', 'fixed' => 'Fixed'], ['prompt' => 'Select Type']) ?>
        </div>

        <div class="col-lg-4 col-md-3 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList([ '0' => 'Expired', '1' => 'Active'], ['prompt' => 'Select Status']) ?>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="form-group pull-right">
                <label class="control-label" style="width: 100%">&nbsp;</label>
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

        </div>


        <?php ActiveForm::end(); ?>

    </div>
</div>
