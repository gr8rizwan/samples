<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DiscountCategory */

$this->title = 'Update Discount Category: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Discount Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="discount-category-update card-box">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
