<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Branch */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Branches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="branch-view card-box">


    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>



    <div class="row">

        <div class="col-lg-12">
            <ul class="nav nav-tabs tabs">

                <li class="active tab">
                    <a href="#basic-info" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                        <span class="hidden-xs">Branch Information</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#pos-setting" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-cog"></i></span>
                        <span class="hidden-xs">POS settings</span>
                    </a>
                </li>

            </ul>
            <div class="tab-content">

                <div class="tab-pane active" id="basic-info">


                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'name',
                            'detailed_address:ntext',
                            'open_time',
                            'close_time',
                            'lng',
                            'lat',
                            'sequence',
                            'is_deleted',
                            'server_url:url',
                        ],
                    ]) ?>


                </div>

                <div class="tab-pane" id="pos-setting">

                    <?php Pjax::begin(['id' => 'pos-vendor-setting']) ?>
                    <?=
                    $this->render('/pos-setting/index', [
                        'model' => $model
                    ]);
                    ?>
                    <?php Pjax::end() ?>

                </div>

            </div>

        </div>
    </div>

</div>
