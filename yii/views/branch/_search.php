<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BranchSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="branch-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'detailed_address') ?>

    <?= $form->field($model, 'open_time') ?>

    <?= $form->field($model, 'close_time') ?>

    <?php echo $form->field($model, 'lng') ?>

    <?php echo $form->field($model, 'lat') ?>

    <?php echo $form->field($model, 'sequence') ?>

    <?php echo $form->field($model, 'is_deleted') ?>

    <?php echo $form->field($model, 'server_url') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
