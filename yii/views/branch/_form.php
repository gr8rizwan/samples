<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Country;


/* @var $this yii\web\View */
/* @var $model backend\models\Branch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="branch-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">

        <div class="col-lg-12">
            <ul class="nav nav-tabs tabs">

                <li class="active tab">
                    <a href="#basic-info" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                        <span class="hidden-xs">Branch Information</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#basic-config" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Basic configuration</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#branch-config" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                        <span class="hidden-xs">Branch Configuration</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#social-data" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-cog"></i></span>
                        <span class="hidden-xs">Social Data</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#tab-images" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-cog"></i></span>
                        <span class="hidden-xs">Images</span>
                    </a>
                </li>

            </ul>
            <div class="tab-content">

                <div class="tab-pane active" id="basic-info">

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'detailed_address')->textarea(['rows' => 6]) ?>


                    <?= $form->field($model, 'opening_hours')->textarea(['rows' => 6]) ?>

                    <?= $form->field($config, 'order_phone_number')->textInput(['maxlength' => true]) ?>

                    <?=
                    $form->field($model, 'country_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Country::find()->all(), 'id', 'name'),
                        'options' => ['placeholder' => 'Select Country'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>

                    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'lng')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'lat')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'sequence')->textInput() ?>

                    <?= $form->field($model, 'server_url')->textInput(['maxlength' => true]) ?>

                </div>

                <div class="tab-pane" id="basic-config">

                    <div class="row">
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'toppings_free_count')->textInput() ?>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'extra_topping_cost')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'max_toppings')->textInput() ?>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'max_bread_type')->textInput() ?>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'max_meat_type')->textInput() ?>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'max_sauces_type')->textInput() ?>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($config, 'sauce_free_count')->textInput() ?>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($config, 'extra_sauce_cost')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <?= $form->field($config, 'mangerMessage')->textarea(['maxlength' => true]) ?>

                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($config, 'agentBonus')->textInput() ?>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($config, 'orderLimit')->textInput() ?>
                        </div>

                    </div>

                </div>

                <div class="tab-pane" id="branch-config">

                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($model, 'open_time')->widget(TimePicker::classname(), [
                                'pluginOptions' => [
                                    'showSeconds' => true,
                                    'showMeridian' => false,
                                    'minuteStep' => 1,
                                    'secondStep' => 5,
                                ]
                            ]); ?>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($model, 'close_time')->widget(TimePicker::classname(), [
                                'pluginOptions' => [
                                    'showSeconds' => true,
                                    'showMeridian' => false,
                                    'minuteStep' => 1,
                                    'secondStep' => 5,
                                ]
                            ]); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($config, 'convert_rewards_point')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($config, 'drivers_carry_change')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <?= $form->field($config, 'menu_url')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($config, 'additem_url')->textInput(['maxlength' => true]) ?>

                    <div class="row">
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'om_swich_id')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'om_side_id')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'om_drink_id')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'om_size_id')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'om_portion_id')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'om_price_id')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'om_swich_plus_group')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'om_printer_group')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($config, 'om_custom_swich_id')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <?= $form->field($config, 'om_category_id')->textInput(['maxlength' => true]) ?>

                </div>

                <div class="tab-pane" id="social-data">

                    <?= $form->field($config, 'app_store_link')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($config, 'youtube_link')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($config, 'facebook_link')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($config, 'instagram_link')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($config, 'twitter_link')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($config, 'facebook_points')->textInput() ?>

                    <?= $form->field($config, 'twitter_points')->textInput() ?>

                </div>

                <div class="tab-pane" id="tab-images">

                    <div id="images" class="form-group">
                        <label class="control-label" for="input-image">Location Image</label>
                        <br/>
                        <a href="" id="location-image" data-toggle="image" class="img-thumbnail">
                            <img src="<?php echo $config->location_image; ?>" alt="" height="125" title=""
                                 data-placeholder="no_image.png"/>
                        </a>

                        <?= $form->field($config, 'location_image')->hiddenInput(['id' => 'location-image'])->label(false) ?>

                    </div>

                    <div id="images" class="form-group">
                        <label class="control-label" for="input-image">Faq Image</label>
                        <br/>
                        <a href="" id="faq-image" data-toggle="image" class="img-thumbnail">
                            <img src="<?php echo $config->faq_image; ?>" alt="" height="125" title=""
                                 data-placeholder="no_image.png"/>
                        </a>

                        <?= $form->field($config, 'faq_image')->hiddenInput(['id' => 'faq-image'])->label(false) ?>

                    </div>

                </div>

            </div>

        </div>
    </div>

</div>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>


<?php $form->end() ?>

</div>
