<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Branch */

$this->title = 'Create Branch';
$this->params['breadcrumbs'][] = ['label' => 'Branches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="branch-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
        'config' => $config,
    ]) ?>

</div>
