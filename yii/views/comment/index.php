<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SpecialCommentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Special Comments';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">

    <p class="pull-right">
        <?= Html::a('Create Special Comment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'comment',
            [
                'attribute' => 'status',
                'filter' => false,
                'value' => function($data) {
                    return ($data->status == 1 ? 'Active' : 'In-Active');
                },
            ],

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div><!-- /.box-body -->


