<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SpecialComments */

$this->title = 'Create Special Comments';
$this->params['breadcrumbs'][] = ['label' => 'Special Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
