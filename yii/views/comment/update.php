<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SpecialComments */

$this->title = 'Update Special Comment ID: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Special Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'ID: '.$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>



<div class="card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
