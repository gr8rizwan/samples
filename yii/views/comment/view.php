<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SpecialComments */

$this->title = 'Special Comment ID:'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Special Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="content card-box">
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'comment',
                            [
                                'attribute' => 'status',
                                'value' =>  ($model->status == 1 ? 'Active' : 'In-Active'),
                            ],
                            [
                                'attribute' => 'is_deleted',
                                'value' =>  ($model->is_deleted == 1 ? 'Yes' : 'No'),
                            ],
                            [
                                'attribute' => 'type',
                            ],
                        ],
                    ]) ?>

                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col (left) -->
    </div><!-- /.row -->

</section><!-- /.content -->
