<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use \app\models\Menu;
use common\models\CategoryType;
use kartik\select2\Select2;
use kartik\file\FileInput;
use backend\models\Ingredient;

/* @var $this yii\web\View */
/* @var $model app\models\MenuItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-item-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create Ingredient' : 'Update Ingredient', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <div class="tabbable-panel">
        <div class="tabbable-line">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#tab_general" data-toggle="tab">
                        General Information
                    </a>
                </li>
                <li>
                    <a href="#tab_data" data-toggle="tab">
                        Data
                    </a>
                </li>
                <li>
                    <a href="#tab_image" data-toggle="tab">
                        Image
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_general">

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'descriptionApp')->textarea(['rows' => 6]) ?>

                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($model, 'sequence')->textInput() ?>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($model, 'om_id')->textInput() ?>
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($model, 'twox_id')->textInput() ?>
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <?= $form->field($model, 'category_type')->dropDownList(ArrayHelper::map(CategoryType::find()->all(), 'id', 'title'), ['prompt'=>'Choose Category Type']) ?>
                        </div>
                    </div>

                    <?= $form->field($model, 'is_deleted')->dropDownList(['0' => 'In-Active', '1' => 'Active'], ['prompt'=>'Select Status']) ?>

                </div>
                <div class="tab-pane" id="tab_data">

                    <?= $form->field($model, 'menu_id')->dropDownList(ArrayHelper::map(Menu::find()->all(), 'id', 'name'), ['prompt'=>'Choose Menu']) ?>

                    <?= $form->field($model, 'quantity')->textInput() ?>

                    <?= $form->field($model, 'isCustomizable')->dropDownList(['0' => 'No', '1' => 'Yes'], ['prompt'=>'Is customizable']) ?>

                    <div class="form-group">
                    <label class="control-label" for="menuitem-category_type">Menu Item Ingredients</label>
                    <?= Select2::widget([
                        'name' => 'MenuItem[menu_item_ingredients][]',
                        'value' => ArrayHelper::getColumn($model->menuItemIngredients, 'ingredient_id'),
                        'data' => ArrayHelper::map(Ingredient::find()->all(), 'id', 'name'),
                        'options' => ['placeholder' => 'Select a state ...',  'multiple' => true],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'tags' => true,
                            'tokenSeparators' => [',', ' '],
                            'maximumInputLength' => 10,
                        ],
                    ]);

                    ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Item Legends</label>
                        <?= Select2::widget([
                            'name' => 'MenuItem[legends][]',
                            'value' => ArrayHelper::getColumn($model->menuItemLegends, 'legend_id'),
                            'data' => ArrayHelper::map(\app\models\Legend::find()->all(), 'id', 'name'),
                            'options' => ['placeholder' => 'Select a legend...',  'multiple' => true],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'tags' => true,
                                'tokenSeparators' => [',', ' '],
                                'maximumInputLength' => 10,
                            ],
                        ]);

                        ?>
                    </div>

                    <?= $form->field($model, 'protein')->textInput() ?>

                    <?= $form->field($model, 'carbs')->textInput() ?>

                    <?= $form->field($model, 'fat')->textInput() ?>

                    <?= $form->field($model, 'calories_count')->textInput() ?>

                </div>
                <div class="tab-pane" id="tab_image">

                    <div id="images" class="form-group">
                        <label class="control-label" for="input-image">Item Image</label>
                        <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                            <img src="<?php echo $model->image; ?>" alt="" height="125" title="" data-placeholder="no_image.png" />
                        </a>

                        <?= $form->field($model, 'image')->hiddenInput(['id' => 'input-image'])->label(false) ?>

                    </div>


                    <div id="images" class="form-group">
                        <label class="control-label" for="input-detail_image">Detail Page Image (1800x1200 PX)</label>
                        <a href="" id="thumb-detail_image" data-toggle="image" class="img-thumbnail">
                            <img src="<?php echo $model->detail_image; ?>" alt="" height="125" title="" data-placeholder="no_image.png" />
                        </a>

                        <?= $form->field($model, 'detail_image')->hiddenInput(['id' => 'input-detail_image'])->label(false) ?>

                    </div>


                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
