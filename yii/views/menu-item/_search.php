<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\CategoryType;

/* @var $this yii\web\View */
/* @var $model app\models\MenuItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-lg-3 col-md-6 col-sm-12">
            <?= $form->field($model, 'name') ?>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-12">
            <?= $form->field($model, 'category_type')->dropDownList( ArrayHelper::map(CategoryType::find()->all(), 'id', 'name'), ['prompt' => 'Select Category']) ?>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-12">
            <?php echo $form->field($model, 'om_id') ?>
        </div>

    </div>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
