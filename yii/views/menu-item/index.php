<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use \app\models\Menu;
use common\models\CategoryType;


/* @var $this yii\web\View */
/* @var $searchModel app\models\MenuItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu Items';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>

<div class="card-box">

    <p class="pull-right">
        <?= Html::a('Create Menu Item', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Image',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::img($data->image, ['width' => '50']);
                }
            ],
            'name',
            'om_id',
            [
                "attribute" => "category_type",
                //'filter' => ArrayHelper::map(CategoryType::find()->asArray()->all(), 'id', 'title'),
                'value' => function($data) {
                    return $data->categoryType->title;
                },
                "format" => "raw",
            ],

            'price',
            // 'image',
            // 'description:ntext',
            // 'descriptionApp:ntext',
            // 'calories_count',
            'sequence',
            'is_deleted',
            // 'ingredients_map',
            // 'customer_id',
            // 'pending',
            // 'protein',
            // 'carbs',
            // 'fat',
            // 'new_image',
            //

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
