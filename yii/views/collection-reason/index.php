<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CollectionReasonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Collection Reasons';
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="card-box">
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">

                    <p class="pull-right">
                        <?= Html::a('Create Collection Reason', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'title',
                            [
                                'attribute' => 'status',
                                'filter' => true,
                                'value' => function($data) {
                                    return ($data->status == 0 ? 'In-Active' : 'Active');
                                }

                            ],


                            ['class' => 'common\helpers\CustomActionColumn'],
                        ],
                    ]); ?>

                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col (left) -->
    </div><!-- /.row -->

</section><!-- /.content -->
