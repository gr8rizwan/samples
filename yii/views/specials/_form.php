<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\MenuItem;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Specials */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="specials-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
        $form->field($model, 'menu_item_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(MenuItem::find()->all(), 'id', 'name'),
        'language' => 'en',
        'options' => ['placeholder' => 'Choose Menu Item'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'unit_sale_price')->textInput() ?>

    <?= $form->field($model, 'special_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'onAmount')->textInput(['maxlength' => true]) ?>

    <?=  $form->field($model, 'start_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter start date'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]); ?>

    <?=  $form->field($model, 'end_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter end date'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
