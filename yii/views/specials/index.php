<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SpecialsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Specials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specials-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?= Html::a('Create Specials', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'menu_item_id',
                'value' => function($data){
                    return $data->menuItem->name;
                },
                'format' => 'raw',
            ],
            'unit_sale_price',
            'special_title',
            'onAmount',
            // 'start_date',
            // 'end_date',

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
