<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;

$params = Yii::$app->request->queryParams;

?>

<?php echo $this->render('_search_customer', ['model' => $searchModel]); ?>

<div class="card-box" ng-app="swichFeed" ng-controller="feedController as feed">

    <div class="table-responsive">
<!--        --><?php //Pjax::begin(); ?>
        <?= GridView::widget([
            'tableOptions' => ['class' => 'swich-data-grid table table-actions-bar table-striped'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label'=> 'Date & Time',
                    'filter' => false,
                    'attribute' => 'created_at',
                    'format' => 'html',
                    'value' => function($model){
                        return yii::$app->dateTime->getReadableDateAndTimeFormat($model->created_at, 'M j, Y')
                            . '<br />' . yii::$app->dateTime->getReadableDateAndTimeFormat($model->created_at, 'G:i:s');
                    }
                ],
                [
                    'attribute' => 'customer_id',
                    'filter' => false,
                    'label' => 'Customer Details',
                    'format' => 'raw',
                    'value' => function($model) {
                        $html = '';
                        $html .= strtoupper($model->first_name.' '.$model->last_name);
                        $html = Html::a($html, ['customer/view', 'id' => $model->id], [ 'target' => '_blank', 'data-pjax' => 'false']);
                        $html .= '<br /><b>'.$model->phone_number.'</b><br />';
                        return $html;
                    }

                ],
                [
                    'attribute' => 'branch',
                    'filter' => false,
                    'label' => 'Outlet',
                    'value' => function($model){
                        if(isset($model->firstOrder))
                            return $model->firstOrder->branch->name;
                        else
                            return "N/A";
                    }
                ],
                [
                    'options' => ['style' => 'width: 25%'],
                    'filter' => false,
                    'label' => 'Address',
                    'format' => 'html',
                    'value' => function($model){
                        if(isset($model->firstOrder) && isset($model->firstOrder->address)){
                            return Yii::$app->setting->formatAddress($model->firstOrder->address_id);
                        }
                        else
                            return "N/A";
                    }
                ],
                [
                    'attribute' => 'area',
                    'filter' => false,
                    'label' => 'Area',
                    'format' => 'html',
                    'value' => function($model){
                        if(isset($model->firstOrder) && isset($model->firstOrder->address)){
                            return $model->firstOrder->address->hasArea->name;
                        }
                        else
                            return "N/A";
                    }
                ],
                [
                    'attribute' => 'source',
                    'filter' => false,
                    'value' => function($model){
                        if(isset($model->firstOrder))
                            return $model->firstOrder->source;
                        else
                            return "N/A";
                    }
                ],
                [
                    'label' => 'Total Orders',
                    'filter' => false,
                    'value' => function($model) use ($params){
                        $from_date = isset($params["CustomerSearch"]["from_date"]) ? $params["CustomerSearch"]["from_date"] : false;
                        $to_date = isset($params["CustomerSearch"]["to_date"]) ? $params["CustomerSearch"]["to_date"] : false;
                        return $model->getOrderCount(['from_date' => $from_date, 'to_date' => $to_date]);
                    }
                ],
                [
                    'class' => 'common\helpers\CustomActionColumn',
                    'template'=>'{view}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            if(isset($model->firstOrder))
                                return '<button ng-click="showOrderDetails(' . $model->firstOrder->id .')"  class="btn btn-info pull-right glyphicon glyphicon-zoom-in"></button>';
                        }
                    ]

                ]
            ],
        ]); ?>
<!--        --><?php //Pjax::end(); ?>
        <div class="pull-right hidden" id="btn-compile-HTML">
            <button type="button" ng-click="compileHTML()" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i> &nbsp; Compile </button>
        </div>
    </div>

    <?php echo $this->render('../customer/_order_modal', []); ?>

</div>

<?php

$this->registerJs(
    '
        $(document).on("pjax:end", function() {
            $("#btn-compile-HTML button").trigger("click");
        });
    '
);

?>
