<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Branch;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Evolution';
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    #stacked-chart{
/*        width: */<?//= count($chartData) * 550 ?>/*px !important;*/
        /*height: 250px !important;*/
    }
</style>

<div class="card-box">
        <?php echo $this->render('reports/_search', ['model' => $searchModel, 'filters' => ['branch_id', 'current_status', 'from_date', 'to_date', 'area', 'source', 'time_filter']]); ?>
</div>

<div class="card-box row">
    <div class="col-lg-10 col-lg-offset-1" style="overflow: auto;">
        <button type="button" class="btn btn-success waves-effect waves-light"><?= $branchData[1] ?></button>
        <button type="button" class="btn btn-danger waves-effect waves-light"><?= $branchData[3] ?></button>
        <canvas id="stacked-chart"></canvas>
    </div>
</div>

<div class="card-box">
    <div class="row">
        <div class="col-sm-10 col-lg-10 col-lg-offset-1">
            <div class="table-responsive">
            <?php
                echo GridView::widget([
                    'dataProvider' => $gridProvider,
                    'tableOptions' => ['class' => 'table table-actions-bar table-striped']
                ])
            ?>
            </div>
        </div>
    </div>
</div>

<script>
    window.onload = function(){

        var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
        var randomColorFactor = function(){ return Math.round(Math.random()*255)};

        var barChartData = {
            labels : <?= \yii\helpers\Json::encode(array_values($timeData)) ?>,
            datasets : [
                <?php
                    $chartColors = ['#f05050', '#5d9cec', '#81c868'];
                    $colorIndex = -1;
                    foreach($chartData as $key => $data)
                    {
                    $colorIndex++;
                ?>
                {
                    fillColor : "<?= $chartColors[$colorIndex] ?>",
                    strokeColor : "#FEFEFE",
                    data : <?= \yii\helpers\Json::encode(array_values($data)) ?>
                },
                <?php
                    }
                ?>
            ]
        };

        var ctx = document.getElementById("stacked-chart").getContext("2d");
        window.myBar = new Chart(ctx).StackedBar(barChartData, {
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
            responsive : true
        });

    };
</script>

