<style type="text/css">
    .chart-detail-list li {
        float: left;
        width: 100%;
        text-align: left;
    }
</style>
<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

$myDataProvider = clone $dataProvider;

$myDataProvider->setPagination(false);

$this->title = 'Agent Analysis';
$this->params['breadcrumbs'][] = $this->title;
$colours = ['#34d3eb', '#5fbeaa', '#ff0000', '#ebeff2', '#484848', '#f9f9f9', '#34d3eb', '#ebeff2'];

$graphData = [];
$totalOrders = 0;

foreach($dataProvider->getModels() as $key => $value) {
    $totalOrders += $value->total_orders;
}


foreach($dataProvider->getModels() as $key => $value) {

    $graphData[] = [
        'value'     => round($value->total_orders / $totalOrders * 100),
        'color'     => $colours[$key],
        'label'     => isset($value->orderAgent) ?  $value->orderAgent->first_name : 'N/A'
    ];
}

$graphData = json_encode($graphData);

Pjax::begin();
?>


<div class="card-box">
    <?php echo $this->render('reports/_search', ['model' => $searchModel, 'filters' => ['from_date', 'branch_id', 'to_date']]); ?>
    <div class="row panel">
        <?= $this->render('reports/durations') ?>
        <div class="br-n pn col-sm-12 col-lg-12">
            <div id="navpills-1" class="tab-panel active">
                <?php if ($dataProvider->getTotalCount()) { ?>
                    <div class="row p-t-10">


                        <div class="col-sm-10 col-lg-10 col-md-10 col-lg-offset-1 text-center"
                             style="overflow: auto;">

                            <div class="row">
                                <div class="col-sm-6">
                                    <!--Area Split Chart will draw from DrawChart function. -->
                                    <canvas id="source-chart" height="260"></canvas>
                                </div>

                                <div class="col-sm-6">
                                    <ul class="list-inline chart-detail-list">

                                        <?php foreach($dataProvider->getModels() as $agent) { ?>
                                            <li><h5><i class="fa fa-circle m-r-5" style="color: #34d3eb"></i><?= isset($agent->agent) ?  $agent->agent->first_name : 'N/A' ?></h5></li>
                                        <?php } ?>

                                    </ul>
                                </div>
                            </div>

                            <div id="chartjs-tooltip" class="below"
                                 style="opacity: 0; left: 663px; top: 12px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal;">
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <hr/>
                <div class="row">
                    <div class="col-sm-10 col-lg-10 col-lg-offset-1">
                        <div class="table-responsive">
                        <?php
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
                            'columns' => [
                                [
                                    'attribute' => 'Agent',
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        return isset($model->agent) ? $model->agent->first_name : 'N/A';
                                    }
                                ],
                                [
                                    'attribute' => 'total_orders',
                                    'format' => 'raw',
                                    'value' => 'total_orders'
                                ],
                                [
                                    'attribute' => 'total_revenue',
                                    'format' => 'raw',
                                    'value' => function ($model){
                                        return Yii::$app->setting->formatePrice($model->revenue);
                                    }
                                ],
                                [
                                    'attribute' => 'average_ticket',
                                    'format' => 'raw',
                                    'value' => function ($model){
                                        return Yii::$app->setting->formatePrice($model->average_ticket);
                                    }
                                ],

                                [
                                    'attribute' => 'percentage',
                                    'format' => 'raw',
                                    'value' => function($model) use ($totalOrders){
                                        return round( $model->total_orders / $totalOrders * 100) . " %";
                                    }
                                ]

                            ]
                        ]);

                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<style>
    #chartjs-tooltip {
        opacity: 0;
        position: absolute;
        background: rgba(0, 0, 0, .7);
        color: white;
        padding: 3px;
        border-radius: 3px;
        -webkit-transition: all .1s ease;
        transition: all .1s ease;
        pointer-events: none;
        -webkit-transform: translate(-50%, 0);
        transform: translate(-50%, 0);
    }
</style>

<?php
if ($dataProvider->getTotalCount())
    $this->registerJs('
        var ctx = $("canvas").get(0).getContext("2d");

        var data = '.$graphData.'

        var myLineChart = new Chart(ctx).Pie(data, {
            scaleLabel: "     <%= value %> ",

            onAnimationComplete: function () {

                var ctx = this.chart.ctx;
//                ctx.font = this.scale.font;
                ctx.fillStyle = this.scale.textColor
                ctx.textAlign = "center";
                ctx.textBaseline = "bottom";

                this.datasets.forEach(function (dataset) {
                    dataset.bars.forEach(function (bar) {
                        ctx.fillText(bar.value, bar.x, bar.y - 5);
                    });
                })
            },



            customTooltips: function (tooltip) {
                var tooltipEl = $("#chartjs-tooltip");

                if (!tooltip) {
                    tooltipEl.css({
                        opacity: 0
                    });
                    return;
                }

                tooltipEl.removeClass("above below");
                tooltipEl.addClass(tooltip.yAlign);
                console.log(data);
                // split out the label and value and make your own tooltip here
                var parts = tooltip.text.split(":");
                var innerHtml = "<span>" + parts[0].trim() + "</span> : <span><b>" + parts[1].trim() + " %</b></span>";
                tooltipEl.html(innerHtml);

                tooltipEl.css({
                    opacity: 1,
                    left: tooltip.chart.canvas.offsetLeft + tooltip.x + "px",
                    top: tooltip.chart.canvas.offsetTop + tooltip.y + "px",
                    fontFamily: tooltip.fontFamily,
                    fontSize: tooltip.fontSize,
                    fontStyle: tooltip.fontStyle,
                });
            }
        });

    ');

    // to highlight correct duration filter aka nav-pills
    $this->registerJs('
        var duration_type = $(\'[name="OrderSearch[type]"]\').val();
        if(duration_type == "")
            duration_type = "daily";
        $(".report-duration").find("." + duration_type).addClass("active");
        setTimeout(function(){
            $(\'[name="OrderSearch[type]"]\').val(-1);
        }, 1000);

    ');


Pjax::end();

?>
