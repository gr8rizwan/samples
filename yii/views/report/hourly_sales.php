<?php
/**
 * Area Analysis report
 * author: Rizwan Arshad
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\Pagination;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hourly Sales';
$this->params['breadcrumbs'][] = $this->title;

Pjax::begin();

?>


<div class="card-box">
    <?php echo $this->render('reports/_search', ['model' => $searchModel, 'filters' => ['from_date', 'branch_id', 'to_date', 'area']]); ?>
    <div class="row panel">
        <?= $this->render('reports/durations') ?>
        <div class="br-n pn col-sm-12 col-lg-12">
            <div id="navpills-1" class="tab-panel active">
                <?php if ($dataProvider->getTotalCount()) { ?>
                    <div class="row p-t-10">


                        <div class="col-sm-10 col-lg-10 col-md-10 col-lg-offset-1 text-center"
                             style="overflow: auto;">
                            <canvas style="overflow:visible; padding: 10px;margin: 10px;" id="bar-single"
                                    width="<?= $dataProvider->getTotalCount() * 60 ?>" height="500"></canvas>
                            <div id="chartjs-tooltip" class="below"
                                 style="opacity: 0; left: 663px; top: 12px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal;">
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <hr/>
                <div class="row">
                    <div class="col-sm-10 col-lg-10 col-lg-offset-1">
                        <div class="table-responsive">
                        <?php
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'showFooter' => true,
                            'footerRowOptions'=>['style'=>'font-weight:bold;background-color: lightgray;color: black;', 'class' => 'text-primary'],
                            'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
                            'columns' => [
                                [
                                    'attribute' => 'hourly_labels',
                                    'format' => 'raw',
                                    'value' => 'hourly_labels',
                                    'footer' => '<b>Aggregate:</b>'
                                ],
                                [
                                    'attribute' => 'total_orders',
                                    'format' => 'raw',
                                    'value' => 'total_orders',
                                    'footer' => $totalOrders
                                ],
                                [
                                    'attribute' => 'total_revenue',
                                    'format' => 'raw',
                                    'value' => function ($model){
                                        return Yii::$app->setting->formatePrice($model->revenue);
                                    },
                                    'footer' => $totalRevenue
                                ],
                                [
                                    'attribute' => 'average_ticket',
                                    'format' => 'raw',
                                    'value' => function ($model){
                                        return Yii::$app->setting->formatePrice($model->average_ticket);
                                    }
                                ]

                            ]
                        ]);

                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<style>
    #chartjs-tooltip {
        opacity: 0;
        position: absolute;
        background: rgba(0, 0, 0, .7);
        color: white;
        padding: 3px;
        border-radius: 3px;
        -webkit-transition: all .1s ease;
        transition: all .1s ease;
        pointer-events: none;
        -webkit-transform: translate(-50%, 0);
        transform: translate(-50%, 0);
    }
</style>

<?php
if ($dataProvider->getTotalCount())
    $this->registerJs('
        var ctx = $("canvas").get(0).getContext("2d");

        var data = {
            labels: ' . \yii\helpers\Json::encode($hourlyLabels) . ',
            datasets: [{
                fillColor: "#5fbeaa",
                strokeColor: "#5fbeaa",
                pointColor: "#f00",
                pointStrokeColor: "#f00",
                pointHighlightFill: "#f00",
                pointHighlightStroke: "#F00",

                data: ' . \yii\helpers\Json::encode($hourlyOrdersCount) . '
            }]
        };

        var myLineChart = new Chart(ctx).Bar(data, {
            scaleLabel: "     <%= value %>",

            customTooltips: function (tooltip) {
                var tooltipEl = $("#chartjs-tooltip");

                if (!tooltip) {
                    tooltipEl.css({
                        opacity: 0
                    });
                    return;
                }

                tooltipEl.removeClass("above below");
                tooltipEl.addClass(tooltip.yAlign);
                console.log(data);
                // split out the label and value and make your own tooltip here
                var parts = tooltip.text.split(":");
                var innerHtml = "<span><b>" + parts[3].trim() + "</b></span>";
                tooltipEl.html(innerHtml);

                tooltipEl.css({
                    opacity: 1,
                    left: tooltip.chart.canvas.offsetLeft + tooltip.x + "px",
                    top: tooltip.chart.canvas.offsetTop + tooltip.y + "px",
                    fontFamily: tooltip.fontFamily,
                    fontSize: tooltip.fontSize,
                    fontStyle: tooltip.fontStyle,
                });
            }
        });

    ');

    // to highlight correct duration filter aka nav-pills
    $this->registerJs('
        var duration_type = $(\'[name="OrderSearch[type]"]\').val();
        if(duration_type == "")
            duration_type = "daily";
        $(".report-duration").find("." + duration_type).addClass("active");
        setTimeout(function(){
            $(\'[name="OrderSearch[type]"]\').val(-1);
        }, 1000);

    ');


Pjax::end();

?>
