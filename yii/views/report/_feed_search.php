<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use backend\models\Branch;
use yii\widgets\Pjax;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */


$this->registerJs(
    '
        $(document).on("pjax:end", function() {
            $("#btn-compile-HTML button").trigger("click");

        });

        setTimeout(function(){
            window.area_id = $("#ordersearch-area").val();
            $("#ordersearch-branch_id").trigger("change");
        }, 500);
        
        $("#ordersearch-area").on("depdrop.afterChange", function(event, id, value) {
            if(window.area_id == "")
                return;

            if($("#ordersearch-branch_id").val() == ""){
                $("#ordersearch-area").val("");
                window.area_id = "";
                return;
            }

            isAreaExists = $("#ordersearch-area").find(\'[value=\' + window.area_id + \']\').length;
            if(isAreaExists > 0){
                $("#ordersearch-area").val(window.area_id);
            }

        });

    '
);

?>



<div class="row">


    <?php $form = ActiveForm::begin([
//        'action' => [''],
        'method' => 'get',
        'options' => ['data-pjax' => true ]
    ]); ?>


    <div class="col-lg-4 col-md-6 col-sm-12">
        <?= $form->field($model, 'branch_id')->dropDownList(ArrayHelper::map(Branch::find()->where(['is_deleted' => 0])->all(), 'id', 'name'), ['prompt'=>'Select Branch']) ?>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-12">
        <?=
        $form->field($model, 'area')->widget(DepDrop::classname(), [
            'data' => ArrayHelper::map(\backend\models\Area::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'), // set the initial display text
            'options' => ['placeholder' => 'Select Area '],
            'pluginOptions'=>[
                'allowClear' => true,
                'depends'=>['ordersearch-branch_id'],
                'url' => Url::to(['/branch/areas']),
                'laodingText' => 'Loading ...'
            ],
        ]);
        ?>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-12">
        <?= $form->field($model, 'customer')->textInput(); ?>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-12">
        <?= $form->field($model, 'current_status')->dropDownList(['Order Created' => 'Order Created', 'Not assigned' => 'Not assigned', 'Assigned to driver' => 'Assigned to driver', 'Completed' => 'Completed'], ['prompt' => 'Select Status']) ?>
    </div>


    <div class="col-lg-8 col-md-8 col-sm-12">
        <div class="form-group pull-right">
            <label class="control-label" style="width: 100%">&nbsp;</label>
            <?= Html::submitButton('<i class="fa fa-search m-r-5"></i> <span>Search</span>', ['class' => 'btn btn-primary waves-effect waves-light']) ?>
            <?= Html::a('<i class="fa fa-eraser m-r-5"></i> <span>Reset</span>', [''], ['class' => 'btn btn-default waves-effect waves-light']); ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
