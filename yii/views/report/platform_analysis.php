<?php

/**
 * Area Analysis report
 * author: Rizwan Arshad
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\Pagination;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$myDataProvider = clone $dataProvider;                  // need no pagination for graph data, but pagination for grid view

$myDataProvider->setPagination(false);

$this->title = 'Platform Analysis';
$this->params['breadcrumbs'][] = $this->title;

// set data for area analysis chart todo: move these arrays building in controller
$source = [];
$orders_count = [];
$total_orders = 0;
foreach ($myDataProvider->models as $record) {
    $source[] = $record->order_source;
    $orders_count[] = $record->total_orders;
    $total_orders += $record->total_orders;
};

$percent_data = [];
foreach ($myDataProvider->models as $record) {
    $percent_data[$record->order_source] = round(($record->total_orders / $total_orders) * 100);
};

$chart_colors = ['34d3eb', '5fbeaa', 'ebeff2'];

Pjax::begin();

?>


<div class="card-box">
    <?php echo $this->render('reports/_search', ['model' => $searchModel, 'filters' => ['from_date', 'branch_id', 'to_date', 'area', 'source']]); ?>
    <div class="row panel">
        <?= $this->render('reports/durations') ?>
        <div class="br-n pn col-sm-12 col-lg-12">
            <div id="navpills-1" class="tab-panel active">
                <?php if ($dataProvider->getTotalCount()) { ?>
                    <div class="row p-t-10">


                        <div class="col-sm-10 col-lg-10 col-md-10 col-lg-offset-1 text-center"
                             style="overflow: auto;">
                            <ul class="list-inline chart-detail-list text-center">

                                <?php foreach ($source as $index => $source_item) { ?>
                                    <li><h5><i class="fa fa-circle m-r-5" style="color: #<?= $chart_colors[$index] ?>"></i><?= $source_item ?></h5></li>
                                <?php } ?>

                            </ul>

                            <!--Area Split Chart will draw from DrawChart function. -->
                            <canvas id="source-chart" height="260"></canvas>

                            <div id="chartjs-tooltip" class="below"
                                 style="opacity: 0; left: 663px; top: 12px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal;">
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <hr/>
                <div class="row">
                    <div class="col-sm-10 col-lg-10 col-lg-offset-1">
                        <div class="table-responsive">
                        <?php
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
                            'columns' => [
                                [
                                    'attribute' => 'order_source',
                                    'format' => 'raw',
                                    'value' => 'order_source'
                                ],
                                [
                                    'attribute' => 'total_orders',
                                    'format' => 'raw',
                                    'value' => 'total_orders'
                                ],
                                [
                                    'attribute' => 'total_revenue',
                                    'format' => 'raw',
                                    'value' => function ($model){
                                        return Yii::$app->setting->formatePrice($model->revenue);
                                    }
                                ],
                                [
                                    'attribute' => 'average_ticket',
                                    'format' => 'raw',
                                    'value' => function ($model){
                                        return Yii::$app->setting->formatePrice($model->average_ticket);
                                    }
                                ],
                                [
                                    'attribute' => 'percentage',
                                    'format' => 'raw',
                                    'value' => function($model) use ($percent_data){
                                        return $percent_data[$model->order_source] . " %";
                                    }
                                ]

                            ]
                        ]);

                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<style>
    #chartjs-tooltip {
        opacity: 0;
        position: absolute;
        background: rgba(0, 0, 0, .7);
        color: white;
        padding: 3px;
        border-radius: 3px;
        -webkit-transition: all .1s ease;
        transition: all .1s ease;
        pointer-events: none;
        -webkit-transform: translate(-50%, 0);
        transform: translate(-50%, 0);
    }
</style>

<?php

$ctx_data = '[';
foreach ($source as $index => $data) {
    $ctx_data .= '{';
    $ctx_data .= 'value: ' . (isset($percent_data[$source[$index]]) ? $percent_data[$source[$index]] : 0) . ',';
    $ctx_data .= 'color:"#' . $chart_colors[$index] . '",';
    $ctx_data .= 'label: "' . $source[$index] . '"';
    $ctx_data .= '},';
}
$ctx_data .= ']';


if ($dataProvider->getTotalCount())
    $this->registerJs('
        var ctx = $("canvas").get(0).getContext("2d");

        var data = ' . $ctx_data . ';

        var myLineChart = new Chart(ctx).Pie(data, {
            scaleLabel: "     <%= value %> ",

            onAnimationComplete: function () {

                var ctx = this.chart.ctx;
//                ctx.font = this.scale.font;
                ctx.fillStyle = this.scale.textColor
                ctx.textAlign = "center";
                ctx.textBaseline = "bottom";

                this.datasets.forEach(function (dataset) {
                    dataset.bars.forEach(function (bar) {
                        ctx.fillText(bar.value, bar.x, bar.y - 5);
                    });
                })
            },



            customTooltips: function (tooltip) {
                var tooltipEl = $("#chartjs-tooltip");

                if (!tooltip) {
                    tooltipEl.css({
                        opacity: 0
                    });
                    return;
                }

                tooltipEl.removeClass("above below");
                tooltipEl.addClass(tooltip.yAlign);
                console.log(data);
                // split out the label and value and make your own tooltip here
                var parts = tooltip.text.split(":");
                var innerHtml = "<span>" + parts[0].trim() + "</span> : <span><b>" + parts[1].trim() + " %</b></span>";
                tooltipEl.html(innerHtml);

                tooltipEl.css({
                    opacity: 1,
                    left: tooltip.chart.canvas.offsetLeft + tooltip.x + "px",
                    top: tooltip.chart.canvas.offsetTop + tooltip.y + "px",
                    fontFamily: tooltip.fontFamily,
                    fontSize: tooltip.fontSize,
                    fontStyle: tooltip.fontStyle,
                });
            }
        });

    ');

    // to highlight correct duration filter aka nav-pills
    $this->registerJs('
        var duration_type = $(\'[name="OrderSearch[type]"]\').val();
        if(duration_type == "")
            duration_type = "daily";
        $(".report-duration").find("." + duration_type).addClass("active");
        setTimeout(function(){
            $(\'[name="OrderSearch[type]"]\').val(-1);
        }, 1000);

    ');


Pjax::end();

?>
