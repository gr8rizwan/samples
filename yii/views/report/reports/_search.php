<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use backend\models\Branch;
use yii\widgets\Pjax;
use kartik\depdrop\DepDrop;
use backend\models\OrderOrigin;
use kartik\daterange\DateRangePicker;
//use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    '$(document).on("pjax:end", function() {
            $("#btn-compile-HTML button").trigger("click");
        });

        setTimeout(function(){
            window.area_id = $("#ordersearch-area").val();
            $("#ordersearch-branch_id").trigger("change");
        }, 500);

        $("#ordersearch-area").on("depdrop.afterChange", function(event, id, value) {
            if(window.area_id == "")
                return;

            if($("#ordersearch-branch_id").val() == ""){
                $("#ordersearch-area").val("");
                window.area_id = "";
                return;
            }

            isAreaExists = $("#ordersearch-area").find(\'[value=\' + window.area_id + \']\').length;
            if(isAreaExists > 0){
                $("#ordersearch-area").val(window.area_id);
            }

        });

    '
);

$this->registerJs(
    '
    var from_date = $(\'[name="OrderSearch[from_date]"]\');
    from_date.off(\'apply.daterangepicker\');
    from_date.on(\'apply.daterangepicker\', function(ev, picker) {
        from_date.val(picker.startDate.format(\'YYYY-MM-DD\'));
    });

    var to_date = $(\'[name="OrderSearch[to_date]"]\');
    to_date.off(\'apply.daterangepicker\');
    to_date.on(\'apply.daterangepicker\', function(ev, picker) {
        to_date.val(picker.startDate.format(\'YYYY-MM-DD\'));
    });
    '
);

?>

<div class="row">


    <?php $form = ActiveForm::begin([
        'method' => 'get',
        'options' => ['data-pjax' => true ]
    ]); ?>


    <div class="row">

        <?php if( in_array('customer', $filters) ) { ?>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <?= $form->field($model, 'customer')->textInput(); ?>
            </div>
        <?php } ?>

        <?php if( in_array('branch_id', $filters) ) { ?>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <?= $form->field($model, 'branch_id')->dropDownList(ArrayHelper::map(Branch::find()->where(['is_deleted' => 0])->all(), 'id', 'name'), ['prompt'=>'Select Branch']) ?>
            </div>
        <?php } ?>

        <?php if( in_array('order_origin', $filters) ) { ?>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <?= $form->field($model, 'order_origin')->dropDownList(ArrayHelper::map(OrderOrigin::find()->all(), 'id', 'name'), ['prompt'=>'Select Origin']) ?>
            </div>
        <?php } ?>

        <?php if( in_array('area', $filters) ) { ?>

            <div class="col-lg-3 col-md-3 col-sm-12">
                <?=
                $form->field($model, 'area')->widget(DepDrop::classname(), [
                    'data' => ArrayHelper::map(\backend\models\Area::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'), // set the initial display text
                    'options' => ['placeholder' => 'Select Area '],
                    'pluginOptions'=>[
                        'allowClear' => true,
                        'depends'=>['ordersearch-branch_id'],
                        'url' => Url::to(['/branch/areas']),
                        'laodingText' => 'Loading ...'
                    ],
                ]);
                ?>
            </div>

        <?php } ?>

        <?php if( in_array('current_status', $filters) ) { ?>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <?= $form->field($model, 'current_status')->dropDownList(['Order Created' => 'Order Created', 'Not assigned' => 'Not assigned', 'Assigned to driver' => 'Assigned to driver', 'Completed' => 'Completed'], ['prompt' => 'Select Status']) ?>
            </div>
        <?php } ?>

        <?php if( in_array('code_status', $filters) ) { ?>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <?= $form->field($model, 'code_status')->dropDownList(['Inactive', 'Active'], ['prompt' => 'Select Status']) ?>
            </div>
        <?php } ?>

        <?php if( in_array('time_category', $filters) ) { ?>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <?= $form->field($model, 'time_category')->dropDownList(['Lunch non peak' => 'Lunch non peak' ,  'Lunch peak' =>  'Lunch peak', 'Dinner peak' => 'Dinner peak', 'Dinner non peak' => 'Dinner non peak',  'N/A'  =>  'N/A' ], ['prompt' => 'Select Status']) ?>
            </div>
        <?php } ?>

        <?php if( in_array('source', $filters) ) { ?>

            <div class="col-lg-3 col-md-3 col-sm-12">
                <?= $form->field($model, 'source')->dropDownList(Yii::$app->params['orderSource'], ['prompt' => 'Select Source']) ?>
            </div>

        <?php } ?>

        <?php if( in_array('from_date', $filters) ) { ?>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="form-group">
                    <label>From Date</label>
                    <?=
                    DateRangePicker::widget([
                        'name'=>'OrderSearch[from_date]',
                        'value'=> $model->from_date,
                        'convertFormat'=>true,
                        'pluginOptions'=>[
                            'singleDatePicker'=>true,
                            'locale'=>['format' => 'Y-m-d'],
                        ]
                    ]);
                    ?>
                </div>
            </div>
        <?php } ?>

        <?php if( in_array('to_date', $filters) ) { ?>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="form-group">
                    <label>To Date</label>
                    <?=
                    DateRangePicker::widget([
                        'name'=>'OrderSearch[to_date]',
                        'value'=> $model->to_date,
                        'convertFormat'=>true,
                        'pluginOptions'=>[
                            'singleDatePicker'=>true,
                            'locale'=>['format' => 'Y-m-d'],
                        ]
                    ]);
                    ?>
                </div>
            </div>
        <?php } ?>

        <?php if( in_array('time_filter', $filters) ) { ?>

            <div class="col-lg-3 col-md-3 col-sm-12">
                <?= $form->field($model, 'time_filter')->dropDownList(['daily' => 'Daily', 'weekly' => 'Weekly', 'monthly' => 'Monthly', 'yearly' => 'Yearly'] , ['prompt' => 'Select Time Filter']) ?>
            </div>

        <?php } ?>

        <div class="col-lg-6 col-md-6 col-sm-12">
            <?= $form->field($model, 'type')->hiddenInput()->label(false); ?>
        </div>


    </div>

    <div class="row">

        <?php if( isset($filters['grouping']) ) { ?>

                <?php foreach($filters['grouping'] as $key => $group) { ?>
                <div class="col-sm-2">

                    <?= $form->field($model, $group)->checkbox(); ?>
                </div>

                <?php } ?>

        <?php } ?>
    </div>

    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="form-group pull-right">
                <label class="control-label" style="width: 100%">&nbsp;</label>
                <?= Html::submitButton('<i class="fa fa-search m-r-5"></i> <span>Search</span>', ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                <?= Html::a('<i class="fa fa-eraser m-r-5"></i> <span>Reset</span>', [''], ['class' => 'btn btn-default waves-effect waves-light']); ?>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
