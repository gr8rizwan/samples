<script>

    // on click of duration nav pills, set filter values and submit search form.
    function setDateFilters(duration_type) {

        window.duration = duration_type;
        $('[name="OrderSearch[type]"]').val(duration_type);
        if (duration == 'daily') {
            $('[name="OrderSearch[from_date]"]').val(moment().format('YYYY-MM-DD'));
            $('[name="OrderSearch[to_date]"]').val(moment().format('YYYY-MM-DD'));
        } else if (duration == 'weekly') {
            $('[name="OrderSearch[from_date]"]').val(moment().subtract(1, 'week').format('YYYY-MM-DD'));
            $('[name="OrderSearch[to_date]"]').val(moment().format('YYYY-MM-DD'));
        } else if (duration == 'monthly') {
            $('[name="OrderSearch[from_date]"]').val(moment().subtract(1, 'month').format('YYYY-MM-DD'));
            $('[name="OrderSearch[to_date]"]').val(moment().format('YYYY-MM-DD'));
        } else if (duration == 'yearly') {
            $('[name="OrderSearch[from_date]"]').val(moment().subtract(1, 'year').format('YYYY-MM-DD'));
            $('[name="OrderSearch[to_date]"]').val(moment().format('YYYY-MM-DD'));
        }

        $(".report-duration li").removeClass("active");

        $(".report-duration").find("." + duration_type).addClass("active");

        setTimeout(function () {
            $(".report-duration").find("." + duration_type).addClass("active");
        }, 1000)


        $('[name="OrderSearch[from_date]"]').closest('form').submit();
    }

</script>

<div class="row">
    <div class="col-sm-12 col-lg-12">
        <hr/>
        <ul class="nav nav-pills pull-right report-duration">
            <li class="daily">
                <a title="Yesterday" href="#" data-toggle="tab" aria-expanded="true"
                   onclick="setDateFilters('daily')">Daily</a>
            </li>
            <li class="weekly">
                <a title="Last 7 days" href="#" data-toggle="tab" aria-expanded="false"
                   onclick="setDateFilters('weekly')">Weekly</a>
            </li>
            <li class="monthly">
                <a title="Last 30 days" href="#" data-toggle="tab" aria-expanded="false"
                   onclick="setDateFilters('monthly')">Monthly</a>
            </li>
            <li class="yearly">
                <a title="Last 365 days" href="#" data-toggle="tab" aria-expanded="false"
                   onclick="setDateFilters('yearly')">Yearly</a>
            </li>
        </ul>
    </div>
</div>
<?php

    // to highlight correct duration filter aka nav-pills
    $this->registerJs('
        var duration_type = $(\'[name="OrderSearch[type]"]\').val();
        if(duration_type == "")
            duration_type = "daily";
        $(".report-duration").find("." + duration_type).addClass("active");
        setTimeout(function(){
            $(\'[name="OrderSearch[type]"]\').val(-1);
        }, 1000);

    ');

?>