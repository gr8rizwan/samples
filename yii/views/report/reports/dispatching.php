<?php
/**
 * Dispatch order report
 * author: Nadeem Akhtar <nadeem@myswich.com>
 * Date: 2/4/16
 * Time: 2:24 PM
 */

use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\StoreProcedures;

$baseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/feed/';
$themeBaseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/ubold/';

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dispatch Report';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">
    <div class="row">

        <?php foreach($branchs as $branch) { ?>
            <div class="col-sm-6">
                <div class="portlet">
                    <div class="portlet-heading bg-primary">
                        <h3 class="portlet-title">
                            <?= $branch['name']; ?>
                        </h3>

                        <div class="clearfix"></div>
                    </div>
                    <div id="bg-primary" class="panel-collapse collapse in">

                        <div class="portlet-body">
                            <p>Number of Orders: <b><?= $totalOrders = $branch['normalDispatch']->total_orders + $branch['lateDispatch']->total_orders; ?></b> </p>
                            <p>Average Dispatch: <b><?= Yii::$app->dateTime->getSecondToHoursSecond( ($branch['normalDispatch']->average_ticket + $branch['lateDispatch']->average_ticket) / 2) ?></b> </p>
                            <p>20 min and more Orders: <b><?= $branch['lateDispatch']->total_orders; ?></b> </p>
                        </div>

                    </div>
                </div>
            </div>

        <?php } ?>

    </div>
</div>

<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel, 'filters' => ['from_date', 'to_date', 'branch_id', 'area', 'time_category'] ]); ?>
</div>

<div class="order-index card-box" ng-app="swichFeed" ng-controller="feedController as feed">

    <div class="table-responsive">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'time',
                'value' => function($model){
                    return yii::$app->dateTime->getReadableDateAndTimeFormat($model->time);
                }
            ],
            [
                'attribute' => 'customer_id',
                'format' => 'raw',
                'value' => function($data) {
                    $html = '';
                    $html .= strtoupper($data->customer->first_name.' '.$data->customer->last_name).'</a> ';
                    $html = Html::a($html, ['customer/view', 'id' => $data->customer->id], [ 'target' => '_blank', 'data-pjax' => 'false']);
                    $html .= '<br /><b>'.$data->customer->phone_number.'</b><br />';
                    return $html;
                }
            ],
            [
                'attribute' => 'branch_id',
                'format' => 'raw',
                'value' => function($data) {
                    return $data->branch->name;
                }
            ],
            [
                'attribute' => 'area',
                'format' => 'raw',
                'value' => function( $model ) {
                    return (int)$model->address_id > 0 ? yii::$app->setting->formatAddress($model->address_id, true) : 'Pickup';
                }
            ],
            [
                'attribute' => 'time_category',
                'value' => function($model) {
                    return Yii::$app->setting->getOrderTimeCategory($model->time);
                },
            ],

            [
                'attribute' => 'time_took',
                'format' => 'raw',
                'value' => function($model) {
                    return Yii::$app->dateTime->getSecondToHoursSecond($model->dispatched_at - $model->created_at);
                }
            ],
            //'payment_type',
            //'is_favorite',
            //'price',
            //'discount',
            //'delivery_time',
            //'customer_id',
            //'address_id',
            //'agent_id',
            //'branch_id',
            //'comments:ntext',
            //'om_id',
            //'delivery_type',
            //'source',
            //'agentMessage',

            [
                'class' => 'common\helpers\CustomActionColumn',
                'template'=>'{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return '<button ng-click="showOrderDetails(' . $model->id .')"  class="btn btn-info pull-right glyphicon glyphicon-zoom-in"></button>';
                    }
                ]

            ],
        ],
    ]); ?>

    </div>

    <?php

    // order details modal box
    echo $this->render('/modals/order-details');

    ?>

</div>

<script src="<?= $themeBaseURL; ?>assets/js/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="<?= $baseURL; ?>js/angular-timeago.js"></script>
<script src="<?= $baseURL; ?>js/app.js"></script>
<script src="<?= $baseURL; ?>js/feedFactory.js"></script>
<script src="<?= $baseURL; ?>js/feedController.js"></script>
<script src="<?= $baseURL; ?>js/frameFilter.js"></script>