<?php

use yii\helpers\Html;
use yii\grid\GridView;

$baseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/feed/';
$themeBaseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/ubold/';

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-box">
    <?php echo $this->render('_order_search', ['model' => $searchModel]); ?>
</div>

<div class="order-index card-box" ng-app="swichFeed" ng-controller="feedController as feed">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-actions-bar'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'customer_id',
                'format' => 'raw',
                'options' => ['style' => 'width:30%;'],
                'value' => function($data) {
                    $html = '';
                    $html .= strtoupper($data->customer->first_name.' '.$data->customer->last_name).'</a> ';
                    $html = Html::a($html, ['customer/view', 'id' => $data->customer->id], [ 'target' => '_blank', 'data-pjax' => 'false']);
                    $html .= '<br /><b>'.$data->customer->phone_number.'</b><br />';
                    return $html;
                }
            ],
            [
                'attribute' => 'Area',
                'format' => 'raw',
                'value' => function( $model ) {
                    return (int)$model->address_id > 0 ? yii::$app->setting->formatAddress($model->address_id, true) : 'Pickup';
                }
            ],
            [
                'attribute' => 'total_sum',
                'value' => function($model) {
                    return Yii::$app->setting->formatePrice($model->total_sum);
                },
            ],

            [
                'attribute' => 'source',
            ],

            [
                'attribute' => 'time',
            ],

            [
                'attribute' => 'branch_id',
                'format' => 'raw',
                'value' => function($data) {
                    return $data->branch->name;
                }
            ],
            'current_status',
            //'payment_type',
            //'is_favorite',
            //'price',
            //'discount',
            //'delivery_time',
            //'customer_id',
            //'address_id',
            //'agent_id',
            //'branch_id',
            //'comments:ntext',
            //'om_id',
            //'delivery_type',
            //'source',
            //'agentMessage',

            [
                'class' => 'common\helpers\CustomActionColumn',
                'template'=>'{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return '<button ng-click="showOrderDetails(' . $model->id .')"  class="btn btn-info pull-right glyphicon glyphicon-zoom-in"></button>';
                    }
                ]

            ],
        ],
    ]);

    // order details modal box
    echo $this->render('/modals/order-details');

    ?>


</div>

<script src="<?= $themeBaseURL; ?>assets/js/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="<?= $baseURL; ?>js/angular-timeago.js"></script>
<script src="<?= $baseURL; ?>js/app.js"></script>
<script src="<?= $baseURL; ?>js/feedFactory.js"></script>
<script src="<?= $baseURL; ?>js/feedController.js"></script>
<script src="<?= $baseURL; ?>js/frameFilter.js"></script>