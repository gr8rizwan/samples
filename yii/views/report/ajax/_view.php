<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$provider = new ArrayDataProvider([
    'allModels' => $model['Items'],
]);

?>

<div class="card-box max-hight-300">

    <?= GridView::widget([
        'dataProvider' => $provider,
        'layout' => "{items}",
        'tableOptions' => ['class' => 'table table-actions-bar'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Name',
                'filter' => false,
                'format' => 'raw',
                'value' => function($data) {

                    $html = '<b>'.$data['name'].'</b><br />';
                    $html .= '<p>'.$data['description'].'<p/>';

                    if($data['isFree'] == 1) {
                        $html .= '<p style="color: #008000">'.$data['freeReason'].'<p/>';
                    }


                    return $html;
                }
            ],

            [
                'attribute' => 'Quantity',
                'filter' => false,
                'format' => 'raw',
                'value' => function($data) {
                    return $data['quantity'];
                }
            ],

            [
                'attribute' => 'Price',
                'filter' => false,
                'format' => 'raw',
                'value' => function($data) {

                    if($data['isFree'] == 1) {
                        return Yii::$app->setting->formatePrice(0);
                    } else {
                        return Yii::$app->setting->formatePrice($data['single_price']);
                    }

                }
            ],

            [
                'attribute' => 'Total',
                'filter' => false,
                'format' => 'raw',
                'value' => function($data) {

                    if($data['isFree'] == 1) {
                        return '<p style="color: #008000"><b>FREE</b></p>';
                    } else {
                        return Yii::$app->setting->formatePrice($data['single_price']*$data['quantity']);
                    }

                }
            ],

        ],
    ]); ?>

</div>


<div class="card-box">
    <p>
        <b>Building: </b><?= $model['Order']['address']['building'] ?> &nbsp;
        <b>Apartment: </b><?= $model['Order']['address']['apartment'] ?> &nbsp;
        <b>Street: </b><?= $model['Order']['address']['street'] ?> &nbsp;
        <b>Area: </b><?= $model['Order']['address']['area']['name'] ?> &nbsp; <br>
        <b>Special Instructions: </b><?= $model['Order']['address']['special_instructions'] ?> &nbsp;
    </p>
</div>

<div class="modal-footer">
    <a href="#" class="btn btn-success btn-custom waves-effect waves-light close_link" data-dismiss="modal">Close</a>
</div>