<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Branch;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Branch Report';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="card-box">
        <?php echo $this->render('reports/_search', ['model' => $searchModel, 'filters' => ['branch_id', 'from_date', 'to_date', 'area', 'source', 'time_category']]); ?>
</div>

<div class="card-box">
    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <div class="table-responsive">
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'Branch',
                        'format' => 'raw',
                        'value' => function($data) {

                            if($data->branch) {
                                return $data->branch->name;
                            } else {
                                return 'N/A';
                            }
                        }
                    ],
                    [
                        'attribute' => 'Area',
                        'format' => 'raw',
                        'value' => function( $model ) {
                            return (int)$model->address_id > 0 ? yii::$app->setting->formatAddress($model->address_id, true) : 'Pickup';
                        }
                    ],
                    [
                        'attribute' => 'Source',
                        'format' => 'raw',
                        'value' => function($data) {
                            return $data->order_source;
                        }
                    ],
                    [
                        'attribute' => 'Total Orders',
                        'format' => 'raw',
                        'value' => function($data) {
                            return $data->total_orders;
                        }
                    ],
                    [
                        'attribute' => 'Revenue',
                        'format' => 'raw',
                        'value' => function($data) {
                            return Yii::$app->setting->formatePrice($data->revenue);
                        }
                    ],
                    [
                        'attribute' => 'Average Ticket',
                        'format' => 'raw',
                        'value' => function($data) {
                            return Yii::$app->setting->formatePrice($data->average_ticket);
                        }
                    ],
                    [
                        'attribute' => 'Time Category',
                        'format' => 'raw',
                        'value' => function($data) {
                            return $data->order_time_category;
                        }
                    ]
                ]
            ])
            ?>
            </div>
        </div>
    </div>
</div>


