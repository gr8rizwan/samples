<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Branch;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promo Code Report';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="card-box">
        <?php echo $this->render('reports/_search', ['model' => $searchModel, 'filters' => ['branch_id', 'from_date', 'to_date', 'time_category', 'code_status']]); ?>
</div>

<div class="card-box">
    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <div class="table-responsive">

                <?php

                echo GridView::widget([
                    'layout' => "{items}\n{pager}",
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
                    'columns' => [
                        [
                            'attribute' => 'Branch',
                            'format' => 'raw',
                            'value' => function($model){
                                return ($model->branch) ? $model->branch->name : 'N/A';
                            }
                        ],
                        [
                            'attribute' => 'source',
                            'format' => 'raw',
                            'value' => 'source'
                        ],
                        [
                            'attribute' => 'code',
                            'format' => 'raw',
                            'value' => 'code'
                        ],
                        [
                            'attribute' => 'total_orders',
                            'format' => 'raw',
                            'value' => function($model){
                                return $model->total_orders;
                            }
                        ],
                        [
                            'attribute' => 'Revenue',
                            'format' => 'raw',
                            'value' => function($model){
                                return $model->totalRevenue;
                            }
                        ],
                        [
                            'attribute' => 'discount',
                            'format' => 'raw',
                            'value' => function($model){
                                return $model->discount;
                            }
                        ],
                        [
                            'attribute' => 'Percentage',
                            'format' => 'raw',
                            'value' => function($model){
                                return round($model->discount/$model->totalRevenue*100) . '%';
                            }
                        ],
                        [
                            'class' => 'common\helpers\CustomActionColumn',
                            'template'=>'{view}',
                            'buttons' => [
                                'view' => function ($url, $model) use ($searchModel) {
                                    return Html::button('', [
                                        'value' => \yii\helpers\Url::to(['/order/promo-code',
                                            'OrderSearch' => ['branch_id' => $model->branch_id,
                                            'source' => $model->source,
                                            'promo_code' => $model->code,
                                            'time_category' => $searchModel->time_category,
                                            'from_date' => $searchModel->from_date,
                                            'to_date' => $searchModel->to_date,
                                        ]]), 'title' => 'Order Details',
                                        'class' => 'showModalButton glyphicon glyphicon-zoom-in btn btn-default btn-custom']);
                                }
                            ]

                        ]
                    ]
                ]);

                ?>

            </div>
        </div>
    </div>
</div>


