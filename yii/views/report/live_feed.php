<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Live Order feeds';
$this->params['breadcrumbs'][] = $this->title;
$baseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/feed/';
$themeBaseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/ubold/';

?>
<div class="row" ng-app="swichFeed" ng-controller="feedController as feed"  >

    <div class="col-sm-12">

        <!--Tabs heading Default should be Live Feed Tab.-->

        <ul class="nav nav-tabs">

            <li class="active">
                <a href="#grid-view" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-table"></i></span>
                    <span class="hidden-xs">Grid View</span>
                </a>
            </li>

            <li>
                <a href="#live-feed" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-refresh"></i></span>
                    <span class="hidden-xs">Live Feed</span>
                </a>
            </li>
            <li class="pull-right">
                <button type="button" ng-click="reloadFeedData()" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i> &nbsp; {{ button }} </button>
            </li>
            <li class="pull-right hidden" id="btn-compile-HTML">
                <button type="button" ng-click="compileHTML()" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i> &nbsp; Compile </button>
            </li>

        </ul>

        <div class="tab-content">


                <div class="tab-pane active" id="grid-view">

                    <?php Pjax::begin(['id' => 'live-feed-data-grid']) ?>

                    <!--This should be a widget, Just pass data and that will print these boxes here -->
                    <div class="row">

                        <?php foreach(Yii::$app->setting->getAllBranches() as $branch) { ?>

                            <div class="col-lg-6 col-sm-12">
                                <div class="portlet">
                                    <div class="portlet-heading bg-primary">
                                        <h3 class="portlet-title">
                                            <?= $branch->name; ?>
                                        </h3>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="bg-primary" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                            <p>Number of Orders: <b><?= $totalOrders = $branch->getTodayOrders()->count('id'); ?></b> </p>

                                            <?php if($canViewRevenue){ // check permission if allowed ?>
                                                <p>Total Revenue: <b> <?= Yii::$app->setting->formatePrice($totalSum = $branch->getTodayOrders()->sum('total_sum')); ?></b> </p>
                                                <p>Average Ticket:<b><?= @Yii::$app->setting->formatePrice($totalSum/($totalOrders - $branch->getTodayResolutions()->count('id'))) ?> </b></p>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                    </div>


                    <div class="row">

                        <div class="card-box">
                            <?php echo $this->render('_feed_search', ['model' => $searchModel]); ?>
                        </div>

                        <div class="card-box">
                        <div class="table-responsive">
                            <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'attribute' => 'customer_id',
                                    'format' => 'raw',
                                    'value' => function($data) {

                                        $html = 'N/A';

                                        if($data->customer) {
                                            $html = '';
                                            $html .= strtoupper($data->customer->first_name.' '.$data->customer->last_name).'</a> ';
                                            $html = Html::a($html, ['customer/view', 'id' => $data->customer->id],
                                                [ 'target' => '_blank', 'data-pjax' => 'false']);
                                            $html .= '<br /><b>'.$data->customer->phone_number.'</b><br />';
                                        }
                                        return $html;

                                    }
                                ],
                                [
                                    'attribute' => 'Area',
                                    'format' => 'raw',
                                    'value' => function( $model ) {
                                            return (int)$model->address_id > 0 ? yii::$app->setting->formatAddress($model->address_id, true) : 'Pickup';
                                    }
                                ],
                                [
                                    'attribute' => 'total_sum',
                                    'value' => function($model) {
                                        return Yii::$app->setting->formatePrice($model->total_sum);
                                    },
                                ],

                                [
                                    'attribute' => 'source',
                                ],

                                [
                                    'attribute' => 'time',
                                    'value' => function($model){
                                        return yii::$app->dateTime->getReadableDateAndTimeFormat($model->time);
                                    }
                                ],

                                [
                                    'attribute' => 'branch_id',
                                    'format' => 'raw',
                                    'value' => function($data) {

                                        if($data->branch) {
                                            return $data->branch->name;
                                        } else {
                                            return 'N/A';
                                        }
                                    }
                                ],
                                'current_status',

                                [
                                    'class' => 'common\helpers\CustomActionColumn',
                                    'template'=>'{view}',
                                    'buttons' => [
                                        'view' => function ($url, $model) {
                                            return '<button ng-click="showOrderDetails(' . $model->id .')" class="btn btn-info pull-right glyphicon glyphicon-zoom-in"></button>';
                                        }
                                    ]

                                ],
                            ],
                        ]); ?>
                        </div>
                        <?php Pjax::end() ?>
                        </div>
                    </div>

                </div>

                <div class="tab-pane" id="live-feed">
                    <?php echo $this->render('_live_view', []); ?>
                </div>

            <?php

            // order details modal box
            echo $this->render('/modals/order-details');

            ?>

        </div>
    </div>

</div>