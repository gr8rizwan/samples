<?php

$baseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/feed/';
$themeBaseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/ubold/';

?>
<link href="<?= $baseURL; ?>css/app.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<div class="row">
    <div id="swich-feed-container">
        <div id="swich-feed">
            <div>
                <h3 class="swich-title col-md-4 text-left">S`WICH ORDERS FEED ({{ orders.length }})</h3>

                <div class="pull-right" ng-repeat="branch in BranchOrders">
                    <div class="label label-primary branch-info">
                        {{ branch.name }}:
                        [ {{ branch.counter }} ]
                    </div>
                </div>
                <div class="clearfix"></div>
                <div ng-repeat="frame in timeFrames" ng-hide="orders.length == 0 || showBranchData[key]">
                    <div class="col-lg-4">
                        <div class="portlet">
                            <div class="portlet-heading {{ frame.colorClass }}">
                                <h3 class="portlet-title">
                                    {{ frame.title }}
                                    [ {{ ( orders | frameFilter : frame).length }} ]
                                </h3>

                                <div class="clearfix"></div>
                            </div>

                        </div>
                        <div class="portlet order-box" ng-repeat="order in orders | orderBy : order.time : true | frameFilter : frame">

                            <div id="bg-primary" class="">
                                <div class="order-top-bar {{ frame.colorClass }}"
                                     ng-click="toggleOrderView(order.id);">
                                    <span class="pull-left">{{ order.source | uppercase }}</span>
                                    <span class="pull-right">{{ order.total_sum | currency:"" }} AED</span>
                                </div>
                                <div class="portlet-body" ng-show="ordersView[order.id]">
                                    <strong>Customer Name:</strong> <span class="pull-right">{{ order.customer.first_name }} {{ order.customer.last_name }}</span>
                                    <br>
                                    <strong>Customer Mobile:</strong> <strong><span class="pull-right">{{ order.customer.phone_number }}</span></strong>
                                    <br>
                                    <strong>Area of Delivery:</strong> <span class="pull-right">{{ order.address.area_name }}</span>
                                    <br>
                                    <strong>Payment Type:</strong>
                                    <span class="pull-right">{{ order.payment_type }}</span>
                                    <br>

                                    <strong>Status:</strong>
                                    <strong class="pull-right bg-primary">{{ order.current_status }}</strong>
                                    <br>
                                    <strong>Order Time:</strong> <span class="pull-right">{{ (order.time).split(' ')[1] }}</span>
                                    <br>

                                    <div ng-show="order.discount > 0">
                                        <strong>Price:</strong> <span class="pull-right">{{ order.price | currency:"" }} AED</span>
                                        <br>
                                    </div>

                                    <span ng-show="order.discount > 0"><strong>Discount:</strong> <span
                                                        class="pull-right">{{ order.discount | currency:"" }} AED </span><br> </span>
                                    <strong>Total:</strong> <strong><span class="pull-right">{{ order.total_sum | currency : "" }} AED</span></strong>
                                    <br>
                                </div>
                                <div class="order-bottom-bar">
                                    <strong class="time-ago pull-left"> {{ timeAgoFilter(order.time) | timeAgo }} </strong>
                                    <button
                                        ng-class="isCustomerCalled(order)?'btn-danger':'btn-info'"
                                        ng-click="!isCustomerCalled(order)?createAction(order):swal('Customer is already called.');"
                                        class="btn pull-right glyphicon glyphicon-earphone"></button>
                                    <button ng-click="showOrderDetails(order.id)"
                                            class="btn btn-info pull-right glyphicon glyphicon-zoom-in"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>
<script>
    var resizefunc = [];
</script>
<!-- jQuery  -->
<script src="<?= $themeBaseURL; ?>assets/js/jquery.min.js"></script>
<script src="<?= $baseURL; ?>js/angular-timeago.js"></script>
<script src="<?= $baseURL; ?>js/app.js"></script>
<script src="<?= $baseURL; ?>js/feedFactory.js"></script>
<script src="<?= $baseURL; ?>js/feedController.js"></script>
<script src="<?= $baseURL; ?>js/frameFilter.js"></script>
