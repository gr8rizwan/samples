<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use backend\models\Branch;
use kartik\daterange\DateRangePicker;
use kartik\depdrop\DepDrop;

$dry_customers = false;
if(isset(Yii::$app->request->queryParams['CustomerSearch']))
    $dry_customers = (boolean)Yii::$app->request->queryParams['CustomerSearch']['dry_customers'];

/* @var $this yii\web\View */
/* @var $model backend\models\CollectionSearch */
/* @var $form yii\widgets\ActiveForm */

$js = '';
if (!$dry_customers) {
    $js = '        setTimeout(function(){
            window.area_id = $("#customersearch-area").val();
            $("#customersearch-branch_id").trigger("change");
        }, 500);
    ';
}
$this->registerJs(
    $js . '

        $("#customersearch-area").on("depdrop.afterChange", function(event, id, value) {
            if(window.area_id == "")
                return;

            if($("#customersearch-branch_id").val() == ""){
                $("#customersearch-area").val("");
                window.area_id = "";
                return;
            }

            isAreaExists = $("#customersearch-area").find(\'[value=\' + window.area_id + \']\').length;
            if(isAreaExists > 0){
                $("#customersearch-area").val(window.area_id);
            }

        });

    '
);

?>

<div class="card-box">

    <?php $form = ActiveForm::begin([
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?= $form->field($model, 'branch_id')
                ->dropDownList(ArrayHelper::map(Branch::find()->all(), 'id', 'name'), ['disabled' => $dry_customers, 'prompt'=>'Select Branch']) ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?=
            $form->field($model, 'area')->widget(DepDrop::classname(), [
                'data' => ArrayHelper::map(\backend\models\Area::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'), // set the initial display text
                'options' => ['disabled' => $dry_customers, 'placeholder' => 'Select Area '],
                'pluginOptions'=>[
                    'allowClear' => true,
                    'depends'=>['customersearch-branch_id'],
                    'url' => Url::to(['/branch/areas']),
                    'laodingText' => 'Loading ...'
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="form-group">
                <label>From Date</label>
                <?=
                DateRangePicker::widget([
                    'name'=>'CustomerSearch[from_date]',
                    'value'=> $model->from_date,
                    'convertFormat'=>true,
                    'pluginOptions'=>[
                        'singleDatePicker'=>true,
                        'locale'=>['format' => 'Y-m-d'],
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="form-group">
                <label>To Date</label>
                <?=
                DateRangePicker::widget([
                    'name'=>'CustomerSearch[to_date]',
                    'value'=> $model->to_date,
                    'convertFormat'=>true,
                    'pluginOptions'=>[
                        'singleDatePicker'=>true,
                        'locale'=>['format' => 'Y-m-d'],
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 pull-left " onclick="disable_area();">

                <?= $form->field($model, 'dry_customers')
                    ->checkbox(['label' => '&nbsp; Show customers with no order placed', 'id' => 'dry_customers']) ?>

        </div>


        <div class="col-lg-3 col-md-3 col-sm-12 pull-right">
            <div class="form-group pull-right text-right">
                <label class="control-label" style="width: 100%">&nbsp;</label>
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

        </div>



    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$this->registerJs(
    '
    var from_date = $(\'[name="CustomerSearch[from_date]"]\');
    from_date.off(\'apply.daterangepicker\');
    from_date.on(\'apply.daterangepicker\', function(ev, picker) {
        from_date.val(picker.startDate.format(\'YYYY-MM-DD\'));
    });

    var to_date = $(\'[name="CustomerSearch[to_date]"]\');
    to_date.off(\'apply.daterangepicker\');
    to_date.on(\'apply.daterangepicker\', function(ev, picker) {
        to_date.val(picker.startDate.format(\'YYYY-MM-DD\'));
    });
    '
);

?>

<script>
    function disable_area(){
        var el = $('#dry_customers');
        var is_checked = el.is(':checked');
        if(is_checked){
            $('#customersearch-branch_id').prop('disabled', true)
            $('#customersearch-area').prop('disabled', true)
        }else{
            $('#customersearch-branch_id').prop('disabled', false)
            $('#customersearch-area').prop('disabled', false)
        }
    }
//    setTimeout('disable_area()', 1000);  // disable branch and area dropdowns if dry_customers is selected
</script>
