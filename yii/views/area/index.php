<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Areas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="area-index">

    <p class="pull-right">
        <?= Html::a('Create Area', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                "attribute" => "branch_id",
                'filter' => true,
                'value' => function($data) {
                    return $data->branch->name;
                },
                "format" => "raw",
            ],

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
