<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use common\helpers\MenuHelper;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="s'wich IT team">
    <meta name="description" content="S'wich artificial dashboard, reporting dashboard, analytics, reporting">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="<?= Yii::getAlias('@web').'/themes/ubold/assets/js/modernizr.min.js' ?>"></script>

</head>

<?php
/*echo Menu::widget([
    'items' => Yii::$app->setting->getAdminMenu(),
    'submenuTemplate' => "\n<ul class='treeview-menu'>\n{items}\n</ul>\n",
    'encodeLabels' => false,
    'options' => ['class' => 'sidebar-menu'],
]);*/
?>

<body class="fixed-left">

<?php $this->beginBody() ?>

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <?= Html::a('<i class="ion-laptop icon-c-logo"></i><span>S\'wich</span>', ['/site/index'], ['class' => 'logo']) ?>

            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="">

                    <div class="pull-left">
                        <button class="button-menu-mobile open-left">
                            <i class="ion-navicon"></i>
                        </button>
                        <span class="clearfix"></span>
                    </div>


                    <ul class="nav navbar-nav navbar-right pull-right">
                        <li class="dropdown hidden-xs">
                            <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">3</span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-lg">
                                <li class="notifi-title"><span class="label label-default pull-right">New 3</span>Notification</li>
                                <li class="list-group nicescroll notification-list">
                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-diamond fa-2x text-primary"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                                                <p class="m-0">
                                                    <small>There are new settings available</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>

                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-cog fa-2x text-custom"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">New settings</h5>
                                                <p class="m-0">
                                                    <small>There are new settings available</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>

                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-bell-o fa-2x text-danger"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">Updates</h5>
                                                <p class="m-0">
                                                    <small>There are <span class="text-primary font-600">2</span> new updates available</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>

                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-user-plus fa-2x text-info"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">New user registered</h5>
                                                <p class="m-0">
                                                    <small>You have 10 unread messages</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>

                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-diamond fa-2x text-primary"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                                                <p class="m-0">
                                                    <small>There are new settings available</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>

                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-cog fa-2x text-custom"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">New settings</h5>
                                                <p class="m-0">
                                                    <small>There are new settings available</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="list-group-item text-right">
                                        <small class="font-600">See all notifications</small>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="hidden-xs">
                            <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img src="<?= Yii::getAlias('@web').'/images/swich.png' ?>" alt="user-img" class="img-circle"> </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <?= Html::a('<i class="ti-settings m-r-5"></i> Profile', ['user/update', 'id' => Yii::$app->user->identity->id]) ?>
                                </li>
                                <li><?= Html::a('<i class="ti-power-off m-r-5"></i> Logout', ['/site/logout'], ['class' => 'profile-link']) ?></li>

                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->



    <div class="left side-menu">
        <div class="sidebar-inner">
            <!--- Divider -->
            <div id="sidebar-menu">

                <?php
                echo Menu::widget([
                    'items' => MenuHelper::getMenu(),
                    'submenuTemplate' => "\n<ul class='list-unstyled'>\n{items}\n</ul>\n",
                    'encodeLabels' => false,
                    'options' => [],
                ]);
                ?>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title"><?= $this->title ?></h4>
                        <?= Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                    </div>
                </div>

                <?= $content ?>

            </div>
            <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            2015 © Munch Capital.
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

</div>
<!-- END wrapper -->


<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true, 'tabindex' => false],
    'options' => ['tabindex' => false]
]);
echo "<div id='modalContent'><div style='text-align:center'><img src='".Yii::getAlias('@web').'/images/loader_gif.gif'."'></div></div>";
yii\bootstrap\Modal::end();
?>

<?php $this->endBody() ?>

</body>


<script>
    var resizefunc = [];
</script>

<script type="text/javascript">


    $(function() {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });

        $(".knob").knob();
        $(".button-menu-mobile").trigger('click');
    });

</script>

<script type="text/javascript">

    $('form#ajaxSubmit').on('beforeSubmit',function () {

        alert('working');

        return false;
        var form = $(this);
        // return false if form still have some validation errors
        if (form.find('.has-error').length) {
            return false;
        }

        // dataGridId is the datagrid id that need to reload
        // refId is a custom form attribute that will allow to access datagrid
        var dataGridId = form.attr('refId');

        // ajaxSubmitBtn is a button that that used to trigger submit action

        $('#ajaxSubmitBtn').attr('disable', true);
        $('#ajaxSubmitBtn').val('Processing...');

        // submit form
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function (response) {
                if(response.status == 'OK') {
                    var html = "<div class='alert alert-success' role='alert'>"+response.message+"</div>";
                    html += "<div class='modal-footer'>";
                    html += "<a href='#' class='btn btn-success close_link' data-dismiss='modal'>Close</a>";
                    html += "</div>";
                    form.html(html);

                    $.pjax.reload({container:'#assignment-detail'});
                }

                if(response.status == 'ERROR') {
                    alert(response.message);
                }
            }
        });
        return false;
    });
</script>

</html>
<?php $this->endPage() ?>

