<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::getAlias('@web').'/themes/admin-lte/dist/img/user2-160x160.jpg'; ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>

        <?php
        $menuItems = [
            [
                'label' => '<i class="fa fa-dashboard"></i> <span>Dashboard</span>',
                'url' => ['/site/index']
            ],
            [
                'label' => ' <i class="fa fa-pie-chart"></i><span>Permissions</span><i class="fa fa-angle-left pull-right"></i>',
                'url'   => '#',
                'items' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i> Auth Assignment',
                        'url' => ['/admin'],
                        'visible' => \Yii::$app->user->can("rule_assignment")
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i> Routes',
                        'url' => ['/admin/route'],
                        'visible' => \Yii::$app->user->can("rule_assignment")
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i> Permission',
                        'url' => ['/admin/permission'],
                        'visible' => \Yii::$app->user->can("rule_assignment")
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i> Menus',
                        'url' => ['/admin/menu'],
                        'visible' => \Yii::$app->user->can("rule_assignment")
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i> Roles',
                        'url' => ['/admin/role'],
                        'visible' => \Yii::$app->user->can("rule_assignment")
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i> Assignment',
                        'url' => ['/admin/assignment'],
                        'visible' => \Yii::$app->user->can("rule_assignment")
                    ]
                ],
                'options'   => ['class' => 'treeview'],
            ],

            [
                'label' => ' <i class="fa fa-user"></i><span>Customers</span><i class="fa fa-angle-left pull-right"></i>',
                'url'   => '#',
                'items' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i> View All',
                        'url' => ['/customer'],
                        'visible' => \Yii::$app->user->can("customer_management")
                    ],
                ],
                'options'   => ['class' => 'treeview'],
            ],
            [
                'label' => ' <i class="fa fa-tasks"></i><span>Ingredient</span><i class="fa fa-angle-left pull-right"></i>',
                'url'   => '#',
                'items' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i> Ingredient Category',
                        'url' => ['/ingredient-category'],
                        'visible' => \Yii::$app->user->can("ingredient_category_management")
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i> Ingredient',
                        'url' => ['/ingredient'],
                        'visible' => \Yii::$app->user->can("ingredient_management")
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i> Legend',
                        'url' => ['/legend'],
                        'visible' => \Yii::$app->user->can("legend_management")
                    ],
                ],
                'options'   => ['class' => 'treeview'],
            ],
        ];


        $menuItems[] = [
            'label' => '<i class="fa fa-user-secret"></i> Logout (' . Yii::$app->user->identity->username . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];


        echo Menu::widget([
            'items' => $menuItems,
            'submenuTemplate' => "\n<ul class='treeview-menu'>\n{items}\n</ul>\n",
            'encodeLabels' => false,
            'options' => ['class' => 'sidebar-menu'],
        ]);
        ?>

    </section>
    <!-- /.sidebar -->
</aside>