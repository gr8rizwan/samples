<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use backend\models\Branch;

/* @var $this yii\web\View */
/* @var $model backend\models\CollectionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-box">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-lg-4 col-md-6 col-sm-12">
            <?= $form->field($model, 'first_name') ?>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12">
            <?= $form->field($model, 'last_name') ?>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12">
            <?= $form->field($model, 'email') ?>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12">
            <?= $form->field($model, 'phone_number') ?>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12">

            <?=  $form->field($model, 'created_at')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Joining Date'],
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]); ?>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="form-group pull-right">
                <label class="control-label" style="width: 100%">&nbsp;</label>
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

        </div>



    </div>

    <?php ActiveForm::end(); ?>

</div>
