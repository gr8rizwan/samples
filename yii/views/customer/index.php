<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="card-box">

    <div class="row"><p class="pull-right">
            <?= Html::a('Create Customer', ['create'], ['class' => 'btn btn-primary']) ?>
        </p></div>

    <div class="table-responsive">
        <?php Pjax::begin(); ?>

            <?= GridView::widget([
            'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'first_name',
                    'filter' => false,
                ],
                [
                    'attribute' => 'last_name',
                    'filter' => false,
                ],
                [
                    'attribute' => 'email',
                    'filter' => false,
                ],
                [
                    'attribute' => 'phone_number',
                    'filter' => false,
                ],
                [
                    'attribute' => 'created_at',
                    'filter' => false,
                    'format' => 'raw',
                    'value' => function($model) {
                        return yii::$app->dateTime->getReadableDateAndTimeFormat($model->created_at);
                    }
                ],
                // 'invited_by',
                // 'password',
                // 'image',
                // 'facebook_id',
                // 'is_active',
                // 'is_deleted',
                // 'role',
                // 'activation_code',
                // 'gender',
                // 'dob',
                //'om_id',
                //'customerkey',

                [
                    'class' => 'common\helpers\CustomActionColumn',
                    'template'=>'&nbsp;&nbsp;&nbsp;{view} {update}',

                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>
    </div>
</div>
