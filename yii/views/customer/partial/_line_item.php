<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="card-box m-b-10">
    <div class="table-box opport-box">

        <div class="table-detail" style="width: 400px; max-width: 400px;">

        </div>

        <div class="table-detail lable-detail">
            <span class="label <?= ($model->is_deleted == 1 ? 'label-danger' : 'label-info') ?>"><?= ($model->is_deleted == 1 ? 'Inactive/Deleted' : 'Active') ?></span>
        </div>

        <div class="table-detail">
            &nbsp;
        </div>

        <div class="table-detail table-actions-bar">
            <?= Html::a('<i class="md md-edit"></i>', 'javascript:;', ['value' => Url::to(['/customer/update-address', 'address_id' => $model->id]), 'title' => 'Update Address', 'class' => 'showModalButton table-action-btn']) ?>
            <?= Html::a('<i class="md md-close"></i>', 'javascript:;', ['value' => Url::to(['/customer/delete-address', 'address_id' => $model->id]), 'gridId' => 'customer-address', 'id' => 'sa-warning'.$model->id,  'title' => 'You want to delete customer address', 'class' => 'table-action-btn sa-warning', 'onclick' => "alertBox.delete('sa-warning$model->id')"]) ?>
        </div>
    </div>
</div>
