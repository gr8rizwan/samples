<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<tr>
    <td><?= yii::$app->dateTime->getReadableDateAndTimeFormat($model->time) ?></td>
    <td><a href="#">SW#<?= $model->id ?></a></td>
    <td>
        <a href="" class="text-dark"><b><?= $model->branch->name ?></b></a>
    </td>
    <td>
        <p class="text-dark"><?= $model->delivery_type ?></p>
    </td>
    <td>
        <span class="label <?= Yii::$app->setting->getLabels($model->status); ?>"><?= $model->status ?></span>
    </td>
    <td><?= Yii::$app->setting->formatePrice($model->discount); ?></td>
    <td><?= Yii::$app->setting->formatePrice($model->total_sum); ?></td>
    <td>
        <?= Html::button('<i class=" ti-comments"></i>', ['value' => Url::to(['/order/view', 'id' => $model->id]), 'title' => 'Order Detail', 'class' => 'showModalButton btn btn-info btn-custom waves-effect waves-light']); ?>
    </td>
</tr>