<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\popover\PopoverX;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use common\components\widget\CardBlockWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = 'Customer :'.$model->first_name;
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// total sum calculation
$totalSum = Yii::$app->setting->getTotalOrderCalculation($model->order);
$monthsReport = $model->getOrder()->select(['MONTH(time) AS time, count(time) as count'])->where(['YEAR(time)' => date("Y")])->groupBy(['MONTH(time)'])->all();

?>

<div class="row" ng-app="swichFeed" ng-controller="feedController as feed">


    <!--No of Order Placed-->
    <div class="col-lg-4">
        <div class="card-box">
            <div class="bar-widget">
                <div class="table-box">
                    <div class="table-detail">
                        <div class="iconbox bg-info">
                            <i class="md md-add-shopping-cart"></i>
                        </div>
                    </div>

                    <div class="table-detail">
                        <h4 class="m-t-0 m-b-5"><b><?= $model->getOrder()->count() ?></b></h4>
                        <p class="text-muted m-b-0 m-t-0">No. of order placed</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--Total Amount Spent-->
    <div class="col-lg-4">
        <div class="card-box">
            <div class="bar-widget">
                <div class="table-box">

                    <div class="table-detail">
                        <div class="iconbox bg-custom">
                            <i class="md md-attach-money"></i>
                        </div>
                    </div>

                    <div class="table-detail">
                        <h4 class="m-t-0 m-b-5"><b><?= Yii::$app->setting->formatePrice($totalSum['total_sum']); ?></b></h4>
                        <p class="text-muted m-b-0 m-t-0">Total Amount spent</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--Average Ticket Price-->
    <div class="col-lg-4">
        <div class="card-box">
            <div class="bar-widget">
                <div class="table-box">
                    <div class="table-detail">
                        <div class="iconbox bg-danger">
                            <i class="icon-layers"></i>
                        </div>
                    </div>

                    <div class="table-detail">
                        <h4 class="m-t-0 m-b-5"><b><?= Yii::$app->setting->formatePrice($totalSum['total_sum']/(count($model->order)?count($model->order):1)); ?></b></h4>
                        <p class="text-muted m-b-0 m-t-0">Average ticket price</p>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!--Tabs [PRofile, Orders, Customer Services]-->
    <div class="col-lg-12 customer-profile">

        <ul class="nav nav-tabs tabs">

            <!--Profile-->
            <li class="active tab">
                <a href="#profile" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                    <span class="hidden-xs">Profile</span>
                </a>
            </li>

            <!--Orders-->
            <li class="tab">
                <a href="#orders" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                    <span class="hidden-xs">Orders</span>
                </a>
            </li>

            <!--Customer Information-->
            <li class="tab">
                <a href="#services" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                    <span class="hidden-xs">Customer Services</span>
                </a>
            </li>

        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="profile">

                <div class="">
                    <div class="panel col-sm-12">
                        <ul class="nav nav-pills text-right">
                            <li class="active">
                                <a href="#profile-general" onclick="tabCaller.profileGeneral()" data-toggle="tab"
                                   aria-expanded="false">
                                    <span class="visible-xs" title="General"><i class="fa fa-info-circle"></i></span>
                                    <span class="hidden-xs">General</span>
                                </a>
                            </li>

                            <li class="">
                                <a href="#address" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs" title="Address"><i class="fa fa-info-map-marker"></i></span>
                                    <span class="hidden-xs">Address</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="#rewards" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs" title="Reward Points"><i class="fa fa-star"></i></span>
                                    <span class="hidden-xs">Reward Points</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" style="width: 100%">

                        <div class="tab-pane active" id="profile-general">

                            <div class="row">
                                <?php Pjax::begin(['id' => 'general-information']) ?>

                                <div class="col-md-4">

                                    <!--General Information-->

                                    <div class="card-box m-t-10">
                                        <h4 class="m-t-0 header-title"><b>General information</b></h4>
                                        <div class="p-20">

                                            <?= DetailView::widget([
                                                'model' => $model,
                                                'attributes' => [
                                                    [
                                                        'attribute' => 'Full Name',
                                                        'format' => 'raw',
                                                        'value' => $model->first_name. ' '.$model->last_name,
                                                    ],
                                                    [
                                                        'attribute' => 'phone_number',
                                                        'value' => $model->phone_number,
                                                    ],

                                                    [
                                                        'attribute' => 'Joining Date',
                                                        'format' => 'raw',
                                                        'value' => yii::$app->dateTime->getReadableDateAndTimeFormat($model->created_at),
                                                    ],
                                                    [
                                                        'attribute' => 'source',
                                                        'value' => $model->source,
                                                    ]

                                                ],
                                                'options' => ['class' => ''],
                                                'template' => '<div class="about-info-p"><strong>{label}</strong><br><p class="text-muted">{value}</p></div>',
                                            ]) ?>


                                            <p>
                                                <?= Html::button('Update profile', ['value' => Url::to(['/customer/update', 'id' => $model->id, 'type' => 'basic']), 'title' => 'Update '.$model->first_name. ' '. $model->last_name.' profile', 'class' => 'showModalButton btn btn-default btn-lg btn-custom waves-effect waves-light btn-full-width']) ?>
                                            </p>

                                        </div>
                                    </div>

                                    <!--Security Information-->
                                    <div class="card-box m-t-10">
                                        <h4 class="m-t-0 header-title"><b>Security Information</b></h4>
                                        <div class="p-20">

                                            <?= DetailView::widget([
                                                'model' => $model,
                                                'attributes' => [
                                                    [
                                                        'attribute' => 'email',
                                                        'format' => 'raw',
                                                        'value' => (!empty($model->email) ? $model->email : 'N/A'),
                                                    ],
                                                    [
                                                        'attribute' => 'password',
                                                    ],
                                                    [
                                                        'attribute' => 'customerkey',
                                                        'value' => '**********',
                                                    ],


                                                ],
                                                'options' => ['class' => ''],
                                                'template' => '<div class="about-info-p"><strong>{label}</strong><br><p class="text-muted">{value}</p></div>',
                                            ]) ?>

                                            <p>
                                                <?= Html::button('Update Security', ['value' => Url::to(['/customer/update', 'id' => $model->id, 'type' => 'security']), 'title' => 'Update '.$model->first_name. ' '. $model->last_name.' profile', 'class' => 'showModalButton btn btn-default btn-lg btn-custom waves-effect waves-light btn-full-width']) ?>
                                            </p>

                                        </div>
                                    </div>

                                    <!--First Order-->
                                    <div class="card-box m-t-10">
                                        <div class="bar-widget">
                                            <div class="table-box">
                                                <div class="table-detail">
                                                    <div class="iconbox bg-success">
                                                        <i class="md md-add-shopping-cart"></i>
                                                    </div>
                                                </div>
                                                <?php if(isset($model->firstOrder)){ ?>
                                                <div class="table-detail">
                                                    <h4 class="m-t-0 m-b-5"><b>First Order</b></h4>
                                                    <p class="text-muted m-b-0 m-t-0"><?= Yii::$app->dateTime->getReadableFormat($model->firstOrder->time); ?></p>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <h4 class="page-header header-title">Detail</h4>
                                        <div class="p-20">
                                            <?php if(isset($model->firstOrder)){ ?>
                                            <?= DetailView::widget([
                                                'model' => $model,
                                                'attributes' => [

                                                    [
                                                        'attribute' => 'Order Type',
                                                        'format' => 'raw',
                                                        'value' => $model->firstOrder->payment_type,

                                                    ],

                                                    [
                                                        'attribute' => 'Total',
                                                        'format' => 'raw',
                                                        'value' => Yii::$app->setting->formatePrice($model->firstOrder->total_sum),

                                                    ],



                                                    [
                                                        'attribute' => 'source',
                                                        'format' => 'raw',
                                                        'value' => $model->firstOrder->source,


                                                    ],


                                                ],
                                                'options' => ['class' => ''],
                                                'template' => '<div class="about-info-p"><strong>{label}</strong><br><p class="text-muted">{value}</p></div>',
                                            ]) ?>

                                            <br/>

                                            <?= Html::button('Order Detail', [ 'ng-click' => 'showOrderDetails(' . $model->firstOrder->id .')', 'title' => 'First Order Detail', 'class' => 'btn btn-default btn-lg btn-custom waves-effect waves-light btn-full-width']); ?>
                                            <?php } ?>

                                        </div>

                                    </div>
                                    <!--!First Order-->


                                </div>

                                <?php Pjax::end() ?>

                                <div class="col-md-8 m-t-10">


                                    <!--Max Order-->
                                    <div class="col-sm-6">
                                        <div class="card-box">
                                            <div class="bar-widget">
                                                <div class="table-box">
                                                    <div class="table-detail">
                                                        <div class="iconbox bg-danger">
                                                            <i class="md md-add-shopping-cart"></i>
                                                        </div>
                                                    </div>
                                                    <?php if (isset($model->highSumOrder)){ ?>
                                                    <div class="table-detail">
                                                        <h4 class="m-t-0 m-b-5"><b><?= Yii::$app->setting->formatePrice($model->highSumOrder->total_sum) ?></b></h4>
                                                        <p class="text-muted m-b-0 m-t-0">Max order</p>
                                                    </div>
                                                    <?php } ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--Min Order-->
                                    <div class="col-sm-6">
                                        <div class="card-box">
                                            <div class="bar-widget">
                                                <div class="table-box">
                                                    <div class="table-detail">
                                                        <div class="iconbox bg-info">
                                                            <i class="md md-add-shopping-cart"></i>
                                                        </div>
                                                    </div>
                                                    <?php if (isset($model->lowestSumOrder)){ ?>
                                                    <div class="table-detail">
                                                        <h4 class="m-t-0 m-b-5"><b><?= Yii::$app->setting->formatePrice($model->lowestSumOrder->total_sum) ?></b></h4>
                                                        <p class="text-muted m-b-0 m-t-0">Min order</p>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-12">

                                        <!--Pia-->
                                        <div class="card-box m-t-10">

                                            <h4 class="m-t-0 header-title"><b>Platform Used</b></h4>

                                            <ul class="list-inline chart-detail-list text-center">

                                                <li><h5><i class="fa fa-circle m-r-5" style="color: #34d3eb"></i>CallCenter</h5></li>
                                                <li><h5><i class="fa fa-circle m-r-5" style="color: #5fbeaa"></i>Mobile</h5></li>
                                                <li><h5><i class="fa fa-circle m-r-5" style="color: #ebeff2"></i>Website</h5></li>
                                            </ul>
                                            <!--Area Split Chart will draw from DrawChart function. -->
                                            <div class="row">
                                                <canvas id="area-split" height="260"></canvas>
                                            </div>

                                        </div>
                                        <!--!Pia-->

                                        <!--Pia-->
                                        <div class="card-box m-t-10">

                                            <h4 class="m-t-0 header-title"><b>Order by Area</b></h4>

                                            <ul class="list-inline chart-detail-list text-center">

                                                <li><h5><i class="fa fa-circle m-r-5" style="color: #34d3eb"></i>Marina</h5></li>
                                                <li><h5><i class="fa fa-circle m-r-5" style="color: #5fbeaa"></i>Jumeirah</h5></li>
                                                <li><h5><i class="fa fa-circle m-r-5" style="color: #ebeff2"></i>Business Bay</h5></li>
                                            </ul>
                                            <!--Area Split Chart will draw from DrawChart function. -->
                                            <canvas id="branch-split" height="260"></canvas>

                                        </div>
                                        <!--!Pia-->


                                        <div class="card-box">
                                            <h4 class="m-t-0 header-title"><b>Order in Current year</b></h4>

                                            <canvas id="order-graph" height="300"></canvas>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="tab-pane" id="address">

                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="pull-right">
                                        <?= Html::button('Add New', ['value' => Url::to(['/customer/create-address', 'customer_id' => $model->id]), 'title' => 'Add New address for '.$model->first_name. ' '. $model->last_name, 'class' => 'showModalButton btn btn-primary waves-effect waves-light']) ?>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">

                                    <?php Pjax::begin(['id' => 'customer-address']); ?>

                                    <?= GridView::widget([
                                        'dataProvider' => new ActiveDataProvider([
                                            'query' => $model->getAddresses(),
                                            'pagination' => [
                                                'pageSize' => 10,
                                            ]]),
                                        'layout' => "{items} \n {summary} \n {pager}",
                                        'tableOptions' => ['class' => 'table table-actions-bar'],
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],
                                            [
                                                'attribute' => 'Address',
                                                'format' => 'raw',
                                                'value' => function($model) {
                                                        $html = '<div class="member-info">';
                                                        $html .= '<p class="text-dark m-b-5"><b>Building: </b> <span class="text-muted">'.$model->building.'</span></p>';
                                                        $html .= '<p class="text-dark m-b-5"><b>Apartment: </b> <span class="text-muted">'.$model->apartment.'</span></p>';
                                                        $html .= '<p class="text-dark m-b-5"><b>Street:</b> <span class="text-muted">'.$model->street.'</span></p>';
                                                        $html .= '<p class="text-dark m-b-5"><b>Area:</b> <span class="text-muted">'.$model->hasArea->name.'</span></p>';

                                                    return $html;

                                                },
                                            ],

                                            [
                                                'attribute' => 'Status',
                                                'format' => 'raw',
                                                'value' => function($model) {
                                                    return '<span class=" '.($model->is_deleted == 1 ? 'label label-danger' : 'label label-info').'">'.($model->is_deleted == 1 ? 'Inactive/Deleted' : 'Active').'</span>';
                                                },
                                            ],

                                            [
                                                'class' => 'common\helpers\CustomActionColumn',
                                                'template'=>'{update} &nbsp;&nbsp; {delete}',
                                                'buttons' => [
                                                    'update' => function ($url, $model) {
                                                        return Html::a('<i class="md md-edit"></i>', 'javascript:;', ['value' => Url::to(['/customer/update-address', 'address_id' => $model->id]), 'title' => 'Update Address', 'class' => 'showModalButton table-action-btn']);
                                                    },

                                                    'delete' => function ($url, $model) {
                                                        return Html::a('<i class="md md-close"></i>', 'javascript:;', ['value' => Url::to(['/customer/delete-address', 'address_id' => $model->id]), 'gridId' => 'customer-address', 'id' => 'sa-warning'.$model->id,  'title' => 'You want to delete customer address', 'class' => 'table-action-btn sa-warning', 'onclick' => "alertBox.delete('sa-warning$model->id')"]);
                                                    },
                                                ]

                                            ],


                                        ],


                                    ]) ?>


                                    <?php Pjax::end(); ?>


                                </div> <!-- end col -->

                            </div>

                        </div>

                        <!--Rewards Points Blocks-->
                        <div class="tab-pane" id="rewards">


                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-lg-3">
                                    <div class="mini-stat clearfix card-box">
                                        <span class="mini-stat-icon bg-info"><i class="ion-social-usd text-white"></i></span>
                                        <div class="mini-stat-info text-right text-dark">
                                            <span class="counter text-dark"><?= $totalEarn = $model->totalRewardEarn->points; ?></span>
                                            Total Earn
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-3">
                                    <div class="mini-stat clearfix card-box">
                                        <span class="mini-stat-icon bg-warning"><i class="ion-ios7-cart text-white"></i></span>
                                        <div class="mini-stat-info text-right text-dark">
                                            <span class="counter text-dark"><?= $totalUsed = $model->totalRewardUsed->points; ?></span>
                                            Total Used
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-3">
                                    <div class="mini-stat clearfix card-box">
                                        <span class="mini-stat-icon bg-pink"><i class="ion-android-contacts text-white"></i></span>
                                        <div class="mini-stat-info text-right text-dark">
                                            <span class="counter text-dark"><?= $totalEarn-$totalUsed ?></span>
                                            Total Not Used
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-3">
                                    <div class="mini-stat clearfix card-box">
                                        <span class="mini-stat-icon bg-success"><i class="ion-eye text-white"></i></span>
                                        <div class="mini-stat-info text-right text-dark">
                                            <span class="counter text-dark"><?= ($totalEarn > 0 ? number_format($totalUsed/$totalEarn*100, 2) : 0); ?></span>
                                            Frequency of use
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="card-box">

                                        <div class="table-responsive">

                                            <?php Pjax::begin(); ?>

                                            <?= GridView::widget([
                                                'dataProvider' => new ActiveDataProvider([
                                                    'query' => $model->getLoyalty(),
                                                    'pagination' => [
                                                        'pageSize' => 10,
                                                    ]]),
                                                'layout' => "{items} \n {summary} \n {pager}",
                                                'tableOptions' => ['class' => 'table table-actions-bar'],
                                                'columns' => [
                                                    ['class' => 'yii\grid\SerialColumn'],
                                                    'points',
                                                    'action',
                                                    [
                                                        'attribute' => 'time',
                                                        'value' => function($model){
                                                            return yii::$app->dateTime->getReadableDateAndTimeFormat($model->time);
                                                        }
                                                    ],
                                                    [
                                                        'attribute' => 'order_id',
                                                        'format' => 'raw',
                                                        'value' => function($model) {
                                                            return Html::button($model->order_id, ['value' => Url::to(['/order/view', 'id' => $model->order_id]), 'title' => 'Order Detail', 'class' => 'showModalButton btn btn-white btn-custom btn-rounded waves-effect']);

                                                        }
                                                    ],

                                                    [
                                                        'class' => 'common\helpers\CustomActionColumn',
                                                        'template'=>'{view}',
                                                        'buttons' => [
                                                            'view' => function ($url, $model) {
                                                                return Html::button('<i class=" ti-comments"></i>', ['value' => Url::to(['/order/view', 'id' => $model->order_id]), 'title' => 'Order Detail', 'class' => 'showModalButton btn btn-info btn-custom waves-effect waves-light']);
                                                            },

                                                            'urlCreator' => function ($action, $model) {
                                                                if ($action === 'view') {
                                                                    return Url::to(['/order/view', 'id' => $model->order_id]);
                                                                }
                                                            }
                                                        ]

                                                    ],


                                                ],
                                            ]); ?>


                                            <?php Pjax::end(); ?>
                                        </div>
                                    </div>

                                </div> <!-- end col -->

                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="tab-pane" id="orders">

                <div class="panel">
                    <ul class="nav nav-pills text-right">
                        <li class="active">
                            <a href="#order-history" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs" title="History"><i class="fa fa-history"></i></span>
                                <span class="hidden-xs">History</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#order-statistic" onclick="tabCaller.orderStatistic();" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs" title="Statistics"><i class="fa fa-table"></i></span>
                                <span class="hidden-xs">Statistics</span>
                            </a>
                        </li>

                    </ul>

                    <div class="tab-content" style="width: 100%">

                        <div class="tab-pane active swich-data-grid" id="order-history">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="card-box">

                                        <div class="table-responsive">

                                            <?php Pjax::begin(); ?>

                                            <?= GridView::widget([
                                                'dataProvider' => new ActiveDataProvider([
                                                    'query' => $model->getOrder(),
                                                    'pagination' => [
                                                        'pageSize' => 10,
                                                    ]]),
                                                'layout' => "{items} \n {summary} \n {pager}",
                                                'tableOptions' => ['class' => 'table table-actions-bar'],
                                                'columns' => [
                                                    ['class' => 'yii\grid\SerialColumn'],
                                                    [
                                                        'attribute' => 'time',
                                                        'value' => function($model){
                                                            return yii::$app->dateTime->getReadableDateAndTimeFormat($model->time);
                                                        }
                                                    ],
                                                    [
                                                        'attribute' => 'Branch Name',
                                                        'format' => 'text',
                                                        'value' => function($model) {
                                                            return $model->branch->name;
                                                        },
                                                    ],

                                                    [
                                                        'attribute' => 'Order type',
                                                        'format' => 'text',
                                                        'value' => function($model) {
                                                            return $model->delivery_type;
                                                        },
                                                    ],

                                                    [
                                                        'attribute' => 'Status',
                                                        'format' => 'raw',
                                                        'value' => function($model) {
                                                            $class = '';
                                                            if ( $model->status == 'open' ) {
                                                                $class = 'label';
                                                            }
                                                            return '<span class=" '.$class . ' ' .Yii::$app->setting->getLabels($model->status).'">'.Yii::$app->setting->getLabels($model->status, true).'</span>';
                                                        },
                                                    ],

                                                    [
                                                        'attribute' => 'Discount',
                                                        'format'=> 'raw',
                                                        'value' => function($model) {
                                                            return Yii::$app->setting->formatePrice($model->discount);
                                                        },
                                                    ],

                                                    [
                                                        'attribute' => 'Total',
                                                        'format'=> 'raw',
                                                        'value' => function($model) {
                                                            return Yii::$app->setting->formatePrice($model->total_sum);
                                                        },
                                                    ],

                                                    [
                                                        'class' => 'common\helpers\CustomActionColumn',
                                                        'template'=>'{view}',
                                                        'buttons' => [
                                                            'view' => function ($url, $model) {
                                                                return '<button ng-click="showOrderDetails(' . $model->id .')"  class="btn btn-info pull-right glyphicon glyphicon-zoom-in"></button>';
                                                            },

                                                            'urlCreator' => function ($action, $model) {
                                                                if ($action === 'view') {
                                                                    return Url::to(['/order/view', 'id' => $model->id]);
                                                                }
                                                            }
                                                        ]

                                                    ],


                                                ],
                                            ]); ?>


                                            <?php Pjax::end(); ?>
                                            <div class="pull-right hidden" id="btn-compile-HTML">
                                                <button type="button" ng-click="compileHTML()" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i> &nbsp; Compile </button>
                                            </div>

                                        </div>
                                    </div>

                                </div> <!-- end col -->


                            </div>
                        </div>

                        <div class="tab-pane" id="order-statistic">

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="card-box">
                                        <div class="bar-widget">
                                            <div class="table-box">
                                                <div class="table-detail">
                                                    <div class="iconbox bg-info">
                                                        <i class="icon-layers"></i>
                                                    </div>
                                                </div>

                                                <div class="table-detail">
                                                    <h4 class="m-t-0 m-b-5"><b>33</b></h4>
                                                    <h5 class="text-muted m-b-0 m-t-0">No. of order this month</h5>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card-box">
                                        <div class="bar-widget">
                                            <div class="table-box">
                                                <div class="table-detail">
                                                    <div class="iconbox bg-custom">
                                                        <i class="icon-layers"></i>
                                                    </div>
                                                </div>

                                                <div class="table-detail">
                                                    <h4 class="m-t-0 m-b-5"><b>4</b></h4>
                                                    <h5 class="text-muted m-b-0 m-t-0">Avg. Order per month</h5>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card-box">
                                        <div class="bar-widget">
                                            <div class="table-box">
                                                <div class="table-detail">
                                                    <div class="iconbox bg-danger">
                                                        <i class="icon-layers"></i>
                                                    </div>
                                                </div>

                                                <div class="table-detail">
                                                    <h4 class="m-t-0 m-b-5"><b>1256</b></h4>
                                                    <h5 class="text-muted m-b-0 m-t-0">No. of order</h5>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- BAR Chart -->
                            <div class="row">

                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="m-t-0 header-title"><b>Ordering Pattern</b></h4>
                                        <p class="text-muted m-b-30 font-13 pull-right">
                                            <label>Select pattern</label>
                                            <select name="pattern" class="form-control">
                                                <option value="daily">Daily</option>
                                                <option value="weekly">Weakly</option>
                                                <option value="monthly" selected>Monthly</option>
                                                <option value="yearly">Yearly</option>
                                            </select>
                                        </p>

                                        <canvas id="order-pattern" height="300"></canvas>
                                    </div>

                                   <div class="col-sm-6">
                                       <!--Pia-->
                                       <div class="card-box m-t-10">

                                           <h4 class="m-t-0 header-title"><b>Area Split</b></h4>

                                           <ul class="list-inline chart-detail-list text-center">

                                               <li><h5><i class="fa fa-circle m-r-5" style="color: #34d3eb"></i>JBR</h5></li>
                                               <li><h5><i class="fa fa-circle m-r-5" style="color: #5fbeaa"></i>JLT</h5></li>
                                               <li><h5><i class="fa fa-circle m-r-5" style="color: #ebeff2"></i>Marina</h5></li>
                                           </ul>
                                           <!--Area Split Chart will draw from DrawChart function. -->
                                           <canvas id="order-area-split" height="260"></canvas>

                                       </div>
                                       <!--!Pia-->
                                   </div>

                                    <div class="col-sm-6">
                                        <!--Pia-->
                                        <div class="card-box m-t-10">

                                            <h4 class="m-t-0 header-title"><b>Order Pattern %</b></h4>

                                            <ul class="list-inline chart-detail-list text-center">

                                                <li><h5><i class="fa fa-circle m-r-5" style="color: #34d3eb"></i>Lunch - Non peak</h5></li>
                                                <li><h5><i class="fa fa-circle m-r-5" style="color: #5fbeaa"></i>Lunch - Peak</h5></li>
                                                <li><h5><i class="fa fa-circle m-r-5" style="color: #ebeff2"></i>Dinner - Non peak</h5></li>
                                                <li><h5><i class="fa fa-circle m-r-5" style="color: #ebeff2"></i>Dinner - peak</h5></li>
                                            </ul>
                                            <!--Area Split Chart will draw from DrawChart function. -->
                                            <canvas id="order-pattern-persantage" height="260"></canvas>

                                        </div>
                                        <!--!Pia-->

                                    </div>

                                </div>
                            </div>
                            <!-- End row-->
                        </div>

                    </div>
                </div>

            </div>

            <div class="tab-pane" id="services">

                <div class="panel">
                    <ul class="nav nav-pills">
                        <li class="active">
                            <a href="#complaints" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs" title="Complaints"><i class="fa fa-book"></i></span>
                                <span class="hidden-xs">Complaints</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="#followups" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs" title="Follow-ups"><i class="fa fa-folder-open"></i></span>
                                <span class="hidden-xs">Follow-ups</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="#complementary" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs" title="Complementary"><i class="fa fa-list"></i></span>
                                <span class="hidden-xs">Complementary</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="#calllog" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs" title="Call Log"><i class="fa fa-phone-square"></i></span>
                                <span class="hidden-xs">Call Log</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="#notes" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs" title="Notes"><i class="fa fa-comments"></i></span>
                                <span class="hidden-xs">Notes</span>
                            </a>
                        </li>

                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane active" id="complaints">
                            <p>Order history</p>
                        </div>
                        <div class="tab-pane" id="followups">
                            <p>Follow ups</p>
                        </div>

                        <div class="tab-pane" id="complementary">
                            <p>Competently tab</p>
                        </div>

                        <div class="tab-pane" id="calllog">
                            <p>Call Logs </p>
                        </div>

                        <div class="tab-pane" id="notes">
                            <p> Notes </p>
                        </div>

                    </div>
                </div>

            </div>
            <?php echo $this->render('_order_modal', []); ?>

        </div>
    </div>

</div>


<script type="text/javascript">

    var areaSplitData = [

        {
            value: <?= $model->getOrder()->where(['source'  => 'call'])->count() ?>,
            color:"#34d3eb",
            label: "Call Center"
        },
        {
            value : <?= $model->getOrder()->where(['source'  => 'ios'])->count() ?>,
            color : "#5fbeaa",
            label: "Mobile"
        },
        {
            value : <?= $model->getOrder()->where(['source'  => 'website'])->count() ?>,
            color : "#5d9cec",
            label: "Website"
        }
    ];

    var BranchSplitData = [

        {
            value: <?= $model->getOrder()->where(['branch_id'  => '1'])->count() ?>,
            color:"#34d3eb",
            label: "Marina"
        },
        {
            value : <?= $model->getOrder()->where(['branch_id'  => '2'])->count() ?>,
            color : "#5fbeaa",
            label: "Jumeirah"
        },
        {
            value : <?= $model->getOrder()->where(['branch_id'  => '3'])->count() ?>,
            color : "#5d9cec",
            label: "Business Bay"
        }
    ];

    <?php
        $label = $datasets = [];
        foreach($monthsReport as $month){
            $label[] = yii::$app->dateTime->getMonth($month->time);
            $datasets[] = $month->count;
        }
    ?>

    //barchart-Single
    var BarChartSingle = {
        labels : <?= \yii\helpers\Json::encode($label) ?>,
        datasets : [
            {
                fillColor: '#ebeff2',
                strokeColor: '#ebeff2',
                highlightFill: '#5fbeaa',
                highlightStroke: '#ebeff2',
                data : <?= \yii\helpers\Json::encode($datasets) ?>
            }
        ]
    }

    var tabCaller = {
        'orderStatistic': function() {
            console.log(BarChartSingle);
            setTimeout(function(){
                drawChart._get('Bar', BarChartSingle, 'order-pattern')
                drawChart._get('Pie', BranchSplitData, 'order-area-split')
                drawChart._get('Pie', BranchSplitData, 'order-pattern-persantage')
            }, 1000 );

        },
        'profileGeneral': function() {

            setTimeout(function(){
                drawChart._get('Pie', areaSplitData, 'area-split')
                drawChart._get('Pie', BranchSplitData, 'branch-split')
                drawChart._get('Bar', BarChartSingle, 'order-graph')

            }, 1000);

        }
    }


    tabCaller.profileGeneral();

</script>

<?php

$this->registerJs(
    '
        $(document).on("pjax:end", function() {
            $("#btn-compile-HTML button").trigger("click");
        });
    '
);

?>

