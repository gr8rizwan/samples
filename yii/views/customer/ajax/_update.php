<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = 'Update ' . ucfirst($model->first_name . ' ' . $model->last_name) . ' profile';
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

    <!--Ajax form submit -->
    <script src="<?= Yii::getAlias('@web') . '/themes/ubold/assets/js/ajax.submit.js' ?>"></script>

    <!-- form start -->
<?php $form = ActiveForm::begin([
    'options' => [
        'role' => 'form',
        'id' => 'ajaxSubmit',
        'refId' => 'general-information',
    ]
]); ?>

<?php if($type == 'basic') { ?>

    <div class="row">

        <div class="col-md-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="row">

        <div class="col-md-12">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'disable' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'is_active')->dropDownList(['0' => 'In-Active', '1' => 'Active'], ['prompt' => 'Select Status']) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'gender')->dropDownList(['0' => 'Female', '1' => 'Male'], ['prompt' => 'Select Gener']) ?>
        </div>

    </div>

<?php } elseif($type == 'security') { ?>

    <div class="row">

        <div class="col-md-12">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="row">

        <div class="col-md-6">
            <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'password')->textInput(['maxlength' => true, ]) ?>
        </div>

    </div>


<?php } ?>


    <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-custom waves-effect waves-light" data-dismiss="modal">Close</button>
        <?= Html::submitButton('Update', ['id' => 'ajaxSubmitBtn', 'class' => 'btn btn-info btn-custom waves-effect waves-light']) ?>
    </div>

<?php ActiveForm::end(); ?>