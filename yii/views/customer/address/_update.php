<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use backend\models\Area;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = 'Update Address';
?>

    <!--Ajax form submit -->
    <script src="<?= Yii::getAlias('@web') . '/themes/ubold/assets/js/ajax.submit.js' ?>"></script>

    <!-- form start -->
<?php $form = ActiveForm::begin([
    'options' => [
        'role' => 'form',
        'id' => 'ajaxSubmit',
        'refId' => 'customer-address',
    ]
]); ?>

    <div class="row">

        <div class="col-md-6">
            <?= $form->field($model, 'building')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'apartment')->textInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="row">

        <div class="col-md-12">
            <?= $form->field($model, 'special_instructions')->textarea(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?=
            $form->field($model, 'area')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Area::find()->all(),
                    'id',
                    'name'
                ),
                'options' => ['placeholder' => 'Select Area'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
        </div>


    </div>


    <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-custom waves-effect waves-light" data-dismiss="modal">Close</button>
        <?= Html::submitButton('Update Address', ['id' => 'ajaxSubmitBtn', 'class' => 'btn btn-info btn-custom waves-effect waves-light']) ?>
    </div>

<?php ActiveForm::end(); ?>