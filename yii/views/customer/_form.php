<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- form start -->
<?php $form = ActiveForm::begin(['options' => ['role' => 'form']]); ?>

    <div class="box-body">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_general" data-toggle="tab">General Information</a></li>
                <li><a href="#tab_address" data-toggle="tab">Address</a></li>
                <?php if(!$model->isNewRecord){ ?>
                <li><a href="#tab_orders" data-toggle="tab">Orders</a></li>
                <?php } ?>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="tab_general">

                    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 're_password')->passwordInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'is_active')->dropDownList(['0' => 'In-Active', '1' => 'Active'], ['prompt' => 'Select Status']) ?>

                    <?= $form->field($model, 'gender')->dropDownList(['0' => 'Female', '1' => 'Male'], ['prompt' => 'Select Gener']) ?>

                    <?=
                        $form->field($model, 'dob')->widget(DateTimePicker::classname(), [
                        'options' => ['placeholder' => 'Select Date of birth'],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'autoclose' => true,
                            'todayHighlight' => true
                        ]
                    ]);
                    ?>

                    <?= $form->field($model, 'om_id')->textInput(['maxlength' => true]) ?>

                </div>

                <div class="tab-pane" id="tab_address">
                    <!--Here is Address Details-->
                </div>
                <!-- /.tab-pane -->

                <?php if(!$model->isNewRecord){ ?>

                <div class="tab-pane" id="tab_orders">

                </div>

                <?php } ?>
                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->

    </div>

<?php ActiveForm::end(); ?>
