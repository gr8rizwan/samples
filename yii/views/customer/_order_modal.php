<?php

$baseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/feed/';
$themeBaseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/ubold/';

// order details modal box
echo $this->render('/modals/order-details');

?>





<script src="<?= $themeBaseURL; ?>assets/js/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="<?= $baseURL; ?>js/angular-timeago.js"></script>
<script src="<?= $baseURL; ?>js/app.js"></script>
<script src="<?= $baseURL; ?>js/feedFactory.js"></script>
<script src="<?= $baseURL; ?>js/feedController.js"></script>
<script src="<?= $baseURL; ?>js/frameFilter.js"></script>