<?php

use backend\widgets\order\OrderWidget;
use backend\widgets\controls\DashboardWidget;
use yii\widgets\Pjax;
use backend\widgets\discount\DiscountWidget;

/* @var $this yii\web\View */
$this->title = 'Welcome, '. yii::$app->user->identity->username;
$userDashboard = \app\models\UserWidget::find()->where(['user_id' => Yii::$app->user->id])->all();

?>

<?= DashboardWidget::widget(['title' => 'Dashboard Widgets', 'position' => 'pull-right']); ?>

<div class="row">

    <?php foreach($userDashboard as $widget) { ?>

        <?php if($widget->widget->code == 'PA') { ?>
            <?php Pjax::begin(['id' => 'grid-'. $widget->widget->code]); ?>
            <div class="col-lg-6 col-sm-12">
                <?= OrderWidget::widget(['title' => $widget->widget->name, 'filters' => ['from_date', 'to_date', 'branch_id'], 'view' => 'platform', 'formId' => 'platformForm' ]) ?>
            </div>
            <?php Pjax::end(); ?>
        <?php } else if($widget->widget->code == 'RR') { ?>

            <div class="col-lg-6 col-sm-12">
                <?= OrderWidget::widget(['title' => $widget->widget->name, 'filters' => ['from_date', 'to_date', 'branch_id'], 'view' => 'retention', 'formId' => 'retentionForm' ]) ?>
            </div>

        <?php } else if($widget->widget->code == 'BA') { ?>

            <div class="col-lg-6 col-sm-12">
                <?= OrderWidget::widget(['title' => $widget->widget->name, 'filters' => ['current_status', 'from_date', 'to_date', 'area', 'source', 'time_filter'], 'view' => 'branch', 'formId' => 'branchForm' ]) ?>
            </div>

        <?php } else if($widget->widget->code == 'SR') { ?>

            <div class="col-lg-6 col-sm-12">
                <?= OrderWidget::widget(['title' => $widget->widget->name, 'filters' => ['from_date', 'to_date', 'time_filter'], 'view' => 'sales-revenue', 'formId' => 'sales-revenueForm' ]) ?>
            </div>

        <?php }else if($widget->widget->code == 'CS') { ?>

            <div class="col-lg-6 col-sm-12">
                <?= OrderWidget::widget(['title' => $widget->widget->name, 'filters' => ['branch_id', 'from_date', 'to_date', 'time_filter'], 'view' => 'complaints-summary', 'formId' => 'complaints-summaryForm' ]) ?>
            </div>

        <?php } else if($widget->widget->code == 'LOS') { ?>

            <div class="col-lg-6 col-sm-12">
                <?= OrderWidget::widget(['title' => $widget->widget->name, 'filters' => ['from_date', 'to_date', 'time_filter'], 'view' => 'late-order-summary', 'formId' => 'late-order-summaryForm' ]) ?>
            </div>

        <?php } else if($widget->widget->code == 'ADT') { ?>

            <div class="col-lg-6 col-sm-12">
                <?= OrderWidget::widget(['title' => $widget->widget->name, 'filters' => ['time_category', 'from_date', 'to_date', 'time_filter'], 'view' => 'average-delivery-time', 'formId' => 'average-delivery-timeForm' ]) ?>
            </div>

        <?php } else if($widget->widget->code == 'AT') { ?>

            <div class="col-lg-6 col-sm-12">
                <?= OrderWidget::widget(['title' => $widget->widget->name, 'filters' => ['from_date', 'to_date', 'time_filter'], 'view' => 'average-ticket', 'formId' => 'average-ticketForm' ]) ?>
            </div>

        <?php } else if($widget->widget->code == 'LDS') { ?>

            <div class="col-lg-6 col-sm-12">
                <?= OrderWidget::widget(['title' => $widget->widget->name, 'filters' => ['from_date', 'to_date', 'branch_id'], 'view' => 'time-category', 'formId' => 'time-categoryForm' ]) ?>
            </div>

        <?php } else if($widget->widget->code == 'TP') { ?>

            <div class="col-lg-6 col-sm-12">
                <?= OrderWidget::widget(['title' => $widget->widget->name, 'filters' => ['from_date', 'to_date', 'time_filter', 'origin_filter'], 'view' => 'third-party', 'formId' => 'third-partyForm' ]) ?>
            </div>

        <?php } else if($widget->widget->code == 'PC') { ?>

            <div class="row">

                <div class="col-lg-6 col-sm-12">
                    <?= DiscountWidget::widget(['title' => $widget->widget->name, 'filters' => ['from_date', 'to_date', 'time_filter', 'branch_id', 'code_status' , 'filter_promos'], 'view' => 'promo-code', 'formId' => 'promo-code' ]) ?>
                </div>

                <div class="col-lg-6 col-sm-12">
                    <?= DiscountWidget::widget(['title' => 'Discount Category', 'filters' => ['from_date', 'to_date', 'time_filter', 'branch_id', 'filter_discount_category'], 'view' => 'discount-category', 'formId' => 'discount-category' ]) ?>
                </div>


            </div>

        <?php } else if($widget->widget->code == 'AS') { ?>

            <div class="col-lg-6 col-sm-12">
                <?= OrderWidget::widget(['title' => $widget->widget->name, 'filters' => ['from_date', 'to_date', 'time_filter', 'branch_id', 'area'], 'view' => 'area', 'formId' => 'area-summary' ]) ?>
            </div>

        <?php } ?>

    <?php } ?>

</div>





