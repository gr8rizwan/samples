<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\NotificationGroup */

$this->title = 'Create Notification Group';
$this->params['breadcrumbs'][] = ['label' => 'Notification Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
