<?php

use backend\models\Branch;
use backend\models\CollectionReason;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model backend\models\CollectionSearch */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    '
    var from_date = $(\'[name="CollectionSearch[created_at]"]\');
    from_date.off(\'apply.daterangepicker\');
    from_date.on(\'apply.daterangepicker\', function(ev, picker) {
        from_date.val(picker.startDate.format(\'YYYY-MM-DD\'));
    })
    '
);

$this->registerJs(
    'setTimeout(function(){
            $("#branch-id").trigger("change");
        }, 500);'
);

if( Yii::$app->setting->getIdentityAttribute('type') == 'Super Admin') {
    $disable = false;
} else {
    $disable = true;
}

?>


<div class="collection-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?= $form->field($model, 'branch_id')->dropDownList(ArrayHelper::map(Branch::find()->all(), 'id', 'name'), ['prompt'=>'Select Branch', 'disabled' => $disable, 'id' => 'branch-id']) ?>

            <?= ($disable ? $form->field($model, 'branch_id')->hiddenInput()->label(false) : '' ); ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList([ '0' => 'Pending', '1' => 'Received'], ['prompt' => 'Select Status']) ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?=
            $form->field($model, 'customer_id')->widget(Select2::classname(), [
                'initValueText' => $model->customer_id, // set the initial display text
                'options' => ['placeholder' => 'Select Customer', 'class' => 'form-control'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 4,
                    'ajax' => [
                        'url' => Url::to(['customer/customer-list']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(customer) { return customer.name; }'),
                    'templateSelection' => new JsExpression('function (customer) { return customer.name; }'),
                ],
            ]);

            ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?=
            $form->field($model, 'reason_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(CollectionReason::find()->all(), 'id','title'),
                'options' => ['placeholder' => 'Select Reason', 'class' => 'form-control'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

    </div>

    <div class="row">

        <div class="col-lg-3">
            <?=
            $form->field($model, 'driver_id')->widget(DepDrop::classname(), [
                'options' => ['id'=>'driver-id'],
                'pluginOptions'=>[
                    'depends'=>['branch-id'],
                    'placeholder' => 'Select driver',
                    'url' => Url::to(['/branch/get-users'])
                ]
            ]);

            ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="form-group">
                <label>Created at</label>
                <?=
                DateRangePicker::widget([
                    'name'=>'CollectionSearch[created_at]',
                    'value'=> $model->created_at,
                    'convertFormat'=>true,
                    'pluginOptions'=>[
                        'singleDatePicker'=>true,
                        'locale'=>['format' => 'Y-m-d'],
                    ]
                ]);
                ?>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="form-group pull-right">
                <label class="control-label" style="width: 100%">&nbsp;</label>
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
