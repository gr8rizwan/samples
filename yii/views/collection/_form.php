<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\widgets\ActiveForm;
use backend\models\User;
use backend\models\CollectionReason;
use backend\models\Branch;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Collection */
/* @var $form yii\widgets\ActiveForm */

if( Yii::$app->setting->getIdentityAttribute('type') == 'Super Admin') {
    $disable = false;
} else {
    $disable = true;
    $model->branch_id = Yii::$app->setting->getUserBranchId(Yii::$app->user->id);
}

$this->registerJs(
    'setTimeout(function(){
            $("#branch-id").trigger("change");
        }, 500);'
);

$this->registerJs(
    'setTimeout(function(){
            $("#customer-id").trigger("change");
        }, 500);'
);
?>

<div class="collection-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::hiddenInput('input-driver-id', $model->driver_id, ['id'=>'input-driver-id']); ?>
    <?= Html::hiddenInput('input-customer-id', $model->customer_id, ['id'=>'input-customer-id']); ?>

    <?= $form->field($model, 'branch_id')->dropDownList(ArrayHelper::map(Branch::find()->all(), 'id', 'name'), ['prompt'=>'Created By', 'disabled' => $disable, 'id' => 'branch-id']) ?>

    <?= ($disable ? $form->field($model, 'branch_id')->hiddenInput()->label(false) : '' ); ?>

    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($model, 'customer_id')->widget(Select2::classname(), [
                'initValueText' => $model->customer->first_name, // set the initial display text
                'options' => ['placeholder' => 'Select Customer', 'id' => 'customer-id'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to(['customer/customer-list']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(customer) { return customer.name; }'),
                    'templateSelection' => new JsExpression('function (customer) { return customer.name; }'),
                ],
            ]);

            ?>
        </div>

        <div class="col-sm-6">
            <?=

            $form->field($model, 'order_id')->widget(DepDrop::classname(), [
                'type'=>DepDrop::TYPE_SELECT2,
                'options' => ['id'=>'order-id'],
                'pluginOptions'=> [
                    'depends'=>['customer-id'],
                    'placeholder' => 'Select Order',
                    'url' => Url::to(['/customer/view-orders']),
                    'params'=>['input-customer-id']
                ]
            ]);

            ?>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">

            <?=
            $form->field($model, 'driver_id')->widget(DepDrop::classname(), [
                'options' => ['id'=>'driver-id'],
                'pluginOptions'=>[
                    'depends'=>['branch-id'],
                    'placeholder' => 'Select driver',
                    'url' => Url::to(['/branch/get-users']),
                    'params'=>['input-driver-id']
                ]
            ]);

            ?>
        </div>

        <div class="col-sm-6">
            <?=
            $form->field($model, 'reason_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(CollectionReason::find()->all(), 'id','title'),
                'options' => ['placeholder' => 'Select Reason'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'amount')->textInput(['placeholder' => 'Add Amount']) ?>
        </div>
    </div>


    <?= $form->field($model, 'created_by')->hiddenInput(['value' => yii::$app->user->identity->id])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
