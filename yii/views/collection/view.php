<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Collection */

$this->title = 'View Collection ID:'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Collections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$html = '';
$html .= strtoupper($model->customer->first_name.' '.$model->customer->last_name).' <br />';
$html .= '<b>'.$model->customer->phone_number.'</b><br />';
$html .= yii::$app->setting->getLatestAddress($model->customer->id).'<br />';

?>

<?php if ($model->status == '0') { ?>

    <div class="alert alert-warning">
        <p>Payments are pending.</p>
    </div>


<?php } else if ($model->status == '1') { ?>

    <div class="alert alert-success">
        <p>Payments has been collected</p>
    </div>

<?php } ?>

<section class="card-box">

    <div class="row">
        <div class="col-sm-12">
            <p class="pull-right">
                <?= $model->status == 0 ?  Html::a('Received payments', ['received', 'id' => $model->id], ['class' => 'btn btn-primary']) : ''; ?>
            </p>
        </div>
    </div>

    <div class="table-responsive">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                [
                    'attribute' => 'customer_id',
                    'format'    => 'raw',
                    'value' => $html,

                ],

                [
                    'attribute' => 'branch_id',
                    'value' => $model->branch->name,

                ],

                [
                    'attribute' => 'order_id',
                    'value' => ( (int)$model->order_id > 0 ? (yii::$app->dateTime->getReadableFormat($model->order->time). ' ('.yii::$app->setting->formatePrice($model->order->total_sum).')') : 'N/A'),

                ],


                [
                    'attribute' => 'reason_id',
                    'value' => $model->reason->title,
                ],

                [
                    'attribute' => 'driver_id',
                    'value' => ($model->driver->first_name.' '.$model->driver->last_name),

                ],

                [
                    'attribute' => 'created_by',
                    'value' => ($model->creator->first_name.' '.$model->creator->last_name),

                ],

                [
                    'attribute' => 'status',
                    'value' => ($model->status == 0 ? 'Pending' : 'Received'),

                ],

                [
                    'attribute' => 'amount',
                    'value' => yii::$app->setting->formatePrice($model->amount),


                ],

                [
                    'attribute' => 'created_at',
                    'value' => yii::$app->dateTime->getReadableFormat($model->created_at),


                ],

                [
                    'attribute' => 'updated_at',
                    'value' => yii::$app->dateTime->getReadableFormat($model->updated_at),


                ],
            ],
        ]) ?>
    </div>

</section>
