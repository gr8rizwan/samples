<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Branch;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use kartik\time\TimePicker;
use kartik\helpers\Enum;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\PromoCodes */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord) {
    $model->code = Yii::$app->setting->getRandomNumber(8);
}

$customers = '';
foreach($model->promoForCustomerList as $customer) {
    $customers .= $customer->customer->first_name.' '.$customer->customer->last_name. ' ('.$customer->customer->phone_number.'),   ' ;
}
?>

<div class="promo-codes-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'type')->dropDownList([ 'fixed' => 'Fixed', 'percentage' => 'Percentage', ], ['prompt' => 'Select Type']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'redeem_type')->dropDownList([ 'single' => 'Single', 'multiple' => 'Multiple', ], ['prompt' => 'Select Redeem Time']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'min_order')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <label>Start Date</label>
            <?=
            $form->field($model, 'from_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Start From'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);
            ?>
        </div>
        <div class="col-sm-6">
            <label>End Date</label>
            <?=
            $form->field($model, 'to_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'End Date'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);
            ?>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'time_from')->widget(TimePicker::classname(), [
                'options' => ['readonly' => true],
                'pluginOptions' => [
                    'showSeconds' => true,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                ]
            ]); ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'time_to')->widget(TimePicker::classname(), [
                'options' => ['readonly' => true],
                'pluginOptions' => [
                    'showSeconds' => true,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                ]
              ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'valid_on_day')->dropDownList(Enum::dayList(), ['prompt' => 'Select day']) ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList([ '0' => 'In-Active', '1' => 'Active'], ['prompt' => 'Select Status']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'branch_id')->dropDownList(ArrayHelper::map(Branch::find()->all(), 'id', 'name'), ['prompt'=>'Select branch']) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'customer_specific')->dropDownList(['0' => 'General', '1' => 'For specific customers'], ['id' => 'customer_specific', 'prompt'=>'Select is specific', 'onchange' => 'this.value == 0 ? $("#promoForCustomer").hide() : $("#promoForCustomer").show() ']) ?>
        </div>

    </div>

    <div class="row" id="promoForCustomer">
        <div class="col-sm-12">
            <label>Customers</label>
            <p><?= $customers ?></p>
            <?= Select2::widget([
                'name' => 'PromoCodes[promoForCustomer][]',
                'value' => ArrayHelper::getColumn(ArrayHelper::toArray($model->promoForCustomerList),
                    'customer_id'
                ),
                'data' => ArrayHelper::map($model->promoForCustomerList,
                    'customer_id',
                    function($data) {
                        return $data->customer->first_name.' '.$data->customer->last_name. ' (' .$data->customer->phone_number.')';
                    }
                ),
                'options' => ['placeholder' => 'Select Customer',  'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true,
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'ajax' => [
                        'url' => \yii\helpers\Url::to(['customer/customer-list']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(customer) { return customer.name; }'),
                    'templateSelection' => new JsExpression('function (customer) { return customer.name; }'),
                ],
            ]); ?>
        </div>
    </div>

    <br><br>
   <div class="row">

       <div class="col-sm-12">
           <div class="form-group">
               <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
           </div>
       </div>
   </div>

    <?php ActiveForm::end(); ?>

</div>

<?=
    $this->registerJs("
       $(function () {
            $('select#customer_specific').change();
        });
    ");
?>
