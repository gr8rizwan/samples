<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use backend\models\Branch;

/* @var $this yii\web\View */
/* @var $model backend\models\PromoCodesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-box">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-lg-4 col-md-6 col-sm-12">
            <?= $form->field($model, 'name') ?>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12">
            <?= $form->field($model, 'code') ?>
        </div>

        <div class="col-lg-4 col-md-3 col-sm-12">
            <?= $form->field($model, 'type')->dropDownList([ 'percentage' => 'Percentage', 'fixed' => 'Fixed'], ['prompt' => 'Select Type']) ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?= $form->field($model, 'redeem_type')->dropDownList([ 'single' => 'One time', 'multiple' => 'Multiple time'], ['prompt' => 'Select Redeem Type']) ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?= $form->field($model, 'branch_id')->dropDownList(ArrayHelper::map(Branch::find()->all(), 'id', 'name'), ['prompt'=>'Select branch']) ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList([ '-1' => 'Upcoming', '0' => 'Expired', '1' => 'Active'], ['prompt' => 'Select Status']) ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="form-group pull-right">
                <label class="control-label" style="width: 100%">&nbsp;</label>
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

        </div>



    </div>

    <?php ActiveForm::end(); ?>

</div>
