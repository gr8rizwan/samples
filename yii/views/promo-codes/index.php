<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PromoCodesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promo Codes';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'name',
        'filter' => false,
    ],
    [
        'attribute' => 'code',
        'filter' => false,
    ],

    [
        'attribute' => 'redeem_type',
        'filter' => false,
    ],
    [
        'attribute' => 'amount',
        'filter' => false,
        'value' => function($model) {
            return ($model->type == 'percentage' ? ( (int)$model->amount ).' %' : ( (int)$model->amount ).' Fixed');
        },
    ],

    [
        'attribute' => 'redeemed',
        'filter' => false,
        'value' => function($model) {
            return count($model->promoToCustomer).' Times';
        },
    ],

    [
        'attribute' => 'discounted',
        'filter' => false,
        'value' => function($model) {
            $value = $model->getOrderTotalSum($model->promoToCustomer, 'discount');
            return Yii::$app->setting->formatePrice($value);
        },
    ],

    [
        'attribute' => 'redeemed_amount',
        'format' => 'raw',
        'value' => function($model) {
            $value = $model->getOrderTotalSum($model->promoToCustomer, 'total_sum');
            return Yii::$app->setting->formatePrice($value);
        },
    ],

    [
        'attribute' => 'Validity',
        'format' => 'raw',
        'value' => function($model){
            return (
                yii::$app->dateTime->getDateFromTimeStamp($model->from_date). '<b>/</b> <br /> '.
                yii::$app->dateTime->getDateFromTimeStamp($model->to_date)
            );
        }
    ],
    [
        'class' => 'common\helpers\CustomActionColumn',
        'template'=>'&nbsp;&nbsp;&nbsp;{view} {update}',

    ]
];

?>

<?php echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="card-box">

    <div class="row">
        <div class="col-sm-12">
            <div class="pull-right">
                <?= ExportMenu::widget([
                    'options' => [ 'class' => 'pull-right'],
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'showColumnSelector' => false,
                    'exportConfig' => Yii::$app->params['exportConfig']

                ]);
                ?>

                <?= Html::a('Create Promo Codes', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>


    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>
    </div>

</div>
