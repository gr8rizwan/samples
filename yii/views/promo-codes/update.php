<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PromoCodes */

$this->title = 'Update Promo Codes: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Promo Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
