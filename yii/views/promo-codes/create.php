<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PromoCodes */

$this->title = 'Create Promo Codes';
$this->params['breadcrumbs'][] = ['label' => 'Promo Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
