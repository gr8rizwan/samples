<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PromoCodes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Promo Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-box">

    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'code',
            'type',
            'redeem_type',
            'min_order',
            'amount',
            'redeem_type',
            'amount',
            [
                'attribute' => 'Redeemed',
                'filter' => false,
                'value' =>  count($model->promoToCustomer).' Times',
            ],
            [
                'attribute' => 'Discounted Value',
                'filter' => false,
                'value' => $model->getOrderTotalSum($model->promoToCustomer, 'discount'),
            ],
            [
                'attribute' => 'Revenue',
                'filter' => false,
                'value' => $model->getOrderTotalSum($model->promoToCustomer, 'total_sum'),
            ],
            'min_order',
            'from_date',
            'to_date',
            'customer_specific',
            'time_from',
            'time_to',
            'valid_on_day',
            'status',
            'branch_id',
            'created_at',
        ],
    ]) ?>

</div>
