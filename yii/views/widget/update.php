<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Widgets */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Widgets',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Widgets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="widgets-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
