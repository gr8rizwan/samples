<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Legend */

$this->title = 'Create Legend';
$this->params['breadcrumbs'][] = ['label' => 'Legends', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="legend-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
