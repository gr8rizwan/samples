<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LegendSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Legends';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="legend-index card-box">

    <p class="pull-right">
        <?= Html::a('Create Legend', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'icon',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::img($data->icon, ['width' => '24']);
                }
            ],
            'name',
            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
