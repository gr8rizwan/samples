<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\file\FileInput;
use backend\models\Ingredient;


/* @var $this yii\web\View */
/* @var $model app\models\Legend */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="legend-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-sm-12">
            <label>Ingredient Legend</label>
            <?= Select2::widget([
                'name' => 'Legend[ingredient_legend][]',
                'value' => ArrayHelper::getColumn($model->ingredientLegends, 'ingredient_id'),
                'data' => ArrayHelper::map(Ingredient::find()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Select a state ...',  'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true,
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10,
                ],
            ]);

            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div id="images" class="form-group">
                <label class="control-label" for="input-image">Image</label>
                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                    <img src="<?php echo $model->icon; ?>" alt="" height="125" title="" data-placeholder="no_image.png" />
                </a>
                <input type="hidden" name="Legend[icon]" value="<?php echo $model->icon; ?>" id="input-image" />

            </div>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
