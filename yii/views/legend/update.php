<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Legend */

$this->title = 'Update Legend: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Legends', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="legend-update card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
