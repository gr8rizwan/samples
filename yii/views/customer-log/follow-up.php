<?php
/**
 * Follow Up report
 * author: Rizwan Arshad <rizwan@myswich.com>
 * Date: 2/4/16
 * Time: 2:24 PM
 */

use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\StoreProcedures;

$baseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/feed/';
$themeBaseURL = Yii::$app->getUrlManager()->getBaseUrl() . '/themes/ubold/';
$params = Yii::$app->request->queryParams;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerLog */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Follow-Up Report';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">
    <?php echo $this->render('reports/_search', ['model' => $searchModel, 'filters' => ['customer', 'from_date', 'to_date', 'branch_id', 'area', 'time_category'] ]); ?>
</div>

<div class="order-index card-box" ng-app="swichFeed" ng-controller="feedController as feed">

    <div class="table-responsive">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'table table-actions-bar table-striped'],
            'showFooter' => true,
            'footerRowOptions'=>['style'=>'font-weight:bold;background-color: lightgray;color: black;', 'class' => 'text-primary'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'time',
                    'value' => function($model){
                        return yii::$app->dateTime->getReadableDateAndTimeFormat($model->time);
                    }
                ],
                [
                    'attribute' => 'customer_id',
                    'format' => 'raw',
                    'options' => ['style' => 'width: 20%'],
                    'value' => function($data) {
                        $html = '';
                        $html .= strtoupper($data->customer->first_name.' '.$data->customer->last_name).'</a> ';
                        $html = Html::a($html, ['customer/view', 'id' => $data->customer->id], [ 'target' => '_blank', 'data-pjax' => 'false']);
                        $html .= '<br /><b>'.$data->customer->phone_number.'</b><br />';
                        return $html;
                    }
                ],
                [
                    'attribute' => 'Branch',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->order->branch->name;
                    }
                ],
                [
                    'attribute' => 'area',
                    'format' => 'raw',
                    'value' => function( $model ) {
                        return (int)$model->order->address_id > 0 ? yii::$app->setting->formatAddress($model->order->address_id, true) : 'Pickup';
                    }
                ],
                [
                    'attribute' => 'platform',
                    'format' => 'raw',
                    'options' => ['style' => 'width: 10%'],
                    'value' => function( $model ) {
                        return $model->order->source;
                    }
                ],
                [
                    'attribute' => 'time_category',
                    'value' => function($model) {
                        return (!empty($model->orderTimeCategory) ? $model->orderTimeCategory->time_category : 'N/A');
                    },
                    'footer' => '<b>Total:</b>'
                ],
                [
                    'label' => 'Total Orders',
                    'value' => function($model) use ($params) {
                        $from_date = false;
                        $to_date = false;
                        if(isset($params['from_date']))
                            $from_date = $params['from_date'];
                        if(isset($params['to_date']))
                            $to_date = $params['to_date'];
                        return $model->customer->getOrderCount(['from_date' => $from_date, 'to_date' => $to_date]);
                    },
                    'footer' => $totalOrders
                ],
                [
                    'label' => ' Duration',
                    'value' => function($model){
                        $order_created = strtotime($model->order->time);
                        $followup_time = strtotime($model->time);
                        return Yii::$app->dateTime->getSecondToHoursSecond($followup_time - $order_created);
                    },
                ],
                [
                    'class' => 'common\helpers\CustomActionColumn',
                    'template'=>'{view}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return '<button title="View Order Details" ng-click="showOrderDetails(' . $model->order->id .')"  class="btn btn-info pull-right glyphicon glyphicon-zoom-in"></button>';
                        }
                    ]

                ],
            ],
        ]); ?>

    </div>

    <!-- MODAL BOX TO DISPLAY ORDER DETAILS -->
    <?php
    // order details modal box
    echo $this->render('/modals/order-details');
    ?>


</div>

<script src="<?= $themeBaseURL; ?>assets/js/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="<?= $baseURL; ?>js/angular-timeago.js"></script>
<script src="<?= $baseURL; ?>js/app.js"></script>
<script src="<?= $baseURL; ?>js/feedFactory.js"></script>
<script src="<?= $baseURL; ?>js/feedController.js"></script>
<script src="<?= $baseURL; ?>js/frameFilter.js"></script>