<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use backend\models\Branch;
use yii\widgets\Pjax;
use kartik\depdrop\DepDrop;


use kartik\daterange\DateRangePicker;
//use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerLogSearch */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    '$(document).on("pjax:end", function() {
            $("#btn-compile-HTML button").trigger("click");
        });

        setTimeout(function(){
            window.area_id = $("#customerlogsearch-area").val();
            $("#customerlogsearch-branch_id").trigger("change");
        }, 500);

        $("#customerlogsearch-area").on("depdrop.afterChange", function(event, id, value) {
            if(window.area_id == "")
                return;

            if($("#customerlogsearch-branch_id").val() == ""){
                $("#customerlogsearch-area").val("");
                window.area_id = "";
                return;
            }

            isAreaExists = $("#customerlogsearch-area").find(\'[value=\' + window.area_id + \']\').length;
            if(isAreaExists > 0){
                $("#customerlogsearch-area").val(window.area_id);
            }

        });
    '
);

$this->registerJs(
    '
    var from_date = $(\'[name="CustomerLogSearch[from_date]"]\');
    from_date.off(\'apply.daterangepicker\');
    from_date.on(\'apply.daterangepicker\', function(ev, picker) {
        from_date.val(picker.startDate.format(\'YYYY-MM-DD\'));
    });

    var to_date = $(\'[name="CustomerLogSearch[to_date]"]\');
    to_date.off(\'apply.daterangepicker\');
    to_date.on(\'apply.daterangepicker\', function(ev, picker) {
        to_date.val(picker.startDate.format(\'YYYY-MM-DD\'));
    });
    '
);

?>

<div class="row">


    <?php $form = ActiveForm::begin([
        'method' => 'get',
        'options' => ['data-pjax' => true ]
    ]); ?>


    <div class="row">

        <?php if( in_array('customer', $filters) ) { ?>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <?= $form->field($model, 'customer')->textInput(); ?>
            </div>
        <?php } ?>

        <?php if( in_array('branch_id', $filters) ) { ?>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <?= $form->field($model, 'branch_id')->dropDownList(ArrayHelper::map(Branch::find()->all(), 'id', 'name'), ['prompt'=>'Select Branch']) ?>
            </div>
        <?php } ?>

        <?php if( in_array('area', $filters) ) { ?>

            <div class="col-lg-3 col-md-3 col-sm-12 form-group">
                <?=
                $form->field($model, 'area')->widget(DepDrop::classname(), [
                    'data' => ArrayHelper::map(\backend\models\Area::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'), // set the initial display text
                    'options' => ['placeholder' => 'Select Area '],
                    'pluginOptions'=>[
                        'allowClear' => true,
                        'depends'=>['customerlogsearch-branch_id'],
                        'url' => Url::to(['/branch/areas']),
                        'laodingText' => 'Loading ...'
                    ],
                ]);
                ?>
            </div>

        <?php } ?>

        <?php if( in_array('current_status', $filters) ) { ?>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <?= $form->field($model, 'current_status')->dropDownList(['Order Created' => 'Order Created', 'Not assigned' => 'Not assigned', 'Assigned to driver' => 'Assigned to driver', 'Completed' => 'Completed'], ['prompt' => 'Select Status']) ?>
            </div>
        <?php } ?>

        <?php if( in_array('source', $filters) ) { ?>

            <div class="col-lg-3 col-md-3 col-sm-12">
                <?= $form->field($model, 'source')->dropDownList(Yii::$app->params['orderSource'], ['prompt' => 'Select Source']) ?>
            </div>

        <?php } ?>

        <?php if( in_array('from_date', $filters) ) { ?>
            <div class="col-lg-3 col-md-3 col-sm-12 form-group">
            <label>From Date</label>
            <?=
            DateRangePicker::widget([
                'name'=>'CustomerLogSearch[from_date]',
                'value'=> $model->from_date,
                'options' => [
                    'class' => 'form-control'
                ],
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'singleDatePicker'=>true,
                    'locale'=>['format' => 'Y-m-d'],
                ]
            ]);
            ?>
        </div>
        <?php } ?>

        <?php if( in_array('to_date', $filters) ) { ?>
            <div class="col-lg-3 col-md-3 col-sm-12 form-group">
                <label>To Date</label>
                <?=
                DateRangePicker::widget([
                    'name'=>'CustomerLogSearch[to_date]',
                    'value'=> $model->to_date,
                    'convertFormat'=>true,
                    'options' => [
                        'class' => 'form-control'
                    ],
                    'pluginOptions'=>[
                        'singleDatePicker'=>true,
                        'locale'=>['format' => 'Y-m-d'],
                    ]
                ]);
                ?>
            </div>
        <?php } ?>

    </div>

    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="form-group pull-right">
                <label class="control-label" style="width: 100%">&nbsp;</label>
                <?= Html::submitButton('<i class="fa fa-search m-r-5"></i> <span>Search</span>', ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                <?= Html::a('<i class="fa fa-eraser m-r-5"></i> <span>Reset</span>', [''], ['class' => 'btn btn-default waves-effect waves-light']); ?>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
