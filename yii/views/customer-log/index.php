<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CustomerLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-log-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Customer Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'customer_id',
            'order_id',
            'action_id',
            'description',
            // 'time',
            // 'log_type',

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
