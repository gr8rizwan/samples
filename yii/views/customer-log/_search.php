<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CustomerLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'order_id') ?>

    <?= $form->field($model, 'action_id') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'time') ?>

    <?php // echo $form->field($model, 'log_type') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
